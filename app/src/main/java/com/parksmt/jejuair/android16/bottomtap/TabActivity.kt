package com.parksmt.jejuair.android16.bottomtap

import android.content.Intent
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.parksmt.jejuair.android16.R

abstract class TabActivity : AppCompatActivity(), BottomNavigationView.OnNavigationItemSelectedListener {

    protected var navigationView: BottomNavigationView? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(contentViewId)
        navigationView = findViewById<View>(R.id.navigation) as BottomNavigationView
        navigationView!!.setOnNavigationItemSelectedListener(this)
    }

    override fun onStart() {
        super.onStart()
        updateNavigationBarState()
    }

    // Remove inter-activity transition to avoid screen tossing on tapping bottom navigation items
    override fun onPause() {
        super.onPause()
        overridePendingTransition(0, 0)
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean { // intent 변수는 따로 줘야함. 재사용시 액티비티 전환 안될때 있음
        navigationView!!.postDelayed({
            val itemId = item.itemId
            if (itemId == R.id.navigation_home) {
                var intent = Intent(this, HomeActivity::class.java)
                intent.flags = Intent.FLAG_ACTIVITY_REORDER_TO_FRONT
                startActivity(intent)
                overridePendingTransition(0,0)
            } else if (itemId == R.id.navigation_dashboard) {
                var intent2 = Intent(this, DashboardActivity::class.java)
                intent2.flags = Intent.FLAG_ACTIVITY_REORDER_TO_FRONT
                startActivity(intent2)
                overridePendingTransition(0,0)
            } else if (itemId == R.id.navigation_notifications) {
                var intent3 = Intent(this, NotificationsActivity::class.java)
                intent3.flags = Intent.FLAG_ACTIVITY_REORDER_TO_FRONT
                startActivity(intent3)
                overridePendingTransition(0,0)
            }

        }, 300)
        return true
    }

    private fun updateNavigationBarState() {
        val actionId = navigationMenuItemId
        selectBottomNavigationBarItem(actionId)
    }

    fun selectBottomNavigationBarItem(itemId: Int) {
        val item = navigationView!!.menu.findItem(itemId)
        item.isChecked = true
    }

    abstract val contentViewId: Int
    abstract val navigationMenuItemId: Int
}