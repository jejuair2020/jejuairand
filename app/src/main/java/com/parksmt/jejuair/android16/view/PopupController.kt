package com.parksmt.jejuair.android16.view

import android.app.Dialog
import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import com.parksmt.jejuair.android16.R

/**
 * 공통 popup 창 slide up, down 애니메이션 효과 를 줌
 *
 *
 * Created by ui-jun on 2017-03-24.
 */
abstract class PopupController(protected var mContext: Context, layoutResId: Int) :
    Dialog(mContext, R.style.FullScreenDialog) {
    protected val tag = this.javaClass.simpleName
    @JvmField
    protected var mContentView: View
    override fun onCreate(savedInstanceState: Bundle) {
        super.onCreate(savedInstanceState)
        setContentView(mContentView)
    }

    /**
     * dismiss 사용
     */
    @Deprecated("")
    fun close() {
        dismiss()
    }

    init {
        mContentView = LayoutInflater.from(mContext).inflate(layoutResId, null)
    }
}