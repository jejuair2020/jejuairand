package com.parksmt.jejuair.android16.airplane.baggage

import android.graphics.Color
import android.os.Bundle
import android.text.SpannableStringBuilder
import android.view.LayoutInflater
import android.view.View
import android.widget.LinearLayout
import android.widget.TextView
import com.parksmt.jejuair.android16.R
import com.parksmt.jejuair.android16.airplane.AirplaneModeMenuActivity
import com.parksmt.jejuair.android16.serviceinfo.baggage.BaggageTableType
import com.parksmt.jejuair.android16.serviceinfo.baggage.RejectBaggageList
import com.parksmt.jejuair.android16.util.DialogUtil
import com.parksmt.jejuair.android16.util.StringUtil
import com.parksmt.jejuair.android16.util.Util
import kotlinx.android.synthetic.main.reject_baggage_layout.*

/**
 * 운송 제한 물품 S-MUI-08-017
 *
 *
 * Created by seungjinoh on 2017. 4. 6..
 */
class RejectBaggage : AirplaneModeMenuActivity() {
    protected override val uiName: String
        protected get() = "S-MUI-08-017"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.reject_baggage_layout)
        setText()
        initClickEvent()
    }

    private fun setText() {
        loadLanguage("serviceinfo/RejectBaggage.json")
        setTitleText(mLanguageJson.optString("reject_baggage_text1000"))
        (findViewById(R.id.reject_baggage_text1) as TextView).setText(mLanguageJson.optString("reject_baggage_text1001"))
        (findViewById(R.id.reject_baggage_text2) as TextView).setText(mLanguageJson.optString("reject_baggage_text1002"))
        (findViewById(R.id.reject_baggage_text3) as TextView).setText(mLanguageJson.optString("reject_baggage_text1003"))
        (findViewById(R.id.reject_baggage_text4) as TextView).setText(mLanguageJson.optString("reject_baggage_text1004"))
        (findViewById(R.id.reject_baggage_text5) as TextView).setText(mLanguageJson.optString("reject_baggage_text1005"))
        (findViewById(R.id.reject_baggage_text6) as TextView).setText(mLanguageJson.optString("reject_baggage_text1006"))
        (findViewById(R.id.reject_baggage_text7) as TextView).setText(mLanguageJson.optString("reject_baggage_text1007"))
        (findViewById(R.id.reject_baggage_text8) as TextView).setText(mLanguageJson.optString("reject_baggage_text1008"))
        (findViewById(R.id.reject_baggage_text9) as TextView).setText(mLanguageJson.optString("reject_baggage_text1009"))
        (findViewById(R.id.reject_baggage_text10) as TextView).setText(mLanguageJson.optString("reject_baggage_text1010"))
        (findViewById(R.id.reject_baggage_text11) as TextView).setText(mLanguageJson.optString("reject_baggage_text1011"))
        (findViewById(R.id.reject_baggage_text12) as TextView).setText(mLanguageJson.optString("reject_baggage_text1012"))
        (findViewById(R.id.reject_baggage_text13) as TextView).setText(mLanguageJson.optString("reject_baggage_text1013"))
        (findViewById(R.id.reject_baggage_text14) as TextView).setText(mLanguageJson.optString("reject_baggage_text1014"))
        (findViewById(R.id.reject_baggage_text15) as TextView).setText(mLanguageJson.optString("reject_baggage_text1015"))
        (findViewById(R.id.reject_baggage_text16) as TextView).setText(mLanguageJson.optString("reject_baggage_text1016"))
        (findViewById(R.id.reject_baggage_text17) as TextView).setText(mLanguageJson.optString("reject_baggage_text1017"))
        (findViewById(R.id.reject_baggage_text18) as TextView).setText(mLanguageJson.optString("reject_baggage_text1018"))
        (findViewById(R.id.reject_baggage_text1100) as TextView).setText(mLanguageJson.optString("reject_baggage_text1100"))
        (findViewById(R.id.reject_baggage_text1101) as TextView).setText(mLanguageJson.optString("reject_baggage_text1101"))
        (findViewById(R.id.reject_baggage_text1102) as TextView).setText(mLanguageJson.optString("reject_baggage_text1102"))
        (findViewById(R.id.reject_baggage_text52) as TextView).setText(mLanguageJson.optString("reject_baggage_text1052"))
        (findViewById(R.id.reject_baggage_text53) as TextView).setText(mLanguageJson.optString("reject_baggage_text1053"))
        (findViewById(R.id.reject_baggage_text54) as TextView).setText(mLanguageJson.optString("reject_baggage_text1054"))
        (findViewById(R.id.reject_baggage_text55) as TextView).setText(mLanguageJson.optString("reject_baggage_text1055"))
        (findViewById(R.id.reject_baggage_text56) as TextView).setText(
            mLanguageJson.optString("reject_baggage_text1056").toString() + " / " + mLanguageJson.optString(
                "reject_baggage_text1060"
            )
        )
        var text = SpannableStringBuilder()
        text.append(mLanguageJson.optString("reject_baggage_text1057"))
        text = StringUtil().appendAndSize(
            text,
            mLanguageJson.optString("reject_baggage_text1057_1"),
            Color.parseColor("#01a7e1"),
            40
        )
        (findViewById(R.id.reject_baggage_text57) as TextView).text = text
        (findViewById(R.id.reject_baggage_text59) as TextView).setText(mLanguageJson.optString("reject_baggage_text1058"))
        (findViewById(R.id.reject_baggage_text60) as TextView).setText(mLanguageJson.optString("reject_baggage_text1059"))
        //        ((TextView) findViewById(R.id.reject_baggage_text61)).setText(mLanguageJson.optString("reject_baggage_text1060"));
//        ((TextView) findViewById(R.id.reject_baggage_text62)).setText(mLanguageJson.optString("reject_baggage_text1061"));
        (findViewById(R.id.reject_baggage_text63) as TextView).setText(mLanguageJson.optString("reject_baggage_text1062"))
        (findViewById(R.id.reject_baggage_text64) as TextView).setText(mLanguageJson.optString("reject_baggage_text1063"))
        (findViewById(R.id.reject_baggage_text65) as TextView).setText(mLanguageJson.optString("reject_baggage_text1064"))
        (findViewById(R.id.reject_baggage_text66) as TextView).setText(mLanguageJson.optString("reject_baggage_text1065"))
        (findViewById(R.id.reject_baggage_text67) as TextView).setText(mLanguageJson.optString("reject_baggage_text1066"))
        (findViewById(R.id.reject_baggage_text68) as TextView).setText(mLanguageJson.optString("reject_baggage_text1067"))
        (findViewById(R.id.reject_baggage_text69) as TextView).setText(mLanguageJson.optString("reject_baggage_text1068"))
        (findViewById(R.id.reject_baggage_text70) as TextView).setText(mLanguageJson.optString("reject_baggage_text1069"))
        (findViewById(R.id.reject_baggage_text71) as TextView).setText(mLanguageJson.optString("reject_baggage_text1070"))
        (findViewById(R.id.reject_baggage_text72) as TextView).setText(mLanguageJson.optString("reject_baggage_text1071"))
        //TODO 2018.05.25 yschoi [ITSM-IM00127155] [APP 네이티브 영역] 운송제한물품 추가 안내
        reject_baggage_text72_.setVisibility(
            if (Util().getLanguage(
                    this
                ) === "KR"
            ) View.VISIBLE else View.GONE
        )
        (findViewById(R.id.reject_baggage_text72_) as TextView).setText(mLanguageJson.optString("reject_baggage_text1072"))
        (findViewById(R.id.reject_baggage_text73) as TextView).setText(mLanguageJson.optString("reject_baggage_text1073"))
        createTablelayout(RejectBaggageList(BaggageTableType.INSTALLED)) //배터리 장착 전자제품
        createTablelayout(RejectBaggageList(BaggageTableType.SPAREBATTERY)) //보조배터리
        createTablelayout(RejectBaggageList(BaggageTableType.SMARTLUGGAGE)) //스마트가방
    }

    private fun initClickEvent() {
        reject_baggage_text4.setOnClickListener(View.OnClickListener {
            DialogUtil().showCommonAlertDialog(
                this@RejectBaggage,
                mLanguageJson.optString("")
            )
        })
        reject_baggage_text5.setOnClickListener(View.OnClickListener {
            DialogUtil().showCommonAlertDialog(
                this@RejectBaggage,
                mLanguageJson.optString("airplaneModeText1009")
            )
        })
    }

    private fun createTablelayout(TableType: RejectBaggageList) {
        val component: LinearLayout =
            findViewById(getResources().getIdentifier(TableType.id, "id", getPackageName()))
        if (TableType.isHeader) {
            val header = LayoutInflater.from(this).inflate(
                R.layout.reject_baggage_row_header,
                null
            ) as LinearLayout
            (header.findViewById<View>(R.id.reject_baggage_row_tv1) as TextView).setText(
                mLanguageJson.optString("reject_baggage_text1103")
            ) //용량
            (header.findViewById<View>(R.id.reject_baggage_row_tv2) as TextView).setText(
                mLanguageJson.optString("reject_baggage_text1104")
            ) //휴대
            (header.findViewById<View>(R.id.reject_baggage_row_tv3) as TextView).setText(
                mLanguageJson.optString("reject_baggage_text1105")
            ) //위탁
            (header.findViewById<View>(R.id.reject_baggage_row_tv4) as TextView).setText(
                mLanguageJson.optString("reject_baggage_text1106")
            ) //기타사항
            component.addView(header)
        }
        for (i in 0..2) {
            val header =
                LayoutInflater.from(this).inflate(R.layout.reject_baggage_row, null) as LinearLayout
            for (j in 0..3) {
                val c = Color.parseColor(
                    if (TableType.getContents(
                            i,
                            j
                        ).equals("reject_baggage_text1120")
                    ) "#00a7e1" else  // Ｏ
                        if (TableType.getContents(
                                i,
                                j
                            ).equals("reject_baggage_text1121")
                        ) "#f58220" else "#555555"
                ) // Ｘ
                val tv: TextView = header.findViewById(
                    getResources().getIdentifier(
                        "reject_baggage_row_tv" + (j + 1),
                        "id",
                        getPackageName()
                    )
                )
                tv.setText(mLanguageJson.optString(TableType.getContents(i, j)))
                tv.setTextColor(c)
            }
            component.addView(header)
        }
    }
}