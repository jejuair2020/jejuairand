package com.parksmt.jejuair.android16.util

import android.content.Context
import android.content.ContextWrapper
import android.os.Build
import com.parksmt.jejuair.android16.common.DLog
import java.util.*

/**
 * 언어 설정 변경 시 앱 locale 을 변경 함
 * Created by jun on 2017-02-01.
 */

class LanguageContextWrapper(base: Context) : ContextWrapper(base) {
    companion object {
        private val tag = "LanguageContextWrapper"

        fun wrap(context: Context): ContextWrapper {
            var context = context
            val language = "ko"//Util().getLanguageLocale(context)
            context = setLanguage(context, language)
            return LanguageContextWrapper(context)
        }

        private fun setLanguage(context: Context, language: String): Context {
            var context = context
            val locale: Locale
            when (language) {
                "zh-rCN" -> locale = Locale.SIMPLIFIED_CHINESE
                "zh-rTW" -> locale = Locale.TRADITIONAL_CHINESE
                else -> locale = Locale(language)
            }
            Locale.setDefault(locale)
            val configuration = context.resources.configuration
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
                configuration.setLocale(locale)
                context = context.createConfigurationContext(configuration)
            } else {
                configuration.locale = locale
                context.resources.updateConfiguration(
                    configuration,
                    context.resources.displayMetrics
                )
            }
            DLog().d("setLanguage : $language")
            return context
        }
    }
}
