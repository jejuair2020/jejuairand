package com.parksmt.jejuair.android16.bottomtap

import android.os.Bundle
import com.parksmt.jejuair.android16.R

class DashboardActivity : TabActivity() {
    override val contentViewId: Int
        get() = R.layout.activity_dashboard

    override val navigationMenuItemId: Int
        get() = R.id.navigation_dashboard

    override fun onResume() {
        super.onResume()
        overridePendingTransition(0, 0)
    }
}