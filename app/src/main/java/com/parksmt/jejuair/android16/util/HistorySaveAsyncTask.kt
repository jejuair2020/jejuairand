package com.parksmt.jejuair.android16.util

import android.content.Context
import android.os.Build
import com.parksmt.jejuair.android16.BuildConfig
import com.parksmt.jejuair.android16.common.DLog
import com.parksmt.jejuair.android16.common.Setting
import com.parksmt.jejuair.android16.common.UserToken
import java.net.HttpURLConnection
import java.util.*

/**
 * 히스토리 로그
 *
 *
 * Created by yschoi 2018-10-08.
 */
class HistorySaveAsyncTask : BaseAsyncTask<Void?, Void?, Int?> {
    private var mOnPostExecuteListenerWithResult: OnPostExecuteListenerWithResult? = null
    lateinit var descType: String
    lateinit var detailLog: String

    constructor(
        context: Context?,
        onPostExecuteListenerWithResult: OnPostExecuteListenerWithResult?
    ) : super(context!!, false) {
        mOnPostExecuteListenerWithResult = onPostExecuteListenerWithResult
    }

    constructor(
        context: Context,
        descType: String,
        detailLog: String
    ) : super(context!!, false) {
        this.descType = descType
        this.detailLog = detailLog
    }

    override fun doInBackground(vararg params: Void?): Int? {
        val url: String =
            Setting.BASE_URL + "/com/jeju/ibe/common/ticHistorySave.do"
        val resultCode: Int
        val param =
            HashMap<String, String>()
        //•단말 OS 종류(AOS, iOS)
//•단말 OS 버전정보
//•IP정보
//•FFP 회원번호 UserToken.getInstance(mContext).getFFPNo();
//•회원ID  UserToken.getInstance(mContext).getUserID()
//•거주국가 정보  UserToken.getInstance(mContext).getCountry()
//•선택한 언어
//•제주항공 앱 버전정보 BuildConfig.APP_VERSION
//•이벤트 타입(호출 url .do)  //파라미터로 url 넘겨줄것. eventType
//•상세 로그 내용(exception 내용) //exception 내용 넘겨줄 것. detailLog
//        UserToken.getInstance(mContext).getNationality()
        DLog().d("BOARD = " + Build.BOARD)
        DLog().i("BRAND = " + Build.BRAND)
        DLog().i("CPU_ABI = " + Build.CPU_ABI)
        DLog().i("DEVICE = " + Build.DEVICE)
        DLog().i("DISPLAY = " + Build.DISPLAY)
        DLog().i("FINGERPRINT = " + Build.FINGERPRINT)
        DLog().i("HOST = " + Build.HOST)
        DLog().i("ID = " + Build.ID)
        DLog().i("MANUFACTURER = " + Build.MANUFACTURER)
        DLog().i("MODEL = " + Build.MODEL)
        DLog().i("PRODUCT = " + Build.PRODUCT)
        DLog().i("TAGS = " + Build.TAGS)
        DLog().i("TYPE = " + Build.TYPE)
        DLog().i("USER = " + Build.USER)
        DLog().i("VERSION.RELEASE = " + Build.VERSION.RELEASE)
        val deviceOS = "aOS"
        val deviceInfo =
            deviceOS + "_" + Build.BRAND + "_" + Build.MODEL + "_" + Build.VERSION.RELEASE + "_v" + BuildConfig.APP_VERSION
        val deviceIp: String = Util().getLocalIpAddress()
        val ffpNo: String = UserToken.getInstance(mContext).fFPNo
        //        String UserId = UserToken.getInstance(mContext).getUserID();
        val Country: String = UserToken.getInstance(mContext).country
        val language: String =
            Util().getCurrentLanguage(mContext).code
        //        String appVersion = BuildConfig.APP_VERSION;
//        String eventType = this.descType;
//        String detailLog = this.detailLog;
        val temp =
            "$deviceInfo,\n$ffpNo,\n$Country,\n$language,\n$detailLog"
        param["USERCODE"] = UserToken.getInstance(mContext).userID
        //        param.put("PNRNO", "aOS");
        param["DESCTYPE"] = descType //변경, 예약 Destroy
        param["DESCDETAIL"] = temp
        try {
            mConnection = NetworkUtil().send(url, param, mContext, NetworkUtil.RequestMethod.POST)
            mConnection!!.connectTimeout = 1000 * 5
            resultCode = NetworkUtil().getResponseCode(mConnection)
        } catch (e: Exception) {
            DLog().e("Exception" + e)
            return NetworkUtil.IO_EXCEPTION_ERROR_CODE
        }
        if (resultCode == HttpURLConnection.HTTP_OK) {
            DLog().d("resultCode : $resultCode")
        }
        return resultCode
    }

    protected fun onPostExecute(result: Int) {
        super.onPostExecute(result)
    }



}