package com.parksmt.jejuair.android16.airplane.baggage

import android.content.Context
import android.os.Bundle
import android.view.View
import android.widget.TextView
import com.parksmt.jejuair.android16.R
import com.parksmt.jejuair.android16.airplane.AirplaneModeMenuActivity
import com.parksmt.jejuair.android16.serviceinfo.RegulationBaggagePopup
import com.parksmt.jejuair.android16.view.PopupController
import kotlinx.android.synthetic.main.over_baggage_layout.*

/**
 * 초과 수하물 S-MUI-08-011
 * Created by seungjinoh on 2017. 4. 4..
 */
class OverBaggage : AirplaneModeMenuActivity() {
    private lateinit var mZoneInfoPopup: ZoneInfoPopup
    private lateinit var regulationBaggagePopup: RegulationBaggagePopup
    protected override val uiName: String
        protected get() = "S-MUI-08-011"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.over_baggage_layout)
        setText()
        initView()
        initClickEvent()
    }

    override fun onClick(v: View) {
        super.onClick(v)
        when (v.id) {
            R.id.over_baggage_text14, R.id.over_baggage_text14_1 -> mZoneInfoPopup!!.show()
            R.id.over_baggage_text27 -> goSubPage(BeforeBaggage::class.java)
        }
    }

    private fun initView() {
        mZoneInfoPopup = ZoneInfoPopup(this)
        regulationBaggagePopup = RegulationBaggagePopup(this)
    }

    private fun setText() {
        loadLanguage("serviceinfo/OverBaggage.json")
        setTitleText(mLanguageJson.optString("over_baggage_text1000"))
        (findViewById(R.id.over_baggage_text1) as TextView).setText(mLanguageJson.optString("over_baggage_text1001"))
        (findViewById(R.id.over_baggage_text2) as TextView).setText(mLanguageJson.optString("over_baggage_text1002"))
        (findViewById(R.id.over_baggage_text3) as TextView).setText(mLanguageJson.optString("over_baggage_text1003"))
        (findViewById(R.id.over_baggage_text4) as TextView).setText(mLanguageJson.optString("over_baggage_text1004"))
        (findViewById(R.id.over_baggage_text5) as TextView).setText(mLanguageJson.optString("over_baggage_text1005"))
        (findViewById(R.id.over_baggage_text5_1) as TextView).setText(mLanguageJson.optString("over_baggage_text1005"))
        (findViewById(R.id.over_baggage_text5_2) as TextView).setText(mLanguageJson.optString("over_baggage_text1039"))
        (findViewById(R.id.over_baggage_text6) as TextView).setText(mLanguageJson.optString("over_baggage_text1006"))
        (findViewById(R.id.over_baggage_text6_1) as TextView).setText(mLanguageJson.optString("over_baggage_text1006"))
        (findViewById(R.id.over_baggage_text6_2) as TextView).setText(mLanguageJson.optString("over_baggage_text1006"))
        (findViewById(R.id.over_baggage_text6_3) as TextView).setText(mLanguageJson.optString("over_baggage_text1006"))
        (findViewById(R.id.over_baggage_text6_4) as TextView).setText(mLanguageJson.optString("over_baggage_text1006"))
        (findViewById(R.id.over_baggage_text6_5) as TextView).setText(mLanguageJson.optString("over_baggage_text1006"))
        (findViewById(R.id.over_baggage_text7) as TextView).setText(mLanguageJson.optString("over_baggage_text1007"))
        (findViewById(R.id.over_baggage_text8) as TextView).setText(mLanguageJson.optString("over_baggage_text1008"))
        (findViewById(R.id.over_baggage_text9) as TextView).setText(mLanguageJson.optString("over_baggage_text1009"))
        (findViewById(R.id.over_baggage_text9_1) as TextView).setText(mLanguageJson.optString("over_baggage_text1038"))
        (findViewById(R.id.over_baggage_text10) as TextView).setText(mLanguageJson.optString("over_baggage_text1010"))
        (findViewById(R.id.over_baggage_text11) as TextView).setText(mLanguageJson.optString("over_baggage_text1011"))
        (findViewById(R.id.over_baggage_text11_1) as TextView).setText(mLanguageJson.optString("over_baggage_text1041"))
        (findViewById(R.id.over_baggage_text11_2) as TextView).setText(mLanguageJson.optString("over_baggage_text1042"))
        (findViewById(R.id.over_baggage_text12) as TextView).setText(mLanguageJson.optString("over_baggage_text1012"))
        (findViewById(R.id.over_baggage_text13) as TextView).setText(mLanguageJson.optString("over_baggage_text1013"))
        (findViewById(R.id.over_baggage_text14) as TextView).setText(mLanguageJson.optString("over_baggage_text1014"))
        (findViewById(R.id.over_baggage_text14_1) as TextView).setText(mLanguageJson.optString("over_baggage_text1014"))
        (findViewById(R.id.over_baggage_text15) as TextView).setText(mLanguageJson.optString("over_baggage_text1040"))
        (findViewById(R.id.over_baggage_text16) as TextView).setText(mLanguageJson.optString("over_baggage_text_zone12"))
        (findViewById(R.id.over_baggage_text16_1) as TextView).setText(mLanguageJson.optString("over_baggage_text_zone12"))
        (findViewById(R.id.over_baggage_text16_2) as TextView).setText(mLanguageJson.optString("over_baggage_text_zone12"))
        (findViewById(R.id.over_baggage_text17) as TextView).setText(mLanguageJson.optString("over_baggage_text1040_z12"))
        (findViewById(R.id.over_baggage_text17_2) as TextView).setText(mLanguageJson.optString("over_baggage_text1042_z12"))
        (findViewById(R.id.over_baggage_text18) as TextView).setText(mLanguageJson.optString("over_baggage_text_zone13"))
        (findViewById(R.id.over_baggage_text18_1) as TextView).setText(mLanguageJson.optString("over_baggage_text_zone13"))
        (findViewById(R.id.over_baggage_text18_2) as TextView).setText(mLanguageJson.optString("over_baggage_text_zone13"))
        (findViewById(R.id.over_baggage_text19) as TextView).setText(mLanguageJson.optString("over_baggage_text1040_z13"))
        (findViewById(R.id.over_baggage_text19_2) as TextView).setText(mLanguageJson.optString("over_baggage_text1042_z13"))
        (findViewById(R.id.over_baggage_text20) as TextView).setText(mLanguageJson.optString("over_baggage_text_zone14"))
        (findViewById(R.id.over_baggage_text20_1) as TextView).setText(mLanguageJson.optString("over_baggage_text_zone14"))
        (findViewById(R.id.over_baggage_text20_2) as TextView).setText(mLanguageJson.optString("over_baggage_text_zone14"))
        (findViewById(R.id.over_baggage_text21) as TextView).setText(mLanguageJson.optString("over_baggage_text1040_z14"))
        (findViewById(R.id.over_baggage_text21_2) as TextView).setText(mLanguageJson.optString("over_baggage_text1042_z14"))
        (findViewById(R.id.over_baggage_text22) as TextView).setText(mLanguageJson.optString("over_baggage_text1041_z12"))
        (findViewById(R.id.over_baggage_text23) as TextView).setText(mLanguageJson.optString("over_baggage_text1041_z13"))
        (findViewById(R.id.over_baggage_text24) as TextView).setText(mLanguageJson.optString("over_baggage_text1041_z14"))
        (findViewById(R.id.over_baggage_text25) as TextView).setText(mLanguageJson.optString("over_baggage_text1025"))
        (findViewById(R.id.over_baggage_text26) as TextView).setText(mLanguageJson.optString("over_baggage_text1026"))
        (findViewById(R.id.over_baggage_text27) as TextView).setText(mLanguageJson.optString("over_baggage_text1027"))
        //국제선 수하물 규정 추가
        (findViewById(R.id.regulation_baggabe_title) as TextView).setText(mLanguageJson.optString("FreeBaggageText_regulation"))
        //미주 외 국제선 노선 무게 초과 시 테이블 추가 작업
        (findViewById(R.id.over_baggage_text6_3_) as TextView).setText(mLanguageJson.optString("over_baggage_text1006"))
        (findViewById(R.id.over_baggage_text15_) as TextView).setText(mLanguageJson.optString("over_baggage_text1043"))
        (findViewById(R.id.over_baggage_text16_) as TextView).setText(mLanguageJson.optString("over_baggage_text_zone12"))
        (findViewById(R.id.over_baggage_text17_) as TextView).setText(mLanguageJson.optString("over_baggage_text1043_z12"))
        (findViewById(R.id.over_baggage_text18_) as TextView).setText(mLanguageJson.optString("over_baggage_text_zone13"))
        (findViewById(R.id.over_baggage_text19_) as TextView).setText(mLanguageJson.optString("over_baggage_text1043_z13"))
        (findViewById(R.id.over_baggage_text20_) as TextView).setText(mLanguageJson.optString("over_baggage_text_zone14"))
        (findViewById(R.id.over_baggage_text21_) as TextView).setText(mLanguageJson.optString("over_baggage_text1043_z14"))
        //초과 수하물 안내 추가 문구
        (findViewById(R.id.over_baggage_description3) as TextView).setText(mLanguageJson.optString("over_baggage_description3"))
        (findViewById(R.id.over_baggage_description4) as TextView).setText(mLanguageJson.optString("over_baggage_description4"))
    }

    private fun initClickEvent() {
        over_baggage_text14.setOnClickListener(this)
        over_baggage_text14_1.setOnClickListener(this)
        over_baggage_text27.setOnClickListener(this)
        //이전 국제선 수하물 규정
        regulation_baggabe_title.setOnClickListener(View.OnClickListener { regulationBaggagePopup.show() })
    }

    private inner class ZoneInfoPopup internal constructor(context: Context?) :
        PopupController(context!!, R.layout.over_baggage_zone_info_popup),
        View.OnClickListener {
        override fun onClick(v: View) {
            when (v.id) {
                R.id.reservation_start_popup_close_btn -> dismiss()
            }
        }

        private fun init() {}
        private fun setText() {
            (mContentView.findViewById<View>(R.id.zone_info_text1) as TextView).setText(
                mLanguageJson.optString("over_baggage_text1028")
            )
            (mContentView.findViewById<View>(R.id.zone_info_text2) as TextView).setText(
                mLanguageJson.optString("over_baggage_text1029")
            )
            (mContentView.findViewById<View>(R.id.zone_info_text3) as TextView).setText(
                mLanguageJson.optString("over_baggage_text1030")
            )
            (mContentView.findViewById<View>(R.id.zone_info_text4) as TextView).setText(
                mLanguageJson.optString("over_baggage_text1031")
            )
            (mContentView.findViewById<View>(R.id.zone_info_text5) as TextView).setText(
                mLanguageJson.optString("over_baggage_text1032")
            )
            (mContentView.findViewById<View>(R.id.zone_info_text6) as TextView).setText(
                mLanguageJson.optString("over_baggage_text1033")
            )
            (mContentView.findViewById<View>(R.id.zone_info_text7) as TextView).setText(
                mLanguageJson.optString("over_baggage_text1034")
            )
            (mContentView.findViewById<View>(R.id.zone_info_text8) as TextView).setText(
                mLanguageJson.optString("over_baggage_text1035")
            )
            (mContentView.findViewById<View>(R.id.zone_info_text9) as TextView).setText(
                mLanguageJson.optString("over_baggage_text1036")
            )
        }

        private fun initOnClick() {
            mContentView.findViewById<View>(R.id.reservation_start_popup_close_btn)
                .setOnClickListener(this)
        }

        init {
            init()
            initOnClick()
            setText()
        }
    }
}