package com.parksmt.jejuair.android16.airplane

import android.content.Context
import android.view.View
import android.view.ViewGroup
import androidx.viewpager.widget.PagerAdapter
import androidx.viewpager.widget.ViewPager.OnPageChangeListener
import com.parksmt.jejuair.android16.R
import com.parksmt.jejuair.android16.common.DLog
import com.parksmt.jejuair.android16.data.DutyFreeInfo
import com.parksmt.jejuair.android16.util.ImageUtil
import com.parksmt.jejuair.android16.view.BaseViewPager
import com.parksmt.jejuair.android16.view.PopupController
import kotlinx.android.synthetic.main.duty_free_menu_popup.*
import java.util.*

/**
 * Created by 12154 on 2018-05-31.
 */
class AirplaneModeDutyFreePopup (mContext: Context) :
    PopupController(mContext, R.layout.duty_free_menu_popup), View.OnClickListener {

    private val mDutyfreeInfo: DutyFreeInfo
    private lateinit var mViewPager: BaseViewPager


    private fun setDutyFreeMenuInfoPopup(): DutyFreeInfo {
        return DutyFreeInfo.getDownloadedDutyFreeInfo(mContext)!!
    }

    private fun init() {
        duty_free_menu_popup_close_btn.setOnClickListener(this)
        duty_free_menu_popup_title_textview.setText(mDutyfreeInfo.title)
        duty_free_menu_popup_page_textview.visibility = View.VISIBLE
        duty_free_menu_popup_left_btn.setOnClickListener(this)
        duty_free_menu_popup_right_btn.setOnClickListener(this)

        mViewPager =
            mContentView.findViewById(R.id.duty_free_menu_popup_viewpager) as BaseViewPager
        mViewPager.setAdapter(
            ViewPagerAdapter(
                mContext,
                mDutyfreeInfo.getContentList()
            )
        )
        mViewPager.addOnPageChangeListener(object : OnPageChangeListener {
            override fun onPageScrolled(
                position: Int,
                positionOffset: Float,
                positionOffsetPixels: Int
            ) {
            }

            override fun onPageSelected(position: Int) {
                DLog().d("position$position")
                setArrowButtonVisible(position)
            }

            override fun onPageScrollStateChanged(state: Int) {}
        })


        setArrowButtonVisible(mViewPager.getCurrentItem())
    }

    private fun setArrowButtonVisible(position: Int) {
        duty_free_menu_popup_page_textview.text =
            (position + 1).toString() + " / " + mViewPager.getAdapter()!!.getCount()
        if (position == 0) {
            duty_free_menu_popup_left_btn.visibility = View.GONE
            if (mDutyfreeInfo.getContentList().size > 1) {
                duty_free_menu_popup_right_btn.visibility = View.VISIBLE
            }
        } else if (position == mDutyfreeInfo.getContentList().size - 1) {
            duty_free_menu_popup_right_btn.visibility = View.GONE
        } else {
            duty_free_menu_popup_left_btn.visibility = View.VISIBLE
            duty_free_menu_popup_right_btn.visibility = View.VISIBLE
        }
    }

    override fun onClick(v: View) {
        when (v.id) {
            R.id.duty_free_menu_popup_close_btn -> dismiss()
            R.id.duty_free_menu_popup_left_btn -> goLeft()
            R.id.duty_free_menu_popup_right_btn -> goRight()
        }
    }

    private fun goLeft() {
        val position: Int = mViewPager.getCurrentItem() - 1
        if (position >= 0) {
            mViewPager.setCurrentItem(position, true)
        } else {
            duty_free_menu_popup_left_btn.visibility = View.GONE
        }
    }

    private fun goRight() {
        val position: Int = mViewPager.getCurrentItem() + 1
        if (position < mDutyfreeInfo.getContentList().size) {
            mViewPager.setCurrentItem(position, true)
        } else {
            duty_free_menu_popup_right_btn.visibility = View.GONE
        }
    }

    private inner class ViewPagerAdapter internal constructor(
        private val mContext: Context,
        private val mItem: ArrayList<String>
    ) : PagerAdapter() {
        override fun getCount(): Int {
            return mItem.size
        }

        override fun instantiateItem(container: ViewGroup, position: Int): Any {
            val view: View =
                ImageUtil().setImageMatchParentSize(mContext, container, mItem[position])
            container.addView(view)
            return view
        }

        override fun isViewFromObject(
            view: View,
            `object`: Any
        ): Boolean {
            return view === `object`
        }

        override fun destroyItem(
            container: ViewGroup,
            position: Int,
            `object`: Any
        ) {
            container.removeView(`object` as View)
        }

    }

    init {
        mDutyfreeInfo = setDutyFreeMenuInfoPopup()
        init()
    }
}