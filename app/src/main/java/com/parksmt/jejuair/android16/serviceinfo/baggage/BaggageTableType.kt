package com.parksmt.jejuair.android16.serviceinfo.baggage

enum class BaggageTableType(//스마트가방
    val tableId: String
) {
    INSTALLED("reject_baggage_text1100_table"),  //배터리 장착 전자제품
    SPAREBATTERY("reject_baggage_text1101_table"),  //보조배터리
    SMARTLUGGAGE("reject_baggage_text1102_table");

}