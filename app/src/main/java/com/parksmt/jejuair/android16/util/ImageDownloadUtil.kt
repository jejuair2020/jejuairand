package com.parksmt.jejuair.android16.util

import android.content.Context
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import com.parksmt.jejuair.android16.common.DLog
import java.io.*
import java.net.HttpURLConnection

/**
 * 이미지 다운로드 유틸 클래스
 *
 * @author ui-jun
 */
class ImageDownloadUtil {
    /**
     * 이미지 경로에서 이미지를 다운로드 함 UIThread 에서 실행하지 않도록 주의
     *
     * @param imageUrl imageUrl
     * @return 이미지가 있으면 bitmap을 그렇지 않으면 null을 리턴
     * @throws Exception
     */
    @Throws(Exception::class)
    fun getImageFromURL(imageUrl: String): Bitmap? {
        var bitmap: Bitmap? = null
        val resultCode: Int
        val connection: HttpURLConnection =
            NetworkUtil().send(imageUrl, NetworkUtil.RequestMethod.GET)
        resultCode = NetworkUtil().getResponseCode(connection)
        if (resultCode == HttpURLConnection.HTTP_OK) {
            var inputStream: InputStream? = null
            try {
                inputStream = connection.inputStream
                bitmap = BitmapFactory.decodeStream(inputStream)
            } finally {
                inputStream?.close()
            }
        }
        return bitmap
    }

    /**
     * 이미지가 file로 저장되어있으면 file에서 불러 오고 그렇지 않으면 url에서 다운로드 한다. 이미지 중복 검사를 하지 않음 이름이 같으면 무조건 로드함
     *
     * @param context context
     * @param url     이미지 경로
     * @return 이미지가 있으면 bitmap을 없으면 null을 리턴
     * @throws Exception
     */
    @Throws(Exception::class)
    fun getImageCacheOrDownload(context: Context, url: String): Bitmap? {
        val fileFullPath = urlToFileFullPath(context, url) ?: return null
        val bitmap: Bitmap?
        val isFileExist = File(fileFullPath).exists()
        DLog().d(
            "isFileExist : $isFileExist   fileFullPath : $fileFullPath"
        )
        if (isFileExist) {
            bitmap = BitmapFactory.decodeFile(fileFullPath)
        } else {
            bitmap = getImageFromURL(url)
            if (bitmap != null) {
                writeFile(bitmap, File(fileFullPath))
            }
        }
        if (bitmap == null) {
            DLog().d("bitmap download fail : $url")
        } else {
            DLog().d("bitmap download success : $url")
        }
        return bitmap
    }

    fun getImageFromFile(context: Context, url: String?): Bitmap? {
        return getImageFromFile(context, url, 1)
    }

    fun getImageFromFile(
        context: Context,
        url: String?,
        inSampleSize: Int
    ): Bitmap? {
        val fileFullPath = urlToFileFullPath(context, url) ?: return null
        var bitmap: Bitmap? = null
        val isFileExist = File(fileFullPath).exists()
        DLog().d(
            "isFileExist : $isFileExist   fileFullPath : $fileFullPath"
        )
        if (isFileExist) {
            val options = BitmapFactory.Options()
            options.inSampleSize = inSampleSize
            bitmap = BitmapFactory.decodeFile(fileFullPath, options)
        }
        return bitmap
    }

    fun getImageFile(context: Context, url: String?): File? {
        val fileFullPath = urlToFileFullPath(context, url) ?: return null
        val file = File(fileFullPath)
        val isFileExist = file.exists()
        DLog().d(
            "isFileExist : $isFileExist   fileFullPath : $fileFullPath"
        )
        return file
    }

    /**
     * url 주소 중 file 이름으로 full path 를 만든다.
     *
     * @param context context
     * @param url     이미지 경로
     * @return file full path
     */
    fun urlToFileFullPath(context: Context, url: String?): String? {
        var path: String? = null
        if (url != null && url.lastIndexOf("/") != -1) {
            path = context.cacheDir.absolutePath + url.substring(
                url.lastIndexOf("/"),
                url.length
            )
        }
        return path
    }

    fun isFileExist(context: Context, url: String?): Boolean {
        var isFileExist = false
        val fileFullPath = urlToFileFullPath(context, url)
        if (fileFullPath != null) {
            isFileExist = File(fileFullPath).exists()
        }
        DLog().d("isFileExist : $isFileExist   fileFullPath : $fileFullPath")
        return isFileExist
    }

    @Throws(IOException::class)
    private fun writeFile(bitmap: Bitmap, file: File) {
        var out: FileOutputStream? = null
        try {
            out = FileOutputStream(file)
            bitmap.compress(Bitmap.CompressFormat.PNG, 100, out)
        } catch (e: FileNotFoundException) {
            throw FileNotFoundException(e.message)
        } finally {
            try {
                out?.close()
            } catch (ex: IOException) {
                throw IOException(ex)
            }
        }
    }

    @JvmOverloads
    @Throws(Exception::class)
    fun downloadImageFromUrl(
        context: Context,
        url: String,
        update: Boolean = false
    ): Boolean {
        var isDownloaded = false
        val fileFullPath = urlToFileFullPath(context, url)
        if (fileFullPath != null) {
            val bitmap: Bitmap?
            val isFileExist = File(fileFullPath).exists()
            DLog().d("isFileExist : $isFileExist   fileFullPath : $fileFullPath   update : $update")
            isDownloaded = isFileExist
            if (!isFileExist || update) {
                bitmap = getImageFromURL(url)
                if (bitmap != null) {
                    writeFile(bitmap, File(fileFullPath))
                    isDownloaded = true
                }
            }
        }
        return isDownloaded
    }

    fun deleteFile(context: Context, path: String?): Boolean {
        val fileFullPath = urlToFileFullPath(context, path)
        var deleted = false
        if (fileFullPath != null) {
            val file = File(fileFullPath)
            deleted = file.delete()
        }
        DLog().d("deleteFile : $deleted   fileFullPath : $fileFullPath")
        return deleted
    }

    @Throws(IOException::class)
    fun saveBitmap(
        context: Context?,
        bitmap: Bitmap?,
        fileName: String
    ): Boolean {
        var flag: Boolean
        if (context == null) return false
        val fileFullPath = context.cacheDir.absolutePath + "/" + fileName
        val isFileExist = File(fileFullPath).exists()
        DLog().d("isFileExist : $isFileExist   fileFullPath : $fileFullPath")
        flag = isFileExist
        if (!isFileExist) {
            if (bitmap != null) {
                writeFile(bitmap, File(fileFullPath))
                flag = true
            }
        }
        return flag
    }

    fun getBitmap(
        context: Context?,
        fileName: String
    ): Bitmap? { //2018.02.04 yschoi nullchk
        if (null == context) {
            return null
        }
        val fileFullPath = context.cacheDir.absolutePath + "/" + fileName
        var bitmap: Bitmap? = null
        val isFileExist = File(fileFullPath).exists()
        DLog().d("isFileExist : $isFileExist   fileFullPath : $fileFullPath")
        if (isFileExist) {
            bitmap = BitmapFactory.decodeFile(fileFullPath)
        }
        return bitmap
    }
}