package com.parksmt.jejuair.android16.bottomtap

import android.content.Intent
import android.os.Bundle
import android.os.PersistableBundle
import android.view.Gravity
import android.view.View
import androidx.drawerlayout.widget.DrawerLayout
import com.parksmt.jejuair.android16.R
import com.parksmt.jejuair.android16.airplane.AirplaneMode
import com.parksmt.jejuair.android16.util.Util
import kotlinx.android.synthetic.main.activity_home.*
import kotlinx.android.synthetic.main.airplane_mode.*
import kotlinx.android.synthetic.main.main_toolbar.*
import kotlinx.android.synthetic.main.menu_drawer.*


class HomeActivity : TabActivity() {
    override val contentViewId: Int = R.layout.activity_home

    override val navigationMenuItemId: Int = R.id.navigation_home

    var count = 0
    var str = "count : " + count

    lateinit var mDrawerLayout: DrawerLayout

    override fun onCreate(savedInstanceState: Bundle?, persistentState: PersistableBundle?) {
        super.onCreate(savedInstanceState, persistentState)
        setContentView(R.layout.activity_home)

        // onCreate에 직접 구현하지 말것
    }



    override fun onResume() {
        super.onResume()
        overridePendingTransition(0, 0)

        mDrawerLayout = menu_dr as DrawerLayout

        if (Util().isAirplaneModeOn(this)) {
            val intent = Intent()
            intent.setClass(this, AirplaneMode::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP or Intent.FLAG_ACTIVITY_CLEAR_TOP)
            startActivity(intent)
            finish()
        } else {
            btn1.setOnClickListener {
                count = count + 1
                str = "count : " + count
                count1.text = str
            }

            btnair.setOnClickListener {
                val intent = Intent()
                intent.setClass(this, AirplaneMode::class.java)
                intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP or Intent.FLAG_ACTIVITY_CLEAR_TOP)
                startActivity(intent)
                finish()
            }

            res_webview.settings.javaScriptEnabled = true
            res_webview.webViewClient = SslWebView()

            res_webview.loadUrl("https://m.naver.com/")

            bt_menu.setOnClickListener {
                if (mDrawerLayout.isDrawerOpen(Gravity.RIGHT)) {
                    mDrawerLayout.closeDrawer(Gravity.RIGHT)
                } else {
                    mDrawerLayout.visibility = View.VISIBLE
                    mDrawerLayout.openDrawer(Gravity.RIGHT)
                }
            }


        }
    }

    override fun onBackPressed() {
        if (mDrawerLayout.isDrawerOpen(Gravity.RIGHT)) {
            mDrawerLayout.closeDrawer(Gravity.RIGHT)
        } else {
            super.onBackPressed()
        }
    }

}