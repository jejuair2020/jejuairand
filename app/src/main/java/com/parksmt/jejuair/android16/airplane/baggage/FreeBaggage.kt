package com.parksmt.jejuair.android16.airplane.baggage

import android.content.Context
import android.os.Bundle
import android.view.View
import android.widget.ImageView
import android.widget.RelativeLayout
import android.widget.TextView
import com.parksmt.jejuair.android16.R
import com.parksmt.jejuair.android16.airplane.AirplaneModeMenuActivity
import com.parksmt.jejuair.android16.serviceinfo.RegulationBaggagePopup
import com.parksmt.jejuair.android16.util.DialogUtil
import com.parksmt.jejuair.android16.util.Util
import com.parksmt.jejuair.android16.view.DateDialog
import com.parksmt.jejuair.android16.view.PopupController
import kotlinx.android.synthetic.main.free_baggage_layout.*
import java.text.SimpleDateFormat
import java.util.*

/**
 * 수하물 > 무료 수하물
 *
 *
 * Created by seungjinoh on 2017. 4. 4..
 */
class FreeBaggage : AirplaneModeMenuActivity() {
    private var mAgeCompare: AgeCompare? = null
    private lateinit var regulationBaggagePopup: RegulationBaggagePopup
    protected override val uiName: String
        protected get() = "S-MUI-08-008"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.free_baggage_layout)
        setText()
        initView()
        initClickEvent()
    }

    override fun onClick(v: View) {
        super.onClick(v)
        when (v.id) {
            R.id.free_baggage_text38 -> mAgeCompare!!.show()
            R.id.free_baggage_text39 -> DialogUtil().showCommonAlertDialog(
                this@FreeBaggage,
                mLanguageJson.optString("airplaneModeText1009")
            )
        }
    }

    private fun initView() {
        mAgeCompare = AgeCompare(this)
        regulationBaggagePopup = RegulationBaggagePopup(this)
    }

    private fun setText() {
        loadLanguage("serviceinfo/FreeBaggage.json")
        setTitleText(mLanguageJson.optString("FreeBaggageText1000"), null)
        (findViewById(R.id.free_baggage_text1) as TextView).setText(mLanguageJson.optString("FreeBaggageText1001"))
        (findViewById(R.id.free_baggage_text2) as TextView).setText(mLanguageJson.optString("FreeBaggageText1002"))
        (findViewById(R.id.free_baggage_text3) as TextView).setText(mLanguageJson.optString("FreeBaggageText1003"))
        //TODO 2018.06.08 [ITSM-IM00128628] [부가사업팀] (모바일앱-네이티브) Farefamily 운임 분류에 따른 수하물허용량 안내
        (findViewById(R.id.free_baggage_domestic_title_text) as TextView).setText(
            mLanguageJson.optString(
                "FreeBaggageText1004"
            )
        )
        (findViewById(R.id.free_baggage_type_text1) as TextView).setText(mLanguageJson.optString("FreeBaggageText1053"))
        (findViewById(R.id.free_baggage_type_text2) as TextView).setText(mLanguageJson.optString("FreeBaggageText1054"))
        (findViewById(R.id.free_baggage_type_text3) as TextView).setText(mLanguageJson.optString("FreeBaggageText1055"))
        (findViewById(R.id.free_baggage_type_weight_text1) as TextView).setText(
            mLanguageJson.optString(
                "FreeBaggageText1057"
            )
        )
        (findViewById(R.id.free_baggage_type_weight_text2) as TextView).setText(
            mLanguageJson.optString(
                "FreeBaggageText1056"
            )
        )
        (findViewById(R.id.free_baggage_type_weight_text3) as TextView).setText(
            mLanguageJson.optString(
                "FreeBaggageText1008"
            )
        )
        //TODO 2018.06.08 [ITSM-IM00128628] [부가사업팀] (모바일앱-네이티브) Farefamily 운임 분류에 따른 수하물허용량 안내
        (findViewById(R.id.free_baggage_text7) as TextView).setText(mLanguageJson.optString("FreeBaggageText1007"))
        (findViewById(R.id.free_baggage_text8) as TextView).setText(mLanguageJson.optString("FreeBaggageText1008"))
        (findViewById(R.id.free_baggage_text9) as TextView).setText(
            mLanguageJson.optString("FreeBaggageText1009") + mLanguageJson.optString(
                "EffectiveAs2019.04.30"
            )
        )
        (findViewById(R.id.free_baggage_text10) as TextView).setText(mLanguageJson.optString("FreeBaggageText1010"))
        (findViewById(R.id.free_baggage_text11) as TextView).setText(mLanguageJson.optString("FreeBaggageText1053")) // fly bag+
        (findViewById(R.id.free_baggage_text12) as TextView).setText(mLanguageJson.optString("FreeBaggageText1012"))
        (findViewById(R.id.free_baggage_text13) as TextView).setText(mLanguageJson.optString("FreeBaggageText1013"))
        (findViewById(R.id.free_baggage_text14) as TextView).setText(mLanguageJson.optString("FreeBaggageText1054")) //fly bag
        (findViewById(R.id.free_baggage_text15) as TextView).setText(mLanguageJson.optString("FreeBaggageText1015"))
        (findViewById(R.id.free_baggage_text16) as TextView).setText(mLanguageJson.optString("FreeBaggageText1016"))
        (findViewById(R.id.free_baggage_text17) as TextView).setText(mLanguageJson.optString("FreeBaggageText1055")) //fly
        (findViewById(R.id.free_baggage_text18) as TextView).setText(mLanguageJson.optString("FreeBaggageText1008"))
        (findViewById(R.id.free_baggage_text19) as TextView).setText(mLanguageJson.optString("FreeBaggageText1019"))
        (findViewById(R.id.free_baggage_text20) as TextView).setText(mLanguageJson.optString("FreeBaggageText1053")) // fly bag+
        (findViewById(R.id.free_baggage_text21) as TextView).setText(mLanguageJson.optString("FreeBaggageText1021"))
        (findViewById(R.id.free_baggage_text22) as TextView).setText(mLanguageJson.optString("FreeBaggageText1022"))
        (findViewById(R.id.free_baggage_text23) as TextView).setText(mLanguageJson.optString("FreeBaggageText1054")) //fly bag
        (findViewById(R.id.free_baggage_text24) as TextView).setText(mLanguageJson.optString("FreeBaggageText1024"))
        (findViewById(R.id.free_baggage_text25) as TextView).setText(mLanguageJson.optString("FreeBaggageText1025"))
        (findViewById(R.id.free_baggage_text26) as TextView).setText(mLanguageJson.optString("FreeBaggageText1055")) //fly
        (findViewById(R.id.free_baggage_text27) as TextView).setText(mLanguageJson.optString("FreeBaggageText1008"))
        (findViewById(R.id.free_baggage_text28) as TextView).setText(
            mLanguageJson.optString("FreeBaggageText1028") + mLanguageJson.optString(
                "EffectiveAs2019.04.30"
            )
        )
        (findViewById(R.id.free_baggage_text35) as TextView).setText(mLanguageJson.optString("FreeBaggageText1035"))
        (findViewById(R.id.free_baggage_text36) as TextView).setText(mLanguageJson.optString("FreeBaggageText1036"))
        (findViewById(R.id.free_baggage_text37) as TextView).setText(mLanguageJson.optString("FreeBaggageText1037"))
        (findViewById(R.id.free_baggage_text38) as TextView).setText(mLanguageJson.optString("FreeBaggageText1038"))
        (findViewById(R.id.free_baggage_text39) as TextView).setText(mLanguageJson.optString("FreeBaggageText1039"))
        /**
         * TODO 2018.03.05 yschoi [ITSM-IM00119107] 모바일앱 비행기모드 텍스트 추가
         */
        (findViewById(R.id.free_baggage_text40) as TextView).setText(mLanguageJson.optString("FreeBaggageText1052"))
        //2018.11.02 yschoi [ITSM-IM00146887] [m-APP 네이티브 ] 무료수하물 안내문구 추가 -임정묵D-
        (findViewById(R.id.free_baggage_text58) as TextView).setText(mLanguageJson.optString("FreeBaggageText1058"))
        //국제선 수하물 규정 추가
        (findViewById(R.id.regulation_baggabe_title) as TextView).setText(mLanguageJson.optString("FreeBaggageText_regulation"))
        //2019.05.03 yschoi 페어패밀리 수하물 변경 사항 추가 작업
        (findViewById(R.id.desc_international_flight_guam_saipan) as TextView).setText(
            mLanguageJson.optString(
                "Desc_InternationalFlight_Guam_Saipan"
            )
        )
        val view1: View = findViewById(R.id.free_baggage_include1) //성인/소아
        (view1.findViewById<View>(R.id.baggage_domestic_title_text) as TextView).setText(
            mLanguageJson.optString("FreeBaggageText1010")
        )
        (view1.findViewById<View>(R.id.baggage_type_text1) as TextView).setText(
            mLanguageJson.optString("FreeBaggageText1053")
        )
        (view1.findViewById<View>(R.id.baggage_type_text2) as TextView).setText(
            mLanguageJson.optString("FreeBaggageText1054")
        )
        (view1.findViewById<View>(R.id.baggage_type_text3) as TextView).setText(
            mLanguageJson.optString("FreeBaggageText1055")
        )
        (view1.findViewById<View>(R.id.baggage_type_weight_text1) as TextView).setText(
            mLanguageJson.optString("FreeBaggageText1031")
        )
        (view1.findViewById<View>(R.id.baggage_type_weight_text2) as TextView).setText(
            mLanguageJson.optString("Under23kg")
        )
        (view1.findViewById<View>(R.id.baggage_type_weight_text3) as TextView).setText(
            mLanguageJson.optString("FreeBaggageText1008")
        )
        (view1.findViewById<View>(R.id.baggage_max_count1) as TextView).setText(
            mLanguageJson.optString("FreeBaggageText1030")
        )
        (view1.findViewById<View>(R.id.baggage_max_count2) as TextView).setText(
            mLanguageJson.optString("FreeBaggageText1033")
        )
        val view2: View = findViewById(R.id.free_baggage_include2) //유아
        (view2.findViewById<View>(R.id.baggage_domestic_title_text) as TextView).setText(
            mLanguageJson.optString("FreeBaggageText1032")
        )
        (view2.findViewById<View>(R.id.baggage_type_text1) as TextView).setText(
            mLanguageJson.optString("FreeBaggageText1053")
        )
        (view2.findViewById<View>(R.id.baggage_type_text2) as TextView).setText(
            mLanguageJson.optString("FreeBaggageText1054")
        )
        (view2.findViewById<View>(R.id.baggage_type_text3) as TextView).setText(
            mLanguageJson.optString("FreeBaggageText1055")
        )
        (view2.findViewById<View>(R.id.baggage_type_weight_text1) as TextView).setText(
            mLanguageJson.optString("FreeBaggageText1034")
        )
        (view2.findViewById<View>(R.id.baggage_type_weight_text2) as TextView).setText(
            mLanguageJson.optString("FreeBaggageText1034")
        )
        (view2.findViewById<View>(R.id.baggage_type_weight_text3) as TextView).setText(
            mLanguageJson.optString("FreeBaggageText1055")
        )
        (view2.findViewById<View>(R.id.baggage_max_count1) as TextView).setText(
            mLanguageJson.optString("FreeBaggageText1033")
        )
        (view2.findViewById<View>(R.id.baggage_max_count2) as TextView).setText(
            mLanguageJson.optString("FreeBaggageText1033")
        )
    }

    private fun initClickEvent() { //나이 계산기
        free_baggage_text38.setOnClickListener(this)
        //나의 예약 운임 확인
        free_baggage_text39.setOnClickListener(this)
        //이전 국제선 수하물 규정
        regulation_baggabe_title.setOnClickListener(View.OnClickListener { regulationBaggagePopup.show() })
    }

    private inner class AgeCompare internal constructor(context: Context?) :
        PopupController(context!!, R.layout.age_compare_popup), View.OnClickListener {
        private lateinit var mDateDialog: DateDialog
        private lateinit var mGuideTextView: TextView
        private var mDateTextView: TextView? = null
        private var mResultDateTextView: TextView? = null
        private lateinit var mResultTextView: TextView
        private var mResultLayout: RelativeLayout? = null
        private var mIconImageView: ImageView? = null
        override fun onClick(v: View) {
            when (v.id) {
                R.id.reservation_start_popup_close_btn -> dismiss()
            }
        }

        private fun init() {
            mGuideTextView =
                mContentView.findViewById<View>(R.id.age_compare_text6) as TextView
            mDateTextView =
                mContentView.findViewById<View>(R.id.age_compare_text5) as TextView
            mResultLayout =
                mContentView.findViewById<View>(R.id.age_compare_result_layout) as RelativeLayout
            mResultDateTextView =
                mContentView.findViewById<View>(R.id.age_compare_date_textview) as TextView
            mResultTextView =
                mContentView.findViewById<View>(R.id.age_compare_result_textview) as TextView
            mIconImageView =
                mContentView.findViewById<View>(R.id.age_compare_icon_imageview) as ImageView
            mDateDialog = DateDialog(this@FreeBaggage, true)
            val calendar = Calendar.getInstance()
            mDateDialog.initCalendar(calendar)
            calendar.add(Calendar.YEAR, -13)
            calendar.add(Calendar.DAY_OF_YEAR, 1)
            mDateDialog.setMinDate(calendar)
/*            mDateDialog.setOnSelectedListener(DateDialog.OnSelectedListener() {
                fun onSelected(calendar: Calendar) {
                    mDateTextView!!.text = SimpleDateFormat("yyyy - MM - dd").format(
                        calendar.time
                    )
                }
            })*/
            mDateTextView!!.text =
                SimpleDateFormat("yyyy - MM - dd").format(mDateDialog.selectedDate.getTime())
        }

        private fun setText() {
            (mContentView.findViewById<View>(R.id.age_compare_text1) as TextView).setText(
                mLanguageJson.optString("FreeBaggageText1040")
            )
            (mContentView.findViewById<View>(R.id.age_compare_text2) as TextView).setText(
                mLanguageJson.optString("FreeBaggageText1041")
            )
            (mContentView.findViewById<View>(R.id.age_compare_text3) as TextView).setText(
                mLanguageJson.optString("FreeBaggageText1042")
            )
            (mContentView.findViewById<View>(R.id.age_compare_text4) as TextView).setText(
                mLanguageJson.optString("FreeBaggageText1043")
            )
            (mContentView.findViewById<View>(R.id.age_compare_text5) as TextView).setHint(
                mLanguageJson.optString("FreeBaggageText1044")
            )
            mGuideTextView.setText(mLanguageJson.optString("FreeBaggageText1045"))
        }

        private fun initOnClick() { //계산 클릭
            mContentView.findViewById<View>(R.id.age_compare_text4)
                .setOnClickListener { cal() }
            //날짜 클릭
            mContentView.findViewById<View>(R.id.age_compare_text5)
                .setOnClickListener { mDateDialog.show() }
            mContentView.findViewById<View>(R.id.reservation_start_popup_close_btn)
                .setOnClickListener(this)
        }

        private fun cal() {
            val calendar = Calendar.getInstance()
            var age: Int = Util().ageCalculator(
                calendar,
                mDateDialog.selectedDate,
                true
            )
            val header: String =
                mLanguageJson.optString("FreeBaggageText1049").toString() + SimpleDateFormat(
                    "yyyy-MM-dd"
                )
                    .format(calendar.time) + mLanguageJson.optString("FreeBaggageText1050")
            mResultDateTextView!!.text = header
            // 유아
            if (age == 2) {
                mIconImageView!!.setBackgroundResource(R.drawable.persons_baby_2)
                mResultTextView.setText(mLanguageJson.optString("FreeBaggageText1048"))
                // 소아
            } else if (age == 1) {
                mIconImageView!!.setBackgroundResource(R.drawable.persons_kid_2)
                age = Util().ageCalculator(
                    calendar,
                    mDateDialog.selectedDate,
                    false
                )
                if (age == 0) {
                    mResultTextView.setText(mLanguageJson.optString("FreeBaggageText1051"))
                } else {
                    mResultTextView.setText(mLanguageJson.optString("FreeBaggageText1047"))
                }
            }
            mGuideTextView!!.visibility = View.GONE
            mResultLayout!!.visibility = View.VISIBLE
        }

        override fun show() {
            super.show()
/*            setOnBackKeyListener(object : OnBackKeyListener {
                override fun onBackKey() {
                    dismiss()
                }
            })*/
        }

        override fun dismiss() {
            super.dismiss()
            restoreOnBackKeyListener()
        }

        init {
            init()
            initOnClick()
            setText()
        }
    }
}