package com.parksmt.jejuair.android16.airplane.baggage

import android.os.Bundle
import android.view.View
import android.widget.TextView
import com.parksmt.jejuair.android16.R
import com.parksmt.jejuair.android16.airplane.AirplaneModeMenuActivity
import com.parksmt.jejuair.android16.util.DialogUtil
import kotlinx.android.synthetic.main.special_baggage_layout.*

/**
 * 특수 수하물  S-MUI-08-016
 * Created by seungjinoh on 2017. 4. 5..
 */
class SpecialBaggage : AirplaneModeMenuActivity() {
    protected override val uiName: String
        protected get() = "S-MUI-08-016"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.special_baggage_layout)
        setText()
        initClickEvent()
    }

    override fun onClick(v: View) {
        super.onClick(v)
        when (v.id) {
            R.id.special_baggage_text15, R.id.special_baggage_confirmation_btn, R.id.special_baggage_text31 -> DialogUtil().showCommonAlertDialog(
                this@SpecialBaggage,
                mLanguageJson.optString("airplaneModeText1009")
            )
        }
    }

    private fun setText() {
        loadLanguage("serviceinfo/SpecialBaggage.json")
        setTitleText(mLanguageJson.optString("special_baggage_text1000"))
        (findViewById(R.id.special_baggage_text1) as TextView).setText(mLanguageJson.optString("special_baggage_text1001"))
        (findViewById(R.id.special_baggage_text2) as TextView).setText(mLanguageJson.optString("special_baggage_text1002"))
        (findViewById(R.id.special_baggage_text3) as TextView).setText(mLanguageJson.optString("special_baggage_text1003"))
        (findViewById(R.id.special_baggage_text4) as TextView).setText(mLanguageJson.optString("special_baggage_text1004"))
        (findViewById(R.id.special_baggage_text5) as TextView).setText(mLanguageJson.optString("special_baggage_text1005"))
        (findViewById(R.id.special_baggage_text6) as TextView).setText(mLanguageJson.optString("special_baggage_for_sports"))
        //TODO 2018.10.11 yschoi [ITSM-IM00144386] [부가사업팀] 외국 환율 통일화에 따른 홈페이지 문구 수정 요청의 건 (모바일) -임수영S-
        (findViewById(R.id.special_baggage_text32) as TextView).setText(mLanguageJson.optString("special_baggage_exchange_info"))
        (findViewById(R.id.special_baggage_text7) as TextView).setText(mLanguageJson.optString("special_baggage_text1007"))
        (findViewById(R.id.special_baggage_text8) as TextView).setText(mLanguageJson.optString("special_baggage_text1008"))
        (findViewById(R.id.special_baggage_text9) as TextView).setText(mLanguageJson.optString("special_baggage_text1009"))
        //TODO 2018.05.23 yschoi [ITSM-IM00127023] [APP 네이티브영역] 특수수하물 사이즈 변경 요청 2.3.1에 적용할 것.
//        ((TextView) findViewById(R.id.special_baggage_text11)).setText(mLanguageJson.optString("special_baggage_text1011"));
//        ((TextView) findViewById(R.id.special_baggage_text12)).setText(mLanguageJson.optString("special_baggage_text1012"));
//        ((TextView) findViewById(R.id.special_baggage_text13)).setText(mLanguageJson.optString("special_baggage_text1013"));
        (findViewById(R.id.special_baggage_text15) as TextView).setText(mLanguageJson.optString("special_baggage_text1015"))
        special_baggage_text27.setText(mLanguageJson.optString("special_baggage_text1026"))
        //2019.05.07 yschoi [ITSM-IM00175830]
//특수 수하물 확인서 버튼 추가
        special_baggage_confirmation_btn.text ="special_baggage_confirmation"
        //특수 수하물 최대 허용 규격 유의사항 변경
        special_baggage_text10.setText(mLanguageJson.optString("special_baggage_text1010"))
        special_baggage_text40.setText(mLanguageJson.optString("special_baggage_text1040"))
        special_baggage_text41.setText(mLanguageJson.optString("special_baggage_text1041"))
        special_baggage_text42.setText(mLanguageJson.optString("special_baggage_text1042"))
        special_baggage_text43.setText(mLanguageJson.optString("special_baggage_text1043"))
        special_baggage_text44.setText(mLanguageJson.optString("special_baggage_text1044"))
        special_baggage_text45.setText(mLanguageJson.optString("special_baggage_text1045"))
        special_baggage_text46.setText(mLanguageJson.optString("special_baggage_text1046"))
        special_baggage_text14.setText(mLanguageJson.optString("special_baggage_text1014"))
        //악기 유의사항 변경
        special_baggage_text47.setText(mLanguageJson.optString("special_baggage_text1047"))
        special_baggage_text48.setText(mLanguageJson.optString("special_baggage_text1048"))
        special_baggage_text49.setText(mLanguageJson.optString("special_baggage_text1049"))
        special_baggage_text50.setText(mLanguageJson.optString("special_baggage_text1050"))
        special_baggage_text51.setText(mLanguageJson.optString("special_baggage_text1051"))
    }

    private fun initClickEvent() {
        special_baggage_text15.setOnClickListener(this)
        special_baggage_text31.setOnClickListener(this)
        special_baggage_confirmation_btn.setOnClickListener(this)
    }
}