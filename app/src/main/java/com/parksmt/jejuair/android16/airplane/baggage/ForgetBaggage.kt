package com.parksmt.jejuair.android16.airplane.baggage

import android.content.Context
import android.os.Bundle
import android.view.View
import android.widget.TextView
import com.parksmt.jejuair.android16.R
import com.parksmt.jejuair.android16.airplane.AirplaneModeMenuActivity
import com.parksmt.jejuair.android16.util.DialogUtil
import com.parksmt.jejuair.android16.view.PopupController
import kotlinx.android.synthetic.main.forget_baggage_layout.*

/**
 * 수하물 > 수하물 분실 및 배상 S-MUI-08-018
 *
 *
 * Created by seungjinoh on 2017. 4. 6..
 */
class ForgetBaggage : AirplaneModeMenuActivity() {
    private var mCompensationAdmittedInfoPopup: CompensationAdmittedInfoPopup? = null
    protected override val uiName: String
        protected get() = "S-MUI-08-018"

    protected override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.forget_baggage_layout)
        setText()
        initView()
        initClickEvent()
    }

    private fun initView() {
        mCompensationAdmittedInfoPopup = CompensationAdmittedInfoPopup(this)
    }

    private fun setText() {
        loadLanguage("serviceinfo/ForgetBaggage.json")
        setTitleText(mLanguageJson.optString("forget_baggabe_text1000"))
        (findViewById(R.id.forget_baggabe_text1) as TextView).setText(mLanguageJson.optString("forget_baggabe_text1001"))
        (findViewById(R.id.forget_baggabe_text2) as TextView).setText(mLanguageJson.optString("forget_baggabe_text1002"))
        (findViewById(R.id.forget_baggabe_text3) as TextView).setText(mLanguageJson.optString("forget_baggabe_text1003"))
        (findViewById(R.id.forget_baggabe_text4) as TextView).setText(mLanguageJson.optString("forget_baggabe_text1004"))
        (findViewById(R.id.forget_baggabe_text5) as TextView).setText(mLanguageJson.optString("forget_baggabe_text1005"))
        (findViewById(R.id.forget_baggabe_text6) as TextView).setText(mLanguageJson.optString("forget_baggabe_text1006"))
        (findViewById(R.id.forget_baggabe_text7) as TextView).setText(mLanguageJson.optString("forget_baggabe_text1007"))
        (findViewById(R.id.forget_baggabe_text8) as TextView).setText(mLanguageJson.optString("forget_baggabe_text1008"))
        (findViewById(R.id.forget_baggabe_text9) as TextView).setText(mLanguageJson.optString("forget_baggabe_text1009"))
        (findViewById(R.id.forget_baggabe_text10) as TextView).setText(mLanguageJson.optString("forget_baggabe_text1010"))
        (findViewById(R.id.forget_baggabe_text11) as TextView).setText(mLanguageJson.optString("forget_baggabe_text1011"))
        (findViewById(R.id.forget_baggabe_text12) as TextView).setText(mLanguageJson.optString("forget_baggabe_text1012"))
        (findViewById(R.id.forget_baggabe_text13) as TextView).setText(mLanguageJson.optString("forget_baggabe_text1013"))
        (findViewById(R.id.forget_baggabe_text14) as TextView).setText(mLanguageJson.optString("forget_baggabe_text1014"))
        (findViewById(R.id.forget_baggabe_text15) as TextView).setText(mLanguageJson.optString("forget_baggabe_text1015"))
        (findViewById(R.id.forget_baggabe_text16) as TextView).setText(mLanguageJson.optString("forget_baggabe_text1016"))
        (findViewById(R.id.forget_baggabe_text17) as TextView).setText(mLanguageJson.optString("forget_baggabe_text1017"))
        (findViewById(R.id.forget_baggabe_text18) as TextView).setText(mLanguageJson.optString("forget_baggabe_text1018"))
        (findViewById(R.id.forget_baggabe_text19) as TextView).setText(mLanguageJson.optString("forget_baggabe_text1019"))
        (findViewById(R.id.forget_baggabe_text20) as TextView).setText(mLanguageJson.optString("forget_baggabe_text1020"))
        (findViewById(R.id.forget_baggabe_text21) as TextView).setText(mLanguageJson.optString("forget_baggabe_text1021"))
    }

    private fun initClickEvent() { //유실물 조회
        forget_baggabe_text13.setOnClickListener(View.OnClickListener {
            DialogUtil().showCommonAlertDialog(
                this@ForgetBaggage,
                mLanguageJson.optString("airplaneModeText1009")
            )
        })
        //배상 불가 안내
        forget_baggabe_text21.setOnClickListener(View.OnClickListener { mCompensationAdmittedInfoPopup!!.show() })
    }

    private inner class CompensationAdmittedInfoPopup(context: Context?) :
        PopupController(context!!, R.layout.compensation_admitted_info_popup),
        View.OnClickListener {
        override fun onClick(v: View) {
            when (v.id) {
                R.id.compensation_admitted_info_popup_close_btn -> dismiss()
            }
        }

        private fun init() {}
        private fun setText() {
            (mContentView.findViewById<View>(R.id.compensation_admitted_info_text1) as TextView)
                .setText(mLanguageJson.optString("forget_baggabe_text1021"))
            (mContentView.findViewById<View>(R.id.compensation_admitted_info_text2) as TextView)
                .setText(mLanguageJson.optString("forget_baggabe_text1022"))
            (mContentView.findViewById<View>(R.id.compensation_admitted_info_text3) as TextView)
                .setText(mLanguageJson.optString("forget_baggabe_text1023"))
            (mContentView.findViewById<View>(R.id.compensation_admitted_info_text4) as TextView)
                .setText(mLanguageJson.optString("forget_baggabe_text1024"))
            (mContentView.findViewById<View>(R.id.compensation_admitted_info_text5) as TextView)
                .setText(mLanguageJson.optString("forget_baggabe_text1025"))
            (mContentView.findViewById<View>(R.id.compensation_admitted_info_text6) as TextView)
                .setText(mLanguageJson.optString("forget_baggabe_text1026"))
            (mContentView.findViewById<View>(R.id.compensation_admitted_info_text7) as TextView)
                .setText(mLanguageJson.optString("forget_baggabe_text1027"))
            (mContentView.findViewById<View>(R.id.compensation_admitted_info_text8) as TextView)
                .setText(mLanguageJson.optString("forget_baggabe_text1028"))
            (mContentView.findViewById<View>(R.id.compensation_admitted_info_text9) as TextView)
                .setText(mLanguageJson.optString("forget_baggabe_text1029"))
        }

        private fun initOnClick() {
            mContentView.findViewById<View>(R.id.compensation_admitted_info_popup_close_btn)
                .setOnClickListener(this)
        }

        init {
            init()
            initOnClick()
            setText()
        }
    }
}