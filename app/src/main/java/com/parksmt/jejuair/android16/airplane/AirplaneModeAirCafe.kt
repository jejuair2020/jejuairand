package com.parksmt.jejuair.android16.airplane

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageButton
import android.widget.ImageView
import android.widget.TextView
import androidx.viewpager.widget.PagerAdapter
import androidx.viewpager.widget.ViewPager.OnPageChangeListener
import com.bumptech.glide.Glide
import com.parksmt.jejuair.android16.R
import com.parksmt.jejuair.android16.common.DLog
import com.parksmt.jejuair.android16.data.AirCafeInfo
import com.parksmt.jejuair.android16.util.ImageDownloadUtil
import com.parksmt.jejuair.android16.util.ImageUtil
import com.parksmt.jejuair.android16.view.BaseViewPager
import com.parksmt.jejuair.android16.view.PopupController
import kotlinx.android.synthetic.main.air_cafe_menu_popup.view.*
import java.util.*

/**
 * Air Café
 *
 *
 * Created by ui-jun on 2017-06-29.
 */
class AirplaneModeAirCafe : AirplaneModeFragment(),
    View.OnClickListener {
    private lateinit var mAirCafeMenuInfoPopup: AirCafeMenuInfoPopup
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        super.onCreateView(inflater, container, savedInstanceState)
        val rootView: View =
            inflater.inflate(R.layout.aircafe_content_layout, container, false)
        initOnClick(rootView)
        setText(rootView)
        setAirCafeMenuInfoPopup()
        return rootView
    }

    override fun onClick(v: View) {
        when (v.id) {
            R.id.aircafe_textview_3 -> showAirCafeMenu()
        }
    }

    private fun initOnClick(rootView: View) {
        rootView.findViewById<View>(R.id.aircafe_textview_3).setOnClickListener(this)
    }

    private fun setText(rootView: View) {
        loadLanguage("serviceinfo/AirCafeLan.json")
        (rootView.findViewById<View>(R.id.aircafe_textview_1) as TextView).text =
            mLanguageJson.optString("AirCafeText1002")
        (rootView.findViewById<View>(R.id.aircafe_textview_2) as TextView).text =
            mLanguageJson.optString("AirCafeText1003")
        (rootView.findViewById<View>(R.id.aircafe_textview_3) as TextView).text =
            mLanguageJson.optString("AirCafeText1004")
        (rootView.findViewById<View>(R.id.aircafe_textview_4) as TextView).text =
            mLanguageJson.optString("AirCafeText1005")
        (rootView.findViewById<View>(R.id.aircafe_textview_5) as TextView).text =
            mLanguageJson.optString("AirCafeText1006")
        (rootView.findViewById<View>(R.id.aircafe_textview_6) as TextView).text =
            mLanguageJson.optString("AirCafeText1007")
        (rootView.findViewById<View>(R.id.aircafe_textview_7) as TextView).text =
            mLanguageJson.optString("AirCafeText1008")
        (rootView.findViewById<View>(R.id.aircafe_textview_8) as TextView).text =
            mLanguageJson.optString("AirCafeText1009")
        (rootView.findViewById<View>(R.id.aircafe_textview_9) as TextView).text =
            mLanguageJson.optString("AirCafeText1010")
        (rootView.findViewById<View>(R.id.aircafe_textview_10) as TextView).text =
            mLanguageJson.optString("AirCafeText1011")
        (rootView.findViewById<View>(R.id.aircafe_textview_11) as TextView).text =
            mLanguageJson.optString("AirCafeText1012")
        (rootView.findViewById<View>(R.id.aircafe_textview_12) as TextView).text =
            mLanguageJson.optString("AirCafeText1013")
        (rootView.findViewById<View>(R.id.aircafe_textview_13) as TextView).text =
            mLanguageJson.optString("AirCafeText1014")
        (rootView.findViewById<View>(R.id.aircafe_textview_14) as TextView).text =
            mLanguageJson.optString("AirCafeText1015")
        (rootView.findViewById<View>(R.id.aircafe_textview_15) as TextView).text =
            mLanguageJson.optString("AirCafeText1016")
        (rootView.findViewById<View>(R.id.aircafe_textview_16) as TextView).text =
            mLanguageJson.optString("AirCafeText1017")
        (rootView.findViewById<View>(R.id.aircafe_textview_17) as TextView).text =
            mLanguageJson.optString("AirCafeText1018")
        (rootView.findViewById<View>(R.id.aircafe_textview_18) as TextView).text =
            mLanguageJson.optString("AirCafeText1019")
        val airCafeInfo: AirCafeInfo = AirCafeInfo.getDownloadedAirCafeInfo(this.requireContext())
        if (airCafeInfo != null) {
            Glide.with(this.requireContext())
                .load(ImageDownloadUtil().getImageFile(this.requireContext(), airCafeInfo.thumPath))
                .error(R.drawable.air_cafe)
                .into(rootView.findViewById<View>(R.id.aircafe_menu_thumbnail_imageview) as ImageView)
        }
    }

    private fun showAirCafeMenu() {
        if (mAirCafeMenuInfoPopup != null) {
            mAirCafeMenuInfoPopup.show()
        } else {
            showEmptyAirCafeMenu()
        }
    }

    private fun showEmptyAirCafeMenu() {}
    private fun setAirCafeMenuInfoPopup() {
        val airCafeInfo: AirCafeInfo = AirCafeInfo.getSavedAirCafeInfo(this.requireContext())
        //        AirCafeInfo airCafeInfo = AirCafeInfo.getDownloadedAirCafeInfo(getActivity());
        if (airCafeInfo != null /*&& airCafeInfo.isDownloaded()*/) {
            val arrayList: ArrayList<String> = airCafeInfo.getContentList()
            //            ArrayList<String> tempList = new ArrayList<>();
//            for (String key : arrayList) {
//                if (ImageDownloadUtil.isFileExist(getActivity(), key)) {
//                    tempList.add(key);
//                }
//            }
            if (arrayList.size > 0) { //                airCafeInfo.setContentList(arrayList);
                mAirCafeMenuInfoPopup = AirCafeMenuInfoPopup(this.requireContext(), airCafeInfo)
            }
        }
    }

    private inner class AirCafeMenuInfoPopup internal constructor(
        context: Context,
        airCafeInfo: AirCafeInfo
    ) : PopupController(context, R.layout.air_cafe_menu_popup), View.OnClickListener {
        private lateinit var mTitleTextView: TextView
        private lateinit var mViewPager: BaseViewPager
        private val mAirCafeInfo: AirCafeInfo
        private var mLeftImageButton: ImageButton? = null
        private var mRightImageButton: ImageButton? = null
        private fun init() {
            mContentView.air_cafe_menu_popup_close_btn.setOnClickListener(this)
            mTitleTextView =
                mContentView.findViewById(R.id.air_cafe_menu_popup_title_textview)
            mViewPager =
                mContentView.findViewById(R.id.air_cafe_menu_popup_viewpager) as BaseViewPager
            mViewPager.setAdapter(
                ViewPagerAdapter(
                    mContext,
                    mAirCafeInfo.getContentList()
                )
            )
            mViewPager.addOnPageChangeListener(object : OnPageChangeListener {
                override fun onPageScrolled(
                    position: Int,
                    positionOffset: Float,
                    positionOffsetPixels: Int
                ) {
                }

                override fun onPageSelected(position: Int) {
                    DLog().d("position$position")
                    if (position == 0) {
                        mLeftImageButton!!.visibility = View.GONE
                        if (mAirCafeInfo.getContentList().size > 0) {
                            mRightImageButton!!.visibility = View.VISIBLE
                        }
                    } else if (position == mAirCafeInfo.getContentList().size - 1) {
                        mRightImageButton!!.visibility = View.GONE
                    } else {
                        mLeftImageButton!!.visibility = View.VISIBLE
                        mRightImageButton!!.visibility = View.VISIBLE
                    }
                }

                override fun onPageScrollStateChanged(state: Int) {}
            })
            mLeftImageButton =
                mContentView.findViewById(R.id.air_cafe_menu_popup_left_btn)
            mLeftImageButton!!.setOnClickListener(this)
            mRightImageButton =
                mContentView.findViewById(R.id.air_cafe_menu_popup_right_btn)
            mRightImageButton!!.setOnClickListener(this)
            mTitleTextView.setText(mAirCafeInfo.title)
        }

        override fun onClick(v: View) {
            when (v.id) {
                R.id.air_cafe_menu_popup_close_btn -> dismiss()
                R.id.air_cafe_menu_popup_left_btn -> goLeft()
                R.id.air_cafe_menu_popup_right_btn -> goRight()
            }
        }

        private fun goLeft() {
            val position: Int = mViewPager.getCurrentItem() - 1
            if (position >= 0) {
                mViewPager.setCurrentItem(position, true)
            } else {
                mLeftImageButton!!.visibility = View.GONE
            }
        }

        private fun goRight() {
            val position: Int = mViewPager.getCurrentItem() + 1
            if (position < mAirCafeInfo.getContentList().size) {
                mViewPager.setCurrentItem(position, true)
            } else {
                mRightImageButton!!.visibility = View.GONE
            }
        }

        private inner class ViewPagerAdapter internal constructor(
            private val mContext: Context,
            private val mItem: ArrayList<String>
        ) : PagerAdapter() {
            override fun getCount(): Int {
                return mItem.size
            }

            override fun instantiateItem(container: ViewGroup, position: Int): Any {
                val view: View =
                    ImageUtil().setImageFitStartCenter(mContext, container, mItem[position])
                container.addView(view)
                return view
            }

            override fun isViewFromObject(
                view: View,
                `object`: Any
            ): Boolean {
                return view === `object`
            }

            override fun destroyItem(
                container: ViewGroup,
                position: Int,
                `object`: Any
            ) {
                container.removeView(`object` as View)
            }

        }

        init {
            mAirCafeInfo = airCafeInfo
            init()
        }
    }
}