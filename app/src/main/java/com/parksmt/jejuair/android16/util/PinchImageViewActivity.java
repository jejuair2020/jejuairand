package com.parksmt.jejuair.android16.util;

import android.content.Intent;
import android.os.Bundle;
import android.widget.ImageView;

import androidx.annotation.Nullable;

import com.bumptech.glide.Glide;
import com.parksmt.jejuair.android16.R;
import com.parksmt.jejuair.android16.base.BaseActivity;

/**
 * Created by seungjinoh on 2017. 4. 7..
 */

public class PinchImageViewActivity extends BaseActivity {
    private ImageView mPinchImageView;
    private int mImgResId;
    private String mImgUrl;

    @Override
    protected String getUiName() {
        return null;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.pinch_imageview_activity_layout);
        initView();
        Intent intent = getIntent();
        mImgResId = intent.getIntExtra("path", -1);
        mImgUrl = intent.getStringExtra("url");
        init();
    }

    private void initView() {
        mPinchImageView = (ImageView) findViewById(R.id.pinch_imageview);
    }

    private void init() {
        if (mImgResId != -1) {
            Glide.with(PinchImageViewActivity.this).load(mImgResId).into(mPinchImageView);
        } else if (mImgUrl != null) {
            Glide.with(PinchImageViewActivity.this).load(mImgUrl).into(mPinchImageView);
        }
    }
}
