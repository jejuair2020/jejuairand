package com.parksmt.jejuair.android16.view

import android.content.Context
import android.util.AttributeSet
import android.view.MotionEvent
import androidx.viewpager.widget.ViewPager

/**
 * 기본 view pager swipe 로의 page 이동을 막을 수 있음
 * Created by jun on 2017-03-03.
 */
class BaseViewPager : ViewPager {
    var isPagingEnabled = true
    var isCanScrollHorizontally = true

    constructor(context: Context?) : super(context!!) {}
    constructor(context: Context?, attrs: AttributeSet?) : super(
        context!!,
        attrs
    ) {
    }

    override fun onInterceptTouchEvent(ev: MotionEvent): Boolean {
        return isPagingEnabled && super.onInterceptTouchEvent(ev)
    }

    override fun onTouchEvent(ev: MotionEvent): Boolean {
        return isPagingEnabled && super.onTouchEvent(ev)
    }

    override fun canScrollHorizontally(direction: Int): Boolean {
        return isCanScrollHorizontally && super.canScrollHorizontally(direction)
    }

}