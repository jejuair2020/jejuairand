package com.parksmt.jejuair.android16.serviceinfo

import android.app.Activity
import android.content.Context
import android.view.View
import android.widget.TextView
import com.parksmt.jejuair.android16.R
import com.parksmt.jejuair.android16.util.Util
import com.parksmt.jejuair.android16.view.PopupController
import org.json.JSONObject

class RegulationBaggagePopup(context: Context) :
    PopupController(context, R.layout.regulation_baggage_popup), View.OnClickListener {
    var mLanguageJson: JSONObject = JSONObject()
    private var zoneInfoPopup: ZoneInfoPopup? =
        null

    override fun show() {
        super.show()
        //배상 불가 안내 스크린 태그 전송
//        TagUtil.sendScreenTag(RegulationBaggagePopup.this, "S-MUI-08-019");
    }

    override fun onClick(v: View) {
        when (v.id) {
            R.id.regulation_baggage_popup_close_btn -> dismiss()
            R.id.over_baggage_text3_1, R.id.over_baggage_text12_1 -> zoneInfoPopup!!.show()
        }
    }

    private fun init() { //무료수하물, 초과수하물 텍스트 추가.
        mLanguageJson = Util().addJSONObject(
            mLanguageJson,
            context,
            "serviceinfo/FreeBaggage.json"
        )
        mLanguageJson = Util().addJSONObject(
            mLanguageJson,
            context,
            "serviceinfo/OverBaggage.json"
        )
        zoneInfoPopup =
            ZoneInfoPopup(context)
    }

    private fun setText() {
        (mContentView.findViewById<View>(R.id.regulation_baggage_popup_title) as TextView).text =
            mLanguageJson!!.optString("FreeBaggageText_regulation")
        //무료수하물
        (mContentView.findViewById<View>(R.id.free_baggage_text1) as TextView).text =
            mLanguageJson!!.optString("FreeBaggageText1000")
        (mContentView.findViewById<View>(R.id.free_baggage_text2) as TextView).text =
            mLanguageJson!!.optString("FreeBaggageText1009")
        (mContentView.findViewById<View>(R.id.free_baggage_text3) as TextView).text =
            mLanguageJson!!.optString("FreeBaggageText1004")
        (mContentView.findViewById<View>(R.id.free_baggage_text4) as TextView).text =
            mLanguageJson!!.optString("FreeBaggageText1011")
        (mContentView.findViewById<View>(R.id.free_baggage_text5) as TextView).text =
            mLanguageJson!!.optString("FreeBaggageText1005")
        (mContentView.findViewById<View>(R.id.free_baggage_text6) as TextView).text =
            mLanguageJson!!.optString("FreeBaggageText1013")
        (mContentView.findViewById<View>(R.id.free_baggage_text7) as TextView).text =
            mLanguageJson!!.optString("FreeBaggageText1014")
        (mContentView.findViewById<View>(R.id.free_baggage_text8) as TextView).text =
            mLanguageJson!!.optString("FreeBaggageText1005")
        (mContentView.findViewById<View>(R.id.free_baggage_text9) as TextView).text =
            mLanguageJson!!.optString("FreeBaggageText1006")
        (mContentView.findViewById<View>(R.id.free_baggage_text10) as TextView).text =
            mLanguageJson!!.optString("FreeBaggageText1017")
        (mContentView.findViewById<View>(R.id.free_baggage_text11) as TextView).text =
            mLanguageJson!!.optString("FreeBaggageText1018")
        (mContentView.findViewById<View>(R.id.free_baggage_text12) as TextView).text =
            mLanguageJson!!.optString("FreeBaggageText1019")
        (mContentView.findViewById<View>(R.id.free_baggage_text13) as TextView).text =
            mLanguageJson!!.optString("FreeBaggageText1020")
        (mContentView.findViewById<View>(R.id.free_baggage_text14) as TextView).text =
            mLanguageJson!!.optString("FreeBaggageText1021")
        (mContentView.findViewById<View>(R.id.free_baggage_text15) as TextView).text =
            mLanguageJson!!.optString("FreeBaggageText1022")
        (mContentView.findViewById<View>(R.id.free_baggage_text16) as TextView).text =
            mLanguageJson!!.optString("FreeBaggageText1023")
        (mContentView.findViewById<View>(R.id.free_baggage_text17) as TextView).text =
            mLanguageJson!!.optString("FreeBaggageText1024")
        (mContentView.findViewById<View>(R.id.free_baggage_text18) as TextView).text =
            mLanguageJson!!.optString("FreeBaggageText1025")
        (mContentView.findViewById<View>(R.id.free_baggage_text19) as TextView).text =
            mLanguageJson!!.optString("FreeBaggageText1026")
        (mContentView.findViewById<View>(R.id.free_baggage_text20) as TextView).text =
            mLanguageJson!!.optString("FreeBaggageText1027")
        (mContentView.findViewById<View>(R.id.free_baggage_text21) as TextView).text =
            mLanguageJson!!.optString("FreeBaggageText1028")
        //초과수하물 over_baggage_text1
        (mContentView.findViewById<View>(R.id.over_baggage_text1) as TextView).text =
            mLanguageJson!!.optString("over_baggage_text1000")
        (mContentView.findViewById<View>(R.id.over_baggage_text2) as TextView).text =
            mLanguageJson!!.optString("over_baggage_text1013")
        (mContentView.findViewById<View>(R.id.over_baggage_text3) as TextView).text =
            mLanguageJson!!.optString("over_baggage_text1009")
        (mContentView.findViewById<View>(R.id.over_baggage_text3_1) as TextView).text =
            mLanguageJson!!.optString("over_baggage_text1014")
        (mContentView.findViewById<View>(R.id.over_baggage_text4) as TextView).text =
            mLanguageJson!!.optString("over_baggage_text1006")
        (mContentView.findViewById<View>(R.id.over_baggage_text5) as TextView).text =
            mLanguageJson!!.optString("over_baggage_text1037")
        (mContentView.findViewById<View>(R.id.over_baggage_text6) as TextView).text =
            mLanguageJson!!.optString("over_baggage_text1016")
        (mContentView.findViewById<View>(R.id.over_baggage_text7) as TextView).text =
            mLanguageJson!!.optString("over_baggage_text1017")
        (mContentView.findViewById<View>(R.id.over_baggage_text8) as TextView).text =
            mLanguageJson!!.optString("over_baggage_text1018")
        (mContentView.findViewById<View>(R.id.over_baggage_text9) as TextView).text =
            mLanguageJson!!.optString("over_baggage_text1019")
        (mContentView.findViewById<View>(R.id.over_baggage_text10) as TextView).text =
            mLanguageJson!!.optString("over_baggage_text1020")
        (mContentView.findViewById<View>(R.id.over_baggage_text11) as TextView).text =
            mLanguageJson!!.optString("over_baggage_text1021")
        (mContentView.findViewById<View>(R.id.over_baggage_text12) as TextView).text =
            mLanguageJson!!.optString("over_baggage_text1005")
        (mContentView.findViewById<View>(R.id.over_baggage_text12_1) as TextView).text =
            mLanguageJson!!.optString("over_baggage_text1014")
        (mContentView.findViewById<View>(R.id.over_baggage_text13) as TextView).text =
            mLanguageJson!!.optString("over_baggage_text1006")
        (mContentView.findViewById<View>(R.id.over_baggage_text14) as TextView).text =
            mLanguageJson!!.optString("over_baggage_text1015")
        (mContentView.findViewById<View>(R.id.over_baggage_text15) as TextView).text =
            mLanguageJson!!.optString("over_baggage_text1016")
        (mContentView.findViewById<View>(R.id.over_baggage_text16) as TextView).text =
            mLanguageJson!!.optString("over_baggage_text1022")
        (mContentView.findViewById<View>(R.id.over_baggage_text17) as TextView).text =
            mLanguageJson!!.optString("over_baggage_text1018")
        (mContentView.findViewById<View>(R.id.over_baggage_text18) as TextView).text =
            mLanguageJson!!.optString("over_baggage_text1023")
        (mContentView.findViewById<View>(R.id.over_baggage_text19) as TextView).text =
            mLanguageJson!!.optString("over_baggage_text1020")
        (mContentView.findViewById<View>(R.id.over_baggage_text20) as TextView).text =
            mLanguageJson!!.optString("over_baggage_text1024")
        (mContentView.findViewById<View>(R.id.over_baggage_text21) as TextView).text =
            mLanguageJson!!.optString("over_baggage_text1006")
        (mContentView.findViewById<View>(R.id.over_baggage_text22) as TextView).text =
            mLanguageJson!!.optString("over_baggage_text1011")
        (mContentView.findViewById<View>(R.id.over_baggage_text23) as TextView).text =
            mLanguageJson!!.optString("over_baggage_text1016")
        (mContentView.findViewById<View>(R.id.over_baggage_text24) as TextView).text =
            mLanguageJson!!.optString("over_baggage_text1017")
        (mContentView.findViewById<View>(R.id.over_baggage_text25) as TextView).text =
            mLanguageJson!!.optString("over_baggage_text1018")
        (mContentView.findViewById<View>(R.id.over_baggage_text26) as TextView).text =
            mLanguageJson!!.optString("over_baggage_text1019")
        (mContentView.findViewById<View>(R.id.over_baggage_text27) as TextView).text =
            mLanguageJson!!.optString("over_baggage_text1020")
        (mContentView.findViewById<View>(R.id.over_baggage_text28) as TextView).text =
            mLanguageJson!!.optString("over_baggage_text1021")
        (mContentView.findViewById<View>(R.id.over_baggage_text29) as TextView).text =
            mLanguageJson!!.optString("over_baggage_text1008")
        (mContentView.findViewById<View>(R.id.over_baggage_text30) as TextView).text =
            mLanguageJson!!.optString("over_baggage_text1009")
        (mContentView.findViewById<View>(R.id.over_baggage_text31) as TextView).text =
            mLanguageJson!!.optString("over_baggage_text1006")
        (mContentView.findViewById<View>(R.id.over_baggage_text32) as TextView).text =
            mLanguageJson!!.optString("over_baggage_text1010")
        (mContentView.findViewById<View>(R.id.over_baggage_text33) as TextView).text =
            mLanguageJson!!.optString("over_baggage_text1005")
        (mContentView.findViewById<View>(R.id.over_baggage_text34) as TextView).text =
            mLanguageJson!!.optString("over_baggage_text1006")
        (mContentView.findViewById<View>(R.id.over_baggage_text35) as TextView).text =
            mLanguageJson!!.optString("over_baggage_text1011")
        (mContentView.findViewById<View>(R.id.over_baggage_text36) as TextView).text =
            mLanguageJson!!.optString("over_baggage_text1012")
        val view1 =
            mContentView.findViewById<View>(R.id.free_baggage_include1) //성인/소아
        (view1.findViewById<View>(R.id.baggage_domestic_title_text) as TextView).text =
            mLanguageJson!!.optString("FreeBaggageText1010")
        (view1.findViewById<View>(R.id.baggage_type_text1) as TextView).text =
            mLanguageJson!!.optString("FreeBaggageText1011")
        (view1.findViewById<View>(R.id.baggage_max_count1) as TextView).text =
            mLanguageJson!!.optString("FreeBaggageText1033")
        (view1.findViewById<View>(R.id.baggage_type_weight_text1) as TextView).text =
            mLanguageJson!!.optString("Under23kg")
        (view1.findViewById<View>(R.id.baggage_type_text2) as TextView).text =
            mLanguageJson!!.optString("FreeBaggageText1014")
        (view1.findViewById<View>(R.id.baggage_max_count2) as TextView).text =
            mLanguageJson!!.optString("FreeBaggageText1033")
        (view1.findViewById<View>(R.id.baggage_type_weight_text2) as TextView).text =
            mLanguageJson!!.optString("Under23kg")
        (view1.findViewById<View>(R.id.baggage_type_text3) as TextView).text =
            mLanguageJson!!.optString("FreeBaggageText1017")
        (view1.findViewById<View>(R.id.baggage_max_count3) as TextView).text =
            mLanguageJson!!.optString("FreeBaggageText1033")
        (view1.findViewById<View>(R.id.baggage_type_weight_text3) as TextView).text =
            mLanguageJson!!.optString("Under23kg")
        val view2 =
            mContentView.findViewById<View>(R.id.free_baggage_include2) //유아
        (view2.findViewById<View>(R.id.baggage_domestic_title_text) as TextView).text =
            mLanguageJson!!.optString("FreeBaggageText1032")
        (view2.findViewById<View>(R.id.baggage_type_text1) as TextView).text =
            mLanguageJson!!.optString("FreeBaggageText1011")
        (view2.findViewById<View>(R.id.baggage_max_count1) as TextView).text =
            mLanguageJson!!.optString("FreeBaggageText1033")
        (view2.findViewById<View>(R.id.baggage_type_weight_text1) as TextView).text =
            mLanguageJson!!.optString("FreeBaggageText1034")
        (view2.findViewById<View>(R.id.baggage_type_text2) as TextView).text =
            mLanguageJson!!.optString("FreeBaggageText1014")
        (view2.findViewById<View>(R.id.baggage_max_count2) as TextView).text =
            mLanguageJson!!.optString("FreeBaggageText1033")
        (view2.findViewById<View>(R.id.baggage_type_weight_text2) as TextView).text =
            mLanguageJson!!.optString("FreeBaggageText1034")
        (view2.findViewById<View>(R.id.baggage_type_text3) as TextView).text =
            mLanguageJson!!.optString("FreeBaggageText1017")
        (view2.findViewById<View>(R.id.baggage_max_count3) as TextView).text =
            mLanguageJson!!.optString("FreeBaggageText1033")
        (view2.findViewById<View>(R.id.baggage_type_weight_text3) as TextView).text =
            mLanguageJson!!.optString("FreeBaggageText1034")
    }

    private fun initOnClick() {
        mContentView.findViewById<View>(R.id.regulation_baggage_popup_close_btn)
            .setOnClickListener(this)
        mContentView.findViewById<View>(R.id.over_baggage_text3_1)
            .setOnClickListener(this)
        mContentView.findViewById<View>(R.id.over_baggage_text12_1)
            .setOnClickListener(this)
    }

    private inner class ZoneInfoPopup internal constructor(context: Context) :
        PopupController(context, R.layout.over_baggage_zone_info_popup),
        View.OnClickListener {
        override fun show() {
            super.show()
            //Zone 구분 안내 스크린 태그 전송
            if (context is Activity) {
                //TagUtil.sendScreenTag(context as Activity, "S-MUI-08-012")
            }
        }

        override fun onClick(v: View) {
            when (v.id) {
                R.id.reservation_start_popup_close_btn -> dismiss()
            }
        }

        private fun init() {}
        private fun setText() {
            (mContentView.findViewById<View>(R.id.zone_info_text1) as TextView).text =
                mLanguageJson!!.optString("over_baggage_text1028")
            (mContentView.findViewById<View>(R.id.zone_info_text2) as TextView).text =
                mLanguageJson!!.optString("over_baggage_text1029")
            (mContentView.findViewById<View>(R.id.zone_info_text3) as TextView).text =
                mLanguageJson!!.optString("over_baggage_text1030")
            (mContentView.findViewById<View>(R.id.zone_info_text4) as TextView).text =
                mLanguageJson!!.optString("over_baggage_text1031")
            (mContentView.findViewById<View>(R.id.zone_info_text5) as TextView).text =
                mLanguageJson!!.optString("over_baggage_text1032")
            (mContentView.findViewById<View>(R.id.zone_info_text6) as TextView).text =
                mLanguageJson!!.optString("over_baggage_text1033")
            (mContentView.findViewById<View>(R.id.zone_info_text7) as TextView).text =
                mLanguageJson!!.optString("over_baggage_text1034")
            (mContentView.findViewById<View>(R.id.zone_info_text8) as TextView).text =
                mLanguageJson!!.optString("over_baggage_text1035")
            (mContentView.findViewById<View>(R.id.zone_info_text9) as TextView).text =
                mLanguageJson!!.optString("over_baggage_text1036")
        }

        private fun initOnClick() {
            mContentView.findViewById<View>(R.id.reservation_start_popup_close_btn)
                .setOnClickListener(this)
        }

        init {
            init()
            initOnClick()
            setText()
        }
    }

    init {
        init()
        initOnClick()
        setText()
    }
}