package com.parksmt.jejuair.android16.util

import android.Manifest
import android.annotation.SuppressLint
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Build
import android.os.Bundle
import android.os.PowerManager
import android.provider.Settings
import android.telephony.TelephonyManager
import android.util.TypedValue
import androidx.core.app.ActivityCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import com.parksmt.jejuair.android16.R
import com.parksmt.jejuair.android16.bottomtap.HomeActivity
import com.parksmt.jejuair.android16.common.*
import com.parksmt.jejuair.android16.enumdata.Language
import org.json.JSONException
import org.json.JSONObject
import java.io.*
import java.net.NetworkInterface
import java.util.*


class Util () {

    private val HASH_TYPE = "SHA-256"
    val NUM_HASHED_BYTES = 9
    val NUM_BASE64_CHAR = 11

    /**
     * 앱을 종료한다.
     *
     * @param context context
     */
    fun finishApplication(context: Context) {
        if (context !is HomeActivity) {
            val intent = Intent(context, HomeActivity::class.java)
            intent.putExtra(IntentKey.MAIN_REQUEST_CODE, IntentKey.EXIT)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_SINGLE_TOP)
            context.startActivity(intent)
        }
        (context as Activity).setResult(IntentKey.FINISH_RESULT_CODE)
        context.finish()
        // UserToken.finish() 차후 유저토큰 제작
        DLog().d("finishApplication")
    }

    fun restartApplication(context: Context) {
        val intent: Intent?
        if (context !is HomeActivity) {
            intent = Intent(context, HomeActivity::class.java)
            intent.putExtra(IntentKey.MAIN_REQUEST_CODE, IntentKey.RESTART)
        } else {
            (context as Activity).setResult(IntentKey.FINISH_RESULT_CODE)
            context.finish()
            intent = context.getPackageManager().getLaunchIntentForPackage(context.getPackageName())
        }
        intent!!.addFlags(
            Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_SINGLE_TOP or Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK
        )
        context.startActivity(intent)
        DLog().d("restartApplication")
    }

    /**
     * 현재 os 언어를 가져온다.
     *
     * @param context context
     * @return language
     */
    fun getSystemLanguage(context: Context): String {
        val configuration = context.resources.configuration
        val language: String
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            language = configuration.locales.get(0).language
        } else {

            language = configuration.locale.language
        }
        DLog().d("checkpoint systemLanguage : $language")
        return language
    }

    /**
     * 언어를 설정한다
     *
     * @param context  context
     * @param language language
     */
    fun setLanguage(context: Context, language: String) {
        Common.putString(SharedPrefKey.LANGUAGE, language, context)
        Common.commit(context)
        DLog().d("setLanguage language : $language")
    }


    fun addJSONObject(oriJsonObject: JSONObject, context: Context, path: String): JSONObject {
        return addJSONObject(oriJsonObject, context, path, false)
    }

    fun addJSONObject(
        oriJsonObject: JSONObject,
        context: Context,
        path: String,
        printLog: Boolean
    ): JSONObject {
        var oriJsonObject = oriJsonObject
        val elapseTime = System.currentTimeMillis()
        if (oriJsonObject == null) {
            oriJsonObject = JSONObject()
        }
        var addJsonObject = Util().loadLanguage(context, path)
        if (addJsonObject == null) {
            addJsonObject = JSONObject()
        }
        val iterator = addJsonObject!!.keys()
        while (iterator.hasNext()) {
            val key = iterator.next()
            try {
                oriJsonObject.put(key, addJsonObject!!.opt(key))
            } catch (e: JSONException) {
                DLog().e( "JSONException"+ e)
            }

        }
        if (printLog) {
            DLog().d("addJSONObject : " + (System.currentTimeMillis() - elapseTime) + "   path : " + path)
        }
        return oriJsonObject
    }


    /**
    * 현재 설정된 언어의 json 을 가져 온다
    *
    * @param context context
    * @param path    json 경로
    * @return JSONObject
    */
    fun loadLanguage(context: Context, path: String): JSONObject? {
        return loadJSONObject(context, path).optJSONObject("ko")
    }

    /**
    * json 을 가져 온다
    *
    * @param context context
    * @param path    json 경로
    * @return JSONObject */

    fun loadJSONObject(context: Context, path: String): JSONObject {
        return loadJSONObject(context, path, false)
    }

    /**
    * json 을 가져 온다
    *
    * @param context  context
    * @param path     json 경로
    * @param printLog log 표시 여부
    * @return JSONObject
    */
    fun loadJSONObject(context: Context, path: String, printLog: Boolean): JSONObject {
        var jsonObject = JSONObject()
        var bis: BufferedInputStream? = null
        try {
            val assetManager = context.resources.assets
            val inputStream= assetManager.open("json" + File.separator + path)

            bis = BufferedInputStream(inputStream)
            val buffer = bis.readBytes()
            bis.read(buffer)
            val string = String(buffer)
            jsonObject = JSONObject(string)
        } catch (e: IOException) {
            DLog().e( "IOException : " + e)
        } catch (e: JSONException) {
            DLog().e( "JSONException"+ e)
        } finally {
            if (bis != null) {
                try {
                    bis.close()
                } catch (e: IOException) {
                    DLog().e( "IOException"+ e)
                }

            }
        }
        return jsonObject
    }



    fun convertDpToPixels(dp: Float, context: Context): Int {
        try {
            return TypedValue.applyDimension(
                TypedValue.COMPLEX_UNIT_DIP,
                dp,
                context.resources.displayMetrics
            ).toInt()
        } catch (e: Exception) {
            return 0
        }

    }


    fun getLanguage(context: Context): String {
        return getCurrentLanguage(context).toString()
    }
    fun getCurrentLanguage(context: Context): Language {
        return Language.getLanguage(Common.getString(SharedPrefKey.LANGUAGE, Setting.DEFAULT_LANGUAGE,context).toString())
    }

    fun getFullLanguageUrl(context: Context): String {
        return Setting.BASE_URL + "/" + getCurrentLanguage(context).langUrl
    }

    fun getLanguageLocale(context: Context): String {
        return getCurrentLanguage(context).locale
    }

    fun getCurrentLanguageLocale(context: Context): Locale {
        return Locale(getCurrentLanguage(context).locale)
    }

    fun isKorean(context: Context): Boolean {
        return getCurrentLanguage(context) === Language.KOREAN
    }


    /**
    * 현재 설정된 언어의 url 을 가져온다
    *
    * @param context context
    * @return url
    */
    fun getLanguageUrl(context: Context): String {
        return "/" + getCurrentLanguage(context).langUrl
    }


    fun startFragment(
        fragmentManager: FragmentManager, containerViewId: Int,
        cls: Class<*>
    ) {
        startFragment(fragmentManager, containerViewId, cls, null)
    }

    fun startFragment(
        fragmentManager: FragmentManager, containerViewId: Int,
        cls: Class<*>,
        data: Bundle?
    ) {
        if (fragmentManager.findFragmentByTag(cls.simpleName) == null) {
            val fragment: Fragment
            try {
                fragment = cls.newInstance() as Fragment
                if (data != null) {
                    fragment.arguments = data
                }
                replaceFragment(fragmentManager, containerViewId, fragment, false)
            } catch (e: InstantiationException) {
                DLog().e( "InstantiationException"+ e)
            } catch (e: IllegalAccessException) {
                DLog().e( "IllegalAccessException"+ e)
            }

        }
    }

    fun startFragment(
        fragmentManager: FragmentManager, containerViewId: Int,
        fragment: Fragment
    ) {
        if (fragmentManager.findFragmentByTag(fragment.javaClass.simpleName) == null) {
            replaceFragment(fragmentManager, containerViewId, fragment, false)
        }
    }

    private fun replaceFragment(
        fragmentManager: FragmentManager, containerViewId: Int, fragment: Fragment,
        allowingStateLoss: Boolean
    ) {
        val fragmentTransaction = fragmentManager.beginTransaction()
        fragmentTransaction
            .setCustomAnimations(
                R.anim.fragment_slide_in_left,
                R.anim.fragment_slide_out_left,
                R.anim.fragment_slide_in_right,
                R.anim.fragment_slide_out_right
            )
        fragmentTransaction.replace(containerViewId, fragment, fragment.javaClass.simpleName)
        fragmentTransaction.addToBackStack(fragment.javaClass.simpleName)
        DLog().d("replaceFragment : " + fragment.javaClass.simpleName)
        if (allowingStateLoss) {
            fragmentTransaction.commitAllowingStateLoss()
        } else {
            fragmentTransaction.commit()
        }
    }

    fun startFragmentAllowingStateLoss(
        fragmentManager: FragmentManager, containerViewId: Int,
        cls: Class<*>
    ) {
        startFragmentAllowingStateLoss(fragmentManager, containerViewId, cls, null)
    }

    fun startFragmentAllowingStateLoss(
        fragmentManager: FragmentManager, containerViewId: Int, cls: Class<*>,
        data: Bundle?
    ) {
        if (fragmentManager.findFragmentByTag(cls.simpleName) == null) {
            val fragment: Fragment
            try {
                fragment = cls.newInstance() as Fragment
                if (data != null) {
                    fragment.arguments = data
                }
                replaceFragment(fragmentManager, containerViewId, fragment, true)
            } catch (e: InstantiationException) {
                DLog().e("InstantiationException" + e)
            } catch (e: IllegalAccessException) {
                DLog().e("IllegalAccessException" + e)
            }

        }
    }


    fun isAirplaneModeOn(context: Context?): Boolean {
        var result = false
        if (context != null) {
            if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN_MR1) {

                result = Settings.System.getInt(
                    context.contentResolver,
                    Settings.System.AIRPLANE_MODE_ON,
                    0
                ) != 0
            } else {
                result = Settings.Global.getInt(
                    context.contentResolver,
                    Settings.Global.AIRPLANE_MODE_ON,
                    0
                ) != 0
            }
        }
        return result
    }

    fun isShowing(): Boolean {
        DLog().d("isScreenOn : " + Common.isShowing)
        return Common.isShowing
    }

    fun isScreenOn(context: Context): Boolean {
        val pm = context.getSystemService(Context.POWER_SERVICE) as PowerManager
        val isScreenOn: Boolean
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT_WATCH) {
            isScreenOn = pm.isInteractive
        } else {
            isScreenOn = pm.isScreenOn
        }
        DLog().d("isScreenOn : $isScreenOn")
        return isScreenOn
    }

    /**
    * 기기의 UUID 값을 가져온다.
    *
    * @param context context
    * @return device Id
    */
    @SuppressLint("MissingPermission")
    fun getDeviceUUID(context: Context): String {
        val id = Common.getString(SharedPrefKey.DEVICE_ID, null!!, context)
        var uuid: UUID? = null
        if (id != null) {
            uuid = UUID.fromString(id!!)
        } else {
            val androidId =
                Settings.Secure.getString(context.contentResolver, Settings.Secure.ANDROID_ID)
            try {
                if ("9774d56d682e549c" != androidId) {
                    uuid = UUID.nameUUIDFromBytes(androidId.toByteArray(charset("utf8")))
                } else {
                    var deviceId: String? = null
                    if (ActivityCompat
                            .checkSelfPermission(
                                context,
                                Manifest.permission.READ_PHONE_STATE
                            ) == PackageManager.PERMISSION_GRANTED
                    ) {
                        deviceId =
                            (context.getSystemService(Context.TELEPHONY_SERVICE) as TelephonyManager).deviceId
                    }
                    uuid =
                        if (deviceId != null) UUID.nameUUIDFromBytes(deviceId.toByteArray(charset("utf8"))) else UUID.randomUUID()
                }
            } catch (e: UnsupportedEncodingException) {
                throw RuntimeException(e)
            }

            Common.putString(SharedPrefKey.DEVICE_ID, uuid!!.toString(), context)
            Common.commit(context)
        }
        return uuid!!.toString()
    }

    /**
    * Mac Address 를 가져 온다
    *
    * @return Mac Address
    */
    fun getMacAddress(): String {
        try {
            val all = Collections.list(NetworkInterface.getNetworkInterfaces())
            for (nif in all) {
                if (!nif.name.equals("wlan0", ignoreCase = true)) {
                    continue
                }
                val macBytes = nif.hardwareAddress ?: return ""
                val res1 = StringBuilder()
                for (b in macBytes) {
                    res1.append(String.format("%02X:", b))
                }

                if (res1.length > 0) {
                    res1.deleteCharAt(res1.length - 1)
                }
                return res1.toString()
            }
        } catch (ex: Exception) {
            DLog().e( "Exception"+ ex)
        }

        return ""
    }


    fun getLocalIpAddress(): String {
        try {
            val en = NetworkInterface.getNetworkInterfaces()
            while (en.hasMoreElements()) {
                val intf = en.nextElement()
                val enumIpAddr = intf.inetAddresses
                while (enumIpAddr.hasMoreElements()) {
                    val inetAddress = enumIpAddr.nextElement()
                    if (!inetAddress.isLoopbackAddress) {
                        return inetAddress.hostAddress
                    }
                }
            }
        } catch (ex: Exception) {
            DLog().e("IP Address"+ ex.toString())
            return "unKnown"
        }

        return "unKnown"
    }

    /**
    * 버젼 비교
    *
    * @param oldVersion 현재 앱의 version
    * @param newVersion 서버에서 전달받은 최신 version
    * @return newVersion 이 oldVersion 보다 높으면 true, 그렇지 않으면 false
    */
    fun compareVersion(oldVersion: String, newVersion: String): Boolean {
        return if (oldVersion.compareTo(newVersion) < 0) true else false
    }

    fun ageCalculator(birthDate: Calendar): Boolean {
        val date: Calendar
        date = Calendar.getInstance()
        //        birthDate = Calendar.getInstance();
        val age: Int

        if (date.get(Calendar.MONTH) > birthDate.get(Calendar.MONTH)) {
            // 생일이 지난경우
            age = date.get(Calendar.YEAR) - birthDate.get(Calendar.YEAR)
        } else if (date.get(Calendar.MONTH) == birthDate.get(Calendar.MONTH)) {
            if (date.get(Calendar.DAY_OF_MONTH) < birthDate.get(Calendar.DAY_OF_MONTH)) {
                // 생일이 아직 안 지난경우
                age = date.get(Calendar.YEAR) - birthDate.get(Calendar.YEAR) - 1
            } else {
                // 생일이 지난경우
                age = date.get(Calendar.YEAR) - birthDate.get(Calendar.YEAR)
            }
        } else {
            age = date.get(Calendar.YEAR) - birthDate.get(Calendar.YEAR) - 1
        }
        return if (age >= 14) true else false
    }

    fun getAge(birthDate: String): Int {
        val date = Integer.valueOf(birthDate)
        return Calendar.getInstance().get(Calendar.YEAR) - date + 1
    }

    /**
    * 탑승일을 기준으로 소아/유아/성인 구분 함
    *
    * @param flightDate 탑승일
    * @param birthDate  생일
    * @param isDomestic true : 국내선, false : 국제선
    * @return 0 : 성인, 1 : 소아, 2 : 유아
    */
    fun ageCalculator(flightDate: Calendar, birthDate: Calendar, isDomestic: Boolean): Int {
        var result = 0
        val age: Int
        if (flightDate.get(Calendar.MONTH) > birthDate.get(Calendar.MONTH)) {
            // 생일이 지난경우
            age = flightDate.get(Calendar.YEAR) - birthDate.get(Calendar.YEAR)
        } else if (flightDate.get(Calendar.MONTH) == birthDate.get(Calendar.MONTH)) {
            // 생일 전날까지만 소아,유아 적용
            if (flightDate.get(Calendar.DAY_OF_MONTH) < birthDate.get(Calendar.DAY_OF_MONTH)) {
                // 생일이 아직 안 지난경우
                age = flightDate.get(Calendar.YEAR) - birthDate.get(Calendar.YEAR) - 1
            } else {
                // 생일이 지난경우
                age = flightDate.get(Calendar.YEAR) - birthDate.get(Calendar.YEAR)
            }
        } else {
            age = flightDate.get(Calendar.YEAR) - birthDate.get(Calendar.YEAR) - 1
        }
        var limitYear = 12
        if (isDomestic) {
            limitYear = 13
        }
        val temp: String
        if (age < 2) {
            // 유아
            result = 2
        } else if (age >= 2 && age < limitYear) {
            // 소아
            result = 1
        } else {
            // 성인
            result = 0
        }
        return result
    }






/*




    *//**
     * 해당 asynctask의 실행 중 여부
     *
     * @param task AsyncTask
     * @return 실행중이면 true, 그렇지 않으면 false
     *//*
    fun isRunningAsyncTask(task: AsyncTask<*, *, *>): Boolean {
        return isRunningAsyncTask(task, true)
    }

    *//**
     * 해당 asynctask의 실행 중 여부
     *
     * @param task     AsyncTask
     * @param printLog 로그 출력 여부
     * @return 실행중이면 true, 그렇지 않으면 false
     *//*
    fun isRunningAsyncTask(task: AsyncTask<*, *, *>?, printLog: Boolean): Boolean {
        var flag = false
        if (task != null && task.status == AsyncTask.Status.RUNNING && !task.isCancelled) {
            flag = true
        }
        var name = ""
        if (task != null) {
            name = task.javaClass.name
        }
        if (printLog) {
            DLog().d("isRunningAsyncTask   $name   flag : $flag")
        }
        return flag
    }

    fun getStringByKey(
        context: Context,
        keyArrayResId: Int,
        valueArrayResId: Int,
        key: String,
        defaultValue: String
    ): String {
        var i = -1
        for (temp in context.resources.getStringArray(keyArrayResId)) {
            i++
            if (temp == key) {
                break
            }
        }
        return if (context.resources.getStringArray(valueArrayResId).size < i) {
            defaultValue
        } else {
            context.resources.getStringArray(valueArrayResId)[i]
        }
    }

    *//**
     * 유효한 이메일인지 체크 한다.
     *
     * @param email email
     * @return 유효한 이메일 형식이면 true, 그렇지 않으 면 false
     *//*
    fun isValidEmail(email: CharSequence?): Boolean {
        return email != null && android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches()
    }

    *//**
     * 유효한 휴대폰 번호인지 체크한다.
     *
     * @param phoneNumber phoneNumber
     * @return 유효한 휴대폰 번호이면 true, 그렇지 않으면 false
     *//*
    fun isValidPhoneNumber(phoneNumber: CharSequence?): Boolean {
        val flag: Boolean
        flag =
            !(phoneNumber == null || TextUtils.isEmpty(phoneNumber)) && android.util.Patterns.PHONE.matcher(
                phoneNumber
            ).matches()
        DLog().d("isValidPhoneNumber flag : $flag")
        return flag
    }

    *//**
     * GPS가 켜져있는지 확인한다.
     *
     * @param context context
     * @return GPS가 켜져있으면 true, 그렇지 않으면 false
     *//*
    fun checkGPS(context: Context): Boolean {
        val lm = context.getSystemService(Context.LOCATION_SERVICE) as LocationManager
        if (lm == null) {
            return false
        } else {
            DLog().d("GPS_PROVIDER : " + lm.isProviderEnabled(LocationManager.GPS_PROVIDER))
            Log.d(
                tag,
                "NETWORK_PROVIDER : " + lm.isProviderEnabled(LocationManager.NETWORK_PROVIDER)
            )
            return lm.isProviderEnabled(LocationManager.GPS_PROVIDER) && lm.isProviderEnabled(
                LocationManager.NETWORK_PROVIDER
            )
        }
    }


    *//**
     * web view 의 쿠키를 삭제한다
     *
     * @param context context
     *//*
    fun clearCookies(context: Context) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP_MR1) {
            CookieManager.getInstance().removeAllCookies(null)
            CookieManager.getInstance().flush()
        } else {
            val cookieSyncManager = CookieSyncManager.createInstance(context)
            cookieSyncManager.startSync()
            val cookieManager = CookieManager.getInstance()
            cookieManager.removeAllCookie()
            cookieManager.removeSessionCookie()
            cookieSyncManager.stopSync()
            cookieSyncManager.sync()
        }
        DLog().d("clearCookies")
    }

    fun setListViewHeightBasedOnChildren(listView: ListView) {
        val listAdapter = listView.adapter
            ?: // pre-condition
            return

        var totalHeight = 0

        if (listAdapter.count > 3) {
            val listItem = listAdapter.getView(0, null, listView)
            listItem.measure(0, 0)
            totalHeight = listItem.measuredHeight * 3
        } else {
            for (i in 0 until listAdapter.count) {
                val listItem = listAdapter.getView(i, null, listView)
                listItem.measure(0, 0)
                totalHeight += listItem.measuredHeight
            }
        }

        val params = listView.layoutParams
        params.height = totalHeight + listView.dividerHeight * (listAdapter.count - 1)
        listView.layoutParams = params
        listView.requestLayout()
    }

    *//**
     * 회원 id 유효성 체크<br></br>
     * 4~12자의 영문 소문자 또는 숫자
     *
     * @param userId 사용자 id
     * @return true : ok, false : invalid
     *//*
    fun checkUserId(userId: String): Boolean {
        val result = Pattern.compile("^[a-z0-9]{4,10}$").matcher(userId).matches()
        DLog().d("userId : $userId   result : $result")
        return result
    }

    *//**
     * password 유효성 체크<br></br>
     * 8자 이상 10자 이하의 영문과 숫자
     *
     * @param password password
     * @return 0 : OK, 1 : length < 8 , 2 : invalid password
     *//*
    fun checkPassword(password: String): Int {
        val result: Int
        if (StringUtil.isNull(password) || password.length < 8) {
            result = 1
        } else {
            //Log.i(tag,"checkpoint checkPassword 1 : "+((Pattern.compile("^[a-z0-9!@#$%^*()\\-_=+\\\\\\|\\[\\]{};:\\'\",.<>\\/?]{8,10}$")).matcher(password)).matches());  // 2017-10-25 비밀번호 check
            //Log.i(tag,"checkpoint checkPassword 2 : "+!((Pattern.compile("^[0-9]{8,10}$")).matcher(password)).matches());
            //Log.i(tag,"checkpoint checkPassword 3 : "+!((Pattern.compile("^[a-z]{8,10}$")).matcher(password)).matches());
            //result = (((Pattern.compile("^[a-z0-9]{8,10}$")).matcher(password)).matches() && !((Pattern.compile("^[0-9]{8,10}$"))
            //        .matcher(password)).matches() && !((Pattern.compile("^[a-z]{8,10}$")).matcher(password)).matches()) ? 0 : 2;
            result =
                if (Pattern.compile("^[a-zA-Z0-9!@#$%~&^*()\\-_=+\\\\\\|\\[\\]{};:\\'\",.<>\\/?]{8,10}$").matcher(
                        password
                    ).matches() && !Pattern.compile("^[0-9]{8,10}$")
                        .matcher(password).matches() && !Pattern.compile("^[a-z]{8,10}$").matcher(
                        password
                    ).matches()
                )
                    0
                else
                    2
        }
        DLog().d("password : $password   result : $result")
        return result
    }

    *//**
     * case별 password 유효성 체크
     * 회원가입 UI 변경, 2018.03.02, 박재성
     * (0) ok, (1) 8~10자리 X, (2) 숫자1개포함 X, (3) 영문소문자1개포함 X
     * (4) 1,2번 X
     * (5) 1,3번 X
     * (6) 2,3번 X
     * (7) 1,2,3번 X
     *//*
    fun checkPasswordCase(password: String): Int {
        var isLengthErr = false
        var isNumberErr = false
        var isLowerErr = false
        var result = 7
        if (StringUtil.isNull(password) || password.length < 8) {
            isLengthErr = true
        }

        for (i in 0 until password.length) {
            val c = password[i]
            if (Pattern.compile("^[0-9]$").matcher(c.toString()).matches()) {
                isNumberErr = false
                break
            } else {
                isNumberErr = true
            }
        }

        for (i in 0 until password.length) {
            val c = password[i]
            if (Pattern.compile("^[a-z]$").matcher(c.toString()).matches()) {
                isLowerErr = false
                break
            } else {
                isLowerErr = true
            }
        }

        //대문자 막는 로직 필요

        if (!isLengthErr && !isNumberErr && !isLowerErr) {
            result = 0
        } else if (isLengthErr && !isNumberErr && !isLowerErr) {
            result = 1
        } else if (!isLengthErr && isNumberErr && !isLowerErr) {
            result = 2
        } else if (!isLengthErr && !isNumberErr && isLowerErr) {
            result = 3
        } else if (isLengthErr && isNumberErr && !isLowerErr) {
            result = 4
        } else if (isLengthErr && !isNumberErr && isLowerErr) {
            result = 5
        } else if (!isLengthErr && isNumberErr && isLowerErr) {
            result = 6
        } else if (isLengthErr && isNumberErr && isLowerErr) {
            result = 7
        }
        return result
    }

    fun checkPasswordCaseSpecial(password: String): Int {
        var isLengthErr = false
        var isNumberErr = false
        var isLowerErr = false
        var isSpecial = false
        var result = 7
        if (StringUtil.isNull(password) || password.length < 8) {
            isLengthErr = true
        }

        for (i in 0 until password.length) {
            val c = password[i]
            if (Pattern.compile("^[0-9]$").matcher(c.toString()).matches()) {
                isNumberErr = false
                break
            } else {
                isNumberErr = true
            }
        }

        for (i in 0 until password.length) {
            val c = password[i]
            if (Pattern.compile("^[a-z]$").matcher(c.toString()).matches()) {
                isLowerErr = false
                break
            } else {
                isLowerErr = true
            }
        }

        for (i in 0 until password.length) {
            val c = password[i]
            if (Pattern.compile("^[a-z0-9]$").matcher(c.toString()).matches()) {
                isSpecial = false
            } else {
                isSpecial = true
                break
            }
        }

        if (isSpecial) {
            result = 8
        } else {
            if (!isLengthErr && !isNumberErr && !isLowerErr) {
                result = 0
            } else if (isLengthErr && !isNumberErr && !isLowerErr) {
                result = 1
            } else if (!isLengthErr && isNumberErr && !isLowerErr) {
                result = 2
            } else if (!isLengthErr && !isNumberErr && isLowerErr) {
                result = 3
            } else if (isLengthErr && isNumberErr && !isLowerErr) {
                result = 4
            } else if (isLengthErr && !isNumberErr && isLowerErr) {
                result = 5
            } else if (!isLengthErr && isNumberErr && isLowerErr) {
                result = 6
            } else if (isLengthErr && isNumberErr && isLowerErr) {
                result = 7
            }
        }
        DLog().d("checkPasswordCaseSpecial result : $result")
        return result
    }

    *//**
     * 앱 업데이트를 위해 appUpdateUrl 로 이동한다. 실패 시 web 으로 이동
     * 앱을 종류 시킴
     *
     * @param activity context
     *//*
    fun updateApplication(activity: Activity) {
        updateApplication(activity, true)
    }

    *//**
     * * 앱 업데이트를 위해 appUpdateUrl 로 이동한다. 실패 시 web 으로 이동
     *
     * @param activity context
     * @param finish   true :  앱종료, false : 종료시키지 않음
     *//*
    fun updateApplication(activity: Activity, finish: Boolean) {
        val intent = Intent()
        intent.action = Intent.ACTION_VIEW
        try {
            intent.data = Uri.parse("market://details?id=" + activity.packageName)
            activity.startActivityForResult(intent, IntentKey().UPDATE_RESULT_CODE)
        } catch (e: ActivityNotFoundException) {
            val webUrl = "https://play.google.com/store/apps/details?id=" + activity.packageName
            intent.data = Uri.parse(webUrl)
            activity.startActivityForResult(intent, IntentKey().UPDATE_RESULT_CODE)
            DLog().e( "ActivityNotFoundException  webUrl : $webUrl"+ e)
        }

        if (finish) {
            Util.finishApplication(activity)
        }
    }



    fun startYoutubeVideo(mContext: Context, url: String) {
        var url = url
        try {
            if (!url.startsWith("http")) {
                url = "http://$url"
            }
            val intent = Intent(Intent.ACTION_VIEW, Uri.parse(url))
            mContext.startActivity(intent)
        } catch (ex: ActivityNotFoundException) {
            DLog().d("ActivityNotFoundException"+ ex)
        }

    }

    fun changeNumberFormat(num: String): String? {
        try {
            val value = java.lang.Long.parseLong(num.replace(",", ""))
            val format = DecimalFormat("###,###")//콤마
            format.format(value)
            return format.format(value)
        } catch (e: Exception) {
            return num
        }

    }



    *//**
     * 네트워크에 연결되어있는지 나타낸다.
     *
     * @param context context
     * @return 네트워크에 연결되어있거나 연결중이면 true, 그렇지 않으면 false
     *//*
    fun isOnline(context: Context): Boolean {
        val cm = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
            ?: return false
        val netInfo = cm.activeNetworkInfo
        return netInfo != null && netInfo.isConnectedOrConnecting
    }

    fun checkWifiNetwork(context: Context): Boolean {
        val cm = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val activeNetwork = cm.activeNetworkInfo
        if (activeNetwork != null) {
            if (activeNetwork.type == ConnectivityManager.TYPE_WIFI) {
                return true
            } else if (activeNetwork.type == ConnectivityManager.TYPE_MOBILE) {
                return false
            }
        }
        return false
    }


    fun getIntroduceDetail(context: Context, name: String, color: String): View? {
        val v = LayoutInflater.from(context).inflate(R.layout.refresh_point_detail_list, null)
        try {
            val word = v.findViewById(R.id.word) as TextView
            word.text = name
            if (color != "Html") {
                if (color != "Default") {
                    word.setTextColor(Color.parseColor(color))
                }
                word.text = name
            } else if (color == "Html") {
                if (android.os.Build.VERSION.SDK_INT >= 24) {
                    word.text = Html.fromHtml(name, Html.FROM_HTML_MODE_COMPACT)
                } else {
                    word.text = Html.fromHtml(name)
                }
            }
        } catch (e: Throwable) {
            DLog().e( "Throwable"+ e)
        }

        return v
    }

    fun getIntroduceDetail(context: Context, text: SpannableStringBuilder): View? {
        val v = LayoutInflater.from(context).inflate(R.layout.refresh_point_detail_list, null)
        try {
            val word = v.findViewById(R.id.word) as TextView
            word.text = text
        } catch (e: Throwable) {
            DLog().e( "Throwable"+ e)
        }

        return v
    }



    *//**
     * 해당 기기의 DPI를 리턴함
     *
     * @param context
     * @return
     *//*
    fun returnDPI(context: Activity): Int {
        val metrics = DisplayMetrics()
        context.windowManager.defaultDisplay.getMetrics(metrics)

        return metrics.densityDpi
    }

    *//**
     * 해당 웹뷰의 전체 화면을 스크린샷으로 저장
     *
     * @param mWebView  캡쳐하려는 WebView
     * @param mFilePath 캡쳐 파일을 저장할 경로
     * @param mFileName 캡쳐한 파일명
     * @return
     *//*
    fun webviewFullCapture(mWebView: WebView, mFilePath: String, mFileName: String): Boolean {
        val mAllCapture = WebViewAllCapture()
        return mAllCapture.onWebViewAllCapture(mWebView, mFilePath, mFileName)
    }

    *//**
     * 버젼 비교
     *
     * @param oldVersion 비교될 version
     * @param newVersion 비교할 version
     * @return newVersion 이 oldVersion 보다 높으면 true, 그렇지 않으면 false
     * 2018.11.09 compareVersionForIntro2, compareVersionForIntro3 추가 by jspark8
     *//*
    fun compareVersionForIntro(oldVersion: String, newVersion: String): Boolean {
        var update = false

        if (StringUtil.isNotNull(oldVersion) && StringUtil.isNotNull(newVersion)) {
            val appVersion =
                oldVersion.split("\\.".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
            val latestVersion =
                newVersion.split("\\.".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
            if (appVersion.size == latestVersion.size) {
                if (java.lang.Long.valueOf(appVersion[0]) < java.lang.Long.valueOf(latestVersion[0])) {
                } else if (java.lang.Long.valueOf(appVersion[0]) >= java.lang.Long.valueOf(
                        latestVersion[0]
                    )
                ) {
                    if (java.lang.Long.valueOf(appVersion[1]) < java.lang.Long.valueOf(latestVersion[1])) {
                    } else if (java.lang.Long.valueOf(appVersion[1]) > java.lang.Long.valueOf(
                            latestVersion[1]
                        )
                    ) {
                        return false
                    } else if (java.lang.Long.valueOf(appVersion[1]) === java.lang.Long.valueOf(
                            latestVersion[1]
                        )
                    ) {
                        if (java.lang.Long.valueOf(appVersion[2]) < java.lang.Long.valueOf(
                                latestVersion[2]
                            )
                        ) {
                            return true
                        }
                    }
                }
            } else {
                update = true
            }
        }
        DLog().d("oldVersion : $oldVersion   newVersion : $newVersion   update : $update")
        return update
    }

    fun compareVersionForIntro2(
        oldVersion: String,
        newVersion: String,
        isForceUpdate: Boolean
    ): Int {
        if (oldVersion.compareTo(newVersion) < 0 && isForceUpdate) {
            *//** 업데이트가 필요하고 강업 요청이 왔다면 FORCE_UPDATE  *//*
            return Intro.FORCE_UPDATE
        }

        var returnData = Intro.COMPLETE_UPDATE //업데이트 완료가 기본상태값으로.
        if (StringUtil.isNotNull(oldVersion) && StringUtil.isNotNull(newVersion)) {
            val appVersion =
                oldVersion.split("\\.".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
            val latestVersion =
                newVersion.split("\\.".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
            if (java.lang.Long.valueOf(appVersion[0]) < java.lang.Long.valueOf(latestVersion[0])) {
                returnData = Intro.FORCE_UPDATE
            } else {
                if (java.lang.Long.valueOf(appVersion[1]) < java.lang.Long.valueOf(latestVersion[1])) {
                    returnData = Intro.POPUP_UPDATE
                } else if (java.lang.Long.valueOf(appVersion[2]) < java.lang.Long.valueOf(
                        latestVersion[2]
                    )
                ) {
                    returnData = Intro.NONPOP_UPDATE
                }
            }
        }
        DLog().d("oldVersion : $oldVersion   newVersion : $newVersion   update : $returnData")
        return returnData
    }

    fun compareVersionForIntro3(oldVersion: String, newVersion: String): Boolean {
        val update = false
        if (StringUtil.isNotNull(oldVersion) && StringUtil.isNotNull(newVersion)) {
            val appVersion =
                oldVersion.split("\\.".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
            val latestVersion =
                newVersion.split("\\.".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
            if (java.lang.Long.valueOf(appVersion[0]) < java.lang.Long.valueOf(latestVersion[0])) {
                return true
            } else {
                if (java.lang.Long.valueOf(appVersion[1]) < java.lang.Long.valueOf(latestVersion[1])) {
                    return true
                } else if (java.lang.Long.valueOf(appVersion[1]) > java.lang.Long.valueOf(
                        latestVersion[1]
                    )
                ) {
                    return false
                } else if (java.lang.Long.valueOf(appVersion[1]) === java.lang.Long.valueOf(
                        latestVersion[1]
                    )
                ) {
                    if (java.lang.Long.valueOf(appVersion[2]) < java.lang.Long.valueOf(latestVersion[2])) {
                        return true
                    }
                }
            }
        }
        DLog().d("oldVersion : $oldVersion   newVersion : $newVersion   update : $update")
        return update
    }




    fun checkLocation(context: Context): Boolean {
        return Util.checkGpsSelfPermission(context) && Util.checkGPS(context) && UserSetting().getInstance(
            context
        ).isLocationAgree()
    }

    fun checkGpsSelfPermission(context: Context): Boolean {
        var permissionCheck = true
        *//**
         * TODO 2018.03.06 yschoi [ITSM-IM00119124] Gps 관련 crash 처리 요청 add to try~catch NullPointerException
         *//*
        try {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                permissionCheck = ActivityCompat
                    .checkSelfPermission(
                        context,
                        ACCESS_FINE_LOCATION
                    ) == PackageManager.PERMISSION_GRANTED && ActivityCompat
                    .checkSelfPermission(
                        context,
                        ACCESS_COARSE_LOCATION
                    ) == PackageManager.PERMISSION_GRANTED
            }
        } catch (e: NullPointerException) {
            return false
        } catch (e: Exception) {
            return false
        }

        DLog().d("permissionCheck : $permissionCheck")
        return permissionCheck
    }

    fun checkGpsPermission(activity: Activity, requestCode: Int): Boolean {
        //위치서비스 켜기 스크린 태그 전송
        TagUtil.sendScreenTag(activity, "S-MUI-01-007")
        var permission = UserSetting().getInstance(activity).isLocationAgree()
        if (!permission) {
            val dialog = CommonAlertDialog(activity)
            dialog.setTitle(R.string.gps_service_on_title)
            dialog.setMessage(R.string.gps_service_on_message)
            dialog.setNegativeButton(R.string.alert_cancel, null)
            dialog.setPositiveButton(R.string.gps_service_change, View.OnClickListener {
                val intent = Intent()
                intent.setClass(activity, AppSettingEnum.getCls())
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_SINGLE_TOP)
                activity.startActivityForResult(intent, requestCode)
            })
            dialog.setCancelable(true)
            dialog.show()
            return permission
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            permission = checkGpsSelfPermission(activity)
            if (!permission) {
                val shouldShowRequestPermissionRationale =
                    ActivityCompat.shouldShowRequestPermissionRationale(
                        activity,
                        ACCESS_FINE_LOCATION
                    )
                Log.d(
                    tag,
                    "shouldShowRequestPermissionRationale : $shouldShowRequestPermissionRationale"
                )
                if (!shouldShowRequestPermissionRationale) {
                    ActivityCompat.requestPermissions(
                        activity,
                        arrayOf(ACCESS_FINE_LOCATION),
                        requestCode
                    )
                } else {
                    val dialog = CommonAlertDialog(activity)
                    dialog.setTitle(R.string.gps_service_on_title)
                    dialog.setMessage(R.string.gps_permission_request_message)
                    dialog.setNegativeButton(R.string.permission_no, null)
                    dialog.setPositiveButton(R.string.permission_ok, View.OnClickListener {
                        val intent = Intent()
                        intent.action = Settings.ACTION_APPLICATION_DETAILS_SETTINGS
                        val uri = Uri.fromParts("package", activity.packageName, null)
                        intent.data = uri
                        activity.startActivityForResult(intent, requestCode)
                    })
                    dialog.setCancelable(true)
                    dialog.show()
                }
                return permission
            }
        }
        permission = Util.checkGPS(activity)
        if (!Util.checkGPS(activity)) {
            val dialog = CommonAlertDialog(activity)
            dialog.setTitle(R.string.gps_service_on_title)
            dialog.setMessage(R.string.gps_permission_request_message)
            dialog.setNegativeButton(R.string.permission_no, null)
            dialog.setPositiveButton(R.string.permission_ok, View.OnClickListener {
                val intent = Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS)
                activity.startActivityForResult(intent, requestCode)
            })
            dialog.setCancelable(true)
            dialog.show()
        }
        return permission
    }

    fun checkGpsJejuTravelPermission(activity: Activity, requestCode: Int): Boolean {
        var permission = UserSetting().getInstance(activity).isLocationAgree()
        if (!permission) {
            val dialog = CommonAlertDialog(activity)
            dialog.setTitle(R.string.gps_service_on_title)
            dialog.setMessage(R.string.gps_service_on_message)
            dialog.setNegativeButton(R.string.alert_cancel, null)
            dialog.setPositiveButton(R.string.gps_service_change, View.OnClickListener {
                val intent = Intent()
                intent.setClass(activity, AppSettingEnum.getCls())
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_SINGLE_TOP)
                activity.startActivityForResult(intent, requestCode)
            })
            dialog.setCancelable(true)
            dialog.show()
            return permission
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            permission = checkGpsSelfPermission(activity)
            if (!permission) {
                val shouldShowRequestPermissionRationale =
                    ActivityCompat.shouldShowRequestPermissionRationale(
                        activity,
                        ACCESS_FINE_LOCATION
                    )
                Log.d(
                    tag,
                    "shouldShowRequestPermissionRationale : $shouldShowRequestPermissionRationale"
                )
                if (!shouldShowRequestPermissionRationale) {
                    ActivityCompat.requestPermissions(
                        activity,
                        arrayOf(ACCESS_FINE_LOCATION),
                        requestCode
                    )
                } else {
                    val dialog = CommonAlertDialog(activity)
                    dialog.setTitle(R.string.gps_service_on_title)
                    dialog.setMessage(R.string.gps_permission_jejutravel_request)
                    dialog.setNegativeButton(R.string.permission_no, null)
                    dialog.setPositiveButton(R.string.permission_ok, View.OnClickListener {
                        val intent = Intent()
                        intent.action = Settings.ACTION_APPLICATION_DETAILS_SETTINGS
                        val uri = Uri.fromParts("package", activity.packageName, null)
                        intent.data = uri
                        activity.startActivityForResult(intent, requestCode)
                    })
                    dialog.setCancelable(true)
                    dialog.show()
                }
                return permission
            }
        }
        permission = Util.checkGPS(activity)
        if (!Util.checkGPS(activity)) {
            val dialog = CommonAlertDialog(activity)
            dialog.setTitle(R.string.gps_service_on_title)
            dialog.setMessage(R.string.gps_permission_jejutravel_request)
            dialog.setNegativeButton(R.string.permission_no, null)
            dialog.setPositiveButton(R.string.permission_ok, View.OnClickListener {
                val intent = Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS)
                activity.startActivityForResult(intent, requestCode)
            })
            dialog.setCancelable(true)
            dialog.show()
        }
        return permission
    }

    @Throws(JSONException::class)
    fun makeUriToJSONObject(uri: Uri): JSONObject {
        val obj = JSONObject()
        val strings = uri.queryParameterNames
        for (name in strings) {
            obj.put(name, uri.getQueryParameter(name))
        }
        return obj
    }

    fun makeLinkIntent(data: JSONObject): Intent {
        val intent = Intent()
        val activityList = ActivityList.getActivityList(data.optString("sid"))
        intent.putExtra(IntentKey().UI_NAME, activityList.getUiName())
        when (activityList) {
            EventDetailEnum, NewsDetailEnum, MyNotificationEnum -> {
                intent.putExtra(IntentKey().EVENT_ID, data.optString("msgIdx"))
                intent.putExtra(IntentKey().IS_EVENT, ActivityList.EventDetailEnum === activityList)
            }
            MainEnum -> {
                intent.putExtra(IntentKey().GO_MAIN_PAGE, true)
                val mainPage = Main.MainPage.getMainPage(data.optInt("pageid", 0))
                intent.putExtra(IntentKey().MAIN_PAGE_POSITION, mainPage)
                if (mainPage === Main.MainPage.RESERVATION) {
                    intent.putExtra(
                        IntentKey().REQUEST_CODE,
                        IntentKey().RESERVATION_SETTING_REQUEST_CODE
                    )
                    intent.putExtra(IntentKey().RESERVATION_SETTING_DATA, data.toString())
                }
            }
        }
        return intent
    }

    fun makeLinkIntent(context: Context, data: JSONObject): Intent {
        val intent = makeLinkIntent(data)
        val activityList = ActivityList.getActivityList(data.optString("sid"))
        if (activityList !== ActivityList.NOT_MATCHED) {
            intent.setClass(context, Main::class.java!!)
            intent.putExtra(IntentKey().MAIN_GO_SUB_PAGE, activityList.getUiName())
        } else {
            intent.setClass(context, Intro::class.java)
        }
        return intent
    }

    fun getScreenHeight(activity: Activity): Int {
        try {
            val display = activity.windowManager.defaultDisplay
            val size = Point()
            display.getSize(size)
            return size.y
        } catch (e: Exception) {
            DLog().e(activity.javaClass.name+ e.message)
        }

        return 0
    }

    fun getFooterHeight(activity: Activity, height: Int): Int {
        return getScreenHeight(activity) - height
    }

    fun isSameDay(cal1: Calendar?, cal2: Calendar?): Boolean {
        return if (cal1 == null || cal2 == null) {
            false
        } else cal1.get(Calendar.ERA) == cal2.get(Calendar.ERA)
                && cal1.get(Calendar.YEAR) == cal2.get(Calendar.YEAR)
                && cal1.get(Calendar.DAY_OF_YEAR) == cal2.get(Calendar.DAY_OF_YEAR)
    }



    *//**
     * get App Signatures
     *//*
    fun getAppSignatures(context: Context): ArrayList<String> {
        val appCodes = ArrayList<String>()

        try {
            // Get all package signatures for the current package
            val packageName = context.packageName
            val packageManager = context.packageManager
            val signatures = packageManager.getPackageInfo(
                packageName,
                PackageManager.GET_SIGNATURES
            ).signatures

            // For each signature create a compatible hash
            for (signature in signatures) {
                val hash = getHash(packageName, signature.toCharsString())
                if (hash != null) {
                    appCodes.add(String.format("%s", hash))
                }
                DLog().d(String.format("이 값을 SMS 뒤에 써서 보내주면 됩니다 : %s", hash))
            }
        } catch (e: PackageManager.NameNotFoundException) {
            DLog().d("Unable to find package to obtain hash. : $e")
        }

        return appCodes
    }

    private fun getHash(packageName: String, signature: String): String? {
        val appInfo = "$packageName $signature"
        try {
            val messageDigest = MessageDigest.getInstance(HASH_TYPE)
            // minSdkVersion이 19이상이면 체크 안해도 됨
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                messageDigest.update(appInfo.toByteArray(StandardCharsets.UTF_8))
            }
            var hashSignature = messageDigest.digest()

            // truncated into NUM_HASHED_BYTES
            hashSignature = Arrays.copyOfRange(hashSignature, 0, NUM_HASHED_BYTES)
            // encode into Base64
            var base64Hash =
                Base64.encodeToString(hashSignature, Base64.NO_PADDING or Base64.NO_WRAP)
            base64Hash = base64Hash.substring(0, NUM_BASE64_CHAR)

            DLog().d(String.format("\nPackage : %s\nHash : %s", packageName, base64Hash))
            return base64Hash
        } catch (e: NoSuchAlgorithmException) {
            DLog().d("hash:NoSuchAlgorithm : $e")
        }

        return null
    }
*/

}