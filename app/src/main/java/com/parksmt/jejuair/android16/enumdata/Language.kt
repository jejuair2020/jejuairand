package com.parksmt.jejuair.android16.enumdata

import com.parksmt.jejuair.android16.util.StringUtil

enum class Language constructor(var code: String,
                                var langUrl: String,
                                var displayString: String,
                                var locale: String,
                                var countryListCode: String,
                                var phoneCode: String,
                                var netthru_p1: String,
                                var netthru_p2: String) {

    /**
     * 한국
     */
    KOREAN("KR", "kr", "한국사이트", "ko", "KR", "82", "kr", "KR"),
    /**
     * 일본
     */
    JAPANESE("JP", "jp", "日本サイト", "ja", "JP", "81", "jp", "JP"),
    /**
     * 중국
     */
    CHINESE("CN", "cn", "中国网站", "zh-rCN", "CN", "86", "cn", "CN"),
    /**
     * 홍콩
     */
    HONGKONG("HK", "ho", "香港網站", "zh-rTW", "HK", "852", "hk", "HK"),
    /**
     * 대만
     */
    TAIWAN("HK", "tw", "台灣網站", "zh-rTW", "TW", "886", "tw", "TW"),
    /**
     * 마카오
     */
    MACAO("HK", "mf", "澳門網站", "zh-rTW", "MO", "853", "hk", "MO"),
    /**
     * 태국
     */
    THAILAND("TH", "th", "เว็บไซต์ไทย", "th", "TH", "66", "th", "TH"),
    /**
     * 필리핀
     */
    PHILIPPINES("EN", "ph", "Philippines Website", "en", "PH", "63", "en", "PH"),
    /**
     * 영어
     */
    ENGLISH("EN", "en", "U.S. Website", "en", "US", "1", "en", "EN"),
    /**
     * 말레이시아
     */
    MALAYSIA("EN", "my", "Malaysian Website", "en", "MY", "60", "en", "MY"),
    /**
     * 라오스
     */
    LAOS("EN", "la", "Laotian Website", "en", "LA", "856", "en", "LA"),
    /**
     * 베트남
     */
    VIETNAMESE("VN", "vn", "Trang web Việt Nam", "vi", "VN", "84", "vn", "VN"),
    /**
     * 러시아
     */
    RUSSIA("RU", "ru", "Сайт для России", "ru", "RU", "7", "ru", "RU"),
    /**
     * 싱가폴
     */
    SINGAPORE("EN", "sg", "Singapore Website", "en", "SG", "65", "en", "SG");


    companion object {

        fun getLanguage(value: String): Language {
            var state = KOREAN
            if (StringUtil().isNotNull(value)) {
                for (temp in Language.values()) {
                    if (temp.name == value || temp.name == value.toUpperCase() || temp.equals(value)) {
                        state = temp
                        break
                    }
                }
            }
            //Log.d("Language", "value : " + value + "  name : " + state.name());
            return state
        }
    }


}