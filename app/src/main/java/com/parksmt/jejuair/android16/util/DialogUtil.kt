package com.parksmt.jejuair.android16.util

import android.content.Context
import android.view.View
import com.parksmt.jejuair.android16.R
import com.parksmt.jejuair.android16.common.DLog
import com.parksmt.jejuair.android16.common.Setting
import com.parksmt.jejuair.android16.view.CommonAlertDialog

/**
 * Dialog 관련 함수를 모아논 class
 *
 * @author ui-jun
 */
class DialogUtil {
    /**
     * 에러 표시용 alert dialog를 띄운다. 확인 버튼을 누르면 앱이 종료된다.<br></br>
     *
     * @param context context
     * @param msg     표시할 메세지 resource Id
     */
    fun showErrorDialog(context: Context, msg: Int) {
        showErrorDialog(context, context.getString(msg))
    }

    /**
     * 에러 표시용 alert dialog를 띄운다. 확인 버튼을 누르면 앱이 종료된다.<br></br>
     *
     * @param context context
     * @param msg     표시할 메세지
     */
    fun showErrorDialog(context: Context, msg: String) {
        val tail = "\n" + DLog().getLineNumber()
        val log = "Class : " + context.javaClass.simpleName + "\n" + msg + tail
        val dialog = CommonAlertDialog(context)
        dialog.setTitle(R.string.alert_alert)
        dialog.setMessage(msg)
        dialog.setPositiveButton(
            R.string.alert_confirm,
            View.OnClickListener { Util().finishApplication(context) })
        dialog.setCancelable(false)
        DLog().e(log)
        dialog.show()
    }

    /**
     * 메세지만 있는 default alert dialog를 띄운다. 확인버튼을 누르면 dialog가 닫힌다
     *
     * @param context context
     * @param msg     표시할 메세지
     */
    fun showCommonAlertDialog(
        context: Context,
        msg: String?
    ): CommonAlertDialog {
        return showCommonAlertDialog(context, msg, null)
    }

    /**
     * 메세지만 있는 default alert dialog를 띄운다. 확인버튼을 누르면 dialog가 닫힌다
     *
     * @param context context
     * @param msg     표시할 메세지 resource Id
     */
    fun showCommonAlertDialog(context: Context, msg: Int): CommonAlertDialog {
        return showCommonAlertDialog(
            context,
            context.resources.getString(msg),
            null
        )
    }

    /**
     * alert dialog를 띄운다.
     *
     * @param context  context
     * @param msg      표시할 메세지
     * @param listener OnClickListener
     * @return dialog
     */
    fun showCommonAlertDialog(
        context: Context,
        msg: Int,
        listener: View.OnClickListener?
    ): CommonAlertDialog {
        return showCommonAlertDialog(
            context,
            context.resources.getString(msg),
            listener
        )
    }

    /**
     * alert dialog를 띄운다.
     *
     * @param context  context
     * @param msg      표시할 메세지
     * @param listener OnClickListener
     * @return dialog
     */
    fun showCommonAlertDialog(
        context: Context,
        msg: String?,
        listener: View.OnClickListener?
    ): CommonAlertDialog {
        val dialog = CommonAlertDialog(context)
        dialog.setTitle(R.string.alert_alert)
        dialog.setMessage(msg)
        dialog.setPositiveButton(R.string.alert_confirm, listener)
        dialog.show()
        return dialog
    }

    fun showCommonAlertDialog(
        context: Context,
        title: String?,
        msg: String?,
        listener: View.OnClickListener?
    ): CommonAlertDialog {
        val dialog = CommonAlertDialog(context)
        dialog.setTitle(title)
        dialog.setMessage(msg)
        dialog.setPositiveButton(R.string.alert_confirm, listener)
        dialog.setCloseButtonListener(View.OnClickListener { dialog.cancel() })
        dialog.show()
        return dialog
    }

    fun showCommonAlertDialog(
        context: Context,
        title: String?,
        msg: String?,
        positiveBtn: View.OnClickListener?,
        closeBtn: View.OnClickListener?
    ): CommonAlertDialog {
        val dialog = CommonAlertDialog(context)
        dialog.setTitle(title)
        dialog.setMessage(msg)
        dialog.setPositiveButton(R.string.alert_confirm, positiveBtn)
        dialog.setCloseButtonListener(closeBtn)
        dialog.show()
        return dialog
    }

    /**
     * 메세지만 있는 default alert dialog를 띄운다. 확인버튼을 누르면 dialog가 닫힌다
     *
     * @param context context
     * @param msg     표시할 메세지
     * @param code    error code
     * @return dialog
     */
    fun showCommonAlertDialog(
        context: Context,
        msg: String?,
        code: Int
    ): CommonAlertDialog {
        var msg = msg
        if (Setting.IS_TEST) {
            msg += "  code : $code"
        }
        return showCommonAlertDialog(context, msg)
    }

    /**
     * 확인 버튼만 있는 default CommonAlertDialog를 생성한다. 확인 버튼을 누르면 dailog가 닫힌다.
     *
     * @param context context
     * @param msg     표시할 메세지 resource Id
     * @return dialog
     */
    fun getCommonAlertDialog(context: Context, msg: Int): CommonAlertDialog {
        return getCommonAlertDialog(context, context.resources.getString(msg))
    }

    /**
     * 확인 버튼만 있는 default CommonAlertDialog를 생성한다. 확인 버튼을 누르면 dailog가 닫힌다.
     *
     * @param context context
     * @param msg     표시할 메세지
     * @return dialog
     */
    fun getCommonAlertDialog(
        context: Context,
        msg: String?
    ): CommonAlertDialog {
        val dialog = CommonAlertDialog(context)
        dialog.setTitle(R.string.alert_alert)
        dialog.setMessage(msg)
        dialog.setCancelable(false)
        dialog.setPositiveButton(R.string.alert_confirm, null)
        return dialog
    }

    /**
     * 로그인 필요한 메뉴에 진입시 발생하는 다이얼로그
     *
     *
     * 확인을 누르면 로그인을 하고 취소를 누르는 경우 다이얼로그를 닫는다.
     *
     * @param context
     * @param listener
     * @return
     */
    fun getLoginAlertDialog(
        context: Context,
        listener: View.OnClickListener?
    ): CommonAlertDialog {
        val dialog = CommonAlertDialog(context)
        dialog.setTitle(R.string.alert_alert)
        dialog.setMessage(R.string.no_member_login_message)
        dialog.setCancelable(false)
        dialog.setPositiveButton(R.string.alert_confirm, listener)
        dialog.setNegativeButton(R.string.alert_cancel, null)
        dialog.show()
        return dialog
    }

    fun getJejuTravelDialog(
        context: Context,
        title: String,
        content: String
    ): CommonAlertDialog {
        return getJejuTravelDialog(context, title, content, null, null)
    }

    fun getJejuTravelDialog(
        context: Context,
        title: String,
        content: String,
        listener1: View.OnClickListener?
    ): CommonAlertDialog {
        return getJejuTravelDialog(context, title, content, listener1, null)
    }

    fun getJejuTravelDialog(
        context: Context,
        title: String,
        content: String,
        listener1: View.OnClickListener?,
        listener2: View.OnClickListener?
    ): CommonAlertDialog {
        val dialog = CommonAlertDialog(context)
        dialog.setTitle(title)
        dialog.setMessage(content)
        dialog.setCancelable(false)
        dialog.setPositiveButton(R.string.alert_confirm, listener1)
        if (listener2 != null) {
            dialog.setNegativeButton(R.string.alert_cancel, null)
        }
        return dialog
    }
}