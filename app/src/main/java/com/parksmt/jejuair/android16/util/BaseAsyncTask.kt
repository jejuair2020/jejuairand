package com.parksmt.jejuair.android16.util

import android.content.Context
import android.content.DialogInterface
import android.os.AsyncTask
import android.view.View
import com.parksmt.jejuair.android16.R
import com.parksmt.jejuair.android16.airplane.AirplaneModeBroadcastReceiver
import com.parksmt.jejuair.android16.common.DLog
import com.parksmt.jejuair.android16.view.CommonAlertDialog
import com.parksmt.jejuair.android16.view.CommonLoadingDialog
import java.net.HttpURLConnection

/**
 * 명시적으로 loading dialog dismiss 하지 않아도 task 가 끝나면 자동으로 dismiss 함<br></br>
 *
 * @param <Params>
 * @param <Progress>
 * @param <Result>
 * @author ui-jun
</Result></Progress></Params> */
abstract class BaseAsyncTask<Params, Progress, Result> @JvmOverloads constructor(
    var mContext: Context,
    var isShowLoadingDialog: Boolean = false
) : AsyncTask<Params, Progress, Result>() {
    /**
     * [AsyncTask]의 onPostExecute 함수의 callback interface
     *
     * @author ui-jun
     */
    interface OnPostExecuteListenerWithResult {
        fun onPostExecuteListenerWithResult(
            asyncTask: BaseAsyncTask<*, *, *>?,
            result: Int
        )
    }

    @JvmField
    protected var tag = this.javaClass.simpleName
    @JvmField
    protected var mConnection: HttpURLConnection? = null
    lateinit var mLoadingDialog: CommonLoadingDialog
    private val loadingDialogCancelable = false
    protected var showRetryAlert = true
    protected var showDuplicatedLoginAlert = true
    override fun onPreExecute() {
        super.onPreExecute()
        if (isShowLoadingDialog) {
            if (mLoadingDialog == null) {
                mLoadingDialog = CommonLoadingDialog(mContext)
            }
            mLoadingDialog.setCancelable(loadingDialogCancelable)
            mLoadingDialog.setOnCancelListener(DialogInterface.OnCancelListener {
                DLog().d("dialog  onCancel")
                cancel(true)
            })
            try {
                mLoadingDialog.show()
            } catch (e: Exception) {
                DLog().e("Exception" + e)
            }
        }
    }

    abstract override fun doInBackground(vararg params: Params): Result
    override fun onPostExecute(result: Result) {
        super.onPostExecute(result)
        if (isShowLoadingDialog && mLoadingDialog != null && mLoadingDialog.isShowing()) {
            try {
                mLoadingDialog.dismiss()
            } catch (e: Exception) {
                DLog().e("Exception$e")
            }
        }
    }

    override fun onCancelled() {
        super.onCancelled()
        DLog().d("onCancelled")
        if (isShowLoadingDialog && mLoadingDialog != null && mLoadingDialog.isShowing()) {
            try {
                mLoadingDialog.dismiss()
            } catch (e: Exception) {
                DLog().e("Exception" + e)
            }
        }
    }

    fun setShowLoadingDialog(showLoadingDialog: Boolean): BaseAsyncTask<Params, Progress, Result> {
        isShowLoadingDialog = mContext != null && showLoadingDialog
        return this
    }

    fun cancel() {
        DLog().d("cancel")
        super.cancel(true)
    }

    fun setShowRetryAlert(showRetryAlert: Boolean): BaseAsyncTask<Params, Progress, Result> {
        this.showRetryAlert = showRetryAlert
        return this
    }

    fun setShowDuplicatedLoginAlert(showDuplicatedLoginAlert: Boolean): BaseAsyncTask<Params, Progress, Result> {
        this.showDuplicatedLoginAlert = showDuplicatedLoginAlert
        return this
    }

    @JvmOverloads
    fun showNetworkErrorDialog(
        retryOnClickListener: View.OnClickListener? = null,
        finishOnClickListener: View.OnClickListener? = null
    ) { //TODO 2018.04.02 yschoi [ITSM-IM00122203] 안드로이드 앱 공항 정보 관련 크래시 처리 요청 - v2.2.0 배포 후 fabric 확인.
        if (null == mContext) return
        if (Util().isAirplaneModeOn(mContext)) {
            AirplaneModeBroadcastReceiver.goAirplaneMode(mContext)
        } else {
            val dialog = CommonAlertDialog(mContext)
            dialog.setTitle(R.string.connect_timeout_error_title_message)
            dialog.setMessage(R.string.connect_timeout_error_message)
            if (retryOnClickListener != null) {
                dialog.setPositiveButton(R.string.alert_retry, retryOnClickListener)
                dialog.setNegativeButton(
                    R.string.alert_finish,
                    View.OnClickListener { v ->
                        finishOnClickListener?.onClick(v)
                        Util().finishApplication(mContext)
                    })
            } else {
                dialog.setPositiveButton(R.string.alert_confirm)
            }
            dialog.show()
        }
    }

    /**
     * default 에러 표시용 alert dialog를 띄운다. 확인 버튼을 누르면 앱이 종료된다. <br></br>
     *
     * @param code 에러코드
     */
    fun showErrorDialog(code: Int) {
        showErrorDialog(R.string.error_message, code)
    }

    /**
     * default 에러 표시용 alert dialog를 띄운다. 확인 버튼을 누르면 앱이 종료된다. <br></br>
     *
     * @param msg  표시할 메세지
     * @param code 에러코드
     */
    fun showErrorDialog(msg: Int, code: Int) {
        showErrorDialog(mContext!!.getString(msg), code)
    }

    /**
     * 에러 표시용 alert dialog를 띄운다. 확인 버튼을 누르면 앱이 종료된다.<br></br>
     *
     * @param msg  표시할 메세지
     * @param code 에러 코드
     */
    fun showErrorDialog(msg: String, code: Int) {
        val tail = "\ncode : " + code + "\n" + DLog().getLineNumber()
        val log = "Class : " + mContext.javaClass.getSimpleName() + "\n" + msg + tail
        val dialog = CommonAlertDialog(mContext)
        dialog.setMessage(msg)
        dialog.setPositiveButton(
            R.string.alert_confirm,
            View.OnClickListener { Util().finishApplication(mContext) })
        dialog.setCancelable(false)
        DLog().e(log)
        dialog.show()
    }

}