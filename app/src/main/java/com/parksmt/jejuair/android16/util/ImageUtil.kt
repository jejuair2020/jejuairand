package com.parksmt.jejuair.android16.util

import android.content.Context
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import com.bumptech.glide.Glide
import com.parksmt.jejuair.android16.R
import java.util.*

/**
 * image util
 *
 *
 * Created by ui-jun on 2017-06-26.
 */
class ImageUtil {
    fun setImageMatchParentSize(
        context: Context,
        rootView: ViewGroup?,
        url: String?
    ): View {
        val inflater = LayoutInflater.from(context)
        val layoutId: Int = R.layout.image_view_layout_full
        val view = inflater.inflate(layoutId, rootView, false)
        val imageView =
            view.findViewById<View>(R.id.image_view_pager) as ImageView
        Glide.with(context).load(ImageDownloadUtil().getImageFile(context, url)).into(imageView)
        return view
    }

    fun setImageFitStartCenter(
        context: Context,
        rootView: ViewGroup?,
        urlList: ArrayList<String?>
    ): View {
        val inflater = LayoutInflater.from(context)
        val layoutId: Int = R.layout.scroll_image_view_layout
        val view = inflater.inflate(layoutId, rootView, false)
        val layout =
            view.findViewById<View>(R.id.image_view_parent_layout) as LinearLayout
        val displayMetrics = context.resources.displayMetrics
        val dpWidth = displayMetrics.widthPixels / displayMetrics.density
        val mParam: LinearLayout.LayoutParams
        mParam =
            if (dpWidth > 360) { //mParam = new LinearLayout.LayoutParams(Util.convertDpToPixels(360, context), ViewGroup.LayoutParams.WRAP_CONTENT);
                LinearLayout.LayoutParams(
                    ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.WRAP_CONTENT
                )
            } else {
                LinearLayout.LayoutParams(
                    ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.WRAP_CONTENT
                )
            }
        mParam.gravity = Gravity.CENTER_HORIZONTAL
        var imageView: ImageView
        for (i in urlList.indices) {
            imageView = ImageView(context)
            imageView.layoutParams = mParam
            imageView.adjustViewBounds = true
            imageView.scaleType = ImageView.ScaleType.FIT_CENTER
            layout.addView(imageView)
            Glide.with(context).load(urlList[i])
                .into(imageView) //ImageDownloadUtil.getImageFile(context, urlList.get(i))).into(imageView);
        }
        return view
    }

    fun setImageFitStartCenter(
        context: Context,
        rootView: ViewGroup?,
        url: String?
    ): View {
        val temp = ArrayList<String?>()
        temp.add(url)
        return setImageFitStartCenter(
            context,
            rootView,
            temp
        )
    }
}