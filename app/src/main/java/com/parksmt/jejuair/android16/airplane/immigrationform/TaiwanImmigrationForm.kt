package com.parksmt.jejuair.android16.airplane.immigrationform

import android.content.Intent
import android.os.Bundle
import android.view.View
import com.parksmt.jejuair.android16.R
import com.parksmt.jejuair.android16.airplane.AirplaneModeMenuActivity
import com.parksmt.jejuair.android16.util.PinchImageViewActivity
import kotlinx.android.synthetic.main.immigration_form_taiwan_layout.*

/**
 * 대만 입국 신고서
 *
 *
 * Created by seungjinoh on 2017. 4. 6..
 */
class TaiwanImmigrationForm : AirplaneModeMenuActivity() {
    override val uiName: String
        protected get() = "S-MUI-08-026"

    override fun onCreate(savedInstanceState: Bundle?) {
        useLanguage = false
        super.onCreate(savedInstanceState)
        setContentView(R.layout.immigration_form_taiwan_layout)
        setText()
        initClickEvent()
    }

    private fun setText() {
        setTitleText(getString(R.string.immigration_form_taiwan_title))
    }

    private fun initClickEvent() {
        immigration_form_taiwan_imageview.setOnClickListener(View.OnClickListener {
            val intent =
                Intent(this@TaiwanImmigrationForm, PinchImageViewActivity::class.java)
            //이미지 파일인 경우
            intent.putExtra("path", R.drawable.arrival_3)
            //이미지 경로일 경우
//                intent.putExtra("url", "http://어쩌고 저쩌고");
            startActivity(intent)
        })
    }
}