package com.parksmt.jejuair.android16.view

import android.app.Dialog
import android.content.Context
import android.graphics.Color
import android.graphics.Paint
import android.os.Bundle
import android.view.View
import android.view.WindowManager
import android.widget.EditText
import android.widget.NumberPicker
import com.parksmt.jejuair.android16.R
import com.parksmt.jejuair.android16.common.DLog
import com.parksmt.jejuair.android16.util.Util
import org.json.JSONObject

/**
 * 공통 email list dialog
 *
 *
 * Created by jun on 2017-03-08.
 */
class EmailDialog(context: Context) :
    Dialog(context!!, android.R.style.Theme_Translucent_NoTitleBar) {
    interface OnEmailSelectedListener {
        fun onEmailSelected(selectedIndex: Int, email: String?)
    }

    private val tag = this.javaClass.simpleName
    private var mNumberPicker: NumberPicker? = null
    private var mOnEmailSelectedListener: OnEmailSelectedListener? = null
    private val mEmailList: Array<String?>
    private var mSelectedPosition = 0
    override fun onCreate(savedInstanceState: Bundle) {
        super.onCreate(savedInstanceState)
        val lpWindow = WindowManager.LayoutParams()
        lpWindow.flags = WindowManager.LayoutParams.FLAG_DIM_BEHIND
        lpWindow.dimAmount = 0.65f
        val window = window
        if (window != null) {
            window.attributes = lpWindow
        }
        setContentView(R.layout.email_dialog)
        setCancelable(true)
        init()
    }

    private fun init() {
        mNumberPicker =
            findViewById<View>(R.id.email_dialog_number_picker) as NumberPicker
        mNumberPicker!!.minValue = 0
        mNumberPicker!!.maxValue = mEmailList.size - 1
        mNumberPicker!!.wrapSelectorWheel = false
        mNumberPicker!!.displayedValues = mEmailList
        mNumberPicker!!.descendantFocusability = NumberPicker.FOCUS_BLOCK_DESCENDANTS
        mNumberPicker!!.value = mSelectedPosition
        mNumberPicker!!.setOnValueChangedListener { picker, oldVal, newVal ->
            mSelectedPosition = newVal
        }
        setNumberPickerTextColor(mNumberPicker)
        findViewById<View>(R.id.email_dialog_confirm_btn).setOnClickListener { close() }
    }

    private fun close() {
        DLog().d(
            "selectedIndex : " + mNumberPicker!!.value + "   item : " + mEmailList[mNumberPicker!!.value]
        )
        dismiss()
        if (mOnEmailSelectedListener != null) {
            mOnEmailSelectedListener!!.onEmailSelected(
                mNumberPicker!!.value,
                mEmailList[mNumberPicker!!.value]
            )
        }
    }

    fun setOnEmailSelectedListener(onEmailSelectedListener: OnEmailSelectedListener?): EmailDialog {
        mOnEmailSelectedListener = onEmailSelectedListener
        return this
    }

    var selection: Int
        get() = mSelectedPosition
        set(position) {
            mSelectedPosition = position
            if (mNumberPicker != null) {
                mNumberPicker!!.value = mSelectedPosition
            }
        }

    val selectedItem: String?
        get() = mEmailList[mSelectedPosition]

    val isLastItem: Boolean
        get() = mEmailList.size - 1 == mSelectedPosition

    fun getLastItem(): String? {
        return mEmailList[mEmailList.size - 1]
    }

    val emailList: Array<String?>
        get() {
            val newEmailList =
                arrayOfNulls<String>(mEmailList.size)
            System.arraycopy(mEmailList, 0, newEmailList, 0, mEmailList.size)
            return newEmailList
        }

    companion object {
        fun setNumberPickerTextColor(numberPicker: NumberPicker?): Boolean {
            val count = numberPicker!!.childCount
            for (i in 0 until count) {
                val child = numberPicker.getChildAt(i)
                if (child is EditText) {
                    try {
                        val selectorWheelPaintField =
                            numberPicker.javaClass
                                .getDeclaredField("mSelectorWheelPaint")
                        selectorWheelPaintField.isAccessible = true
                        (selectorWheelPaintField[numberPicker] as Paint).color =
                            Color.parseColor("black")
                        child.setTextColor(Color.parseColor("black"))
                        numberPicker.invalidate()
                        return true
                    } catch (e: NoSuchFieldException) {
                        DLog().e("setNumberPickerTextColor" + e)
                    } catch (e: IllegalAccessException) {
                        DLog().e("setNumberPickerTextColor" + e)
                    } catch (e: IllegalArgumentException) {
                        DLog().e("setNumberPickerTextColor" +  e)
                    }
                }
            }
            return false
        }
    }

    init {
        var temp: JSONObject =
            Util().loadJSONObject(context, "com/email.json")
        temp = temp!!.optJSONObject(Util().getLanguage(context))
        if (temp == null || temp.length() == 0) {
            mEmailList = arrayOfNulls(1)
            mEmailList[0] = "직접입력"
        } else {
            val array = temp.optJSONArray("email")
            if (array == null || array.length() == 0) {
                mEmailList = arrayOfNulls(1)
                mEmailList[0] = "직접입력"
            } else {
                mEmailList = arrayOfNulls(array.length())
                for (i in 0 until array.length()) {
                    mEmailList[i] = array.optString(i)
                }
            }
        }
    }
}