package com.parksmt.jejuair.android16.view

import android.app.Activity
import android.app.Dialog
import android.content.Context
import android.graphics.drawable.AnimationDrawable
import android.os.Bundle
import android.view.View
import android.view.WindowManager
import android.view.WindowManager.BadTokenException
import android.widget.ImageView
import com.parksmt.jejuair.android16.R
import com.parksmt.jejuair.android16.common.DLog
import com.parksmt.jejuair.android16.common.Setting
import java.lang.ref.WeakReference

/**
 * 기본 로딩 dialog
 *
 *
 * Created by jun on 2017-02-10.
 */
class CommonLoadingDialog(private val mContext: Context) :
    Dialog(mContext, R.style.transparentDialog) {
    private val tag = this.javaClass.simpleName
    private var mStartTime: Long = 0
    private var cancelable = false
    private var mChangeAnimationDrawable: AnimationDrawable? = null
    override fun onCreate(savedInstanceState: Bundle) {
        super.onCreate(savedInstanceState)
        val window = window
        if (window != null) {
            val lpWindow = WindowManager.LayoutParams()
            lpWindow.flags = WindowManager.LayoutParams.FLAG_DIM_BEHIND
            lpWindow.dimAmount = 0f
            window.attributes = lpWindow
        }
        setContentView(R.layout.loading)
        initAnimation()
        super.setCancelable(true)
    }

    override fun show() {
        try {
            val activityWeakRef =
                WeakReference(mContext as Activity)
            if (activityWeakRef.get() != null && !activityWeakRef.get()!!.isFinishing) {
                super.show()
            } else {
                DLog().d(
                    "show activityWeakRef.get() == null || activityWeakRef.get().isFinishing()"
                )
            }
        } catch (e: ClassCastException) {
            DLog().e("ClassCastException" + e)
        } catch (e: BadTokenException) {
            DLog().e("BadTokenException" + e)
        }
        mStartTime = System.currentTimeMillis()
        mChangeAnimationDrawable!!.start()
    }

    override fun onStart() {
        try {
            val activityWeakRef =
                WeakReference(mContext as Activity)
            if (activityWeakRef.get() != null && !activityWeakRef.get()!!.isFinishing) {
                super.onStart()
            } else {
                DLog().d(
                    "onStart activityWeakRef.get() == null || activityWeakRef.get().isFinishing()"
                )
            }
        } catch (e: ClassCastException) {
            DLog().e("ClassCastException" + e)
        } catch (e: BadTokenException) {
            DLog().e("BadTokenException" + e)
        }
    }

    override fun cancel() {
        DLog().d(
            "elapse Time : " + ((System.currentTimeMillis() - mStartTime).toString() + "  cancelable : " + cancelable)
        )
        if (cancelable) {
            super.cancel()
        } else {
            if (System.currentTimeMillis() - mStartTime > Setting.LOADING_DIALOG_CANCELABLE_TIME) {
                super.cancel()
            }
        }
    }

    override fun dismiss() {
        try {
            val activityWeakRef =
                WeakReference(mContext as Activity)
            if (activityWeakRef.get() != null && !activityWeakRef.get()!!.isFinishing) {
                super.dismiss()
            } else {
                DLog().d(
                    "dismiss activityWeakRef.get() == null || activityWeakRef.get().isFinishing()"
                )
            }
        } catch (e: ClassCastException) {
            DLog().e("ClassCastException" + e)
        } catch (e: BadTokenException) {
            DLog().e("BadTokenException" + e)
        }
    }

    override fun setCancelable(cancelable: Boolean) {
        this.cancelable = cancelable
    }

    private fun initAnimation() {
        val mFlightImageView =
            findViewById<View>(R.id.loading_flight_image_view) as ImageView
        mFlightImageView.setBackgroundResource(R.drawable.loading_animation)
        mChangeAnimationDrawable = mFlightImageView.background as AnimationDrawable
    }

}