package com.parksmt.jejuair.android16.common

import android.app.Dialog
import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import android.widget.AdapterView
import android.widget.AdapterView.OnItemClickListener
import android.widget.BaseAdapter
import android.widget.ListView
import android.widget.TextView
import androidx.core.content.ContextCompat
import com.parksmt.jejuair.android16.R
import com.parksmt.jejuair.android16.enumdata.Language
import java.util.*

/**
 * 언어 선택 dialog
 *
 *
 * Created by ui-jun on 2017-04-26.
 */
class LanguageDialog(private val mContext: Context) :
    Dialog(mContext, android.R.style.Theme_Translucent_NoTitleBar) {
    interface OnListSelectedListener {
        fun onListSelected(
            itemIndex: Int,
            item: Language?
        )
    }

    private val tag = this.javaClass.simpleName
    private lateinit var mListView: ListView
    private lateinit var mLanguageAdapter: LanguageAdapter
    private lateinit var mOnListSelectedListener: OnListSelectedListener
    private val mItemList: ArrayList<Language>
    var selection = 0
    override fun onCreate(savedInstanceState: Bundle) {
        super.onCreate(savedInstanceState)
        val lpWindow = WindowManager.LayoutParams()
        lpWindow.flags = WindowManager.LayoutParams.FLAG_DIM_BEHIND
        lpWindow.dimAmount = 0.65f
        window!!.attributes = lpWindow
        setContentView(R.layout.list_dialog)
        setCancelable(true)
        init()
    }

    private fun init() {
        mListView =
            findViewById<View>(R.id.list_dialog_listview) as ListView
        mLanguageAdapter = LanguageAdapter(mContext, mItemList)
        mListView.adapter = mLanguageAdapter
        mListView.setSelection(selection)
        mListView.onItemClickListener = object : OnItemClickListener {
            override fun onItemClick(
                parent: AdapterView<*>?,
                view: View,
                position: Int,
                id: Long
            ) {
                DLog().d("position : $position")
                selection = position
                mLanguageAdapter.notifyDataSetChanged()
                close()
            }
        }
    }

    private fun close() {
        DLog().d(
            "selectedIndex : " + selection + "   item : " + mItemList[selection]
        )
        dismiss()
        if (mOnListSelectedListener != null) {
            mOnListSelectedListener.onListSelected(
                selection,
                mItemList[selection]
            )
        }
    }

    fun setOnListSelectedListener(onListSelectedListener: OnListSelectedListener): LanguageDialog {
        mOnListSelectedListener = onListSelectedListener
        return this
    }

    fun setSelection(language: Language) {
        for (i in mItemList.indices) {
            if (mItemList[i] === language) {
                selection = i
            }
        }
    }

    val selectedItem: Language
        get() = mItemList[selection]

    val isLastItem: Boolean
        get() = mItemList.size - 1 == selection

    private inner class LanguageAdapter internal constructor(
        context: Context,
        item: ArrayList<Language>
    ) : BaseAdapter() {
        private val mInflater: LayoutInflater
        private val mItem: ArrayList<Language>
        private var mHolder: ViewHolder? =
            null

        override fun getCount(): Int {
            return mItem.size
        }

        override fun getItem(position: Int): Any {
            return mItem[position]
        }

        override fun getItemId(position: Int): Long {
            return position.toLong()
        }

        override fun getView(
            position: Int,
            convertView: View,
            parent: ViewGroup
        ): View {
            var convertView = convertView
            if (convertView == null) {
                convertView = mInflater.inflate(R.layout.list_dialog_row, parent, false)
                mHolder =
                    ViewHolder()
                mHolder!!.mTextView =
                    convertView.findViewById<View>(R.id.list_dialog_row_textview) as TextView
                convertView.tag = mHolder
            } else {
                mHolder =
                    convertView.tag as ViewHolder
            }
            mHolder!!.mTextView!!.text = mItem[position].displayString
            if (selection == position) {
                mHolder!!.mTextView!!.setTextColor(
                    ContextCompat.getColor(
                        mContext,
                        R.color.sky_blue_text_color
                    )
                )
                mHolder!!.mTextView!!.setCompoundDrawablesWithIntrinsicBounds(
                    0,
                    0,
                    R.drawable.check_list_on,
                    0
                )
            } else {
                mHolder!!.mTextView!!.setTextColor(
                    ContextCompat.getColor(
                        mContext,
                        R.color.black_text_color
                    )
                )
                mHolder!!.mTextView!!.setCompoundDrawablesWithIntrinsicBounds(
                    0,
                    0,
                    R.drawable.check_list_off,
                    0
                )
            }
            return convertView
        }

        private inner class ViewHolder {
            var mTextView: TextView? = null
        }

        init {
            mInflater =
                context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
            mItem = item
        }
    }

    init {
        mItemList = ArrayList()
        Collections.addAll(
            mItemList,
            *Language.values()
        )
    }
}