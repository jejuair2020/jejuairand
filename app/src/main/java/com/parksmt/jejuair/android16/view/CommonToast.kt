package com.parksmt.jejuair.android16.view

import android.content.Context
import android.graphics.Color
import android.view.Gravity
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView
import android.widget.Toast
import androidx.core.content.ContextCompat
import com.parksmt.jejuair.android16.R

/**
 * 공통 toast
 * Created by ui-jun on 2017-03-17.
 */
class CommonToast {
    private var mToast: Toast?

    constructor(context: Context, text: String?) {
        mToast = Toast(context)
        val scale = context.resources.displayMetrics.density
        val vPadding = (20 * scale + 0.5f).toInt()
        val hPadding = (15 * scale + 0.5f).toInt()
        val layout = LinearLayout(context)
        layout.setPadding(hPadding, 0, hPadding, hPadding)
        layout.layoutParams = LinearLayout.LayoutParams(
            ViewGroup.LayoutParams.MATCH_PARENT,
            ViewGroup.LayoutParams.WRAP_CONTENT
        )
        val textView = TextView(context)
        textView.textSize = 13f
        textView.setTextColor(Color.parseColor("#ffffff"))
        textView.text = text
        textView.setPadding(hPadding, vPadding, hPadding, vPadding)
        textView.setBackgroundColor(ContextCompat.getColor(context, R.color.sky_blue_text_color))
        textView.layoutParams = LinearLayout.LayoutParams(
            ViewGroup.LayoutParams.MATCH_PARENT,
            ViewGroup.LayoutParams.WRAP_CONTENT
        )
        layout.addView(textView)
        mToast!!.setGravity(Gravity.BOTTOM or Gravity.FILL_HORIZONTAL, 0, 0)
        mToast!!.duration = Toast.LENGTH_SHORT
        mToast!!.view = layout
    }

    constructor(
        context: Context,
        text: String?,
        selected: Boolean,
        color: String?
    ) {
        mToast = Toast(context)
        val scale = context.resources.displayMetrics.density
        val vPadding = (20 * scale + 0.5f).toInt()
        val hPadding = (15 * scale + 0.5f).toInt()
        val layout = LinearLayout(context)
        layout.setPadding(hPadding, 0, hPadding, hPadding)
        layout.layoutParams = LinearLayout.LayoutParams(
            ViewGroup.LayoutParams.MATCH_PARENT,
            ViewGroup.LayoutParams.WRAP_CONTENT
        )
        val textView = TextView(context)
        textView.textSize = 13f
        textView.setTextColor(Color.parseColor("#ffffff"))
        textView.text = text
        textView.setPadding(hPadding, vPadding, hPadding, vPadding)
        textView.setBackgroundColor(ContextCompat.getColor(context, R.color.main_color))
        textView.layoutParams = LinearLayout.LayoutParams(
            ViewGroup.LayoutParams.MATCH_PARENT,
            ViewGroup.LayoutParams.WRAP_CONTENT
        )
        layout.addView(textView)
        mToast!!.setGravity(Gravity.BOTTOM or Gravity.FILL_HORIZONTAL, 0, 0)
        mToast!!.duration = if (selected) Toast.LENGTH_SHORT else Toast.LENGTH_LONG
        mToast!!.view = layout
    }

    fun show() {
        if (mToast != null) {
            mToast!!.show()
        }
    }

    fun cancel() {
        if (mToast != null) {
            mToast!!.cancel()
        }
    }

    constructor(context: Context, text: String?, duration: Int) {
        mToast = Toast(context)
        val scale = context.resources.displayMetrics.density
        val vPadding = (20 * scale + 0.5f).toInt()
        val hPadding = (15 * scale + 0.5f).toInt()
        val layout = LinearLayout(context)
        layout.setPadding(hPadding, 0, hPadding, hPadding)
        layout.layoutParams = LinearLayout.LayoutParams(
            ViewGroup.LayoutParams.MATCH_PARENT,
            ViewGroup.LayoutParams.WRAP_CONTENT
        )
        val textView = TextView(context)
        textView.textSize = 13f
        textView.setTextColor(Color.parseColor("#ffffff"))
        textView.text = text
        textView.setPadding(hPadding, vPadding, hPadding, vPadding)
        textView.setBackgroundColor(ContextCompat.getColor(context, R.color.sky_blue_text_color))
        textView.layoutParams = LinearLayout.LayoutParams(
            ViewGroup.LayoutParams.MATCH_PARENT,
            ViewGroup.LayoutParams.WRAP_CONTENT
        )
        layout.addView(textView)
        mToast!!.setGravity(Gravity.BOTTOM or Gravity.FILL_HORIZONTAL, 0, 0)
        mToast!!.duration = duration
        mToast!!.view = layout
    }

    companion object {
        fun makeToast(context: Context, text: String?): CommonToast {
            return CommonToast(context, text)
        }

        fun makeToast(
            context: Context,
            text: String?,
            selected: Boolean,
            color: String?
        ): CommonToast {
            return CommonToast(context, text, selected, color)
        }

        fun makeToast(
            context: Context,
            text: String?,
            duration: Int
        ): CommonToast {
            return CommonToast(context, text, duration)
        }
    }
}