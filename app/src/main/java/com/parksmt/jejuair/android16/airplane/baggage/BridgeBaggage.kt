package com.parksmt.jejuair.android16.airplane.baggage

import android.content.Context
import android.os.Bundle
import android.view.View
import android.widget.LinearLayout
import android.widget.TextView
import com.parksmt.jejuair.android16.R
import com.parksmt.jejuair.android16.airplane.AirplaneModeMenuActivity
import com.parksmt.jejuair.android16.view.PopupController
import kotlinx.android.synthetic.main.bridge_baggage_layout.*

/**
 * 연결구간 수하물 S-MUI-08-014
 * Created by seungjinoh on 2017. 4. 5..
 */
class BridgeBaggage : AirplaneModeMenuActivity() {
    private var mPriceList: PriceList? = null
    protected override val uiName: String
        protected get() = "S-MUI-08-014"

    protected override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.bridge_baggage_layout)
        setText()
        initView()
        initClickEvent()
    }

    private fun initView() {
        mPriceList = PriceList(this)
    }

    private fun setText() {
        loadLanguage("serviceinfo/BridgeBaggageLan.json")
        setTitleText(mLanguageJson.optString("bridge_baggage_text1000"))
        (findViewById(R.id.bridge_baggage_text1) as TextView).setText(mLanguageJson.optString("bridge_baggage_text1001"))
        (findViewById(R.id.bridge_baggage_text2) as TextView).setText(mLanguageJson.optString("bridge_baggage_text1002"))
        (findViewById(R.id.bridge_baggage_text3) as TextView).setText(mLanguageJson.optString("bridge_baggage_text1003"))
        (findViewById(R.id.bridge_baggage_text4) as TextView).setText(mLanguageJson.optString("bridge_baggage_text1004"))
        //        ((TextView) findViewById(R.id.bridge_baggage_text5)).setText(mLanguageJson.optString("bridge_baggage_text1005"));
        (findViewById(R.id.bridge_baggage_text6) as TextView).setText(mLanguageJson.optString("bridge_baggage_text1006"))
        (findViewById(R.id.bridge_baggage_text7) as TextView).setText(mLanguageJson.optString("bridge_baggage_text1007"))
        //        ((TextView) findViewById(R.id.bridge_baggage_text8)).setText(mLanguageJson.optString("bridge_baggage_text1008"));
        (findViewById(R.id.bridge_baggage_text9) as TextView).setText(mLanguageJson.optString("bridge_baggage_text1009"))
        (findViewById(R.id.bridge_baggage_text10) as TextView).setText(mLanguageJson.optString("bridge_baggage_text1010"))
        (findViewById(R.id.bridge_baggage_text11) as TextView).setText(mLanguageJson.optString("bridge_baggage_text1011"))
        (findViewById(R.id.bridge_baggage_text12) as TextView).setText(mLanguageJson.optString("bridge_baggage_text1012"))
        (findViewById(R.id.bridge_baggage_text13) as TextView).setText(mLanguageJson.optString("bridge_baggage_text1013"))
        (findViewById(R.id.bridge_baggage_text14) as TextView).setText(mLanguageJson.optString("bridge_baggage_text1014"))
        (findViewById(R.id.bridge_baggage_text15) as TextView).setText(mLanguageJson.optString("bridge_baggage_text1015"))
        (findViewById(R.id.bridge_baggage_text16) as TextView).setText(mLanguageJson.optString("bridge_baggage_text1016"))
        (findViewById(R.id.bridge_baggage_text19) as TextView).setText(mLanguageJson.optString("bridge_baggage_text1019"))
        (findViewById(R.id.bridge_baggage_text20) as TextView).setText(mLanguageJson.optString("bridge_baggage_text1020"))
        (findViewById(R.id.bridge_baggage_text21) as TextView).setText(mLanguageJson.optString("bridge_baggage_text1021"))
        (findViewById(R.id.bridge_baggage_text22) as TextView).setText(mLanguageJson.optString("bridge_baggage_text1015"))
        (findViewById(R.id.bridge_baggage_text23) as TextView).setText(mLanguageJson.optString("bridge_baggage_text1023"))
        (findViewById(R.id.bridge_baggage_text24) as TextView).setText(mLanguageJson.optString("bridge_baggage_text1024"))
        (findViewById(R.id.bridge_baggage_text25) as TextView).setText(mLanguageJson.optString("bridge_baggage_text1025"))
        (findViewById(R.id.bridge_baggage_text26) as TextView).setText(mLanguageJson.optString("bridge_baggage_text1026"))
        (findViewById(R.id.bridge_baggage_text27) as TextView).setText(mLanguageJson.optString("bridge_baggage_text1027"))
        (findViewById(R.id.bridge_baggage_text28) as TextView).setText(mLanguageJson.optString("bridge_baggage_text1028"))
        (findViewById(R.id.bridge_baggage_text29) as TextView).setText(mLanguageJson.optString("bridge_baggage_text1029"))
        (findViewById(R.id.bridge_baggage_text30) as TextView).setText(mLanguageJson.optString("bridge_baggage_text1030"))
        (findViewById(R.id.bridge_baggage_text31) as TextView).setText(mLanguageJson.optString("bridge_baggage_text1031"))
        (findViewById(R.id.bridge_baggage_text32) as TextView).setText(mLanguageJson.optString("bridge_baggage_text1032"))
        (findViewById(R.id.bridge_baggage_text33) as TextView).setText(mLanguageJson.optString("bridge_baggage_text1033"))
        (findViewById(R.id.bridge_baggage_text34) as TextView).setText(mLanguageJson.optString("bridge_baggage_text1034"))
        (findViewById(R.id.bridge_baggage_text35) as TextView).setText(mLanguageJson.optString("bridge_baggage_text1035"))
        (findViewById(R.id.bridge_baggage_text36) as TextView).setText(mLanguageJson.optString("bridge_baggage_text1036"))
        (findViewById(R.id.bridge_baggage_text37) as TextView).setText(mLanguageJson.optString("bridge_baggage_text1037"))
        //2019.05.08 yschoi 페어패밀리 수하물 변경 사항 추가 작업
        (findViewById(R.id.bridge_baggage_allowed_free_luggage) as TextView).setText(
            mLanguageJson.optString(
                "bridge_baggage_text1017"
            )
        )
        (findViewById(R.id.bridge_baggage_effective_as) as TextView).setText(
            mLanguageJson.optString(
                "EffectiveAs2019.04.30"
            )
        )
        val view1: View = findViewById(R.id.bridge_baggage_row1)
        (view1.findViewById<View>(R.id.bridge_baggage_row_title) as TextView).setText(
            mLanguageJson.optString("BridgeBaggage_FlyBagPlus")
        )
        (view1.findViewById<View>(R.id.bridge_baggage_row_body) as TextView).setText(
            mLanguageJson.optString("bridge_baggage_text1018_2")
        )
        val view2: View = findViewById(R.id.bridge_baggage_row2)
        (view2.findViewById<View>(R.id.bridge_baggage_row_title) as TextView).setText(
            mLanguageJson.optString("BridgeBaggage_FlyBag")
        )
        (view2.findViewById<View>(R.id.bridge_baggage_row_body) as TextView).setText(
            mLanguageJson.optString("bridge_baggage_text1018")
        )
        val view3: View = findViewById(R.id.bridge_baggage_row3)
        (view3.findViewById<View>(R.id.bridge_baggage_row_title) as TextView).setText(
            mLanguageJson.optString("BridgeBaggage_Fly")
        )
        (view3.findViewById<View>(R.id.bridge_baggage_row_body) as TextView).setText(
            mLanguageJson.optString("bridge_baggage_text1011")
        )
        (view3.findViewById<View>(R.id.bridge_baggage_row_body) as TextView).setCompoundDrawablesWithIntrinsicBounds(
            0,
            0,
            0,
            0
        )
        (findViewById(R.id.bridge_baggage_effective_before) as TextView).setText(
            mLanguageJson.optString(
                "EffectiveBefore2019.04.30"
            )
        )
        val view4: View = findViewById(R.id.bridge_baggage_row4)
        (view4.findViewById<View>(R.id.bridge_baggage_row_title) as TextView).setText(
            mLanguageJson.optString("AllFare")
        )
        (view4.findViewById<View>(R.id.bridge_baggage_row_body) as TextView).setText(
            mLanguageJson.optString("bridge_baggage_text1018")
        )
    }

    private fun initClickEvent() {
        bridge_baggage_text13.setOnClickListener(View.OnClickListener { mPriceList!!.show() })
    }

    private inner class PriceList internal constructor(context: Context?) :
        PopupController(context!!, R.layout.price_list_popup), View.OnClickListener {
        private var mZone1Layout: LinearLayout? = null
        private var mZone2Layout: LinearLayout? = null
        private var mZone3Layout: LinearLayout? = null
        private var mZone4Layout: LinearLayout? = null
        private var mZone1LayoutParent: LinearLayout? = null
        private var mZone2LayoutParent: LinearLayout? = null
        private var mZone3LayoutParent: LinearLayout? = null
        private var mZone4LayoutParent: LinearLayout? = null
        private var mZone1LayoutisShowing = false
        private var mZone2LayoutisShowing = false
        private var mZone3LayoutisShowing = false
        private var mZone4LayoutisShowing = false
        private lateinit var mZone1TextView: TextView
        private lateinit var mZone2TextView: TextView
        private lateinit var mZone3TextView: TextView
        private lateinit var mZone4TextView: TextView
        override fun onClick(v: View) {
            when (v.id) {
                R.id.reservation_start_popup_close_btn, R.id.price_list_popup_complete_btn -> dismiss()
            }
        }

        private fun init() {
            mZone1LayoutParent =
                mContentView.findViewById<View>(R.id.price_list_sub_layout1) as LinearLayout
            mZone2LayoutParent =
                mContentView.findViewById<View>(R.id.price_list_sub_layout2) as LinearLayout
            mZone3LayoutParent =
                mContentView.findViewById<View>(R.id.price_list_sub_layout3) as LinearLayout
            mZone4LayoutParent =
                mContentView.findViewById<View>(R.id.price_list_sub_layout4) as LinearLayout
            mZone1Layout =
                mZone1LayoutParent!!.findViewById<View>(R.id.price_list_sub_layout) as LinearLayout
            mZone2Layout =
                mZone2LayoutParent!!.findViewById<View>(R.id.price_list_sub_layout) as LinearLayout
            mZone3Layout =
                mZone3LayoutParent!!.findViewById<View>(R.id.price_list_sub_layout) as LinearLayout
            mZone4Layout =
                mZone4LayoutParent!!.findViewById<View>(R.id.price_list_sub_layout) as LinearLayout
            mZone1TextView =
                mContentView.findViewById<View>(R.id.price_list_text12) as TextView
            mZone2TextView =
                mContentView.findViewById<View>(R.id.price_list_text13) as TextView
            mZone3TextView =
                mContentView.findViewById<View>(R.id.price_list_text14) as TextView
            mZone4TextView =
                mContentView.findViewById<View>(R.id.price_list_text15) as TextView
        }

        private fun setText() {
            (mContentView.findViewById<View>(R.id.price_list_text1) as TextView).setText(
                mLanguageJson.optString("bridge_baggage_text1038")
            )
            (mContentView.findViewById<View>(R.id.price_list_text2) as TextView).setText(
                mLanguageJson.optString("bridge_baggage_text1039")
            )
            (mContentView.findViewById<View>(R.id.price_list_text3) as TextView).setText(
                mLanguageJson.optString("bridge_baggage_text1040")
            )
            (mContentView.findViewById<View>(R.id.price_list_text4) as TextView).setText(
                mLanguageJson.optString("bridge_baggage_text1041")
            )
            (mContentView.findViewById<View>(R.id.price_list_text5) as TextView).setText(
                mLanguageJson.optString("bridge_baggage_text1042")
            )
            (mContentView.findViewById<View>(R.id.price_list_text6) as TextView).setText(
                mLanguageJson.optString("bridge_baggage_text1043")
            )
            (mContentView.findViewById<View>(R.id.price_list_text7) as TextView).setText(
                mLanguageJson.optString("bridge_baggage_text1044")
            )
            (mContentView.findViewById<View>(R.id.price_list_text8) as TextView).setText(
                mLanguageJson.optString("bridge_baggage_text1045")
            )
            (mContentView.findViewById<View>(R.id.price_list_text9) as TextView).setText(
                mLanguageJson.optString("bridge_baggage_text1046")
            )
            (mContentView.findViewById<View>(R.id.price_list_text10) as TextView).setText(
                mLanguageJson.optString("bridge_baggage_text1047")
            )
            (mContentView.findViewById<View>(R.id.price_list_text11) as TextView).setText(
                mLanguageJson.optString("bridge_baggage_text1048")
            )
            mZone1TextView.setText(mLanguageJson.optString("bridge_baggage_text1049"))
            mZone2TextView.setText(mLanguageJson.optString("bridge_baggage_text1050"))
            mZone3TextView.setText(mLanguageJson.optString("bridge_baggage_text1051"))
            mZone4TextView.setText(mLanguageJson.optString("bridge_baggage_text1052"))
            (mContentView.findViewById<View>(R.id.price_list_popup_complete_btn) as TextView)
                .setText(mLanguageJson.optString("bridge_baggage_text1053"))
            (mZone1LayoutParent!!.findViewById<View>(R.id.price_list_sub_text1) as TextView)
                .setText(mLanguageJson.optString("bridge_baggage_text1054"))
            (mZone1LayoutParent!!.findViewById<View>(R.id.price_list_sub_text2) as TextView)
                .setText(mLanguageJson.optString("bridge_baggage_text1055"))
            (mZone1LayoutParent!!.findViewById<View>(R.id.price_list_sub_text3) as TextView)
                .setText(mLanguageJson.optString("bridge_baggage_text1056"))
            (mZone1LayoutParent!!.findViewById<View>(R.id.price_list_sub_text4) as TextView)
                .setText(mLanguageJson.optString("bridge_baggage_text1057"))
            (mZone1LayoutParent!!.findViewById<View>(R.id.price_list_sub_text5) as TextView)
                .setText(mLanguageJson.optString("bridge_baggage_text1058"))
            (mZone1LayoutParent!!.findViewById<View>(R.id.price_list_sub_text6) as TextView)
                .setText(mLanguageJson.optString("bridge_baggage_text1059"))
            (mZone1LayoutParent!!.findViewById<View>(R.id.price_list_sub_text7) as TextView)
                .setText(mLanguageJson.optString("bridge_baggage_text1060"))
            (mZone1LayoutParent!!.findViewById<View>(R.id.price_list_sub_text8) as TextView)
                .setText(mLanguageJson.optString("bridge_baggage_text1061"))
            (mZone1LayoutParent!!.findViewById<View>(R.id.price_list_sub_text9) as TextView)
                .setText(mLanguageJson.optString("bridge_baggage_text1062"))
            (mZone1LayoutParent!!.findViewById<View>(R.id.price_list_sub_text10) as TextView)
                .setText(mLanguageJson.optString("bridge_baggage_text1063"))
            (mZone1LayoutParent!!.findViewById<View>(R.id.price_list_sub_text11) as TextView)
                .setText(mLanguageJson.optString("bridge_baggage_text1064"))
            (mZone1LayoutParent!!.findViewById<View>(R.id.price_list_sub_text12) as TextView)
                .setText(mLanguageJson.optString("bridge_baggage_text1065"))
            (mZone1LayoutParent!!.findViewById<View>(R.id.price_list_sub_text13) as TextView)
                .setText(mLanguageJson.optString("bridge_baggage_text1066"))
            (mZone1LayoutParent!!.findViewById<View>(R.id.price_list_sub_text14) as TextView)
                .setText(mLanguageJson.optString("bridge_baggage_text1067"))
            (mZone1LayoutParent!!.findViewById<View>(R.id.price_list_sub_text15) as TextView)
                .setText(mLanguageJson.optString("bridge_baggage_text1068"))
            (mZone1LayoutParent!!.findViewById<View>(R.id.price_list_sub_text16) as TextView)
                .setText(mLanguageJson.optString("bridge_baggage_text1069"))
            (mZone1LayoutParent!!.findViewById<View>(R.id.price_list_sub_text17) as TextView)
                .setText(mLanguageJson.optString("bridge_baggage_text1070"))
            (mZone1LayoutParent!!.findViewById<View>(R.id.price_list_sub_text18) as TextView)
                .setText(mLanguageJson.optString("bridge_baggage_text1071"))
            (mZone1LayoutParent!!.findViewById<View>(R.id.price_list_sub_text19) as TextView)
                .setText(mLanguageJson.optString("bridge_baggage_text1072"))
            (mZone1LayoutParent!!.findViewById<View>(R.id.price_list_sub_text20) as TextView)
                .setText(mLanguageJson.optString("bridge_baggage_text1073"))
            (mZone1LayoutParent!!.findViewById<View>(R.id.price_list_sub_text21) as TextView)
                .setText(mLanguageJson.optString("bridge_baggage_text1074"))
            (mZone1LayoutParent!!.findViewById<View>(R.id.price_list_sub_text22) as TextView)
                .setText(mLanguageJson.optString("bridge_baggage_text1075"))
            (mZone2LayoutParent!!.findViewById<View>(R.id.price_list_sub_text1) as TextView)
                .setText(mLanguageJson.optString("bridge_baggage_text1087"))
            (mZone2LayoutParent!!.findViewById<View>(R.id.price_list_sub_text2) as TextView)
                .setText(mLanguageJson.optString("bridge_baggage_text1088"))
            (mZone2LayoutParent!!.findViewById<View>(R.id.price_list_sub_text3) as TextView)
                .setText(mLanguageJson.optString("bridge_baggage_text1089"))
            (mZone2LayoutParent!!.findViewById<View>(R.id.price_list_sub_text4) as TextView)
                .setText(mLanguageJson.optString("bridge_baggage_text1090"))
            (mZone2LayoutParent!!.findViewById<View>(R.id.price_list_sub_text5) as TextView)
                .setText(mLanguageJson.optString("bridge_baggage_text1091"))
            (mZone2LayoutParent!!.findViewById<View>(R.id.price_list_sub_text6) as TextView)
                .setText(mLanguageJson.optString("bridge_baggage_text1092"))
            (mZone2LayoutParent!!.findViewById<View>(R.id.price_list_sub_text7) as TextView)
                .setText(mLanguageJson.optString("bridge_baggage_text1093"))
            (mZone2LayoutParent!!.findViewById<View>(R.id.price_list_sub_text8) as TextView)
                .setText(mLanguageJson.optString("bridge_baggage_text1094"))
            (mZone2LayoutParent!!.findViewById<View>(R.id.price_list_sub_text9) as TextView)
                .setText(mLanguageJson.optString("bridge_baggage_text1095"))
            (mZone2LayoutParent!!.findViewById<View>(R.id.price_list_sub_text10) as TextView)
                .setText(mLanguageJson.optString("bridge_baggage_text1096"))
            (mZone2LayoutParent!!.findViewById<View>(R.id.price_list_sub_text11) as TextView)
                .setText(mLanguageJson.optString("bridge_baggage_text1097"))
            (mZone2LayoutParent!!.findViewById<View>(R.id.price_list_sub_text12) as TextView)
                .setText(mLanguageJson.optString("bridge_baggage_text1098"))
            (mZone2LayoutParent!!.findViewById<View>(R.id.price_list_sub_text13) as TextView)
                .setText(mLanguageJson.optString("bridge_baggage_text1099"))
            (mZone2LayoutParent!!.findViewById<View>(R.id.price_list_sub_text14) as TextView)
                .setText(mLanguageJson.optString("bridge_baggage_text1100"))
            (mZone2LayoutParent!!.findViewById<View>(R.id.price_list_sub_text15) as TextView)
                .setText(mLanguageJson.optString("bridge_baggage_text1101"))
            (mZone2LayoutParent!!.findViewById<View>(R.id.price_list_sub_text16) as TextView)
                .setText(mLanguageJson.optString("bridge_baggage_text1102"))
            (mZone2LayoutParent!!.findViewById<View>(R.id.price_list_sub_text17) as TextView)
                .setText(mLanguageJson.optString("bridge_baggage_text1103"))
            (mZone2LayoutParent!!.findViewById<View>(R.id.price_list_sub_text18) as TextView)
                .setText(mLanguageJson.optString("bridge_baggage_text1104"))
            (mZone2LayoutParent!!.findViewById<View>(R.id.price_list_sub_text19) as TextView)
                .setText(mLanguageJson.optString("bridge_baggage_text1105"))
            (mZone2LayoutParent!!.findViewById<View>(R.id.price_list_sub_text20) as TextView)
                .setText(mLanguageJson.optString("bridge_baggage_text1106"))
            (mZone2LayoutParent!!.findViewById<View>(R.id.price_list_sub_text21) as TextView)
                .setText(mLanguageJson.optString("bridge_baggage_text1107"))
            (mZone2LayoutParent!!.findViewById<View>(R.id.price_list_sub_text22) as TextView)
                .setText(mLanguageJson.optString("bridge_baggage_text1108"))
            (mZone3LayoutParent!!.findViewById<View>(R.id.price_list_sub_text1) as TextView)
                .setText(mLanguageJson.optString("bridge_baggage_text1120"))
            (mZone3LayoutParent!!.findViewById<View>(R.id.price_list_sub_text2) as TextView)
                .setText(mLanguageJson.optString("bridge_baggage_text1121"))
            (mZone3LayoutParent!!.findViewById<View>(R.id.price_list_sub_text3) as TextView)
                .setText(mLanguageJson.optString("bridge_baggage_text1122"))
            (mZone3LayoutParent!!.findViewById<View>(R.id.price_list_sub_text4) as TextView)
                .setText(mLanguageJson.optString("bridge_baggage_text1123"))
            (mZone3LayoutParent!!.findViewById<View>(R.id.price_list_sub_text5) as TextView)
                .setText(mLanguageJson.optString("bridge_baggage_text1124"))
            (mZone3LayoutParent!!.findViewById<View>(R.id.price_list_sub_text6) as TextView)
                .setText(mLanguageJson.optString("bridge_baggage_text1125"))
            (mZone3LayoutParent!!.findViewById<View>(R.id.price_list_sub_text7) as TextView)
                .setText(mLanguageJson.optString("bridge_baggage_text1126"))
            (mZone3LayoutParent!!.findViewById<View>(R.id.price_list_sub_text8) as TextView)
                .setText(mLanguageJson.optString("bridge_baggage_text1127"))
            (mZone3LayoutParent!!.findViewById<View>(R.id.price_list_sub_text9) as TextView)
                .setText(mLanguageJson.optString("bridge_baggage_text1128"))
            (mZone3LayoutParent!!.findViewById<View>(R.id.price_list_sub_text10) as TextView)
                .setText(mLanguageJson.optString("bridge_baggage_text1129"))
            (mZone3LayoutParent!!.findViewById<View>(R.id.price_list_sub_text11) as TextView)
                .setText(mLanguageJson.optString("bridge_baggage_text1130"))
            (mZone3LayoutParent!!.findViewById<View>(R.id.price_list_sub_text12) as TextView)
                .setText(mLanguageJson.optString("bridge_baggage_text1131"))
            (mZone3LayoutParent!!.findViewById<View>(R.id.price_list_sub_text13) as TextView)
                .setText(mLanguageJson.optString("bridge_baggage_text1132"))
            (mZone3LayoutParent!!.findViewById<View>(R.id.price_list_sub_text14) as TextView)
                .setText(mLanguageJson.optString("bridge_baggage_text1133"))
            (mZone3LayoutParent!!.findViewById<View>(R.id.price_list_sub_text15) as TextView)
                .setText(mLanguageJson.optString("bridge_baggage_text1134"))
            (mZone3LayoutParent!!.findViewById<View>(R.id.price_list_sub_text16) as TextView)
                .setText(mLanguageJson.optString("bridge_baggage_text1135"))
            (mZone3LayoutParent!!.findViewById<View>(R.id.price_list_sub_text17) as TextView)
                .setText(mLanguageJson.optString("bridge_baggage_text1136"))
            (mZone3LayoutParent!!.findViewById<View>(R.id.price_list_sub_text18) as TextView)
                .setText(mLanguageJson.optString("bridge_baggage_text1137"))
            (mZone3LayoutParent!!.findViewById<View>(R.id.price_list_sub_text19) as TextView)
                .setText(mLanguageJson.optString("bridge_baggage_text1138"))
            (mZone3LayoutParent!!.findViewById<View>(R.id.price_list_sub_text20) as TextView)
                .setText(mLanguageJson.optString("bridge_baggage_text1139"))
            (mZone3LayoutParent!!.findViewById<View>(R.id.price_list_sub_text21) as TextView)
                .setText(mLanguageJson.optString("bridge_baggage_text1140"))
            (mZone3LayoutParent!!.findViewById<View>(R.id.price_list_sub_text22) as TextView)
                .setText(mLanguageJson.optString("bridge_baggage_text1141"))
            (mZone4LayoutParent!!.findViewById<View>(R.id.price_list_sub_text1) as TextView)
                .setText(mLanguageJson.optString("bridge_baggage_text1153"))
            (mZone4LayoutParent!!.findViewById<View>(R.id.price_list_sub_text2) as TextView)
                .setText(mLanguageJson.optString("bridge_baggage_text1154"))
            (mZone4LayoutParent!!.findViewById<View>(R.id.price_list_sub_text3) as TextView)
                .setText(mLanguageJson.optString("bridge_baggage_text1155"))
            (mZone4LayoutParent!!.findViewById<View>(R.id.price_list_sub_text4) as TextView)
                .setText(mLanguageJson.optString("bridge_baggage_text1156"))
            (mZone4LayoutParent!!.findViewById<View>(R.id.price_list_sub_text5) as TextView)
                .setText(mLanguageJson.optString("bridge_baggage_text1157"))
            (mZone4LayoutParent!!.findViewById<View>(R.id.price_list_sub_text6) as TextView)
                .setText(mLanguageJson.optString("bridge_baggage_text1158"))
            (mZone4LayoutParent!!.findViewById<View>(R.id.price_list_sub_text7) as TextView)
                .setText(mLanguageJson.optString("bridge_baggage_text1159"))
            (mZone4LayoutParent!!.findViewById<View>(R.id.price_list_sub_text8) as TextView)
                .setText(mLanguageJson.optString("bridge_baggage_text1160"))
            (mZone4LayoutParent!!.findViewById<View>(R.id.price_list_sub_text9) as TextView)
                .setText(mLanguageJson.optString("bridge_baggage_text1161"))
            (mZone4LayoutParent!!.findViewById<View>(R.id.price_list_sub_text10) as TextView)
                .setText(mLanguageJson.optString("bridge_baggage_text1162"))
            (mZone4LayoutParent!!.findViewById<View>(R.id.price_list_sub_text11) as TextView)
                .setText(mLanguageJson.optString("bridge_baggage_text1163"))
            (mZone4LayoutParent!!.findViewById<View>(R.id.price_list_sub_text12) as TextView)
                .setText(mLanguageJson.optString("bridge_baggage_text1164"))
            (mZone4LayoutParent!!.findViewById<View>(R.id.price_list_sub_text13) as TextView)
                .setText(mLanguageJson.optString("bridge_baggage_text1165"))
            (mZone4LayoutParent!!.findViewById<View>(R.id.price_list_sub_text14) as TextView)
                .setText(mLanguageJson.optString("bridge_baggage_text1166"))
            (mZone4LayoutParent!!.findViewById<View>(R.id.price_list_sub_text15) as TextView)
                .setText(mLanguageJson.optString("bridge_baggage_text1167"))
            (mZone4LayoutParent!!.findViewById<View>(R.id.price_list_sub_text16) as TextView)
                .setText(mLanguageJson.optString("bridge_baggage_text1168"))
            (mZone4LayoutParent!!.findViewById<View>(R.id.price_list_sub_text17) as TextView)
                .setText(mLanguageJson.optString("bridge_baggage_text1169"))
            (mZone4LayoutParent!!.findViewById<View>(R.id.price_list_sub_text19) as TextView)
                .setText(mLanguageJson.optString("bridge_baggage_text1171"))
            (mZone4LayoutParent!!.findViewById<View>(R.id.price_list_sub_text20) as TextView)
                .setText(mLanguageJson.optString("bridge_baggage_text1170"))
            (mZone4LayoutParent!!.findViewById<View>(R.id.price_list_sub_text18) as TextView)
                .setText(mLanguageJson.optString("bridge_baggage_text1172"))
            (mZone4LayoutParent!!.findViewById<View>(R.id.price_list_sub_text21) as TextView)
                .setText(mLanguageJson.optString("bridge_baggage_text1173"))
            (mZone4LayoutParent!!.findViewById<View>(R.id.price_list_sub_text22) as TextView)
                .setText(mLanguageJson.optString("bridge_baggage_text1174"))
        }

        private fun initOnClick() {
            mContentView.findViewById<View>(R.id.reservation_start_popup_close_btn)
                .setOnClickListener(this)
            mContentView.findViewById<View>(R.id.price_list_popup_complete_btn)
                .setOnClickListener(this)
            mContentView.findViewById<View>(R.id.price_list_text12)
                .setOnClickListener {
                    if (mZone1LayoutisShowing) {
                        (mContentView.findViewById<View>(R.id.price_list_text12) as TextView)
                            .setTextColor(getResources().getColor(R.color.color_000000))
                        mZone1Layout!!.visibility = View.GONE
                        mZone1TextView!!.setCompoundDrawablesWithIntrinsicBounds(
                            0,
                            0,
                            R.drawable.arrow_main_up,
                            0
                        )
                    } else {
                        (mContentView.findViewById<View>(R.id.price_list_text12) as TextView)
                            .setTextColor(getResources().getColor(R.color.color_15a6df))
                        mZone1Layout!!.visibility = View.VISIBLE
                        mZone1TextView!!.setCompoundDrawablesWithIntrinsicBounds(
                            0,
                            0,
                            R.drawable.arrow_main_down,
                            0
                        )
                    }
                    mZone1LayoutisShowing = !mZone1LayoutisShowing
                }
            mContentView.findViewById<View>(R.id.price_list_text13)
                .setOnClickListener {
                    if (mZone2LayoutisShowing) {
                        (mContentView.findViewById<View>(R.id.price_list_text13) as TextView)
                            .setTextColor(getResources().getColor(R.color.color_000000))
                        mZone2Layout!!.visibility = View.GONE
                        mZone2TextView!!.setCompoundDrawablesWithIntrinsicBounds(
                            0,
                            0,
                            R.drawable.arrow_main_up,
                            0
                        )
                    } else {
                        (mContentView.findViewById<View>(R.id.price_list_text13) as TextView)
                            .setTextColor(getResources().getColor(R.color.color_15a6df))
                        mZone2Layout!!.visibility = View.VISIBLE
                        mZone2TextView!!.setCompoundDrawablesWithIntrinsicBounds(
                            0,
                            0,
                            R.drawable.arrow_main_down,
                            0
                        )
                    }
                    mZone2LayoutisShowing = !mZone2LayoutisShowing
                }
            mContentView.findViewById<View>(R.id.price_list_text14)
                .setOnClickListener {
                    if (mZone3LayoutisShowing) {
                        (mContentView.findViewById<View>(R.id.price_list_text14) as TextView)
                            .setTextColor(getResources().getColor(R.color.color_000000))
                        mZone3Layout!!.visibility = View.GONE
                        mZone3TextView!!.setCompoundDrawablesWithIntrinsicBounds(
                            0,
                            0,
                            R.drawable.arrow_main_up,
                            0
                        )
                    } else {
                        (mContentView.findViewById<View>(R.id.price_list_text14) as TextView)
                            .setTextColor(getResources().getColor(R.color.color_15a6df))
                        mZone3Layout!!.visibility = View.VISIBLE
                        mZone3TextView!!.setCompoundDrawablesWithIntrinsicBounds(
                            0,
                            0,
                            R.drawable.arrow_main_down,
                            0
                        )
                    }
                    mZone3LayoutisShowing = !mZone3LayoutisShowing
                }
            mContentView.findViewById<View>(R.id.price_list_text15)
                .setOnClickListener {
                    if (mZone4LayoutisShowing) {
                        (mContentView.findViewById<View>(R.id.price_list_text15) as TextView)
                            .setTextColor(getResources().getColor(R.color.color_000000))
                        mZone4Layout!!.visibility = View.GONE
                        mZone4TextView!!.setCompoundDrawablesWithIntrinsicBounds(
                            0,
                            0,
                            R.drawable.arrow_main_up,
                            0
                        )
                    } else {
                        (mContentView.findViewById<View>(R.id.price_list_text15) as TextView)
                            .setTextColor(getResources().getColor(R.color.color_15a6df))
                        mZone4Layout!!.visibility = View.VISIBLE
                        mZone4TextView!!.setCompoundDrawablesWithIntrinsicBounds(
                            0,
                            0,
                            R.drawable.arrow_main_down,
                            0
                        )
                    }
                    mZone4LayoutisShowing = !mZone4LayoutisShowing
                }
        }

        init {
            init()
            initOnClick()
            setText()
        }
    }
}