package com.parksmt.jejuair.android16.util

import android.content.Context
import android.util.Base64
import java.io.UnsupportedEncodingException
import java.security.InvalidAlgorithmParameterException
import java.security.InvalidKeyException
import java.security.NoSuchAlgorithmException
import javax.crypto.*
import javax.crypto.spec.IvParameterSpec
import javax.crypto.spec.SecretKeySpec

/**
 * AES256 암호화
 *
 *
 * Created by ui-jun on 2017-08-21.
 */
class AES256Cipher private constructor(context: Context) {
    private var mPrivateKey: String? = null
    private val mIV: String
    //암호화
    @Throws(
        UnsupportedEncodingException::class,
        NoSuchAlgorithmException::class,
        NoSuchPaddingException::class,
        InvalidKeyException::class,
        InvalidAlgorithmParameterException::class,
        IllegalBlockSizeException::class,
        BadPaddingException::class
    )
    fun AESEncode(str: String): String {
        if (StringUtil().isNull(str)) {
            return ""
        }
        val keyData = mPrivateKey!!.toByteArray()
        val secureKey: SecretKey = SecretKeySpec(keyData, "AES")
        val c = Cipher.getInstance("AES/CBC/PKCS5Padding")
        c.init(
            Cipher.ENCRYPT_MODE,
            secureKey,
            IvParameterSpec(mIV.toByteArray())
        )
        val encrypted = c.doFinal(str.toByteArray(charset("UTF-8")))
        return Base64.encodeToString(encrypted, Base64.DEFAULT)
    }

    //복호화
    @Throws(
        UnsupportedEncodingException::class,
        NoSuchAlgorithmException::class,
        NoSuchPaddingException::class,
        InvalidKeyException::class,
        InvalidAlgorithmParameterException::class,
        IllegalBlockSizeException::class,
        BadPaddingException::class
    )
    fun AESDecode(str: String): String {
        if (StringUtil().isNull(str)) {
            return ""
        }
        val keyData = mPrivateKey!!.toByteArray()
        val secureKey: SecretKey = SecretKeySpec(keyData, "AES")
        val c = Cipher.getInstance("AES/CBC/PKCS5Padding")
        c.init(
            Cipher.DECRYPT_MODE,
            secureKey,
            IvParameterSpec(mIV.toByteArray(charset("UTF-8")))
        )
        val byteStr =
            Base64.decode(str.toByteArray(), Base64.DEFAULT)
        return String(c.doFinal(byteStr), charset("UTF-8"))
    }

    companion object {
        @Volatile
        private var INSTANCE: AES256Cipher? = null

        fun getInstance(context: Context): AES256Cipher? {
            if (INSTANCE == null) {
                synchronized(AES256Cipher::class.java) {
                    if (INSTANCE == null) {
                        INSTANCE = AES256Cipher(context)
                    }
                }
            }
            return INSTANCE
        }
    }

    init {
        var key: String = "DDD" // 임시
        if (key.length > 32) {
            mPrivateKey = key.substring(0, 32)
        } else {
            if (key.length < 16) {
                var temp = ""
                for (i in key.length..15) {
                    temp += "0"
                }
                key += temp
            }
            mPrivateKey = key.substring(0, 16)
        }
        mIV = key.substring(0, 16)
    }
}