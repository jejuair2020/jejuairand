package com.parksmt.jejuair.android16.data

import android.content.Context
import com.parksmt.jejuair.android16.common.Common
import com.parksmt.jejuair.android16.common.DLog
import com.parksmt.jejuair.android16.common.Setting
import com.parksmt.jejuair.android16.common.SharedPrefKey
import com.parksmt.jejuair.android16.util.ImageDownloadUtil
import com.parksmt.jejuair.android16.util.StringUtil
import com.parksmt.jejuair.android16.util.Util
import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject
import java.util.*

/**
 * air cafe menu info
 *
 *
 * Created by ui-jun on 2017-05-24.
 */
class AirCafeInfo(data: JSONObject) {
    val title //에어카페 타이틀
            : String
    val msgIdx //에어카페 고유번호
            : String
    val thumPath //에어카페 썸네일 URL
            : String
    val versionNo //에어카페 버전 정보
            : String
    var isDownloaded: Boolean = false
    private var contentList //에어카페 이미지URL 리스트
            : ArrayList<String>?

    fun getContentList(): ArrayList<String> {
        if (contentList == null) {
            contentList = ArrayList()
        }
        return ArrayList(contentList!!)
    }

    fun setContentList(contentList: ArrayList<String>?) {
        this.contentList = ArrayList(contentList!!)
    }

    fun toJSONObject(): JSONObject {
        val jsonObject = JSONObject()
        try {
            jsonObject.put("msgIdx", msgIdx)
            jsonObject.put("versionNo", versionNo)
            jsonObject.put("title", title)
            jsonObject.put("thumPath", thumPath)
            val array = JSONArray()
            for (content in contentList!!) {
                array.put(content)
            }
            jsonObject.put("acContDetailList", array)
            jsonObject.put("isDownloaded", isDownloaded)
        } catch (e: JSONException) {
            DLog().e("JSONException" + e)
        }
        return jsonObject
    }

    companion object {
        fun getSavedAirCafeInfo(context: Context): AirCafeInfo {
            val tag = context.javaClass.simpleName
            var airCafeInfo: AirCafeInfo? = null
            val json : String = Common.getString(
                SharedPrefKey.AIR_CAFE_INFO,
                "",
                context
            ).toString()
            if (StringUtil().isNotNull(json)) {
                try {
                    val data = JSONObject(json)
                    var airCafeInfo = AirCafeInfo(data)
                } catch (e: JSONException) {
                    DLog().e("JSONException" + e)
                }
            }
            return airCafeInfo!!
        }

        fun setAirCafeInfoList(context: Context, array: JSONArray) {
            var version: String =
                Setting().getAircafeVersion(context).toString()
            var airCafeInfo: AirCafeInfo
            var latestAirCafeInfo: AirCafeInfo? = null
            var i = 0
            while (array != null && i < array.length()) {
                airCafeInfo = AirCafeInfo(array.optJSONObject(i))
                if (Util().compareVersion(
                        version,
                        airCafeInfo.versionNo
                    )
                ) {
                    version = airCafeInfo.versionNo
                    latestAirCafeInfo = airCafeInfo
                }
                i++
            }
            if (latestAirCafeInfo != null) { //            airCafeInfo = getSavedAirCafeInfo(context);
//            if (airCafeInfo != null) {
//                ArrayList<String> arrayList = airCafeInfo.getContentList();
//                if (arrayList != null) {
//                    for (String key : arrayList) {
//                        ImageDownloadUtil.deleteFile(context, key);
//                    }
//                }
//            }
                Setting().setAircafeVersion(
                    context,
                    latestAirCafeInfo.versionNo
                )
                Common.putString(
                    SharedPrefKey.AIR_CAFE_INFO,
                    latestAirCafeInfo.toJSONObject().toString(),
                    context!!
                )
                Common.commit(context)
                //            downloadAirCafeInfo(context, latestAirCafeInfo.getThumPath());
//            if (!Common.getBoolean(SharedPrefKey.AIR_CAFE_INITIALIZED, false, context)) {
//                downloadAirCafeInfo(context);
//                Common.putBoolean(SharedPrefKey.AIR_CAFE_INITIALIZED, true, context);
//                Common.commit(context);
//            }
            }
        }

        fun getDownloadedAirCafeInfo(context: Context): AirCafeInfo {
            val tag = context.javaClass.simpleName
            lateinit var airCafeInfo: AirCafeInfo
            val json : String = Common.getString(
                SharedPrefKey.DOWNLOADED_AIR_CAFE_INFO,
                "",
                context
            ).toString()
            if (StringUtil().isNotNull(json)) {
                try {
                    val data = JSONObject(json)
                    airCafeInfo = AirCafeInfo(data)
                } catch (e: JSONException) {
                    DLog().e("JSONException" + e)
                }
            }
            return airCafeInfo
        }

        fun downloadAirCafeInfo(context: Context) {
            Thread(Runnable {
                val airCafeInfo : AirCafeInfo =
                    getSavedAirCafeInfo(context)
                if (airCafeInfo != null) {
                    val arrayList =
                        airCafeInfo.getContentList()
                    var path: String
                    for (i in arrayList.indices) {
                        path = arrayList[i]
                        try {
                            ImageDownloadUtil().downloadImageFromUrl(context, path)
                        } catch (e: Exception) {
                            DLog().e("ImageDownloadUtil" + e)
                        }
                    }
                    airCafeInfo.isDownloaded = true
                    Common.putString(
                        SharedPrefKey.DOWNLOADED_AIR_CAFE_INFO,
                        airCafeInfo.toJSONObject().toString(),
                        context
                    )
                    Common.commit(context)
                }
            }).start()
        }

        private fun downloadAirCafeInfo(
            context: Context,
            path: String
        ) {
            Thread(Runnable {
                try {
                    ImageDownloadUtil().downloadImageFromUrl(context, path)
                } catch (e: Exception) {
                    DLog().e("ImageDownloadUtil" +  e)
                }
            }).start()
        }
    }

    init {
        versionNo = data.optString("versionNo")
        title = data.optString("title")
        msgIdx = data.optString("msgIdx")
        thumPath = data.optString("thumPath")
        contentList = ArrayList()
        val array = data.optJSONArray("acContDetailList")
        var i = 0
        while (array != null && i < array.length()) {
            contentList!!.add(array.optString(i))
            i++
        }
        isDownloaded = data.optBoolean("isDownloaded", false)
    }
}