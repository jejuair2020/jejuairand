package com.parksmt.jejuair.android16.util

import android.content.Context
import android.graphics.Typeface
import android.text.InputFilter
import android.text.InputType
import android.text.SpannableStringBuilder
import android.text.Spanned
import android.text.style.AbsoluteSizeSpan
import android.text.style.ForegroundColorSpan
import android.text.style.StyleSpan
import android.util.TypedValue
import android.widget.EditText
import androidx.annotation.ColorInt
import com.parksmt.jejuair.android16.common.DLog
import java.util.regex.Pattern

class StringUtil {


    fun isNull(str: String?): Boolean {
        try {
            return str == null || "" == str || "null" == str || str.length == 0
        } catch (e: NullPointerException) {
            return true
        }
    }

    fun isNotNull(str: String): Boolean {
        return !isNull(str)
    }

    fun append(
        spannableStringBuilder: SpannableStringBuilder,
        text: String, @ColorInt color: Int
    ): SpannableStringBuilder {
        val start = spannableStringBuilder.length
        spannableStringBuilder.append(text)
        spannableStringBuilder.setSpan(
            ForegroundColorSpan(color), start, spannableStringBuilder.length,
            Spanned.SPAN_EXCLUSIVE_EXCLUSIVE
        )
        return spannableStringBuilder
    }

    fun appendAndSize(
        spannableStringBuilder: SpannableStringBuilder, text: String, @ColorInt color: Int,
        size: Int
    ): SpannableStringBuilder {
        val start = spannableStringBuilder.length
        spannableStringBuilder.append(text)
        spannableStringBuilder.setSpan(
            ForegroundColorSpan(color), start, spannableStringBuilder.length,
            Spanned.SPAN_EXCLUSIVE_EXCLUSIVE
        )
        spannableStringBuilder
            .setSpan(
                AbsoluteSizeSpan(size),
                start,
                spannableStringBuilder.length,
                Spanned.SPAN_EXCLUSIVE_EXCLUSIVE
            )
        return spannableStringBuilder
    }

    fun appendAndSizeBold(
        spannableStringBuilder: SpannableStringBuilder, text: String, @ColorInt color: Int,
        size: Int
    ): SpannableStringBuilder {
        val start = spannableStringBuilder.length
        spannableStringBuilder.append(text)
        spannableStringBuilder.setSpan(
            ForegroundColorSpan(color), start, spannableStringBuilder.length,
            Spanned.SPAN_EXCLUSIVE_EXCLUSIVE
        )
        spannableStringBuilder
            .setSpan(
                AbsoluteSizeSpan(size),
                start,
                spannableStringBuilder.length,
                Spanned.SPAN_EXCLUSIVE_EXCLUSIVE
            )
        spannableStringBuilder
            .setSpan(
                StyleSpan(Typeface.BOLD),
                start,
                spannableStringBuilder.length,
                Spanned.SPAN_EXCLUSIVE_EXCLUSIVE
            )
        return spannableStringBuilder
    }

    fun setFormatStringColor(formatString:String, arg:String, @ColorInt color:Int):SpannableStringBuilder {
        val index = formatString.indexOf(arg)
        val builder = SpannableStringBuilder(formatString)
        builder.setSpan(ForegroundColorSpan(color), index, index + arg.length, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE)
        DLog().d("formatString : $formatString   arg : $arg")
        DLog().d("indexOf : " + index + "   lastIndexOf : " + formatString.lastIndexOf(arg) + "   index + length : " + (index + arg.length))
        DLog().d("builder : $builder")
        return builder
    }

    fun appendFormatStringColor(
        spannableStringBuilder: SpannableStringBuilder, formatString: String,
        arg: String,
        @ColorInt color: Int
    ): SpannableStringBuilder {
        val index = formatString.indexOf(arg)
        spannableStringBuilder.append(formatString)
        spannableStringBuilder.setSpan(
            ForegroundColorSpan(color), index, index + arg.length,
            Spanned.SPAN_EXCLUSIVE_EXCLUSIVE
        )
        return spannableStringBuilder
    }

    fun convertSpToPixels(sp: Float, context: Context): Int {
        return TypedValue.applyDimension(
            TypedValue.COMPLEX_UNIT_SP,
            sp,
            context.resources.displayMetrics
        ).toInt()
    }


    fun setEditTextCapEnglish(editText: EditText) {
         editText.filters = arrayOf(InputFilter.AllCaps(), object: InputFilter {
             override fun filter(charSequence:CharSequence, i:Int, i1:Int, spanned: Spanned, i2:Int, i3:Int):CharSequence? {
                 val ps = Pattern.compile("^[a-zA-Z]+$")
                 if (!ps.matcher(charSequence).matches()) {
                     return ""
                 }
                 return null
             }
         })
         editText.inputType = InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD
         editText.privateImeOptions = "defaultInputmode=english;"
    }

    fun setEditTextCapEnglish(editText: EditText, length:Int) {
         editText.filters = arrayOf(InputFilter.AllCaps(), object: InputFilter {
             override fun filter(charSequence:CharSequence, i:Int, i1:Int, spanned: Spanned, i2:Int, i3:Int):CharSequence? {
                 val ps = Pattern.compile("^[a-zA-Z]+$")
                 if (!ps.matcher(charSequence).matches()) {
                     return ""
                 }
                 return null
             }
         }, InputFilter.LengthFilter(length))
         editText.inputType = InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD
         editText.privateImeOptions = "defaultInputmode=english;"
    }

    fun setEditTextLowercase(editText: EditText) {
         editText.filters = arrayOf<InputFilter>(object: InputFilter {
             override fun filter(charSequence:CharSequence, i:Int, i1:Int, spanned: Spanned, i2:Int, i3:Int):CharSequence? {
                 DLog().e("$charSequence    $spanned")
                 val ps = Pattern.compile("^[a-z0-9]+$")
                 if (!ps.matcher(charSequence).matches()) {
                     return charSequence.toString().toLowerCase()
                 }
                 return null
             }
         })
    }

    fun setEditTextLowercase(editText: EditText, length:Int) {
         editText.filters = arrayOf(object: InputFilter {
             override fun filter(charSequence:CharSequence, i:Int, i1:Int, spanned: Spanned, i2:Int, i3:Int):CharSequence? {
                 val ps = Pattern.compile("^[a-z0-9]+$")
                 if (!ps.matcher(charSequence).matches()) {
                     return ""
                 }
                 return null
             }
         }, InputFilter.LengthFilter(length))
    }

    fun setEditTextKR(editText: EditText, length:Int) {
         editText.filters = arrayOf(object: InputFilter {
             override fun filter(charSequence:CharSequence, i:Int, i1:Int, spanned: Spanned, i2:Int, i3:Int):CharSequence? {
                 val ps = Pattern.compile("^[ㄱ-ㅎㅏ-ㅣ가-힣\u318D\u119E\u11A2\u2022\u2025\u00B7\uFE55]+$")
                 if (!ps.matcher(charSequence).matches()) {
                     return ""
                 }
                 return null
             }
         }, InputFilter.LengthFilter(length))
    }

    fun isNumeric(str: String): Boolean {
        return str.matches("-?\\d+(\\.\\d+)?".toRegex())  //match a number with optional '-' and decimal.
    }

    fun checkKorName(text: String): Boolean {
        return isNotNull(text) && Pattern.compile("^[ㄱ-ㅎㅏ-ㅣ가-힣]{1,15}$").matcher(text).matches()
    }

    fun checkEngName(text: String): Boolean {
        return isNotNull(text) && Pattern.compile("^[A-Z]{1,30}$").matcher(text).matches()
    }

}