package com.parksmt.jejuair.android16

import android.content.Intent
import android.os.Bundle
import com.parksmt.jejuair.android16.airplane.AirplaneMode
import com.parksmt.jejuair.android16.base.BaseActivity
import com.parksmt.jejuair.android16.bottomtap.HomeActivity
import com.parksmt.jejuair.android16.util.Util

class Intro : BaseActivity() {
    override val uiName: String = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        if (Util().isAirplaneModeOn(this)) { //TODO 2018.05.29 yschoi [ITSM-IM00127686] 안드로이드 7.0 모바일 앱 텍스트 보이지 않는 오류
            val intent = Intent()
            intent.setClass(this, AirplaneMode::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP or Intent.FLAG_ACTIVITY_CLEAR_TOP)
            startActivity(intent)
            finish()
        } else {
            init()

        }
    }

    fun init() {
        window.decorView.postDelayed({
            val intent = Intent(this, HomeActivity::class.java)
            intent.flags = Intent.FLAG_ACTIVITY_NO_HISTORY
            startActivity(intent)
            // 액티비티 이동시 페이드인/아웃 효과를 보여준다. 즉, 인트로 화면이 부드럽게 사라진다.
            overridePendingTransition(
                android.R.anim.fade_in,
                android.R.anim.fade_out
            )
        }, 2000)
    }
}