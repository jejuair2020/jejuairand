package com.parksmt.jejuair.android16.enumdata

import com.parksmt.jejuair.android16.util.StringUtil

/**
 * 캠페인 팝업 화면 타입
 *
 */

enum class CampaignType private constructor(val code: String, val type: String) {
    //로그인:L, 마이페이지:M, 예매:P
    LOGIN("DO_NOT_SEE_TODAY_CAMPAIGN_POPUP_LOGIN", "L"),
    MY_PAGE("DO_NOT_SEE_TODAY_CAMPAIGN_POPUP_MY_PAGE", "M"),
    RESERVATION("DO_NOT_SEE_TODAY_CAMPAIGN_POPUP_RESERVATION", "P");


    companion object {

        fun getCampaignType(value: String): CampaignType {
            var state = LOGIN
            if (StringUtil().isNotNull(value)) {
                for (temp in CampaignType.values()) {
                    if (temp.code == value || temp.type == value) {
                        state = temp
                        break
                    }
                }
            }
            return state
        }
    }
}
