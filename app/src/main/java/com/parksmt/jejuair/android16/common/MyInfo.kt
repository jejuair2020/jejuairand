package com.parksmt.jejuair.android16.common

import org.json.JSONObject

/**
 * 나의 정보
 *
 *
 * Created by ui-jun on 2017-04-04.
 */
class MyInfo {

    companion object {
        private var mInstance: MyInfo? = null
        val instance: MyInfo?
            get() {
                if (mInstance == null) {
                    mInstance = MyInfo()
                }
                return mInstance
            }

        fun removeMyInfo() {
            mInstance = null
        }

        var isInitialized = false
            private set
        var balancePoint //가용포인트
                : String? = null
        var memberGrade //멤버등급
                : String? = null
        var korLSTNM //
                : String? = null
        var korFRTNM //
                : String? = null
        var engLSTNM //
                : String? = null
        var engFRTNM //
                : String? = null
        var myInqCnt //나의문의내역 카운트
                : String? = null
        var myNoticeCnt //나의알림함 카운트
                : String? = null
        var myResDepStn //나의예약 출발지
                : String? = null
        var myResArrStn //나의예약 도착지
                : String? = null
        var myResDepDate //출발일시
                : String? = null
        var myResPnrAlpha //예약번호
                : String? = null
        var myResPnrNumeric //예약번호
                : String? = null
        var myProfileImg //나의프로필 이미지 (비회원일때 리턴값)
                : String? = null
        var arrStnImg //도착취항지 이미지
                : String? = null
        var myJejuJjimCnt //나의제주 찜 카운트 (비회원일때 리턴값)
                : String? = null
        var myJejuReplCnt //나의제주 댓글 카운트 (비회원일때 리턴값)
                : String? = null
        var myCouponCnt //나의 쿠폰카운트
                : String? = null
        var myJejuJjimBgImg //나의찜 카운트 배경이미지 (비회원일때 리턴값)
                : String? = null
        var myJejuReplBgImg //나의댓글 카운트 배경이미지 (비회원일때 리턴값)
                : String? = null
        var resvDate //예약일
                : String? = null
        var routeType //국제/국내선 구분 (I/D)
                : String? = null
        var resvStatusDesc //예약상태
                : String? = null
        var myLoungeNumber //라운지이용권 번호
                : String? = null

    }


    fun setMyInfo(data: JSONObject) {
        balancePoint = data.optString("balancePoint")
        memberGrade = data.optString("memberGrade")
        korLSTNM = data.optString("korLSTNM")
        korFRTNM = data.optString("korFRTNM")
        engLSTNM = data.optString("engLSTNM")
        engFRTNM = data.optString("engFRTNM")
        myInqCnt = data.optString("myInqCnt")
        myNoticeCnt = data.optString("myNoticeCnt")
        myResDepStn = data.optString("myResDepStn")
        myResArrStn = data.optString("myResArrStn")
        myResDepDate = data.optString("myResDepDate")
        myResPnrAlpha = data.optString("myResPnrAlpha")
        myResPnrNumeric = data.optString("myResPnrNumeric")
        myProfileImg = data.optString("myProfileImg")
        arrStnImg = data.optString("arrStnImg")
        myJejuJjimCnt = data.optString("myJejuJjimCnt")
        myJejuReplCnt = data.optString("myJejuReplCnt")
        myCouponCnt = data.optString("myCouponCnt")
        myJejuJjimBgImg = data.optString("myJejuJjimBgImg")
        myJejuReplBgImg = data.optString("myJejuReplBgImg")
        resvDate = data.optString("resvDate")
        routeType = data.optString("routeType")
        resvStatusDesc = data.optString("resvStatusDesc")
        //라운지이용권 번호
        try {
            val jArr = data.getJSONArray("myLoungeBuyList")
            if (jArr.length() > 0) {
                val jObj = jArr.getJSONObject(0)
                myLoungeNumber = jObj.optString("LOUNGETICKETNO")
            }
        } catch (e: Exception) {
            myLoungeNumber = ""
        }
        isInitialized = true
    }

    val memberGradeText: String
        get() {
            var grade = ""
            when (memberGrade) {
                "S" -> grade = "SILVER"
                "U" -> grade = "SILVER+"
                "G" -> grade = "GOLD"
                "V" -> grade = "VIP"
            }
            return grade
        }

}