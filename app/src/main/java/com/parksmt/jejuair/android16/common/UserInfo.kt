package com.parksmt.jejuair.android16.common

import com.parksmt.jejuair.android16.util.StringUtil
import org.json.JSONObject

/**
 * 사용자 정보
 * Created by ui-jun on 2017-03-10.
 */
class UserInfo {
    companion object {
        private var mInstance: UserInfo? = null
        val instance: UserInfo?
            get() {
                if (mInstance == null) {
                    mInstance = UserInfo()
                }
                return mInstance
            }

        fun removeInfo() {
            mInstance = null
        }
        var isInitialized = false
            private set
        private lateinit var korLastName //국문 성
                : String
        private lateinit var korFirstName //국문 이름
                : String
        var engLastName //영문 성
                : String? = null
            private set
        var engFirstName //영문 이름
                : String? = null
            private set
        var sex //성별
                : String? = null
            private set
        var birthDate //생일
                : String? = null
            private set
        var mobilePhone //휴대폰 번호
                : String? = null
            private set
        private var smsYN //SMS 수신 여부
                : String? = null
        private var emailYN //이메일 수신여부
                : String? = null
        var memberType //회원타입 (외국인: FOR, 일반: NAT, 14세이하: CHD)
                : String? = null
            private set
        var email //이메일
                : String? = null
            private set
        var foreignYN //외국인여부
                : String? = null
            private set
        var country //거주국가
                : String? = null
        var nationality //국적
                : String? = null
            private set
        var snsLinked // SNS Login
                : String? = null
        var snsPw // 비밀번호 여부
                : String? = null
    }


    fun setUserInfo(data: JSONObject) {
        isInitialized = true
        val temp = data.optJSONObject("USER_INFO") ?: return
        korLastName = temp.optString("korLastName")
        korFirstName = temp.optString("korFirstName")
        engLastName = temp.optString("engLastName")
        engFirstName = temp.optString("engFirstName")
        sex = temp.optString("sex")
        birthDate = temp.optString("birthDate")
        mobilePhone = temp.optString("mobilePhone")
        smsYN = temp.optString("smsYN")
        emailYN = temp.optString("emailYN")
        memberType = temp.optString("memberType")
        email = temp.optString("email")
        foreignYN = temp.optString("foreignYN")
        country = temp.optString("country")
        nationality = temp.optString("nationality")
        snsLinked = temp.optString("snsLinked")
        snsPw = temp.optString("setPw")
    }

    fun setUserInfoMyInfoModify(data: JSONObject) {
        if (StringUtil().isNotNull(data.optString("bathDate"))) {
            birthDate = data.optString("bathDate")
        }
        if (StringUtil().isNotNull(data.optString("email"))) {
            email = data.optString("email")
        }
        smsYN = data.optString("smsYN")
        emailYN = data.optString("emailYN")
        var phoneNumber = data.optString("mobile")
        phoneNumber = phoneNumber.substring(phoneNumber.indexOf("-"), phoneNumber.length)
        if (StringUtil().isNotNull(phoneNumber)) {
            mobilePhone = phoneNumber
        }
        korLastName = data.optString("korLSTNM")
        korFirstName = data.optString("korFRTNM")
        engLastName = data.optString("engLSTNM")
        engFirstName = data.optString("engFRTNM")
        snsLinked = data.optString("snsLinked")
        snsPw = data.optString("setPw")
    }

    fun getKorLastName(): String? {
        return if (StringUtil().isNotNull(korLastName)) korLastName else engLastName
    }

    fun getKorFirstName(): String? {
        return if (StringUtil().isNotNull(korFirstName)) korFirstName else engFirstName
    }

    val korFullName: String
        get() = korLastName + korFirstName

    val engFullName: String
        get() = "$engLastName $engFirstName"

    fun getSmsYN(): String {
        return if (StringUtil().isNull(smsYN)) "" else smsYN!!
    }

    fun getEmailYN(): String {
        return if (StringUtil().isNull(emailYN)) "" else emailYN!!
    }

    val isForeigner: Boolean
        get() = "Y" == foreignYN

    //회원타입 (외국인: FOR, 일반: NAT, 14세이하: CHD)
    val isChildren: Boolean
        get() =//회원타입 (외국인: FOR, 일반: NAT, 14세이하: CHD)
            "CHD" == memberType



}