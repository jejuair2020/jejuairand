package com.parksmt.jejuair.android16.airplane.immigrationform

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import com.parksmt.jejuair.android16.R
import com.parksmt.jejuair.android16.airplane.AirplaneModeMenuActivity
import com.parksmt.jejuair.android16.util.PinchImageViewActivity
import kotlinx.android.synthetic.main.immigration_form_china_layout.*

/**
 * 중국 출국 / 입국신고서
 *
 *
 * Created by seungjinoh on 2017. 4. 7..
 */
class ChinaImmigrationForm : AirplaneModeMenuActivity() {
    private var mTabImage1: ImageView? = null
    private var mTabImage2: ImageView? = null
    private var mArrivalImageView: ImageView? = null
    private var subFormText1: TextView? = null
    private var subFormText2: TextView? = null
    private var subFormText3: TextView? = null
    private var subFormText4: TextView? = null
    private var subFormText5: TextView? = null
    private var subFormText6: TextView? = null
    private var subFormText7: TextView? = null
    private var subFormText8: TextView? = null
    private var subFormLayout9: View? = null
    private var subFormLayout10: View? = null
    private var subFormLayout11: View? = null
    private var subFormLayout12: View? = null
    override val uiName: String
        protected get() = "S-MUI-08-030"

    override fun onCreate(savedInstanceState: Bundle?) {
        useLanguage = false
        super.onCreate(savedInstanceState)
        setContentView(R.layout.immigration_form_china_layout)
        initView()
        setText()
        initClickEvent()
    }

    private fun initView() {
        mTabImage1 =
            findViewById(R.id.immigration_form_china_tab_image1) as ImageView
        mTabImage2 = findViewById(R.id.immigration_form_tab_image2) as ImageView
        mArrivalImageView =
            findViewById(R.id.immigration_form_china_imageview) as ImageView
        subFormText1 = findViewById(R.id.immigration_form_china_textview1) as TextView
        subFormText2 = findViewById(R.id.immigration_form_china_textview2) as TextView
        subFormText3 = findViewById(R.id.immigration_form_china_textview3) as TextView
        subFormText4 = findViewById(R.id.immigration_form_china_textview4) as TextView
        subFormText5 = findViewById(R.id.immigration_form_china_textview5) as TextView
        subFormText6 = findViewById(R.id.immigration_form_china_textview6) as TextView
        subFormText7 = findViewById(R.id.immigration_form_china_textview7) as TextView
        subFormText8 = findViewById(R.id.immigration_form_china_textview8) as TextView
        subFormLayout9 = findViewById(R.id.immigration_form_china_layout9)
        subFormLayout10 = findViewById(R.id.immigration_form_china_layout10)
        subFormLayout11 = findViewById(R.id.immigration_form_china_layout11)
        subFormLayout12 = findViewById(R.id.immigration_form_china_layout12)
    }

    private fun setText() {
        setTitleText(getString(R.string.immigration_form_china_title))
    }

    private fun initClickEvent() {
        immigration_form_china_tab_text1.setOnClickListener(View.OnClickListener {
            mTabImage1!!.visibility = View.VISIBLE
            mTabImage2!!.visibility = View.GONE
            mArrivalImageView!!.setImageResource(R.drawable.arrival_7)
            subFormText1!!.text = getString(R.string.immigration_form_china_text1)
            subFormText2!!.text = getString(R.string.immigration_form_china_text2)
            subFormText3!!.text = getString(R.string.immigration_form_china_text3)
            subFormText4!!.text = getString(R.string.immigration_form_china_text4)
            subFormText5!!.text = getString(R.string.immigration_form_china_text5)
            subFormText6!!.text = getString(R.string.immigration_form_china_text6)
            subFormText7!!.text = getString(R.string.immigration_form_china_text7)
            subFormText8!!.text = getString(R.string.immigration_form_china_text8)
            subFormLayout9!!.visibility = View.GONE
            subFormLayout10!!.visibility = View.GONE
            subFormLayout11!!.visibility = View.GONE
            subFormLayout12!!.visibility = View.GONE
        })
        immigration_form_tab_text2.setOnClickListener(View.OnClickListener {
            mTabImage1!!.visibility = View.GONE
            mTabImage2!!.visibility = View.VISIBLE
            mArrivalImageView!!.setImageResource(R.drawable.arrival_8)
            subFormText1!!.text = getString(R.string.immigration_form_china_text1_1)
            subFormText2!!.text = getString(R.string.immigration_form_china_text2_1)
            subFormText3!!.text = getString(R.string.immigration_form_china_text3_1)
            subFormText4!!.text = getString(R.string.immigration_form_china_text4_1)
            subFormText5!!.text = getString(R.string.immigration_form_china_text5_1)
            subFormText6!!.text = getString(R.string.immigration_form_china_text6_1)
            subFormText7!!.text = getString(R.string.immigration_form_china_text7_1)
            subFormText8!!.text = getString(R.string.immigration_form_china_text8_1)
            subFormLayout9!!.visibility = View.VISIBLE
            subFormLayout10!!.visibility = View.VISIBLE
            subFormLayout11!!.visibility = View.VISIBLE
            subFormLayout12!!.visibility = View.VISIBLE
        })
        immigration_form_china_imageview.setOnClickListener(View.OnClickListener {
            val intent =
                Intent(this@ChinaImmigrationForm, PinchImageViewActivity::class.java)
            //이미지 파일인 경우
            if (mTabImage1!!.visibility == View.VISIBLE) {
                intent.putExtra("path", R.drawable.arrival_7)
            } else {
                intent.putExtra("path", R.drawable.arrival_8)
            }
            startActivity(intent)
        })
    }
}