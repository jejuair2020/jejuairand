package com.parksmt.jejuair.android16.airplane

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.parksmt.jejuair.android16.airplane.AirplaneMode.AirplaneModePage
import com.parksmt.jejuair.android16.base.BaseFragment
import com.parksmt.jejuair.android16.common.IntentKey

/**
 * airplane mode fragment
 *
 *
 * Created by ui-jun on 2017-06-29.
 */
abstract class AirplaneModeFragment : BaseFragment() {
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        init()
        return super.onCreateView(inflater, container, savedInstanceState)
    }

    private fun init() {}
    /**
     * back key 가 눌렸을때의 처리
     *
     * @return true : consume event, false : not consume event
     */
    fun onBackKey(): Boolean {
        return false
    }

    @JvmOverloads
    fun goMain(position: AirplaneModePage?, intent: Intent? = null) {
        var intent = intent
        if (intent == null) {
            intent = Intent()
        }
        intent.putExtra(IntentKey.GO_MAIN_PAGE, true)
        intent.putExtra(IntentKey.MAIN_PAGE_POSITION, position)
        intent.setClass(activity!!, AirplaneMode::class.java)
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_SINGLE_TOP)
        startActivity(intent)
    }

    fun goSubPage(cls: Class<*>?) {
        val intent = Intent()
        intent.setClass(activity!!, cls!!)
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_SINGLE_TOP)
        startActivity(intent)
    }
}