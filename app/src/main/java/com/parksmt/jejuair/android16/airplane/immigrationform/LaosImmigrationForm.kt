package com.parksmt.jejuair.android16.airplane.immigrationform

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.ImageView
import com.parksmt.jejuair.android16.R
import com.parksmt.jejuair.android16.airplane.AirplaneModeMenuActivity
import com.parksmt.jejuair.android16.util.PinchImageViewActivity
import kotlinx.android.synthetic.main.immigration_form_laos_layout.*

/**
 * Created by 12154 on 2018-06-05.
 */
class LaosImmigrationForm : AirplaneModeMenuActivity() {
    private var mTabImage1: ImageView? = null
    private var mTabImage2: ImageView? = null
    private var mArrivalImageView: ImageView? = null
    protected override val uiName: String
        protected get() = "S-MUI-08-024"

    override fun onCreate(savedInstanceState: Bundle?) {
        useLanguage = false
        super.onCreate(savedInstanceState)
        setContentView(R.layout.immigration_form_laos_layout)
        initView()
        setText()
        initClickEvent()
    }

    private fun initView() {
        mTabImage1 =
            findViewById(R.id.immigration_form_laos_tab_image1) as ImageView?
        mTabImage2 =
            findViewById(R.id.immigration_form_laos_tab_image2) as ImageView?
        mArrivalImageView =
            findViewById(R.id.immigration_form_laos_imageview) as ImageView?
    }

    private fun setText() {
        setTitleText(getString(R.string.immigration_form_laos_title))
    }

    private fun initClickEvent() {
        immigration_form_laos_tab_text1.setOnClickListener(View.OnClickListener {
            mTabImage1!!.visibility = View.VISIBLE
            mTabImage2!!.visibility = View.GONE
            mArrivalImageView!!.setImageResource(R.drawable.arrival_15)
        })
        immigration_form_laos_tab_text2.setOnClickListener(View.OnClickListener {
            mTabImage1!!.visibility = View.GONE
            mTabImage2!!.visibility = View.VISIBLE
            mArrivalImageView!!.setImageResource(R.drawable.arrival_15_1)
        })
        mArrivalImageView!!.setOnClickListener {
            val intent =
                Intent(this@LaosImmigrationForm, PinchImageViewActivity::class.java)
            if (mTabImage1!!.visibility == View.VISIBLE) {
                intent.putExtra("path", R.drawable.arrival_15)
            } else {
                intent.putExtra("path", R.drawable.arrival_15_1)
            }
            startActivity(intent)
        }
        //        findViewById(R.id.immigration_form_korea_imageview).setOnClickListener(new View.OnClickListener() {
//
//            @Override
//            public void onClick(View v) {
//                Intent intent = new Intent(LaosImmigrationForm.this, PinchImageViewActivity.class);
//                //이미지 파일인 경우
//                intent.putExtra("path", R.drawable.arrival_12);
//                //이미지 경로일 경우
////                intent.putExtra("url", "http://어쩌고 저쩌고");
//                startActivity(intent);
//            }
//        });
    }
}