package com.parksmt.jejuair.android16.airplane.immigrationform

import android.content.Intent
import android.os.Bundle
import android.view.View
import com.parksmt.jejuair.android16.R
import com.parksmt.jejuair.android16.airplane.AirplaneModeMenuActivity
import com.parksmt.jejuair.android16.util.PinchImageViewActivity

/**
 * 일본 출국 / 입국신고서
 *
 *
 * Created by seungjinoh on 2017. 4. 6..
 */
class JapanImmigrationForm : AirplaneModeMenuActivity() {
    override val uiName: String
        protected get() = "S-MUI-08-028"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.japan_immigration_form_layout)
        setText()
        initClickEvent()
    }

    private fun setText() {
        setTitleText(getString(R.string.immigration_form_japan_title))
    }

    private fun initClickEvent() {
        findViewById<View>(R.id.immigration_form_japan_imageview).setOnClickListener {
            val intent =
                Intent(this@JapanImmigrationForm, PinchImageViewActivity::class.java)
            intent.putExtra("path", R.drawable.arrival_6)
            startActivity(intent)
        }
    }
}