package com.parksmt.jejuair.android16.airplane.immigrationform

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import com.parksmt.jejuair.android16.R
import com.parksmt.jejuair.android16.airplane.AirplaneModeMenuActivity
import com.parksmt.jejuair.android16.util.PinchImageViewActivity
import kotlinx.android.synthetic.main.immigration_form_philippines_layout.*

/**
 * 필리핀 입국/세관 신고서
 *
 *
 * Created by seungjinoh on 2017. 4. 7..
 */
class PhilippinesImmigrationForm : AirplaneModeMenuActivity() {
    private var mTabImage1: ImageView? = null
    private var mTabImage2: ImageView? = null
    private var mArrivalImageView: ImageView? = null
    private var subFormText1: TextView? = null
    private var subFormText2: TextView? = null
    private var subFormText3: TextView? = null
    private var subFormText4: TextView? = null
    private var subFormText5: TextView? = null
    private var subFormText6: TextView? = null
    private var subFormText7: TextView? = null
    private var subFormText8: TextView? = null
    private var subFormText9: TextView? = null
    private var subFormText10: TextView? = null
    private var subFormLayout11: View? = null
    private var subFormLayout12: View? = null
    private var subFormLayout13: View? = null
    private var subFormLayout14: View? = null
    private var subFormLayout15: View? = null
    private var subFormLayout16: View? = null
    private var subFormLayout17: View? = null
    private var subFormLayout18: View? = null
    private var subFormLayout19: View? = null
    private var subFormLayout20: View? = null
    private var subFormLayout21: View? = null
    private var subFormLayout22: View? = null
    override val uiName: String
        protected get() = "S-MUI-08-032"

    override fun onCreate(savedInstanceState: Bundle?) {
        useLanguage = false
        super.onCreate(savedInstanceState)
        setContentView(R.layout.immigration_form_philippines_layout)
        initView()
        setText()
        initClickEvent()
    }

    private fun initView() {
        mTabImage1 =
            findViewById(R.id.immigration_form_philippines_tab_image1) as ImageView
        mTabImage2 =
            findViewById(R.id.immigration_form_philippines_tab_image2) as ImageView
        mArrivalImageView =
            findViewById(R.id.immigration_form_philippines_imageview) as ImageView
        subFormText1 = findViewById(R.id.immigration_form_philippines_textview1) as TextView
        subFormText2 = findViewById(R.id.immigration_form_philippines_textview2) as TextView
        subFormText3 = findViewById(R.id.immigration_form_philippines_textview3) as TextView
        subFormText4 = findViewById(R.id.immigration_form_philippines_textview4) as TextView
        subFormText5 = findViewById(R.id.immigration_form_philippines_textview5) as TextView
        subFormText6 = findViewById(R.id.immigration_form_philippines_textview6) as TextView
        subFormText7 = findViewById(R.id.immigration_form_philippines_textview7) as TextView
        subFormText8 = findViewById(R.id.immigration_form_philippines_textview8) as TextView
        subFormText9 = findViewById(R.id.immigration_form_philippines_textview9) as TextView
        subFormText10 = findViewById(R.id.immigration_form_philippines_textview10) as TextView
        subFormLayout11 = findViewById(R.id.immigration_form_philippines_layout11)
        subFormLayout12 = findViewById(R.id.immigration_form_philippines_layout12)
        subFormLayout13 = findViewById(R.id.immigration_form_philippines_layout13)
        subFormLayout14 = findViewById(R.id.immigration_form_philippines_layout14)
        subFormLayout15 = findViewById(R.id.immigration_form_philippines_layout15)
        subFormLayout16 = findViewById(R.id.immigration_form_philippines_layout16)
        subFormLayout17 = findViewById(R.id.immigration_form_philippines_layout17)
        subFormLayout18 = findViewById(R.id.immigration_form_philippines_layout18)
        subFormLayout19 = findViewById(R.id.immigration_form_philippines_layout19)
        subFormLayout20 = findViewById(R.id.immigration_form_philippines_layout20)
        subFormLayout21 = findViewById(R.id.immigration_form_philippines_layout21)
        subFormLayout22 = findViewById(R.id.immigration_form_philippines_layout22)
    }

    private fun setText() {
        setTitleText(getString(R.string.immigration_form_philippines_title))
    }

    private fun initClickEvent() {
        immigration_form_philippines_tab_text1.setOnClickListener(View.OnClickListener {
            mTabImage1!!.visibility = View.VISIBLE
            mTabImage2!!.visibility = View.GONE
            mArrivalImageView!!.setImageResource(R.drawable.arrival_9)
            subFormText1!!.text = getString(R.string.immigration_form_philippines_text1)
            subFormText2!!.text = getString(R.string.immigration_form_philippines_text2)
            subFormText3!!.text = getString(R.string.immigration_form_philippines_text3)
            subFormText4!!.text = getString(R.string.immigration_form_philippines_text4)
            subFormText5!!.text = getString(R.string.immigration_form_philippines_text5)
            subFormText6!!.text = getString(R.string.immigration_form_philippines_text6)
            subFormText7!!.text = getString(R.string.immigration_form_philippines_text7)
            subFormText8!!.text = getString(R.string.immigration_form_philippines_text8)
            subFormText9!!.text = getString(R.string.immigration_form_philippines_text9)
            subFormText10!!.text = getString(R.string.immigration_form_philippines_text10)
            subFormLayout11!!.visibility = View.GONE
            subFormLayout12!!.visibility = View.GONE
            subFormLayout13!!.visibility = View.GONE
            subFormLayout14!!.visibility = View.GONE
            subFormLayout15!!.visibility = View.GONE
            subFormLayout16!!.visibility = View.GONE
            subFormLayout17!!.visibility = View.GONE
            subFormLayout18!!.visibility = View.GONE
            subFormLayout19!!.visibility = View.GONE
            subFormLayout20!!.visibility = View.GONE
            subFormLayout21!!.visibility = View.GONE
            subFormLayout22!!.visibility = View.GONE
        })
        immigration_form_philippines_tab_text2.setOnClickListener(View.OnClickListener {
            mTabImage1!!.visibility = View.GONE
            mTabImage2!!.visibility = View.VISIBLE
            mArrivalImageView!!.setImageResource(R.drawable.arrival_10)
            subFormText1!!.text = getString(R.string.immigration_form_philippines_text1_1)
            subFormText2!!.text = getString(R.string.immigration_form_philippines_text2_1)
            subFormText3!!.text = getString(R.string.immigration_form_philippines_text3_1)
            subFormText4!!.text = getString(R.string.immigration_form_philippines_text4_1)
            subFormText5!!.text = getString(R.string.immigration_form_philippines_text5_1)
            subFormText6!!.text = getString(R.string.immigration_form_philippines_text6_1)
            subFormText7!!.text = getString(R.string.immigration_form_philippines_text7_1)
            subFormText8!!.text = getString(R.string.immigration_form_philippines_text8_1)
            subFormText9!!.text = getString(R.string.immigration_form_philippines_text9_1)
            subFormText10!!.text = getString(R.string.immigration_form_philippines_text10_1)
            subFormLayout11!!.visibility = View.VISIBLE
            subFormLayout12!!.visibility = View.VISIBLE
            subFormLayout13!!.visibility = View.VISIBLE
            subFormLayout14!!.visibility = View.VISIBLE
            subFormLayout15!!.visibility = View.VISIBLE
            subFormLayout16!!.visibility = View.VISIBLE
            subFormLayout17!!.visibility = View.VISIBLE
            subFormLayout18!!.visibility = View.VISIBLE
            subFormLayout19!!.visibility = View.VISIBLE
            subFormLayout20!!.visibility = View.VISIBLE
            subFormLayout21!!.visibility = View.VISIBLE
            subFormLayout22!!.visibility = View.VISIBLE
        })
        mArrivalImageView!!.setOnClickListener {
            val intent =
                Intent(this@PhilippinesImmigrationForm, PinchImageViewActivity::class.java)
            if (mTabImage1!!.visibility == View.VISIBLE) {
                intent.putExtra("path", R.drawable.arrival_9)
            } else {
                intent.putExtra("path", R.drawable.arrival_10)
            }
            startActivity(intent)
        }
    }
}