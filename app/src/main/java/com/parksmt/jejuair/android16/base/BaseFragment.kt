package com.parksmt.jejuair.android16.base

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import androidx.annotation.IdRes
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import com.parksmt.jejuair.android16.common.DLog
import com.parksmt.jejuair.android16.util.Util
import org.json.JSONObject

/**
 * Created by jun on 2017-02-16.
 */
abstract class BaseFragment : Fragment() {

    var mLanguageJson: JSONObject = JSONObject()


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        DLog().d("onCreateView")
        loadCommonLanguage()
        return super.onCreateView(inflater, container, savedInstanceState)
    }

    @JvmOverloads
    fun startFragment(
        containerViewId: Int, cls: Class<*>,
        data: Bundle? = null
    ) {
        hideKeyBoard()
        Util().startFragment(
            activity!!.supportFragmentManager,
            containerViewId,
            cls,
            data
        )
    }

    @JvmOverloads
    fun startFragmentAllowingStateLoss(
        @IdRes containerViewId: Int, cls: Class<*>,
        data: Bundle? = null
    ) {
        hideKeyBoard()
        Util().startFragmentAllowingStateLoss(
            activity!!.supportFragmentManager,
            containerViewId,
            cls,
            data
        )
    }

    fun loadLanguage(path: String) {
        mLanguageJson = Util().addJSONObject(
            mLanguageJson,
            this.requireContext(),
            path
        )
    }

    private fun loadCommonLanguage() {
        mLanguageJson = Util().addJSONObject(
            mLanguageJson,
            this.requireContext(),
            "com/common.json"
        )
    }

    /**
     * 모든 fragment 종료
     */
    fun finish() {
        val first =
            activity!!.supportFragmentManager.getBackStackEntryAt(0)
        activity!!.supportFragmentManager.popBackStackImmediate(
            first.id,
            FragmentManager.POP_BACK_STACK_INCLUSIVE
        )
    }

    /**
     * 해당 fragment 만 종료
     */
    fun close() {
        fragmentManager!!.popBackStack()
    }

    fun hideKeyBoard() {
        val view = activity!!.currentFocus
        if (view != null) {
            val imm =
                activity!!.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            imm.hideSoftInputFromWindow(view.windowToken, 0)
        }
    }
}