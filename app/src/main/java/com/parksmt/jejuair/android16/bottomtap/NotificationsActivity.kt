package com.parksmt.jejuair.android16.bottomtap

import android.os.Bundle
import android.os.PersistableBundle
import com.parksmt.jejuair.android16.R
import kotlinx.android.synthetic.main.activity_notifications.*

class NotificationsActivity : TabActivity() {
    override val contentViewId: Int
        get() = R.layout.activity_notifications

    override val navigationMenuItemId: Int
        get() = R.id.navigation_notifications

    var count = 0
    var str = "count : " + count

    override fun onCreate(savedInstanceState: Bundle?, persistentState: PersistableBundle?) {
        super.onCreate(savedInstanceState, persistentState)
        setContentView(contentViewId)

        // onCreate에 직접 구현하지 말것
    }

    override fun onResume() {
        super.onResume()
        overridePendingTransition(0, 0)
        btn2.setOnClickListener {
            count = count + 1
            str = "count : " + count
            count2.text = str
        }

        res_webview2.settings.javaScriptEnabled = true
        res_webview2.webViewClient = SslWebView()

        res_webview2.loadUrl("https://m.naver.com/")
    }
}