package com.parksmt.jejuair.android16.airplane

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.parksmt.jejuair.android16.R
import com.parksmt.jejuair.android16.airplane.baggage.*


/**
 * 수하물
 *
 *
 * Created by ui-jun on 2017-06-29.
 */
class AirplaneModeBaggage : AirplaneModeFragment(),
    View.OnClickListener {
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        super.onCreateView(inflater, container, savedInstanceState)
        val rootView =
            inflater.inflate(R.layout.baggage_content_layout, container, false)
        initOnClick(rootView)
        setText(rootView)
        return rootView
    }

    override fun onClick(v: View) {
        when (v.id) {
            R.id.baggage_text1 -> goSubPage(FreeBaggage::class.java)
            R.id.baggage_text2 -> goSubPage(cabinBaggage::class.java)
            R.id.baggage_text3 -> goSubPage(OverBaggage::class.java)
            R.id.baggage_text4 -> goSubPage(BeforeBaggage::class.java)
            R.id.baggage_text5 -> goSubPage(BridgeBaggage::class.java)
            R.id.baggage_text6 -> goSubPage(SpecialBaggage::class.java)
            R.id.baggage_text7 -> goSubPage(RejectBaggage::class.java)
            R.id.baggage_text8 -> goSubPage(ForgetBaggage::class.java)
        }
    }

    private fun setText(rootView: View) {
        loadLanguage("serviceinfo/BaggageMenuList.json")
        (rootView.findViewById<View>(R.id.baggage_text1) as TextView).text =
            mLanguageJson.optString("BaggageMenuText1001")
        (rootView.findViewById<View>(R.id.baggage_text2) as TextView).text =
            mLanguageJson.optString("BaggageMenuText1002")
        (rootView.findViewById<View>(R.id.baggage_text3) as TextView).text =
            mLanguageJson.optString("BaggageMenuText1003")
        (rootView.findViewById<View>(R.id.baggage_text4) as TextView).text =
            mLanguageJson.optString("BaggageMenuText1004")
        (rootView.findViewById<View>(R.id.baggage_text5) as TextView).text =
            mLanguageJson.optString("BaggageMenuText1005")
        (rootView.findViewById<View>(R.id.baggage_text6) as TextView).text =
            mLanguageJson.optString("BaggageMenuText1006")
        (rootView.findViewById<View>(R.id.baggage_text7) as TextView).text =
            mLanguageJson.optString("BaggageMenuText1007")
        (rootView.findViewById<View>(R.id.baggage_text8) as TextView).text =
            mLanguageJson.optString("BaggageMenuText1008")
    }

    private fun initOnClick(rootView: View) {
        rootView.findViewById<View>(R.id.baggage_text1).setOnClickListener(this)
        rootView.findViewById<View>(R.id.baggage_text2).setOnClickListener(this)
        rootView.findViewById<View>(R.id.baggage_text3).setOnClickListener(this)
        rootView.findViewById<View>(R.id.baggage_text4).setOnClickListener(this)
        rootView.findViewById<View>(R.id.baggage_text5).setOnClickListener(this)
        rootView.findViewById<View>(R.id.baggage_text6).setOnClickListener(this)
        rootView.findViewById<View>(R.id.baggage_text7).setOnClickListener(this)
        rootView.findViewById<View>(R.id.baggage_text8).setOnClickListener(this)
    }
}