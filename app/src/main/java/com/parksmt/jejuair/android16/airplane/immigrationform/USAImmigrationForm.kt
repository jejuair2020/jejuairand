package com.parksmt.jejuair.android16.airplane.immigrationform

import android.content.Intent
import android.os.Bundle
import android.text.SpannableStringBuilder
import android.view.View
import android.widget.TextView
import androidx.core.content.ContextCompat
import com.parksmt.jejuair.android16.R
import com.parksmt.jejuair.android16.airplane.AirplaneModeMenuActivity
import com.parksmt.jejuair.android16.util.PinchImageViewActivity
import com.parksmt.jejuair.android16.util.StringUtil
import kotlinx.android.synthetic.main.immigration_form_usa_layout.*

/**
 * 미국 출입국 신고서
 *
 *
 * Created by seungjinoh on 2017. 4. 7..
 */
class USAImmigrationForm : AirplaneModeMenuActivity() {
    override val uiName: String
        protected get() = "S-MUI-08-034"

    override fun onCreate(savedInstanceState: Bundle?) {
        useLanguage = false
        super.onCreate(savedInstanceState)
        setContentView(R.layout.immigration_form_usa_layout)
        setText()
        initClickEvent()
    }

    private fun setText() {
        setTitleText(getString(R.string.immigration_form_usa_title))
        var text =
            SpannableStringBuilder(getString(R.string.immigration_form_usa_text3))
        text = StringUtil().append(
            text,
            getString(R.string.immigration_form_usa_text3_1),
            ContextCompat.getColor(this, R.color.main_color)
        )
        immigration_form_usa_textview3.text = text
        text.clear()
        text.append(getString(R.string.immigration_form_usa_text12))
        text = StringUtil().append(
            text,
            getString(R.string.immigration_form_usa_text12_1),
            ContextCompat.getColor(this, R.color.main_color)
        )
        immigration_form_usa_textview12.text = text
        text.clear()
        text.append(getString(R.string.immigration_form_usa_text13))
        text = StringUtil().append(
            text,
            getString(R.string.immigration_form_usa_text13_1),
            ContextCompat.getColor(this, R.color.main_color)
        )
        immigration_form_usa_textview13.text = text
        text.clear()
        text.append(getString(R.string.immigration_form_usa_text14))
        text = StringUtil().append(
            text,
            getString(R.string.immigration_form_usa_text14_1),
            ContextCompat.getColor(this, R.color.main_color)
        )
        immigration_form_usa_textview14.text = text
        text.clear()
        text.append(getString(R.string.immigration_form_usa_text15))
        text = StringUtil().append(
            text,
            getString(R.string.immigration_form_usa_text15_1),
            ContextCompat.getColor(this, R.color.main_color)
        )
        immigration_form_usa_textview15.text = text
        text.clear()
        text.append(getString(R.string.immigration_form_usa_text16))
        text = StringUtil().append(
            text,
            getString(R.string.immigration_form_usa_text16_1),
            ContextCompat.getColor(this, R.color.main_color)
        )
        immigration_form_usa_textview16.text = text
        text.clear()
        text.append(getString(R.string.immigration_form_usa_text17))
        text = StringUtil().append(
            text,
            getString(R.string.immigration_form_usa_text17_1),
            ContextCompat.getColor(this, R.color.main_color)
        )
        immigration_form_usa_textview17.text = text
    }

    private fun initClickEvent() {
        immigration_form_usa_imageview.setOnClickListener(View.OnClickListener {
            val intent =
                Intent(this@USAImmigrationForm, PinchImageViewActivity::class.java)
            intent.putExtra("path", R.drawable.arrival_11)
            startActivity(intent)
        })
    }
}