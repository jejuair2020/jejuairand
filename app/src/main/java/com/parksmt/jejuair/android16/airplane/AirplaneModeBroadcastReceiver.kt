package com.parksmt.jejuair.android16.airplane

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import com.parksmt.jejuair.android16.common.DLog
import com.parksmt.jejuair.android16.util.Util

/**
 * airplane mode broadcast receiver
 *
 *
 * Created by ui-jun on 2017-06-30.
 */
class AirplaneModeBroadcastReceiver : BroadcastReceiver() {
    override fun onReceive(context: Context, intent: Intent) {
        DLog().d("AirplaneModeBroadcastReceiver")
        if (Util().isShowing()) {
            goAirplaneMode(context)
        }
    }

    companion object {
        fun goAirplaneMode(context: Context) {
            Util().restartApplication(context)
        }
    }
}