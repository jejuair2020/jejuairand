package com.parksmt.jejuair.android16.common

import android.util.Log

class DLog {
    val TAG = "ITERROR"
    val CLIENT_CODE_STACK_INDEX = 5

    /** Log Level Error  */
    fun e(message: String) {
        if (Setting.IS_TEST) Log.e(TAG, buildLogMsg(message))
    }

    /** Log Level Warning  */
    fun w(message: String) {
        if (Setting.IS_TEST) Log.w(TAG, buildLogMsg(message))
    }

    /** Log Level Information  */
    fun i(message: String) {
        if (Setting.IS_TEST) Log.i(TAG, buildLogMsg(message))
    }

    /** Log Level Debug  */
    fun d(message: String) {
        if (Setting.IS_TEST) Log.d(TAG, buildLogMsg(message))
    }

    /** Log Level Verbose  */
    fun v(message: String) {
        if (Setting.IS_TEST) Log.v(TAG, buildLogMsg(message))
    }

    fun buildLogMsg(message: String): String {
        val ste = Thread.currentThread().stackTrace[4]
        val sb = StringBuilder()
        sb.append("[")
        sb.append(ste.fileName!!.replace(".kt", ""))
        sb.append("::")
        sb.append(ste.methodName)
        sb.append("]")
        sb.append(message)
        return sb.toString()
    }

    fun getLineNumber(): String? {
        val fullClassName =
            Thread.currentThread().stackTrace[CLIENT_CODE_STACK_INDEX].className
        val className = fullClassName.substring(fullClassName.lastIndexOf(".") + 1)
        val methodName =
            Thread.currentThread().stackTrace[CLIENT_CODE_STACK_INDEX].methodName
        val lineNumber =
            Thread.currentThread().stackTrace[CLIENT_CODE_STACK_INDEX].lineNumber
        return " ($className.$methodName():$lineNumber)"
    }

}
