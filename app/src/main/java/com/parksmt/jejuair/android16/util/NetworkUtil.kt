package com.parksmt.jejuair.android16.util

import android.content.Context
import com.google.gson.Gson
import com.parksmt.jejuair.android16.common.DLog
import com.parksmt.jejuair.android16.common.Setting
import com.parksmt.jejuair.android16.common.UserToken
import java.io.*
import java.net.HttpURLConnection
import java.net.SocketTimeoutException
import java.net.URL
import java.net.URLEncoder
import java.util.*
import javax.net.ssl.HttpsURLConnection

/**
 * 네트워크 통신에 사용될 공통 함수 모음
 *
 * @author ui-jun
 */
class NetworkUtil {

    companion object {
        const val RESULT_OK = 200
        const val RESULT_FAIL = 210
        const val RESULT_FAIL_TYPE_1 = 220
        const val RESULT_FAIL_TYPE_2 = 221
        const val RESULT_FAIL_TYPE_3 = 222
        const val RESULT_FAIL_TYPE_4 = 223
        const val RESULT_FAIL_TOKEN_INVALID = 230
        const val RESULT_FAIL_CONFIRM_PASSWD = 240
        const val RESULT_FAIL_THISYEAR = 250
        const val E1501 = 260

        /**
         * http header에 전송할  key 값
         */
        const val HTTP_HEADER_DEVICE_ID = "deviceId"
        const val HTTP_HEADER_TOKEN = "token"
        const val HTTP_HEADER_MAC_ADDRESS = "macAddress"
        const val HTTP_HEADER_PUSH_KEY = "pushKey"
        const val HTTP_HEADER_MEMBER_TYPE = "memberType"
        const val HTTP_HEADER_USER_VALID_RESULT_CODE = "userValid"
        /**
         * 아래 4개의 error일 경우 재시도 alert을 띄우기 때문에 error alert을 띄우지 않음 <br></br>
         * [.HTTP_CONNECTION_ERROR_CODE], [.CONNECT_TIMEOUT_ERROR_CODE], [.JSON_EXCEPTION_ERROR_CODE],
         * [.IO_EXCEPTION_ERROR_CODE]
         */
        const val HTTP_CONNECTION_ERROR_CODE = 40000
        /**
         * 아래 4개의 error일 경우 재시도 alert을 띄우기 때문에 error alert을 띄우지 않음 <br></br>
         * [.HTTP_CONNECTION_ERROR_CODE], [.CONNECT_TIMEOUT_ERROR_CODE], [.JSON_EXCEPTION_ERROR_CODE],
         * [.IO_EXCEPTION_ERROR_CODE]
         */
        const val CONNECT_TIMEOUT_ERROR_CODE = 1005
        /**
         * 아래 4개의 error일 경우 재시도 alert을 띄우기 때문에 error alert을 띄우지 않음 <br></br>
         * [.HTTP_CONNECTION_ERROR_CODE], [.CONNECT_TIMEOUT_ERROR_CODE], [.JSON_EXCEPTION_ERROR_CODE],
         * [.IO_EXCEPTION_ERROR_CODE]
         */
        const val JSON_EXCEPTION_ERROR_CODE = 1009
        /**
         * 아래 4개의 error일 경우 재시도 alert을 띄우기 때문에 error alert을 띄우지 않음 <br></br>
         * [.HTTP_CONNECTION_ERROR_CODE], [.CONNECT_TIMEOUT_ERROR_CODE], [.JSON_EXCEPTION_ERROR_CODE],
         * [.IO_EXCEPTION_ERROR_CODE]
         */
        const val IO_EXCEPTION_ERROR_CODE = 1008

        const val tag = "NetworkUtil"
    }


    private val gson = Gson()


    /**
     * [RequestMethod]에 맞는 형식으로 해당 url로 값을 전달 한다
     *
     * @param urlString     전송할 url
     * @param requestMethod requestMethod
     * @return HttpURLConnection
     * @throws Exception exception
     */
    @Throws(Exception::class)
    fun send(urlString: String, requestMethod: RequestMethod?): HttpURLConnection {
        return send(urlString, null, null, requestMethod)
    }

    /**
     * [RequestMethod]에 맞는 형식으로 해당 url로 값을 전달 한다
     *
     * @param urlString     전송할 url
     * @param context       context
     * @param requestMethod requestMethod
     * @return HttpURLConnection
     * @throws Exception exception
     */
    @Throws(Exception::class)
    fun send(
        urlString: String,
        context: Context,
        requestMethod: RequestMethod?
    ): HttpURLConnection {
        return send(urlString, null, context, requestMethod)
    }

    /**
     * [RequestMethod]에 맞는 형식으로 해당 url로 값을 전달 한다
     *
     * @param urlString     전송할 url
     * @param param         파라미터
     * @param requestMethod requestMethod
     * @return HttpURLConnection
     * @throws Exception exception
     */
    @Throws(Exception::class)
    fun send(
        urlString: String,
        param: HashMap<String, String>,
        requestMethod: RequestMethod?
    ): HttpURLConnection {
        return send(urlString, param, null, requestMethod)
    }

    /**
     * * [RequestMethod.POST]로 해당 url로 값을 전달 한다
     *
     * @param urlString 전송할 url
     * @param context   context
     * @return HttpURLConnection
     * @throws Exception exception
     */
    @Throws(Exception::class)
    fun send(
        urlString: String,
        context: Context
    ): HttpURLConnection {
        return send(urlString, null, context, RequestMethod.POST, true)
    }

    /**
     * * [RequestMethod.POST]로 해당 url로 값을 전달 한다
     *
     * @param urlString 전송할 url
     * @param param     파라미터
     * @param context   context
     * @return HttpURLConnection
     * @throws Exception exception
     */
    @Throws(Exception::class)
    fun send(
        urlString: String,
        param: HashMap<String, String>,
        context: Context
    ): HttpURLConnection {
        DLog().d("API CALL : " + urlString + gson.toJson(param))
        return send(urlString, param, context, RequestMethod.POST, true)
    }
    /**
     * [RequestMethod]에 맞는 형식으로 해당 url로 값을 전달 한다
     *
     * @param urlString     전송할 url
     * @param param         파라미터
     * @param context       context
     * @param requestMethod requestMethod
     * @param printLog      로그 출력 여부
     * @return HttpURLConnection
     * @throws Exception exception
     */
    /**
     * [RequestMethod]에 맞는 형식으로 해당 url로 값을 전달 한다
     *
     * @param urlString     전송할 url
     * @param param         파라미터
     * @param context       context
     * @param requestMethod requestMethod
     * @return HttpURLConnection
     * @throws Exception exception
     */
    /**
     * [RequestMethod]에 맞는 형식으로 해당 url로 값을 전달 한다
     *
     * @param urlString 전송할 url
     * @param param     파라미터
     * @return HttpURLConnection
     * @throws Exception exception
     */
    fun send(
        urlString: String,
        param: HashMap<String, String>?,
        context: Context ?,
        requestMethod: RequestMethod? = RequestMethod.POST,
        printLog: Boolean = true
    ): HttpURLConnection {
        var connection: HttpURLConnection? = null
        if (requestMethod != null) {
            connection = when (requestMethod) {
                RequestMethod.GET, RequestMethod.DELETE -> sendGetOrDelete(
                    urlString,
                    param,
                    context,
                    requestMethod,
                    printLog
                )
                RequestMethod.POST, RequestMethod.PUT -> sendPostOrPut(
                    urlString,
                    param,
                    context,
                    requestMethod,
                    printLog
                )
            }
        }
        if (connection == null) {
            throw Exception("connection == null")
        }
        return connection
    }

    @Throws(Exception::class)
    private fun sendGetOrDelete(
        urlString: String,
        param: HashMap<String, String>?,
        context: Context?,
        requestMethod: RequestMethod,
        printLog: Boolean
    ): HttpURLConnection {
        var tag: String? = tag
        if (context != null) {
            tag = context.javaClass.simpleName
        }
        var temp = urlString
        var logString =
            "url : $urlString   requestMethod : $requestMethod\n"
        if (param != null) {
            temp += getDataString(param, requestMethod)
            for ((key, value) in param) {
                logString += "key : $key   value : $value\n"
            }
        }
        if (printLog) {
            DLog().d(logString)
        }
        val url = URL(temp)
        val connection = getHttpURLConnection(url, context)
        connection.requestMethod = requestMethod.name
        return connection
    }

    @Throws(Exception::class)
    private fun sendPostOrPut(
        urlString: String,
        param: HashMap<String, String>?,
        context: Context?,
        requestMethod: RequestMethod,
        printLog: Boolean
    ): HttpURLConnection {
        var tag: String? = tag
        if (context != null) {
            tag = context.javaClass.simpleName
        }
        var logString =
            "url : $urlString   requestMethod : $requestMethod\n"
        val url = URL(urlString)
        val connection = getHttpURLConnection(url, context)
        connection.requestMethod = requestMethod.name
        connection.doInput = true
        connection.doOutput = true
        if (param != null) {
            val paramString = getDataString(param, requestMethod)
            val os = connection.outputStream
            var writer: BufferedWriter? = null
            try {
                writer = BufferedWriter(OutputStreamWriter(os, "UTF-8"))
                writer.write(paramString)
                writer.flush()
            } finally {
                writer?.close()
                os?.close()
            }
            for ((key, value) in param) {
                logString += "key : $key   value : $value\n"
            }
        }
        if (printLog) {
            DLog().d(logString)
        }
        return connection
    }

    @Throws(UnsupportedEncodingException::class)
    private fun getDataString(
        param: HashMap<String, String>,
        requestMethod: RequestMethod?
    ): String {
        val result = StringBuilder()
        var first = true
        for ((key, value) in param) {
            if (first) {
                if (requestMethod != null) {
                    when (requestMethod) {
                        RequestMethod.GET, RequestMethod.DELETE -> result.append("?")
                        else -> {
                        }
                    }
                }
                first = false
            } else {
                result.append("&")
            }
            if (key != null) {
                result.append(URLEncoder.encode(key, "UTF-8"))
                result.append("=")
                if (value != null) {
                    result.append(URLEncoder.encode(value, "UTF-8"))
                }
            }
        }
        return result.toString()
    }

    @Throws(Exception::class)
    fun getHttpURLConnection(
        url: URL,
        context: Context?
    ): HttpURLConnection {
        val connection: HttpURLConnection
        connection = if ("http" == url.protocol) {
            url.openConnection() as HttpURLConnection
        } else {
            url.openConnection() as HttpsURLConnection
        }
        connection.connectTimeout = Setting.HTTP_TIME_OUT
        connection.readTimeout = Setting.HTTP_TIME_OUT
        // macAddress, deviceId, token ,memberType  (GM:일반회원 , EM:이메일비회원 , SM:SNS비회원) ,pushKey
        connection.setRequestProperty(
            HTTP_HEADER_MAC_ADDRESS,
            Util().getMacAddress()
        )
        if (context != null) {
            connection.setRequestProperty(
                HTTP_HEADER_DEVICE_ID,
                Util().getDeviceUUID(context)
            )
            val userToken: UserToken = UserToken.getInstance(context)
            if (userToken.isLogin) {
                connection.setRequestProperty(HTTP_HEADER_TOKEN, userToken.token)
                connection.setRequestProperty(
                    HTTP_HEADER_PUSH_KEY,
                    userToken.pushKey
                )
                connection.setRequestProperty(
                    HTTP_HEADER_MEMBER_TYPE,
                    userToken.getMemberType()
                )
            }
            if (userToken.isSimpleLogin) {
                connection.setRequestProperty(
                    HTTP_HEADER_TOKEN,
                    userToken.simpleLoginToken
                )
            }
        }
        return connection
    }

    @Throws(Exception::class)
    fun getJsonFromHttpURLConnection(connection: HttpURLConnection): String {
        val reader =
            BufferedReader(InputStreamReader(connection.inputStream, "UTF-8"))
        val sb = StringBuilder()
        var line: String?
        try {
            while (reader.readLine().also { line = it } != null) {
                sb.append(line).append("\n")
            }
        } finally {
            reader.close()
        }
        return sb.toString()
    }

    @Throws(Exception::class)
    fun getResponseCode(connection: HttpURLConnection?): Int {
        return getResponseCode(connection, true)
    }

    @Throws(Exception::class)
    fun getResponseCode(connection: HttpURLConnection?, printLog: Boolean): Int {
        var result = 0
        try {
            if (connection != null) {
                result = connection.responseCode
                if (UserValid.getUserValid(
                        connection.getHeaderField(
                            HTTP_HEADER_USER_VALID_RESULT_CODE
                        )
                    ) == UserValid.INVALID
                ) {
                    result = RESULT_FAIL_TOKEN_INVALID
                }
            }
        } catch (e: Exception) {
            result = if (e is SocketTimeoutException) {
                if (printLog) {
                    DLog().e("SocketTimeoutException : " + e)
                }
                CONNECT_TIMEOUT_ERROR_CODE
            } else {
                if (printLog) {
                    DLog().e("Exception : " + e)
                }
                throw Exception(e)
            }
        }
        if (printLog) {
            DLog().d("responseCode : $result")
        }
        if (result != HttpURLConnection.HTTP_OK && result != RESULT_FAIL_TOKEN_INVALID) {
            result = HTTP_CONNECTION_ERROR_CODE
        }
        return result
    }

    //메인까지 실행되는 동안 api 익셉션시 로그전달. 1차로 메인까지만.. -김학동T-
    fun sendHistorySaveLog(
        ctx: Context,
        descType: String,
        detailDesc: String
    ) {
        HistorySaveAsyncTask(ctx, descType, detailDesc).execute()
    }

    //.do까지만..
    fun getUrlDescType(path: String): String {
        return if (path.lastIndexOf("/") < 0) "" else path.substring(path.lastIndexOf("/"))
    }

    /**
     * request method
     *
     * @author ui-jun
     */
    enum class RequestMethod {
        GET, PUT, POST, DELETE
    }

    /**
     * 토큰 유효성체크
     */
    enum class UserValid(val code: String) {
        /**
         * 헤더에 토큰값이 존재하고 유효할경우
         */
        VALID("valid"),
        /**
         * 헤더에 토큰값이 존재하고 유효하지 않을경우
         */
        INVALID("invalid"),
        /**
         * 토큰값이 존재하지 않는 미로그인 사용자가 요청시
         */
        NO_LOGIN("nologin");

        companion object {
            fun getUserValid(value: String): UserValid {
                var state = NO_LOGIN
                if (StringUtil().isNotNull(value)) {
                    for (temp in values()) {
                        if (temp.code == value) {
                            state = temp
                            break
                        }
                    }
                }
                //Log.d("UserValid", "value : " + value + "  name : " + state.name());
                return state
            }
        }

    }
}