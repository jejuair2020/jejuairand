package com.parksmt.jejuair.android16.data

import android.content.Context
import android.util.Log
import com.parksmt.jejuair.android16.common.Common
import com.parksmt.jejuair.android16.common.DLog
import com.parksmt.jejuair.android16.common.Setting
import com.parksmt.jejuair.android16.common.SharedPrefKey
import com.parksmt.jejuair.android16.util.ImageDownloadUtil
import com.parksmt.jejuair.android16.util.StringUtil
import com.parksmt.jejuair.android16.util.Util
import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject
import java.util.*

/**
 * 기내면세 info
 *
 *
 * Created by yschoi on 2018-07-25.
 */
class DutyFreeInfo {
    var title //기내면세 타이틀
            : String? = null
        private set
    var msgIdx //기내면세 고유번호
            : String? = null
        private set
    var versionNo //기내면세 버전 정보
            : String? = null
        private set
    var thumPath //기내면세 썸네일 URL
            : String? = null
        private set
    var useYn //사용여부 Y/N
            : String? = null
        private set
    private var contentList //기내면세 이미지URL 리스트
            : ArrayList<String>? = null
    var isDownloaded = false

    constructor() {}
    constructor(data: JSONObject) {
        title = data.optString("title")
        msgIdx = data.optString("msgIdx")
        versionNo = data.optString("versionNo")
        thumPath = data.optString("thumPath")
        useYn = data.optString("useYn")
        contentList = ArrayList()
        val array = data.optJSONArray("dfsContDetailList")
        var i = 0
        while (array != null && i < array.length()) {
            contentList!!.add(array.optString(i))
            i++
        }
        isDownloaded = data.optBoolean("isDownloaded", false)
    }

    fun getContentList(): ArrayList<String> {
        if (contentList == null) {
            contentList = ArrayList()
        }
        return ArrayList(contentList!!)
    }

    fun toJSONObject(): JSONObject {
        val jsonObject = JSONObject()
        try {
            jsonObject.put("title", title)
            jsonObject.put("msgIdx", msgIdx)
            jsonObject.put("versionNo", versionNo)
            jsonObject.put("thumPath", thumPath)
            jsonObject.put("useYn", useYn)
            jsonObject.put("isDownloaded", isDownloaded)
            val array = JSONArray()
            for (content in contentList!!) {
                array.put(content)
            }
            jsonObject.put("dfsContDetailList", array)
        } catch (e: JSONException) {
            DLog().e("JSONException" + e)
        }
        return jsonObject
    }

    companion object {
        fun getSavedDutyFreeInfo(context: Context): DutyFreeInfo? {
            val tag = context.javaClass.simpleName
            var dutyFreeInfo: DutyFreeInfo? = null
            val json : String = Common.getString(
                SharedPrefKey.DUTY_FREE_INFO,
                "",
                context
            ).toString()
            if (StringUtil().isNotNull(json)) {
                try {
                    val data = JSONObject(json)
                    dutyFreeInfo = DutyFreeInfo(data)
                } catch (e: JSONException) {
                    DLog().e("JSONException" + e)
                }
            }
            return dutyFreeInfo
            //        String tag = context.getClass().getSimpleName();
//        ArrayList<DutyFreeInfo> dutyFreeList = new ArrayList<>();
//        DutyFreeInfo dutyFreeInfo;
//        String json = Common.getString(SharedPrefKey.DUTY_FREE_INFO, "", context);
//        if (StringUtil.isNotNull(json)) {
//            try {
//                JSONArray array = new JSONArray(json);
//                TreeMap<Integer, DutyFreeInfo> treeMap = new TreeMap<>();
//                for (int i = 0; i < array.length(); i++) {
//                    dutyFreeInfo = new DutyFreeInfo(array.optJSONObject(i));
//                    treeMap.put(Integer.valueOf(dutyFreeInfo.getMsgIdx()), dutyFreeInfo);
//                }
//                for (Integer key : treeMap.keySet()) {
//                    dutyFreeList.add(treeMap.get(key));
//                }
//            } catch (JSONException e) {
//                Log.e(tag, "JSONException", e);
//            }
//        }
//        return dutyFreeList;
        }

        fun setDutyFreeInfoList(context: Context, array: JSONArray?) {
            var version: String =
                Setting().getDutyFreeVersion(context).toString()
            var dutyFreeInfo: DutyFreeInfo?
            var latestDutyFreeInfo: DutyFreeInfo? = null
            var i = 0
            while (array != null && i < array.length()) {
                dutyFreeInfo = DutyFreeInfo(array.optJSONObject(i))
                if (dutyFreeInfo.useYn == "Y") {
                    if (Util().compareVersion(
                            version,
                            dutyFreeInfo.versionNo.toString()
                        )
                    ) {
                        version = dutyFreeInfo.versionNo.toString()
                        latestDutyFreeInfo = dutyFreeInfo
                    }
                }
                i++
            }
            if (latestDutyFreeInfo != null) {
                dutyFreeInfo = getSavedDutyFreeInfo(context)
                if (dutyFreeInfo != null) {
                    val arrayList =
                        dutyFreeInfo.getContentList()
                    if (arrayList != null) {
                        for (key in arrayList) {
                            ImageDownloadUtil().deleteFile(context, key)
                        }
                    }
                }
                Setting().setDutyFreeVersion(
                    context,
                    latestDutyFreeInfo.versionNo.toString()
                )
                Common.putString(
                    SharedPrefKey.DUTY_FREE_INFO,
                    latestDutyFreeInfo.toJSONObject().toString(),
                    context
                )
                Common.commit(context)
                downloadDutyFreeInfo(
                    context,
                    latestDutyFreeInfo.thumPath.toString()
                )
                if (!Common.getBoolean(
                        SharedPrefKey.DUTY_FREE_INITIALIZED,
                        false,
                        context
                    )
                ) {
                    downloadDutyFreeInfo(context)
                    Common.putBoolean(
                        SharedPrefKey.DUTY_FREE_INITIALIZED,
                        true,
                        context
                    )
                    Common.commit(context)
                }
            }
        }

        //    public static void setDutyFreeInfoList(Context context, JSONArray array) {
//        String version = Setting.getDutyFreeVersion(context);
//        DutyFreeInfo dutyFreeInfo, latestDutyFreeInfo = null, savedDutyFree;
//        HashMap<String, DutyFreeInfo> totalList = new HashMap<>();
//        ArrayList<DutyFreeInfo> savedDutyFreeInfo = getSavedDutyFreeInfo(context);
//        for (int i = 0; i < savedDutyFreeInfo.size(); i++) {
//            savedDutyFree = savedDutyFreeInfo.get(i);
//            totalList.put(savedDutyFree.getMsgIdx(), savedDutyFree);
//        }
//        for (int i = 0; array != null && i < array.length(); i++) {
//            dutyFreeInfo = new DutyFreeInfo(array.optJSONObject(i));
//            if ("N".equals(dutyFreeInfo.getUseYn())) {
//                if (totalList.containsKey(dutyFreeInfo.getMsgIdx())) {
//                    totalList.remove(dutyFreeInfo.getMsgIdx());
//                    removeDutyFreeInfo(context, dutyFreeInfo);
//                }
//            } else {
//                totalList.put(dutyFreeInfo.getMsgIdx(), dutyFreeInfo);
//            }
//        }
//        TreeMap<String, DutyFreeInfo> treeMap = new TreeMap<>();
//        for (String key : totalList.keySet()) {
//            dutyFreeInfo = totalList.get(key);
//            if (Util.compareVersion(version, dutyFreeInfo.getVersionNo())) {
//                version = dutyFreeInfo.getVersionNo();
//                latestDutyFreeInfo = dutyFreeInfo;
//            }
//            treeMap.put(key, dutyFreeInfo);
//        }
//        savedDutyFreeInfo.clear();
//        for (String key : treeMap.keySet()) {
//            savedDutyFreeInfo.add(treeMap.get(key));
//        }
//        if (latestDutyFreeInfo != null) {
//            Setting.setDutyFreeVersion(context, latestDutyFreeInfo.getVersionNo());
//            setDutyFreeInfoList(context, savedDutyFreeInfo);
//            if (!Common.getBoolean(SharedPrefKey.DUTY_FREE_INITIALIZED, false, context)) {
//                downloadDutyFreeInfo(context, latestDutyFreeInfo);
//                Common.putBoolean(SharedPrefKey.DUTY_FREE_INITIALIZED, true, context);
//                Common.commit(context);
//            }
//        }
//    }
        fun setDutyFreeInfoList(
            context: Context?,
            array: ArrayList<DutyFreeInfo>
        ) {
            val jsonArray = JSONArray()
            for (dutyfreeInfo in array) {
                jsonArray.put(dutyfreeInfo.toJSONObject())
            }
            Common.putString(
                SharedPrefKey.DUTY_FREE_INFO,
                jsonArray.toString(),
                context!!
            )
            Common.commit(context)
        }

        fun getDownloadedDutyFreeInfo(context: Context): DutyFreeInfo? {
            val tag = context.javaClass.simpleName
            var dutyFreeInfo: DutyFreeInfo? = null
            val json : String = Common.getString(
                SharedPrefKey.DOWNLOADED_DUTY_FREE_INFO,
                "",
                context
            ).toString()
            if (StringUtil().isNotNull(json)) {
                try {
                    val data = JSONObject(json)
                    dutyFreeInfo = DutyFreeInfo(data)
                } catch (e: JSONException) {
                    Log.e(tag, "JSONException", e)
                }
            } else {
                dutyFreeInfo = DutyFreeInfo()
            }
            return dutyFreeInfo
        }

        fun downloadDutyFreeInfo(context: Context) { //        downloadDutyFreeInfo(context, null);
            Thread(Runnable {
                val dutyFreeInfo =
                    getSavedDutyFreeInfo(context)
                if (dutyFreeInfo != null) {
                    val arrayList =
                        dutyFreeInfo.getContentList()
                    var path: String
                    for (i in arrayList.indices) {
                        path = arrayList[i]
                        try {
                            ImageDownloadUtil().downloadImageFromUrl(context, path)
                            Log.e(
                                "",
                                "path downloadDutyFreeInfo : " + arrayList[i]
                            )
                        } catch (e: Exception) {
                            Log.e(
                                "ImageDownloadUtil",
                                "downloadDutyFreeInfo Exception $e"
                            )
                        }
                    }
                    dutyFreeInfo.isDownloaded = true
                    Common.putString(
                        SharedPrefKey.DOWNLOADED_DUTY_FREE_INFO,
                        dutyFreeInfo.toJSONObject().toString(),
                        context
                    )
                    Common.commit(context)
                }
            }).start()
        }

        //    private static void downloadDutyFreeInfo(final Context context, final DutyFreeInfo downloadDutyFreeInfo) {
//        new Thread(new Runnable() {
//
//            @Override
//            public void run() {
//                ArrayList<DutyFreeInfo> tempList = new ArrayList<>();
//                ArrayList<DutyFreeInfo> dutyFreeList = getSavedDutyFreeInfo(context);
//                DutyFreeInfo dutyFreeInfo;
//                for (int i = 0; i < dutyFreeList.size(); i++) {
//                    dutyFreeInfo = dutyFreeList.get(i);
//                    if (downloadDutyFreeInfo == null || downloadDutyFreeInfo.getMsgIdx().equals(dutyFreeInfo.getMsgIdx())) {
//                        try {
//                            ImageDownloadUtil.downloadImageFromUrl(context, dutyFreeInfo.getThumPath());
//                        } catch (Exception e) {
//                            Log.e("ImageDownloadUtil", "Exception", e);
//                        }
//                        ArrayList<String> arrayList = dutyFreeInfo.getContentList();
//                        String path;
//                        for (int j = 0; j < arrayList.size(); j++) {
//                            path = arrayList.get(j);
//                            try {
//                                ImageDownloadUtil.downloadImageFromUrl(context, path);
//                            } catch (Exception e) {
//                                Log.e("ImageDownloadUtil", "Exception", e);
//                            }
//                        }
//                        dutyFreeInfo.setDownloaded(true);
//                    }
//                    tempList.add(dutyFreeInfo);
//                }
//                setDutyFreeInfoList(context, tempList);
//            }
//        }).start();
//    }
        private fun downloadDutyFreeInfo(
            context: Context,
            path: String
        ) {
            Thread(Runnable {
                try {
                    ImageDownloadUtil().downloadImageFromUrl(context, path)
                } catch (e: Exception) {
                    Log.e("ImageDownloadUtil", "Exception", e)
                }
            }).start()
        }

        private fun removeDutyFreeInfo(
            context: Context,
            dutyFreeInfo: DutyFreeInfo
        ) {
            try {
                ImageDownloadUtil().deleteFile(context, dutyFreeInfo.thumPath)
            } catch (e: Exception) {
                Log.e("ImageDownloadUtil", "Exception", e)
            }
            val arrayList = dutyFreeInfo.getContentList()
            var path: String
            for (j in arrayList.indices) {
                path = arrayList[j]
                try {
                    ImageDownloadUtil().deleteFile(context, path)
                } catch (e: Exception) {
                    Log.e("ImageDownloadUtil", "Exception", e)
                }
            }
        }
    }
}