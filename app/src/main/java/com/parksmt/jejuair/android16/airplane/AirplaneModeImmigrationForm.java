package com.parksmt.jejuair.android16.airplane;

import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.parksmt.jejuair.android16.R;
import com.parksmt.jejuair.android16.airplane.immigrationform.ChinaImmigrationForm;
import com.parksmt.jejuair.android16.airplane.immigrationform.GuamSaipanImmigrationForm;
import com.parksmt.jejuair.android16.airplane.immigrationform.GuamSaipanVisaImmigrationForm;
import com.parksmt.jejuair.android16.airplane.immigrationform.HongKongImmigrationForm;
import com.parksmt.jejuair.android16.airplane.immigrationform.JapanImmigrationForm;
import com.parksmt.jejuair.android16.airplane.immigrationform.KoreaImmigrationForm;
import com.parksmt.jejuair.android16.airplane.immigrationform.LaosImmigrationForm;
import com.parksmt.jejuair.android16.airplane.immigrationform.PhilippinesImmigrationForm;
import com.parksmt.jejuair.android16.airplane.immigrationform.TaiwanImmigrationForm;
import com.parksmt.jejuair.android16.airplane.immigrationform.ThailandImmigrationForm;
import com.parksmt.jejuair.android16.airplane.immigrationform.USAImmigrationForm;


/**
 * 출입국신고서 작성방법
 * <p>
 * Created by ui-jun on 2017-06-29.
 */

public class AirplaneModeImmigrationForm extends AirplaneModeFragment implements View.OnClickListener {

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View rootView = inflater.inflate(R.layout.immigration_form_content_layout, container, false);
        initOnClick(rootView);
        return rootView;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.immigration_form_text1://대한민국 입국 신고서
                startActivity(new Intent(getActivity(), KoreaImmigrationForm.class));
                break;

            case R.id.immigration_form_text2://일본 입국 신고서
                startActivity(new Intent(getActivity(), JapanImmigrationForm.class));
                break;

            case R.id.immigration_form_text3://중국 출국/입국신고서
                startActivity(new Intent(getActivity(), ChinaImmigrationForm.class));
                break;

            case R.id.immigration_form_text4://홍콩 입국 신고서
                startActivity(new Intent(getActivity(), HongKongImmigrationForm.class));
                break;

            case R.id.immigration_form_text5://대만 입국 신고서
                startActivity(new Intent(getActivity(), TaiwanImmigrationForm.class));
                break;

            case R.id.immigration_form_text6://괌/사이판 비자 면제 신청서
                startActivity(new Intent(getActivity(), GuamSaipanVisaImmigrationForm.class));
                break;

            case R.id.immigration_form_text7://괌/사이판 세관 신고서
                startActivity(new Intent(getActivity(), GuamSaipanImmigrationForm.class));
                break;

            case R.id.immigration_form_text8://필리핀 입국/세관 신고서
                startActivity(new Intent(getActivity(), PhilippinesImmigrationForm.class));
                break;

            case R.id.immigration_form_text9://태국 입국 신고서
                startActivity(new Intent(getActivity(), ThailandImmigrationForm.class));
                break;

            case R.id.immigration_form_text10://라오스 출입국 신고서
                startActivity(new Intent(getActivity(), LaosImmigrationForm.class));
                break;

            case R.id.immigration_form_text11://미국 출입국 신고서
                startActivity(new Intent(getActivity(), USAImmigrationForm.class));
                break;
        }
    }

    private void initOnClick(View rootView) {
        rootView.findViewById(R.id.immigration_form_text1).setOnClickListener(this);
        rootView.findViewById(R.id.immigration_form_text2).setOnClickListener(this);
        rootView.findViewById(R.id.immigration_form_text3).setOnClickListener(this);
        rootView.findViewById(R.id.immigration_form_text4).setOnClickListener(this);
        rootView.findViewById(R.id.immigration_form_text5).setOnClickListener(this);
        rootView.findViewById(R.id.immigration_form_text6).setOnClickListener(this);
        rootView.findViewById(R.id.immigration_form_text7).setOnClickListener(this);
        rootView.findViewById(R.id.immigration_form_text8).setOnClickListener(this);
        rootView.findViewById(R.id.immigration_form_text9).setOnClickListener(this);
        rootView.findViewById(R.id.immigration_form_text10).setOnClickListener(this);
        rootView.findViewById(R.id.immigration_form_text11).setOnClickListener(this);
    }
}