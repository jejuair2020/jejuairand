package com.parksmt.jejuair.android16.airplane.immigrationform

import android.content.Intent
import android.os.Bundle
import android.view.View
import com.parksmt.jejuair.android16.R
import com.parksmt.jejuair.android16.airplane.AirplaneModeMenuActivity
import com.parksmt.jejuair.android16.util.PinchImageViewActivity
import kotlinx.android.synthetic.main.immigration_form_guam_saipan_visa_layout.*

/**
 * Created by 12154 on 2018-06-05.
 */
class GuamSaipanVisaImmigrationForm : AirplaneModeMenuActivity() {
    protected override val uiName: String
        protected get() = "S-MUI-08-024"

    protected override fun onCreate(savedInstanceState: Bundle?) {
        useLanguage = false
        super.onCreate(savedInstanceState)
        setContentView(R.layout.immigration_form_guam_saipan_visa_layout)
        setText()
        initClickEvent()
    }

    private fun setText() {
        setTitleText(getString(R.string.immigration_form_guam_saipan_visa_title))
    }

    private fun initClickEvent() {
        immigration_form_guam_saipan_visa_imageview.setOnClickListener(View.OnClickListener {
            val intent =
                Intent(this@GuamSaipanVisaImmigrationForm, PinchImageViewActivity::class.java)
            //이미지 파일인 경우
            intent.putExtra("path", R.drawable.arrival_12)
            //이미지 경로일 경우
//                intent.putExtra("url", "http://어쩌고 저쩌고");
            startActivity(intent)
        })
    }
}