package com.parksmt.jejuair.android16.view

import android.app.Activity
import android.app.Dialog
import android.content.Context
import android.os.Bundle
import android.view.View
import android.view.WindowManager
import android.view.WindowManager.BadTokenException
import android.widget.*
import com.parksmt.jejuair.android16.R
import com.parksmt.jejuair.android16.common.DLog
import com.parksmt.jejuair.android16.util.StringUtil
import java.lang.ref.WeakReference

/**
 * 기본 alert dialog
 * Created by jun on 2017-02-10.
 */
class CommonAlertDialog : Dialog {
    private val tag = this.javaClass.simpleName
    private var mContext: Context
    private var mTitleView: TextView? = null
    private var mContentView: TextView? = null
    private var mConfirmPwd: TextView? = null
    private var mLeftButton: Button? = null
    private var mRightButton: Button? = null
    private var mOneButton: Button? = null
    private var mCloseButton: ImageButton? = null
    private var mTwoButtonLayout: LinearLayout? = null
    private var mTitle: String? = null
    private var mContent: CharSequence? = null
    private var mLeftText: String? = null
    private var mRightText: String? = null
    private var mLeftClickListener: View.OnClickListener? = null
    private var mRightClickListener: View.OnClickListener? = null
    private var mCloseClickListener: View.OnClickListener? = null
    private var isCancelable = false
    private var mDimAmount = 0.65f
    private var linPwdLayout: LinearLayout? = null
    private var edtPwd: EditText? = null
    private var isPwdViewYn: Boolean

    constructor(context: Context) : super(
        context,
        android.R.style.Theme_Translucent_NoTitleBar
    ) {
        mContext = context
        isPwdViewYn = false
    }

    constructor(context: Context, isPwdYn: Boolean) : super(
        context,
        android.R.style.Theme_Translucent_NoTitleBar
    ) {
        mContext = context
        isPwdViewYn = isPwdYn
    }

    override fun onCreate(savedInstanceState: Bundle) {
        super.onCreate(savedInstanceState)
        val lpWindow = WindowManager.LayoutParams()
        lpWindow.flags = WindowManager.LayoutParams.FLAG_DIM_BEHIND
        lpWindow.dimAmount = mDimAmount
        val window = window
        if (window != null) {
            window.attributes = lpWindow
        }
        setContentView(R.layout.alert_dialog)
        setLayout()
        setDialog()
        setMessage(mContent)
        setClickListener(mLeftClickListener, mRightClickListener)
        setButtonText(mLeftText, mRightText)
        setCancelable(isCancelable)
    }

    override fun setTitle(textId: Int) {
        mTitle = mContext.resources.getString(textId)
    }

    fun setTitle(title: String?): CommonAlertDialog {
        if (title != null) {
            mTitle = title
        }
        return this
    }

    fun setMessage(content: Int): CommonAlertDialog {
        mContent = mContext.resources.getString(content)
        return this
    }

    fun setMessage(content: String?): CommonAlertDialog {
        if (content != null) {
            mContent = content
        }
        return this
    }

    fun setMessage(content: CharSequence?): CommonAlertDialog {
        if (content != null) {
            mContent = content
        }
        return this
    }

    override fun setCancelable(flag: Boolean) {
        super.setCancelable(flag)
        isCancelable = flag
    }

    fun setPositiveButton(textId: Int): CommonAlertDialog {
        return setPositiveButton(textId, null)
    }

    fun setPositiveButton(
        textId: Int,
        listener: View.OnClickListener?
    ): CommonAlertDialog {
        return setPositiveButton(mContext.resources.getString(textId), listener)
    }

    fun setPositiveButton(
        text: String?,
        listener: View.OnClickListener?
    ): CommonAlertDialog {
        mRightText = text ?: mContext.resources.getString(R.string.alert_cancel)
        mRightClickListener = listener?.let { setOnClickListener(it) } ?: defaultOnClickListener()
        return this
    }

    fun setNegativeButton(textId: Int): CommonAlertDialog {
        return setNegativeButton(textId, null)
    }

    fun setNegativeButton(
        textId: Int,
        listener: View.OnClickListener?
    ): CommonAlertDialog {
        return setNegativeButton(mContext.resources.getString(textId), listener)
    }

    fun setNegativeButton(
        text: String?,
        listener: View.OnClickListener?
    ): CommonAlertDialog {
        mLeftText = text ?: mContext.resources.getString(R.string.alert_confirm)
        mLeftClickListener = listener?.let { setOnClickListener(it) } ?: defaultOnClickListener()
        return this
    }

    fun setDimAmount(dimAmount: Float): CommonAlertDialog {
        mDimAmount = if (dimAmount < 1.0) {
            1.0f
        } else if (dimAmount > 0) {
            0.1f
        } else {
            dimAmount
        }
        return this
    }

    fun setCloseButtonListener(listener: View.OnClickListener?): CommonAlertDialog {
        mCloseClickListener = listener
        return this
    }

    override fun show() {
        try {
            val activityWeakRef =
                WeakReference(mContext as Activity)
            if (activityWeakRef.get() != null && !activityWeakRef.get()!!.isFinishing) {
                super.show()
            } else {
                DLog().d(
                    "show activityWeakRef.get() == null || activityWeakRef.get().isFinishing()"
                )
            }
        } catch (e: ClassCastException) {
            DLog().e("ClassCastException" + e)
        } catch (e: BadTokenException) {
            DLog().e("BadTokenException" + e)
        }
    }

    override fun onStart() {
        try {
            val activityWeakRef =
                WeakReference(mContext as Activity)
            if (activityWeakRef.get() != null && !activityWeakRef.get()!!.isFinishing) {
                super.onStart()
            } else {
                DLog().d(
                    "onStart activityWeakRef.get() == null || activityWeakRef.get().isFinishing()"
                )
            }
        } catch (e: ClassCastException) {
            DLog().e("ClassCastException"+ e)
        } catch (e: BadTokenException) {
            DLog().e("BadTokenException"+ e)
        }
    }

    private fun setDialog() {
        if (StringUtil().isNull(mTitle)) {
            mTitle = mContext.getString(R.string.alert_alert)
        }
        mTitleView!!.text = mTitle
        mContentView!!.text = mContent
    }

    private fun setButtonText(left: String?, right: String?) {
        if (left != null && right != null) {
            mLeftButton!!.text = left
            mRightButton!!.text = right
        } else if (left != null) {
            mOneButton!!.text = left
        } else if (right != null) {
            mOneButton!!.text = right
        }
    }

    val editPwd: String
        get() = if (edtPwd != null) {
            edtPwd!!.text.toString()
        } else ""

    fun setConfirmPwd(confirmPwd: String?) {
        if (mConfirmPwd != null) {
            mConfirmPwd!!.visibility = View.VISIBLE
            mConfirmPwd!!.text = confirmPwd
            mConfirmPwd!!.invalidate()
        }
    }

    private fun setClickListener(
        left: View.OnClickListener?,
        right: View.OnClickListener?
    ) {
        var isOneButton = true
        if (left != null && right != null) {
            mLeftButton!!.setOnClickListener(left)
            mRightButton!!.setOnClickListener(right)
            isOneButton = false
        } else if (left != null) {
            mOneButton!!.setOnClickListener(left)
        } else if (right != null) {
            mOneButton!!.setOnClickListener(right)
        } else {
            mOneButton!!.setOnClickListener(defaultOnClickListener())
        }
        setOneButton(isOneButton)
        if (mCloseClickListener == null) {
            mCloseClickListener = left ?: (right ?: defaultOnClickListener())
        }
        mCloseButton!!.setOnClickListener(mCloseClickListener)
    }

    private fun setOneButton(isOneButton: Boolean) {
        if (isOneButton) {
            mOneButton!!.visibility = View.VISIBLE
            mTwoButtonLayout!!.visibility = View.GONE
        } else {
            mOneButton!!.visibility = View.GONE
            mTwoButtonLayout!!.visibility = View.VISIBLE
        }
    }

    private fun setLayout() {
        mTitleView =
            findViewById<View>(R.id.beacon_alert_dialog_title_text_view) as TextView
        mContentView =
            findViewById<View>(R.id.beacon_alert_dialog_msg_text_view) as TextView
        mOneButton =
            findViewById<View>(R.id.alert_dialog_one_btn) as Button
        mTwoButtonLayout =
            findViewById<View>(R.id.alert_dialog_two_btn_layout) as LinearLayout
        mLeftButton =
            findViewById<View>(R.id.beacon_alert_dialog_left_btn) as Button
        mRightButton =
            findViewById<View>(R.id.beacon_alert_dialog_right_btn) as Button
        mCloseButton = findViewById<View>(R.id.alert_dialog_close_btn) as ImageButton
        linPwdLayout = findViewById<View>(R.id.pwd_layout) as LinearLayout
        edtPwd = findViewById<View>(R.id.password_edit_text) as EditText
        if (isPwdViewYn) {
            linPwdLayout!!.visibility = View.VISIBLE
        } else {
            linPwdLayout!!.visibility = View.GONE
        }
        mConfirmPwd = findViewById<View>(R.id.no_confirm_pwd) as TextView
    }

    private fun defaultOnClickListener(): View.OnClickListener {
        return View.OnClickListener { dismiss() }
    }

    private fun setOnClickListener(listener: View.OnClickListener?): View.OnClickListener {
        return View.OnClickListener { v ->
            if (!isPwdViewYn) {
                dismiss()
            }
            listener?.onClick(v)
        }
    }
}