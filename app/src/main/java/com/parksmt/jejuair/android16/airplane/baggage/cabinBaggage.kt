package com.parksmt.jejuair.android16.airplane.baggage

import android.os.Bundle
import android.widget.TextView
import com.parksmt.jejuair.android16.R
import com.parksmt.jejuair.android16.airplane.AirplaneModeMenuActivity

/**
 * 기내 수하물 S-MUI-08-010
 *
 *
 * Created by seungjinoh on 2017. 4. 4..
 */
class cabinBaggage : AirplaneModeMenuActivity() {
    protected override val uiName: String
        protected get() = "S-MUI-08-010"

    protected override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.cabin_baggage_layout)
        setText()
    }

    private fun setText() {
        loadLanguage("serviceinfo/CabinBaggage.json")
        setTitleText(mLanguageJson.optString("CabinBaggageText1000"), null)
        (findViewById(R.id.cabin_baggage_text1) as TextView).setText(mLanguageJson.optString("CabinBaggageText1001"))
        (findViewById(R.id.cabin_baggage_text2) as TextView).setText(mLanguageJson.optString("CabinBaggageText1002"))
        (findViewById(R.id.cabin_baggage_text3) as TextView).setText(mLanguageJson.optString("CabinBaggageText1003"))
        (findViewById(R.id.cabin_baggage_text4) as TextView).setText(mLanguageJson.optString("CabinBaggageText1004"))
        (findViewById(R.id.cabin_baggage_text5) as TextView).setText(mLanguageJson.optString("CabinBaggageText1005"))
        (findViewById(R.id.cabin_baggage_text6) as TextView).setText(mLanguageJson.optString("CabinBaggageText1006"))
        (findViewById(R.id.cabin_baggage_text7) as TextView).setText(mLanguageJson.optString("CabinBaggageText1007"))
        (findViewById(R.id.cabin_baggage_text8) as TextView).setText(mLanguageJson.optString("CabinBaggageText1008"))
        (findViewById(R.id.cabin_baggage_text9) as TextView).setText(mLanguageJson.optString("CabinBaggageText1009"))
        (findViewById(R.id.cabin_baggage_text10) as TextView).setText(mLanguageJson.optString("CabinBaggageText1010"))
        (findViewById(R.id.cabin_baggage_text11) as TextView).setText(mLanguageJson.optString("CabinBaggageText1011"))
        (findViewById(R.id.cabin_baggage_text12) as TextView).setText(mLanguageJson.optString("CabinBaggageText1012"))
        (findViewById(R.id.cabin_baggage_text13) as TextView).setText(mLanguageJson.optString("CabinBaggageText_doorside_domestic")) //3000원 -> 3,000 KRW / 3 USD
        (findViewById(R.id.cabin_baggage_text14) as TextView).setText(mLanguageJson.optString("CabinBaggageText1014"))
        (findViewById(R.id.cabin_baggage_text15) as TextView).setText(mLanguageJson.optString("CabinBaggageText1015"))
        (findViewById(R.id.cabin_baggage_text16) as TextView).setText(mLanguageJson.optString("CabinBaggageText_doorside_international")) //5000원 -> 5,000 KRW / 5 USD
        (findViewById(R.id.cabin_baggage_text17) as TextView).setText(mLanguageJson.optString("CabinBaggageText1017"))
        (findViewById(R.id.cabin_baggage_text18) as TextView).setText(mLanguageJson.optString("CabinBaggageText1018"))
        (findViewById(R.id.cabin_baggage_text19) as TextView).setText(mLanguageJson.optString("CabinBaggageText1019"))
        (findViewById(R.id.cabin_baggage_text20) as TextView).setText(mLanguageJson.optString("CabinBaggageText1020"))
        (findViewById(R.id.cabin_baggage_text21) as TextView).setText(mLanguageJson.optString("CabinBaggageText1021"))
        (findViewById(R.id.cabin_baggage_text22) as TextView).setText(mLanguageJson.optString("CabinBaggageText1022"))
        (findViewById(R.id.cabin_baggage_text23) as TextView).setText(mLanguageJson.optString("CabinBaggageText1023"))
        (findViewById(R.id.cabin_baggage_text41) as TextView).setText(mLanguageJson.optString("CabinBaggageText_baggage_exchange_info")) //환율 공시
        (findViewById(R.id.cabin_baggage_text24) as TextView).setText(mLanguageJson.optString("CabinBaggageText1024"))
        (findViewById(R.id.cabin_baggage_text25) as TextView).setText(mLanguageJson.optString("CabinBaggageText1025"))
        //        ((TextView) findViewById(R.id.cabin_baggage_text26)).setText(mLanguageJson.optString("CabinBaggageText1026"));
//        ((TextView) findViewById(R.id.cabin_baggage_text27)).setText(mLanguageJson.optString("CabinBaggageText1027"));
        (findViewById(R.id.cabin_baggage_text28) as TextView).setText(mLanguageJson.optString("CabinBaggageText1028"))
        (findViewById(R.id.cabin_baggage_text29) as TextView).setText(mLanguageJson.optString("CabinBaggageText1029"))
        (findViewById(R.id.cabin_baggage_text30) as TextView).setText(mLanguageJson.optString("CabinBaggageText1030"))
        (findViewById(R.id.cabin_baggage_text31) as TextView).setText(mLanguageJson.optString("CabinBaggageText1031"))
        (findViewById(R.id.cabin_baggage_text32) as TextView).setText(mLanguageJson.optString("CabinBaggageText1032"))
        (findViewById(R.id.cabin_baggage_text33) as TextView).setText(mLanguageJson.optString("CabinBaggageText1033"))
        val unit: String = mLanguageJson.optString("CabinBaggageText1046")
        //        ((TextView) findViewById(R.id.cabin_baggage_text35)).setText(mLanguageJson.optString("CabinBaggageText_price"));
        (findViewById(R.id.cabin_baggage_text35) as TextView).setText(mLanguageJson.optString("CabinBaggageText_price"))
        (findViewById(R.id.cabin_baggage_text43) as TextView).setText(mLanguageJson.optString("CabinBaggageText_carry_on_first"))
        //        ((TextView) findViewById(R.id.cabin_baggage_text36)).setText(mLanguageJson.optString("CabinBaggageText1036"));
//        ((TextView) findViewById(R.id.cabin_baggage_text44)).setText(mLanguageJson.optString("CabinBaggageText_carry_on_second" ) );
//        ((TextView) findViewById(R.id.cabin_baggage_text37)).setText(mLanguageJson.optString("CabinBaggageText1037"));
//        ((TextView) findViewById(R.id.cabin_baggage_text45)).setText(mLanguageJson.optString("CabinBaggageText_carry_on_third_and_after") );
        (findViewById(R.id.cabin_baggage_text39) as TextView).setText(mLanguageJson.optString("CabinBaggageText1039"))
        //TODO 2018.10.11 yschoi [ITSM-IM00144386] [부가사업팀] 외국 환율 통일화에 따른 홈페이지 문구 수정 요청의 건 (모바일) -임수영S-
        (findViewById(R.id.cabin_baggage_text40) as TextView).setText(mLanguageJson.optString("CabinBaggageText_baggage_exchange_info"))
    }
}