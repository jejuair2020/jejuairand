package com.parksmt.jejuair.android16.enumdata

import com.parksmt.jejuair.android16.common.DLog
import com.parksmt.jejuair.android16.util.StringUtil

/**
 * sns 종류
 * Created by ui-jun on 2017-05-02.
 */
enum class SnsType(//웨이보
    val code: String, val title: String
) {
    NAVER("NAVER", "Naver"),  //네이버
    GOOGLE_PLUS("GOOGLE", "Google+"),  //구글+
    FACEBOOK("FACEBOOK", "Facebook"),  //페이스북
    KAKAO_TALK("KAKAO", "Kakao"),  //카카오톡
    LINE("LI", "Line"),  //라인
    WE_CHAT("WC", "wechat"),  //위챗
    WEIBO("WI", "weibo");

    companion object {
        fun getSnsType(value: String): SnsType {
            var state = NAVER
            if (StringUtil().isNotNull(value)) {
                for (temp in values()) {
                    if (temp.code == value) {
                        state = temp
                        break
                    }
                }
            }
            DLog().d( "value : " + value + "  name : " + state.name)
            return state
        }
    }

}