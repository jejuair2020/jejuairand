package com.parksmt.jejuair.android16.airplane

import android.content.Context
import android.content.Intent
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import android.widget.ImageButton
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.annotation.DrawableRes
import androidx.annotation.LayoutRes
import androidx.appcompat.app.ActionBarDrawerToggle
import androidx.appcompat.content.res.AppCompatResources
import androidx.appcompat.widget.Toolbar
import androidx.core.content.ContextCompat
import androidx.core.view.ViewCompat
import androidx.drawerlayout.widget.DrawerLayout
import com.parksmt.jejuair.android16.R
import com.parksmt.jejuair.android16.airplane.AirplaneMode.AirplaneModePage
import com.parksmt.jejuair.android16.base.BaseActivity
import com.parksmt.jejuair.android16.common.DLog
import com.parksmt.jejuair.android16.common.IntentKey
import com.parksmt.jejuair.android16.common.LanguageDialog
import com.parksmt.jejuair.android16.util.StringUtil
import com.parksmt.jejuair.android16.util.Util
import kotlinx.android.synthetic.main.airplane_mode.*

/**
 * airplane mode menu
 *
 *
 * Created by ui-jun on 2017-06-28.
 */
abstract class AirplaneModeMenuActivity : BaseActivity(),
    View.OnClickListener {
    interface OnNavigationClickListener {
        fun onNavigationClick()
    }

    private var mOnNavigationClickListener: OnNavigationClickListener? = null
    private lateinit var mDrawerLayout: DrawerLayout
    private var mMainLayout: ViewGroup? = null
    private var mToolbar: Toolbar? = null
    private var mTitleButton: TextView? = null
    /**
     * jeju air logo
     */
    private var mLogoImageView: ImageView? = null
    private var mMenuButton: ImageButton? = null
    private var mTitle: String? = null
    private var mTitleOnClickListener: View.OnClickListener? = null
    private var mLanguageDialog: LanguageDialog? = null
    override fun setContentView(@LayoutRes layoutResID: Int) {
        DLog().d("setContentView")
        super.setContentView(layoutResID)
        mDrawerLayout = main_drawer_layout
        val sideMenu : LinearLayout? =
            findViewById<View>(R.id.side_menu_layout_container) as LinearLayout?



        if (sideMenu != null) {
            val view =
                LayoutInflater.from(this).inflate(R.layout.airplane_mode_menu,sideMenu,false)
            sideMenu.removeAllViews()
            sideMenu.addView(view)
        }
        init()
        initMenu()
        initMenuOnClick()
        initTitle()
        initLanguageType()
    }

    override fun onBackPressed() {
        if (mDrawerLayout.isDrawerOpen(Gravity.RIGHT)) {
            mDrawerLayout.closeDrawer(Gravity.RIGHT)
        } else {
            super.onBackPressed()
        }
    }

    override fun onClick(v: View) {
        when (v.id) {
            R.id.main_menu_btn -> onMainMenuClick()
            R.id.airplane_mode_menu_btn1 -> setLanguage()
            R.id.airplane_mode_menu_btn2 -> { goMain(AirplaneModePage.HOME) }
            R.id.airplane_mode_menu_btn3 -> goMain(AirplaneModePage.IN_FLIGHT_SERVICE)
            R.id.airplane_mode_menu_btn5 -> goMain(AirplaneModePage.BAGGAGE)
            R.id.airplane_mode_menu_btn6 -> goMain(AirplaneModePage.IMMIGRATION_FORM)
        }
    }


    private fun init() {
        mMenuButton = findViewById<View>(R.id.main_menu_btn) as ImageButton
        mLogoImageView =
            findViewById<View>(R.id.main_logo_image_view) as ImageView
        mMainLayout = findViewById<View>(R.id.main_layout) as ViewGroup
        mToolbar =
            findViewById<View>(R.id.main_tool_bar) as Toolbar
        mTitleButton = findViewById<View>(R.id.main_title) as TextView
        setSupportActionBar(mToolbar)
        setNavigationIcon(R.drawable.main_prev_btn)
        mDrawerLayout.setScrimColor(
            AppCompatResources.getColorStateList(
                this,
                R.color.main_menu_background_color
            ).defaultColor
        )
        mDrawerLayout.drawerElevation = 0f
        val actionBarDrawerToggle: ActionBarDrawerToggle = object :
            ActionBarDrawerToggle(
                this,
                mDrawerLayout,
                mToolbar,
                R.string.drawer_open,
                R.string.drawer_close
            ) {
            override fun onDrawerSlide(
                drawerView: View,
                slideOffset: Float
            ) { //Log.d(tag, "slideOffset : " + slideOffset);
                ViewCompat.setElevation(
                    mMainLayout!!,
                    Util().convertDpToPixels( (10 * slideOffset).toFloat(), this@AirplaneModeMenuActivity).toFloat()
                )
                mMainLayout!!.translationX = -slideOffset * drawerView.width
                mMainLayout!!.scaleX = 1 - slideOffset / 10
                mMainLayout!!.scaleY = 1 - slideOffset / 10
                mDrawerLayout.bringChildToFront(drawerView)
                mDrawerLayout.requestLayout()
            }
        }
        mDrawerLayout.addDrawerListener(actionBarDrawerToggle)
        actionBarDrawerToggle.toolbarNavigationClickListener = View.OnClickListener {
            DLog().d("navigation onClick   count : " + supportFragmentManager.backStackEntryCount)
            if (supportFragmentManager.backStackEntryCount > 0) {
                val view = currentFocus
                if (view != null) {
                    val imm =
                        getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
                    imm.hideSoftInputFromWindow(view.windowToken, 0)
                }
                onBackPressed()
            } else {
                if (mOnNavigationClickListener != null) {
                    mOnNavigationClickListener!!.onNavigationClick()
                } else {
                    onBackPressed()
                }
            }
        }
        actionBarDrawerToggle.isDrawerIndicatorEnabled = false
        actionBarDrawerToggle.syncState()
        mLanguageDialog = LanguageDialog(this)
/*        mLanguageDialog!!.selection = Util().getCurrentLanguage(context = this)
        mLanguageDialog!!.setOnListSelectedListener { itemIndex, item ->
            if (Util().getCurrentLanguage(this@AirplaneModeMenuActivity) !== item) {
                Util().setLanguage(
                    this@AirplaneModeMenuActivity,
                    item.toString()
                )
                finish()
                val intent =
                    packageManager.getLaunchIntentForPackage(packageName)
                intent!!.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_SINGLE_TOP)
                startActivity(intent)
            }
        }*/
    }

    private fun initMenu() {
        loadLanguage("airplanemode/airplaneMode.json")
        (findViewById<View>(R.id.airplane_mode_menu_textview) as TextView).text =
            mLanguageJson.optString("airplaneModeText1000")
        (findViewById<View>(R.id.airplane_mode_menu_btn1) as TextView).text =
            mLanguageJson.optString("airplaneModeText1007")
        (findViewById<View>(R.id.airplane_mode_menu_btn2) as TextView).text =
            mLanguageJson.optString("airplaneModeText1001")
        (findViewById<View>(R.id.airplane_mode_menu_btn3) as TextView).text =
            mLanguageJson.optString("airplaneModeText1002")
        //        ((TextView) findViewById(R.id.airplane_mode_menu_btn4)).setText(mLanguageJson.optString("airplaneModeText1003"));
        (findViewById<View>(R.id.airplane_mode_menu_btn5) as TextView).text =
            mLanguageJson.optString("airplaneModeText1004")
        (findViewById<View>(R.id.airplane_mode_menu_btn6) as TextView).text =
            mLanguageJson.optString("airplaneModeText1005")
        //        ((TextView) findViewById(R.id.airplane_mode_menu_btn7)).setText(mLanguageJson.optString("airplaneModeText1006"));
    }

    private fun initMenuOnClick() {
        mMenuButton!!.setOnClickListener(this)
        findViewById<View>(R.id.airplane_mode_menu_btn1).setOnClickListener(this)
        findViewById<View>(R.id.airplane_mode_menu_btn2).setOnClickListener(this)
        findViewById<View>(R.id.airplane_mode_menu_btn3).setOnClickListener(this)
        //        findViewById(R.id.airplane_mode_menu_btn4).setOnClickListener(this);
        findViewById<View>(R.id.airplane_mode_menu_btn5).setOnClickListener(this)
        findViewById<View>(R.id.airplane_mode_menu_btn6).setOnClickListener(this)
        //        findViewById(R.id.airplane_mode_menu_btn7).setOnClickListener(this);
    }

    /**
     * Toolbar title 변경함<br></br>
     *
     * @param title 변경할 text 값이 null 이면 기본 logo 표시 함
     */
    fun setTitleText(title: String?) {
        setTitleText(title, null)
    }

    /**
     * * Toolbar title 변경함<br></br>
     *
     * @param title           변경할 text 값이 null 이면 기본 logo 표시 함
     * @param onClickListener onClickListener null 이 아니면 open / close 버튼 이미지 표시
     */
    fun setTitleText(
        title: String?,
        onClickListener: View.OnClickListener?
    ) {
        mTitle = title
        mTitleOnClickListener = onClickListener
        initTitle()
    }

    private fun initTitle() {
        if (StringUtil().isNull(mTitle)) {
            if (mLogoImageView != null) {
                mLogoImageView!!.visibility = View.VISIBLE
                if (mTitleOnClickListener == null) {
                    mTitleOnClickListener = View.OnClickListener { goMain(AirplaneModePage.HOME) }
                }
                mLogoImageView!!.setOnClickListener(mTitleOnClickListener)
                mLogoImageView!!.isClickable = mTitleOnClickListener != null
            }
        } else {
            if (mLogoImageView != null) {
                mLogoImageView!!.visibility = View.GONE
            }
            if (mTitleButton != null) {
                mTitleButton!!.visibility = View.VISIBLE
                mTitleButton!!.text = mTitle
                val temp = mTitleOnClickListener != null
                if (temp) {
                    mTitleButton!!.setOnClickListener(mTitleOnClickListener)
                }
                mTitleButton!!.isClickable = temp
            }
            mTitleButton!!.post {
                if (mTitleButton!!.lineCount > 1) {
                    mTitleButton!!.textSize = 14f
                }
            }
        }
    }

    private fun initLanguageType() {
        if (Util().isKorean(this)) {
            findViewById<View>(R.id.airplane_mode_menu_btn1).visibility = View.VISIBLE
            findViewById<View>(R.id.airplane_mode_menu_btn2).visibility = View.VISIBLE
            findViewById<View>(R.id.airplane_mode_menu_btn3).visibility = View.VISIBLE
            //            findViewById(R.id.airplane_mode_menu_btn4).setVisibility(View.VISIBLE);
            findViewById<View>(R.id.airplane_mode_menu_btn5).visibility = View.VISIBLE
        } else {
            findViewById<View>(R.id.airplane_mode_menu_btn6).visibility = View.GONE
            //            findViewById(R.id.airplane_mode_menu_btn7).setVisibility(View.GONE);
        }
        //TODO 2018.09.03 yschoi [ITSM-IM00139926] [모바일][AOS] 모바일 웹툰메뉴 제거요청
//        findViewById(R.id.airplane_mode_menu_btn7).setVisibility(View.GONE);
    }

    /**
     * toolbar 의 navigation icon 변경
     *
     * @param resId resId
     */
    protected fun setNavigationIcon(@DrawableRes resId: Int) {
        setNavigationIcon(resId, mOnNavigationClickListener)
    }

    /**
     * toolbar 의 navigation icon 변경
     * click listener 등록
     *
     * @param resId                     resId
     * @param onNavigationClickListener onNavigationClickListener
     */
    protected fun setNavigationIcon(@DrawableRes resId: Int, onNavigationClickListener: OnNavigationClickListener?) {
        if (resId == 0) {
            mToolbar!!.navigationIcon = null
        } else {
            mToolbar!!.navigationIcon = ContextCompat.getDrawable(this, resId)
        }
        setOnNavigationClickListener(onNavigationClickListener)
    }

    /**
     * navigation click listener 등록
     *
     * @param onNavigationClickListener onNavigationClickListener
     */
    protected fun setOnNavigationClickListener(onNavigationClickListener: OnNavigationClickListener?) {
        mOnNavigationClickListener = onNavigationClickListener
    }

    protected fun closeSideMenu(animation: Boolean) {
        mDrawerLayout.closeDrawer(Gravity.RIGHT, animation)
    }

    private fun onMainMenuClick() {
        if (mDrawerLayout.isDrawerOpen(Gravity.RIGHT)) {
            mDrawerLayout.closeDrawer(Gravity.RIGHT)
        } else {
            mDrawerLayout.openDrawer(Gravity.RIGHT)
        }
    }

    /**
     * 언어 설정 dialog를 띄움
     */
    fun setLanguage() {
        mLanguageDialog!!.show()
    }

    fun hideKeyBoard() {
        val view = currentFocus
        if (view != null) {
            val imm =
                getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            imm.hideSoftInputFromWindow(view.windowToken, 0)
        }
    }

    fun goMain(position: AirplaneModePage) {
        var intent : Intent = Intent()
        intent.putExtra(IntentKey.GO_MAIN_PAGE, true)
        intent.putExtra(IntentKey.MAIN_PAGE_POSITION, position)
        intent.setClass(this, AirplaneMode::class.java)
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_SINGLE_TOP)
        startActivity(intent)
    }

    fun goSubPage(cls: Class<*>?) {
        val intent = Intent()
        intent.setClass(this, cls!!)
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_SINGLE_TOP)
        startActivity(intent)
        closeSideMenu(false)
    }
}