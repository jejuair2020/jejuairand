package com.parksmt.jejuair.android16.airplane

import android.os.Bundle
import android.text.SpannableString
import android.text.style.UnderlineSpan
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.parksmt.jejuair.android16.R
import com.parksmt.jejuair.android16.util.StringUtil
import kotlinx.android.synthetic.main.in_flight_service_content_layout.view.*

/**
 * 기내 서비스
 *
 *
 * Created by ui-jun on 2017-06-29.
 */
class AirplaneModeInFlightService : AirplaneModeFragment() {


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        super.onCreateView(inflater, container, savedInstanceState)
        val rootView =
            inflater.inflate(R.layout.in_flight_service_content_layout, container, false)
        initView(rootView)
        setText(rootView)
        return rootView
    }

    private fun initView(rootView: View) {
        var subLayout1 = rootView.in_flight_service_sublayout1
        var subLayout2 = rootView.in_flight_service_sublayout2
        var subLayout3 = rootView.in_flight_service_sublayout3
        var subLayout4 = rootView.in_flight_service_sublayout4
        var subLayout5 = rootView.in_flight_service_sublayout5
        var subLayout6 = rootView.in_flight_service_sublayout6
        subLayout1.findViewById<View>(R.id.in_flight_service_sublayout_imageView1)
            .setBackgroundResource(
                R.drawable.service_pic_1
            )
        subLayout1.findViewById<View>(R.id.in_flight_service_sublayout_imageView2)
            .setBackgroundResource(
                R.drawable.service_pic_2
            )
        subLayout2.findViewById<View>(R.id.in_flight_service_sublayout_imageView1)
            .setBackgroundResource(
                R.drawable.service_pic_3
            )
        subLayout2.findViewById<View>(R.id.in_flight_service_sublayout_imageView2)
            .setBackgroundResource(
                R.drawable.service_pic_4
            )
        subLayout3.findViewById<View>(R.id.in_flight_service_sublayout_imageView1)
            .setBackgroundResource(
                R.drawable.service_pic_5
            )
        subLayout3.findViewById<View>(R.id.in_flight_service_sublayout_imageView2)
            .setBackgroundResource(
                R.drawable.service_pic_6
            )
        subLayout4.findViewById<View>(R.id.in_flight_service_sublayout_imageView1)
            .setBackgroundResource(
                R.drawable.service_pic_7
            )
        subLayout4.findViewById<View>(R.id.in_flight_service_sublayout_imageView2)
            .setBackgroundResource(
                R.drawable.service_pic_8
            )
        subLayout5.findViewById<View>(R.id.in_flight_service_sublayout_imageView1)
            .setBackgroundResource(
                R.drawable.service_pic_9
            )
        subLayout5.findViewById<View>(R.id.in_flight_service_sublayout_imageView2)
            .setBackgroundResource(
                R.drawable.service_pic_10
            )
        subLayout6.findViewById<View>(R.id.in_flight_service_sublayout_imageView1)
            .setBackgroundResource(
                R.drawable.service_pic_11
            )
    }

    private fun setText(rootView: View) {
        var subLayout1 = rootView.in_flight_service_sublayout1
        var subLayout2 = rootView.in_flight_service_sublayout2
        var subLayout3 = rootView.in_flight_service_sublayout3
        var subLayout4 = rootView.in_flight_service_sublayout4
        var subLayout5 = rootView.in_flight_service_sublayout5
        var subLayout6 = rootView.in_flight_service_sublayout6

        loadLanguage("serviceinfo/InFlightService.json")
        var sb = StringBuilder()
        var text = mLanguageJson.optString("in_flight_service_text1001")
        sb.append(text)
        sb.append(if (StringUtil().isNotNull(text)) '\n' else "")
        text = mLanguageJson.optString("in_flight_service_text1001_1")
        sb.append(text)
        sb.append(if (StringUtil().isNotNull(text)) '\n' else "")
        sb.append(mLanguageJson.optString("in_flight_service_text1001_2"))
        (rootView.findViewById<View>(R.id.in_flight_service_textView_subtitle1) as TextView).text =
            sb.toString()
        (rootView.findViewById<View>(R.id.in_flight_service_textView_subtitle2) as TextView).text =
            mLanguageJson.optString("in_flight_service_text1002")
        sb = StringBuilder()
        sb.append(mLanguageJson.optString("in_flight_service_text1003"))
        sb.append('\n')
        sb.append(mLanguageJson.optString("in_flight_service_text1003_1"))
        (rootView.findViewById<View>(R.id.in_flight_service_textView_subtitle3) as TextView).text =
            sb.toString()
        (rootView.findViewById<View>(R.id.in_flight_service_textView_subtitle4) as TextView).text =
            mLanguageJson.optString("in_flight_service_text1026")
        (rootView.findViewById<View>(R.id.in_flight_service_textView_subtitle5) as TextView).text =
            mLanguageJson.optString("in_flight_service_text1027")
        val temp = mLanguageJson.optString("in_flight_service_text1029")
        val content = SpannableString(temp)
        content.setSpan(UnderlineSpan(), 0, temp.length, 0)
        (rootView.findViewById<View>(R.id.in_flight_service_instagram_textView) as TextView).text =
            content
        (subLayout1!!.findViewById<View>(R.id.in_flight_service_sublayout_textView_title1) as TextView).text =
            mLanguageJson.optString("in_flight_service_text1004")
        (subLayout1!!.findViewById<View>(R.id.in_flight_service_sublayout_textView_sub_title1) as TextView).text =
            mLanguageJson.optString("in_flight_service_text1005")
        (subLayout1!!.findViewById<View>(R.id.in_flight_service_sublayout_textView_title2) as TextView).text =
            mLanguageJson.optString("in_flight_service_text1006")
        (subLayout1!!.findViewById<View>(R.id.in_flight_service_sublayout_textView_sub_title2) as TextView).text =
            mLanguageJson.optString("in_flight_service_text1007")
        (subLayout2!!.findViewById<View>(R.id.in_flight_service_sublayout_textView_title1) as TextView).text =
            mLanguageJson.optString("in_flight_service_text1008")
        (subLayout2!!.findViewById<View>(R.id.in_flight_service_sublayout_textView_sub_title1) as TextView).text =
            mLanguageJson.optString("in_flight_service_text1009")
        (subLayout2!!.findViewById<View>(R.id.in_flight_service_sublayout_textView_title2) as TextView).text =
            mLanguageJson.optString("in_flight_service_text1010")
        (subLayout2!!.findViewById<View>(R.id.in_flight_service_sublayout_textView_sub_title2) as TextView).text =
            mLanguageJson.optString("in_flight_service_text1011")
        (subLayout3!!.findViewById<View>(R.id.in_flight_service_sublayout_textView_title1) as TextView).text =
            mLanguageJson.optString("in_flight_service_text1012")
        (subLayout3!!.findViewById<View>(R.id.in_flight_service_sublayout_textView_sub_title1) as TextView).text =
            mLanguageJson.optString("in_flight_service_text1013")
        (subLayout3!!.findViewById<View>(R.id.in_flight_service_sublayout_textView_title2) as TextView).text =
            mLanguageJson.optString("in_flight_service_text1014")
        (subLayout3!!.findViewById<View>(R.id.in_flight_service_sublayout_textView_sub_title2) as TextView).text =
            mLanguageJson.optString("in_flight_service_text1015")
        (subLayout4!!.findViewById<View>(R.id.in_flight_service_sublayout_textView_title1) as TextView).text =
            mLanguageJson.optString("in_flight_service_text1016")
        (subLayout4!!.findViewById<View>(R.id.in_flight_service_sublayout_textView_sub_title1) as TextView).text =
            mLanguageJson.optString("in_flight_service_text1017")
        (subLayout4!!.findViewById<View>(R.id.in_flight_service_sublayout_textView_title2) as TextView).text =
            mLanguageJson.optString("in_flight_service_text1018")
        (subLayout4!!.findViewById<View>(R.id.in_flight_service_sublayout_textView_sub_title2) as TextView).text =
            mLanguageJson.optString("in_flight_service_text1019")
        (subLayout5!!.findViewById<View>(R.id.in_flight_service_sublayout_textView_title1) as TextView).text =
            mLanguageJson.optString("in_flight_service_text1020")
        (subLayout5!!.findViewById<View>(R.id.in_flight_service_sublayout_textView_sub_title1) as TextView).text =
            mLanguageJson.optString("in_flight_service_text1021")
        (subLayout5!!.findViewById<View>(R.id.in_flight_service_sublayout_textView_title2) as TextView).text =
            mLanguageJson.optString("in_flight_service_text1022")
        (subLayout5!!.findViewById<View>(R.id.in_flight_service_sublayout_textView_sub_title2) as TextView).text =
            mLanguageJson.optString("in_flight_service_text1023")
        (subLayout6!!.findViewById<View>(R.id.in_flight_service_sublayout_textView_title1) as TextView).text =
            mLanguageJson.optString("in_flight_service_text1024")
        (subLayout6!!.findViewById<View>(R.id.in_flight_service_sublayout_textView_sub_title1) as TextView).text =
            mLanguageJson.optString("in_flight_service_text1025")
    }
}