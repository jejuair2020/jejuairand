package com.parksmt.jejuair.android16.bottomtap

import android.net.http.SslError
import android.webkit.*

//웹뷰 로드시 SSL 인증서 에러 방지
class SslWebView : WebViewClient() {

    override fun onReceivedSslError(view: WebView?, handler: SslErrorHandler?, error: SslError?) {
        super.onReceivedSslError(view, handler, error)
        handler?.proceed()
    }

    override fun shouldOverrideUrlLoading(view: WebView?, request: WebResourceRequest?): Boolean {
        return super.shouldOverrideUrlLoading(view, request)
        view?.loadUrl(request?.url.toString())
    }

}