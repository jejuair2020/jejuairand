package com.parksmt.jejuair.android16.view

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentStatePagerAdapter
import java.util.*

/**
 * 기본 fragment pager adapter
 *
 *
 * Created by jun on 2017-02-13.
 */
class TabAdapter(fm: FragmentManager?) :
    FragmentStatePagerAdapter(fm!!) {
    private val mFragments: MutableList<Fragment>
    private val mTitleList: MutableList<String>
    override fun getItem(position: Int): Fragment {
        return mFragments[position]
    }

    override fun getCount(): Int {
        return mFragments.size
    }

    override fun getPageTitle(position: Int): CharSequence? {
        return mTitleList[position]
    }

    fun addFragment(fragment: Fragment, title: String) {
        mFragments.add(fragment)
        mTitleList.add(title)
    }

    init {
        mFragments = ArrayList()
        mTitleList = ArrayList()
    }
}