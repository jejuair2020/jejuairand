package com.parksmt.jejuair.android16.base

import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.annotation.LayoutRes
import androidx.appcompat.app.AppCompatActivity
import com.parksmt.jejuair.android16.common.Common
import com.parksmt.jejuair.android16.common.DLog
import com.parksmt.jejuair.android16.common.IntentKey
import com.parksmt.jejuair.android16.enumdata.CampaignType
import com.parksmt.jejuair.android16.util.LanguageContextWrapper
import com.parksmt.jejuair.android16.util.Util
import org.json.JSONObject

abstract class BaseActivity : AppCompatActivity() {

    var mLanguageJson: JSONObject = JSONObject()
    var mErrorCodeLanguageJson: JSONObject = JSONObject()
    private var mOnBackKeyListener: OnBackKeyListener? = null
    private var mPrevOnBackKeyListener: OnBackKeyListener? = null
    protected var useLanguage = true

    protected abstract val uiName: String

    /**
     * onBackPressed 가 호출되었을때의 call back listener
     *
     * @param onBackKeyListener onBackKeyListener
     */
    var onBackKeyListener: OnBackKeyListener?
        get() = mOnBackKeyListener
        set(onBackKeyListener) {
            mPrevOnBackKeyListener = mOnBackKeyListener
            mOnBackKeyListener = onBackKeyListener
        }

    val pageName: String
        get() = uiName

    interface OnBackKeyListener {
        fun onBackKey()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        DLog().d("Base onCreate")
        super.onCreate(savedInstanceState)
        if (useLanguage) {
            loadCommonLanguage()
        }
        //loadErrorCodeLanguage()

    }

    override fun setContentView(@LayoutRes layoutResID: Int) {
        super.setContentView(layoutResID)
        //checkCampaignPopup(intent)

    }

    override fun onStart() {
        super.onStart()
        //sendTag()
    }

    override fun onPause() {
        super.onPause()
        Common.isShowing = false
    }

    override fun onResume() {
        super.onResume()
        Common.isShowing = true
        if (showCampaignPopup) {
            val intent = Intent()
            intent.putExtra(IntentKey.SHOW_CAMPAIGN_POPUP, true)
            intent.putExtra(IntentKey.CAMPAIGN_TYPE, CampaignType.LOGIN.type)
            //checkCampaignPopup(intent)
        }
    }

    override fun onDestroy() {
        DLog().d("base onDestroy")
        super.onDestroy()
    }

    override fun onNewIntent(intent: Intent) {
        DLog().d("onCreate Base onNewIntent")
        super.onNewIntent(intent)
        //sendTag()
        //checkFingerPush(intent)
        //checkCampaignPopup(intent)
        //checkPushTag(intent)
        //checkCampaignPushTag(intent)
    }

    override fun attachBaseContext(newBase: Context) {
        if (useLanguage) {
            super.attachBaseContext(LanguageContextWrapper.wrap(newBase))
        } else {
            super.attachBaseContext(newBase)
        }
    }

    override fun onBackPressed() {
        if (mOnBackKeyListener != null) {
            DLog().d("checkpoint Base mOnBackKeyListener")
            mOnBackKeyListener!!.onBackKey()
        } else {
            DLog().d("checkpoint Base onBackPressed")
            super.onBackPressed()
        }
    }

    override fun finish() {
        DLog().d("finish")
        super.finish()
    }

    override fun startActivity(intent: Intent) {
        DLog().d("base startActivity intent $intent")
        super.startActivity(intent)
    }

    override fun startActivityForResult(intent: Intent, requestCode: Int) {
        DLog().d("base startActivityForResult   requestCode : $requestCode")
        DLog().d("base startActivityForResult   intent : $intent")
        super.startActivityForResult(intent, requestCode)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        DLog().d("onActivityResult   requestCode : $requestCode   resultCode : $resultCode")
        super.onActivityResult(requestCode, resultCode, data)
    }

    protected fun loadLanguage(path: String) {
        mLanguageJson = Util().addJSONObject(mLanguageJson, this, path)
    }

    private fun loadCommonLanguage() {
        mLanguageJson = Util().addJSONObject(mLanguageJson, this, "com/common.json")
    }

    /**
     * 2018.03.09 yschoi NetWorkErrorCode to String
     * 에러코드에 대한 언어팩을 따로 추가함.
     */
    private fun loadErrorCodeLanguage() {
        if (null == mErrorCodeLanguageJson) {
            mErrorCodeLanguageJson =
                Util().addJSONObject(mErrorCodeLanguageJson, this, "com/localization.json")
            mErrorCodeLanguageJson =
                Util().addJSONObject(mErrorCodeLanguageJson, this, "login/join/easyForm.json")
            mErrorCodeLanguageJson =
                Util().addJSONObject(mErrorCodeLanguageJson, this, "login/member/login.json")
            mErrorCodeLanguageJson =
                Util().addJSONObject(mErrorCodeLanguageJson, this, "login/join/join.json")
        }
    }

    fun getmLanguageJson(): JSONObject {
        return mLanguageJson
    }

    /**
     * 이전의 onBackKeyListener 로 되돌린다.
     */
    fun restoreOnBackKeyListener() {
        mOnBackKeyListener = mPrevOnBackKeyListener
    }

    fun setOnBackKeyListener(baseActivity: BaseActivity, onBackKeyListener: OnBackKeyListener) {
        baseActivity.mPrevOnBackKeyListener = baseActivity.mOnBackKeyListener
        baseActivity.mOnBackKeyListener = onBackKeyListener
    }

    companion object {
        protected var mRequestRefreshMyInfo = 0
        var showCampaignPopup = false
        //TODO 2018.02.13 yschoi 예매(웹)에서 팝업을 닫고 우측상단을 통해 로그인을 할 경우 True
        var isReservationNoneEventLogin = false

        /**
         * onBackPressed 가 호출되었을때의 call back listener
         *
         * @param onBackKeyListener onBackKeyListener
         */

    }
}
