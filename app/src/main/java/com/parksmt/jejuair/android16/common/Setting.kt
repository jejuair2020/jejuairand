package com.parksmt.jejuair.android16.common

import android.content.Context
import com.parksmt.jejuair.android16.BuildConfig

class Setting {

    companion object {

        var IS_PUBLISHED: Boolean = BuildConfig.IS_PUBLISHED
        @kotlin.jvm.JvmField
        val CHANNEL_ID01 = "channel_id_01"
        val CHANNEL_NAME01 = "전체알림"

        var IS_TEST = BuildConfig.IS_TEST
        val IS_TEST_URL = BuildConfig.IS_TEST_URL
        var PRINT_LOG = BuildConfig.PRINT_LOG
        val USE_NETFUNNEL = BuildConfig.USE_NETFUNNEL

        val USE_CAMPAIGN_POPUP = true

        val APP_VERSION = BuildConfig.APP_VERSION
        private val LANGUAGE_VERSION = "0"
        private val AIRCAFE_VERSION = "0"
        private val WEBTOON_VERSION = "20000101111111"
        private val DUTYFREE_VERSION = "20000101111111"//TODO DUTYFREE ADDED TO yschoi
        var BASE_URL : String = ""
        var BASE_LOG_URL: String = ""// = "https://wiselog.jejuair.net:8443";

        val HTTP_TIME_OUT = 60000
        val PREFERENCE_NAME = "JejuAir"
        val DEFAULT_LANGUAGE = "ko"
        val LINE_CHANNEL_ID = "1510241480"

        val CALL_GATE_ID = "jejuair2"  // add for call gate by parkgy

        /**
         * intro 지연 시간 {@value #SPLASH_DELAY_TIME}ms
         */
        val SPLASH_DELAY_TIME = 1000
        /**
         * 종료 지연 시간 {@value #FINISH_DELAY_TIME}ms
         */
        val FINISH_DELAY_TIME = 1000
        /**
         * 취소 가능한 시간 {@value #LOADING_DIALOG_CANCELABLE_TIME}ms 후
         */
        val LOADING_DIALOG_CANCELABLE_TIME = 10000

        /**
         * 비콘 접근 가능 시간 24h
         */
        var BEACON_ACCESS_TIME: Int = 0
        /**
         * file upload max size 5MB
         */
        val UPLOAD_FILE_MAX_SIZE = 5242880
        //    개발 URL : http://sysinfo.jejuair.net/check/dev/check.json
        //
        //    운영 URL : http://sysinfo.jejuair.net/check/prod/check.json
    }


    init {
        if (IS_TEST_URL) {
            // RELEASE_BUILD_REMOVE START
            BASE_URL = "https://t9.jejuair.net/jejuair"  //https://tjapp.jejuair.net/jejuair
            BASE_LOG_URL = "https://wiselog.jejuair.net:8443"
            BEACON_ACCESS_TIME = 30000
            // RELEASE_BUILD_REMOVE END
            //CHECK_APP_NOTICE_CHECK = "http://sysinfo.jejuair.net/check/dev/check.json"
        } else {
            // 상용
            BASE_URL = "https://japp.jejuair.net/jejuair"
            BASE_LOG_URL = "https://wiselog.jejuair.net:8443"
            //BEACON_ACCESS_TIME = 30000;
            BEACON_ACCESS_TIME = 86400000
        }
        DLog().e("========================================")
        DLog().e("●●● " + BuildConfig.BUILD_TYPE + " ●●●")

        DLog().w("IS_TEST : $IS_TEST")
        DLog().w("IS_TEST_URL : $IS_TEST_URL")
        DLog().w("IS_PUBLISHED : $IS_PUBLISHED")
        DLog().w("PRINT_LOG : $PRINT_LOG")
        DLog().w("USE_NETFUNNEL : $USE_NETFUNNEL")
        DLog().w("APP_VERSION : $APP_VERSION")
        DLog().e("========================================")
        DLog().e("========================================")
    }

    fun getLanguageVersion(context: Context): String? {
        return Common.getString(SharedPrefKey.LANGUAGE_VERSION, LANGUAGE_VERSION, context)
    }

    fun getAircafeVersion(context: Context): String? {
        return Common.getString(SharedPrefKey.AIRCAFE_VERSION, AIRCAFE_VERSION, context)
    }

    fun setAircafeVersion(context: Context, version: String) {
        Common.putString(SharedPrefKey.AIRCAFE_VERSION, version, context)
        Common.commit(context)
    }

    fun getWebtoonVersion(context: Context): String? {
        return Common.getString(SharedPrefKey.WEBTOON_VERSION, WEBTOON_VERSION, context)
    }

    fun setWebtoonVersion(context: Context, version: String) {
        Common.putString(SharedPrefKey.WEBTOON_VERSION, version, context)
        Common.commit(context)
    }

    fun getDutyFreeVersion(context: Context): String? {
        return Common.getString(SharedPrefKey.DUTYFREE_VERSION, DUTYFREE_VERSION, context)
    }

    fun setDutyFreeVersion(context: Context, version: String) {
        Common.putString(SharedPrefKey.DUTYFREE_VERSION, version, context)
        Common.commit(context)
    }

    /**
     * 2018.02.26 add to yschoi test/applauncher.html을 통해 type별 server url을 셋팅한다.
     * @param type real 운영, dev 개발, t1, t2, t3
     */
    fun setLauncherUrl(type: String) {
        when (type) {
            "real" -> BASE_URL = "https://japp.jejuair.net/jejuair"
            "dev" -> BASE_URL = "https://t9.jejuair.net/jejuair"
            "t1" -> BASE_URL = "https://tjapp1.jejuair.net/jejuair"
            "t2" -> BASE_URL = "https://tjapp2.jejuair.net/jejuair"
            "t3" -> BASE_URL = "https://tjapp3.jejuair.net/jejuair"
            else -> BASE_URL = "https://$type.jejuair.net/jejuair"
        }
        DLog().e("TYPE : $type\n $BASE_URL\n$BASE_LOG_URL")
    }
}
