package com.parksmt.jejuair.android16.view

import android.app.Dialog
import android.content.Context
import android.os.Bundle
import android.view.View
import android.view.WindowManager
import android.widget.NumberPicker
import com.parksmt.jejuair.android16.R
import com.parksmt.jejuair.android16.common.DLog
import com.parksmt.jejuair.android16.view.EmailDialog.Companion.setNumberPickerTextColor
import java.text.SimpleDateFormat
import java.util.*

/**
 * date picker
 * Created by ui-jun on 2017-03-13.
 */
class DateDialog(context: Context?, isBirthDay: Boolean) :
    Dialog(context!!, android.R.style.Theme_Translucent_NoTitleBar) {
    interface OnSelectedListener {
        fun onSelected(calendar: Calendar)
    }

    private val tag = this.javaClass.simpleName
    private var mYearNumberPicker: NumberPicker? = null
    private var mMonthNumberPicker: NumberPicker? = null
    private var mDayNumberPicker: NumberPicker? = null
    private var mOnSelectedListener: OnSelectedListener? = null
    val selectedDate: Calendar
    private val mCurrentYear: Int
    private val mCurrentMonth: Int
    private val mCurrentDay: Int
    var isSelected = false
    private var isInitialized = false
    private var mMinYear = 1900
    private var mMinMonth = 0
    private var mMinDate = 1
    private var mIsBirthDay = false
    override fun onCreate(savedInstanceState: Bundle) {
        super.onCreate(savedInstanceState)
        val lpWindow = WindowManager.LayoutParams()
        lpWindow.flags = WindowManager.LayoutParams.FLAG_DIM_BEHIND
        lpWindow.dimAmount = 0.65f
        val window = window
        if (window != null) {
            window.attributes = lpWindow
        }
        setContentView(R.layout.date_dialog)
        setCancelable(true)
        init()
    }

    private fun init() {
        mYearNumberPicker =
            findViewById<View>(R.id.date_dialog_year_number_picker) as NumberPicker
        mMonthNumberPicker =
            findViewById<View>(R.id.date_dialog_month_number_picker) as NumberPicker
        mDayNumberPicker =
            findViewById<View>(R.id.date_dialog_day_number_picker) as NumberPicker
        setNumberPicker(
            mYearNumberPicker,
            mMinYear,
            mCurrentYear,
            selectedDate[Calendar.YEAR]
        )
        setNumberPicker(
            mMonthNumberPicker,
            1,
            12,
            selectedDate[Calendar.MONTH] + 1
        )
        setNumberPicker(
            mDayNumberPicker,
            1,
            getMaxDay(
                selectedDate[Calendar.YEAR],
                selectedDate[Calendar.MONTH] + 1
            ),
            selectedDate[Calendar.DAY_OF_MONTH]
        )
        mYearNumberPicker!!.setOnValueChangedListener { picker, oldVal, newVal ->
            if (mIsBirthDay) {
                setMonthNumberPickerDateForBirthday(newVal)
            } else {
                setMonthNumberPickerDate(newVal)
            }
        }
        mMonthNumberPicker!!.setOnValueChangedListener { picker, oldVal, newVal ->
            if (mIsBirthDay) {
                setDayNumberPickerDateForBirthday(newVal)
            } else {
                setDayNumberPickerDate(newVal)
            }
        }
        findViewById<View>(R.id.date_dialog_confirm_btn).setOnClickListener { close() }
        isInitialized = true
    }

    private fun getMaxDay(year: Int, month: Int): Int {
        val calendar = Calendar.getInstance()
        calendar[year, month - 1] = 1
        val maxDay = calendar.getActualMaximum(Calendar.DAY_OF_MONTH)
        DLog().d(
            "DateDialog getMaxDay  year : $year   month : $month   maxDay : $maxDay"
        )
        return maxDay
    }

    private fun setNumberPicker(
        numberPicker: NumberPicker?,
        min: Int,
        max: Int,
        set: Int
    ) {
        numberPicker!!.minValue = min
        numberPicker.maxValue = max
        numberPicker.wrapSelectorWheel = false
        numberPicker.descendantFocusability = NumberPicker.FOCUS_BLOCK_DESCENDANTS
        numberPicker.value = set
        setNumberPickerTextColor(numberPicker)
    }

    private fun setMonthNumberPickerDate(newYear: Int) {
        if (mMinYear == newYear) {
            mMonthNumberPicker!!.minValue = mMinMonth + 1
            if (mMinMonth == mCurrentMonth) {
                mDayNumberPicker!!.minValue = mMinDate
            } else {
                mDayNumberPicker!!.minValue = 1
            }
        } else {
            mMonthNumberPicker!!.minValue = 1
            mDayNumberPicker!!.minValue = 1
        }
        if (mCurrentYear == newYear) {
            mMonthNumberPicker!!.maxValue = mCurrentMonth + 1
            if (mMonthNumberPicker!!.value == mCurrentMonth + 1) {
                mDayNumberPicker!!.maxValue = mCurrentDay
            } else {
                mDayNumberPicker!!.maxValue = getMaxDay(newYear, mMonthNumberPicker!!.value)
            }
        } else {
            mMonthNumberPicker!!.maxValue = 12
            mDayNumberPicker!!.maxValue = getMaxDay(newYear, mMonthNumberPicker!!.value)
        }
    }

    private fun setMonthNumberPickerDateForBirthday(newYear: Int) {
        mMonthNumberPicker!!.minValue = 1
        mMonthNumberPicker!!.maxValue = 12
        mDayNumberPicker!!.maxValue = getMaxDay(newYear, mMonthNumberPicker!!.value)
    }

    private fun setDayNumberPickerDate(newMonth: Int) {
        if (mMinMonth == newMonth - 1 && mMinYear == mYearNumberPicker!!.value) {
            mDayNumberPicker!!.minValue = mMinDate
        } else {
            mDayNumberPicker!!.minValue = 1
        }
        if (mMonthNumberPicker!!.maxValue == newMonth && mCurrentYear == mYearNumberPicker!!.value) {
            mDayNumberPicker!!.maxValue = mCurrentDay
        } else {
            mDayNumberPicker!!.maxValue = getMaxDay(mYearNumberPicker!!.value, newMonth)
        }
    }

    private fun setDayNumberPickerDateForBirthday(newMonth: Int) {
        mDayNumberPicker!!.minValue = 1
        mDayNumberPicker!!.maxValue = getMaxDay(mYearNumberPicker!!.value, newMonth)
    }

    private fun refresh() {
        if (isInitialized) {
            mYearNumberPicker!!.minValue = mMinYear
            mYearNumberPicker!!.value = selectedDate[Calendar.YEAR]
            mMonthNumberPicker!!.value = selectedDate[Calendar.MONTH] + 1
            mDayNumberPicker!!.value = selectedDate[Calendar.DAY_OF_MONTH]
            if (mIsBirthDay) {
                setMonthNumberPickerDateForBirthday(mYearNumberPicker!!.value)
                setDayNumberPickerDateForBirthday(mMonthNumberPicker!!.value)
            } else {
                setMonthNumberPickerDate(mYearNumberPicker!!.value)
                setDayNumberPickerDate(mMonthNumberPicker!!.value)
            }
            selectedDate[mYearNumberPicker!!.value, mMonthNumberPicker!!.value - 1] =
                mDayNumberPicker!!.value
        }
    }

    override fun show() {
        super.show()
        refresh()
    }

    private fun close() {
        selectedDate[mYearNumberPicker!!.value, mMonthNumberPicker!!.value - 1] =
            mDayNumberPicker!!.value
        //Log.d(tag, "DateDialog close  year : " + mSelectedCalendar.get(Calendar.YEAR) + "   month : " + mSelectedCalendar
//        .get(Calendar.MONTH) + "   Day : " + mSelectedCalendar.get(Calendar.DAY_OF_MONTH));
        dismiss()
        if (mOnSelectedListener != null) {
            isSelected = true
            mOnSelectedListener!!.onSelected(selectedDate)
        }
    }

    fun setOnSelectedListener(onSelectedListener: OnSelectedListener?): DateDialog {
        mOnSelectedListener = onSelectedListener
        return this
    }

    /**
     * calendar 설정 함
     *
     * @param year  year
     * @param month month 0부터 시작
     * @param date  day 1부터 시작
     */
    fun initCalendar(year: Int, month: Int, date: Int) {
        selectedDate[year, month] = date
        refresh()
    }

    fun initCalendar(calendar: Calendar) {
        initCalendar(
            calendar[Calendar.YEAR],
            calendar[Calendar.MONTH],
            calendar[Calendar.DAY_OF_MONTH]
        )
    }

    /**
     * 최소 일 설정
     *
     * @param year  year
     * @param month month 0부터 시작
     * @param date  day 1부터 시작
     */
    fun setMinDate(year: Int, month: Int, date: Int) {
        mMinYear = year
        mMinMonth = month
        mMinDate = date
        refresh()
    }

    fun setMinDate(calendar: Calendar) {
        setMinDate(
            calendar[Calendar.YEAR],
            calendar[Calendar.MONTH],
            calendar[Calendar.DAY_OF_MONTH]
        )
    }

    companion object {
        /**
         * yyyy - MM - dd 형식으로 바꿔 줌
         *
         * @param calendar calendar
         * @return formatted string : yyyy - MM - dd
         */
        fun convertCalendarToString(calendar: Calendar): String {
            return SimpleDateFormat("yyyy - MM - dd").format(calendar.time)
        }

        /**
         * format 형식으로 바꿔 줌
         *
         * @param calendar calendar
         * @param format   format yyyy - MM - dd
         * @return formatted string
         */
        fun convertCalendarToString(
            calendar: Calendar,
            format: String?
        ): String {
            return SimpleDateFormat(format).format(calendar.time)
        }
    }

    init {
        mIsBirthDay = isBirthDay
        selectedDate = Calendar.getInstance()
        selectedDate[1980, 0] = 1
        val calendar = Calendar.getInstance()
        mCurrentYear = calendar[Calendar.YEAR]
        mCurrentMonth = calendar[Calendar.MONTH]
        mCurrentDay = calendar[Calendar.DAY_OF_MONTH]
    }
}