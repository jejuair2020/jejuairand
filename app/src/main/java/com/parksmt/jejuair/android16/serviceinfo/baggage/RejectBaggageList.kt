package com.parksmt.jejuair.android16.serviceinfo.baggage

class RejectBaggageList(private val list: BaggageTableType) {
    val isHeader = true
    val cols = 3
    lateinit var contents: Array<Array<String?>>
        private set

    private fun setContents() {
        contents = Array(3) { arrayOfNulls<String>(4) }
        contents[0][0] =
            if (list == BaggageTableType.INSTALLED) "reject_baggage_text1110" else if (list == BaggageTableType.SPAREBATTERY) "reject_baggage_text1110" else "reject_baggage_text1111"
        contents[0][1] =
            if (list == BaggageTableType.INSTALLED) "reject_baggage_text1120" else if (list == BaggageTableType.SPAREBATTERY) "reject_baggage_text1120" else "reject_baggage_text1120"
        contents[0][2] =
            if (list == BaggageTableType.INSTALLED) "reject_baggage_text1115" else if (list == BaggageTableType.SPAREBATTERY) "reject_baggage_text1121" else "reject_baggage_text1115"
        contents[0][3] =
            if (list == BaggageTableType.INSTALLED) "reject_baggage_text1116" else if (list == BaggageTableType.SPAREBATTERY) "reject_baggage_text1117" else "reject_baggage_text1119"
        contents[1][0] =
            if (list == BaggageTableType.INSTALLED) "reject_baggage_text1108" else if (list == BaggageTableType.SPAREBATTERY) "reject_baggage_text1108" else "reject_baggage_text1112"
        contents[1][1] =
            if (list == BaggageTableType.INSTALLED) "reject_baggage_text1120" else if (list == BaggageTableType.SPAREBATTERY) "reject_baggage_text1120" else "reject_baggage_text1120"
        contents[1][2] =
            if (list == BaggageTableType.INSTALLED) "reject_baggage_text1115" else if (list == BaggageTableType.SPAREBATTERY) "reject_baggage_text1121" else "reject_baggage_text1120"
        contents[1][3] =
            if (list == BaggageTableType.INSTALLED) "reject_baggage_text1122" else if (list == BaggageTableType.SPAREBATTERY) "reject_baggage_text1118" else "reject_baggage_text1122"
        contents[2][0] =
            if (list == BaggageTableType.INSTALLED) "reject_baggage_text1109" else if (list == BaggageTableType.SPAREBATTERY) "reject_baggage_text1109" else "reject_baggage_text1113"
        contents[2][1] =
            if (list == BaggageTableType.INSTALLED) "reject_baggage_text1121" else if (list == BaggageTableType.SPAREBATTERY) "reject_baggage_text1121" else "reject_baggage_text1121"
        contents[2][2] =
            if (list == BaggageTableType.INSTALLED) "reject_baggage_text1121" else if (list == BaggageTableType.SPAREBATTERY) "reject_baggage_text1121" else "reject_baggage_text1121"
        contents[2][3] =
            if (list == BaggageTableType.INSTALLED) "reject_baggage_text1122" else if (list == BaggageTableType.SPAREBATTERY) "reject_baggage_text1122" else "reject_baggage_text1122"
    }

    fun getContents(col: Int, row: Int): String? {
        return contents[col][row]
    }

    val id: String?
        get() = list.tableId

    init {
        setContents()
    }
}