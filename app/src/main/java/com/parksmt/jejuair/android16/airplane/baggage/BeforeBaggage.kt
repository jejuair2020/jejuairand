package com.parksmt.jejuair.android16.airplane.baggage

import android.graphics.Color
import android.os.Bundle
import android.text.SpannableString
import android.text.SpannableStringBuilder
import android.text.Spanned
import android.text.method.LinkMovementMethod
import android.text.style.ClickableSpan
import android.util.TypedValue
import android.view.View
import android.widget.TextView
import com.parksmt.jejuair.android16.R
import com.parksmt.jejuair.android16.airplane.AirplaneModeMenuActivity
import com.parksmt.jejuair.android16.util.DialogUtil
import com.parksmt.jejuair.android16.util.StringUtil

/**
 * 사전 수하물 구매 S-MUI-08-013
 * Created by seungjinoh on 2017. 4. 5..
 */
class BeforeBaggage : AirplaneModeMenuActivity() {
    protected override val uiName: String
        protected get() = "S-MUI-08-013"

    protected override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.before_baggage_layout)
        initView()
        setText()
    }

    private fun initView() {}
    private fun setText() {
        loadLanguage("serviceinfo/BeforeBaggage.json")
        setTitleText(mLanguageJson.optString("before_baggage_text1000"))
        (findViewById(R.id.before_baggage_text1) as TextView).setText(mLanguageJson.optString("before_baggage_text1001"))
        var text = SpannableStringBuilder()
        text = StringUtil()
            .appendAndSize(
                text,
                mLanguageJson.optString("before_baggage_text1002"),
                Color.parseColor("#f15a22"),
                TypedValue
                    .applyDimension(
                        TypedValue.COMPLEX_UNIT_DIP,
                        12f,
                        getResources().getDisplayMetrics()
                    ).toInt()
            )
        text = StringUtil()
            .appendAndSize(
                text,
                mLanguageJson.optString("before_baggage_text1002_1"),
                Color.parseColor("#000000"),
                TypedValue
                    .applyDimension(
                        TypedValue.COMPLEX_UNIT_DIP,
                        12f,
                        getResources().getDisplayMetrics()
                    ).toInt()
            )
        (findViewById(R.id.before_baggage_text2) as TextView).text = text
        text = SpannableStringBuilder()
        text = StringUtil()
            .appendAndSize(
                text,
                mLanguageJson.optString("before_baggage_text1003"),
                Color.parseColor("#f15a22"),
                TypedValue
                    .applyDimension(
                        TypedValue.COMPLEX_UNIT_DIP,
                        12f,
                        getResources().getDisplayMetrics()
                    ).toInt()
            )
        //        text = StringUtil
//                .appendAndSize(text, mLanguageJson.optString("before_baggage_text1003_1"), Color.parseColor("#000000"), (int) TypedValue
//                        .applyDimension(TypedValue.COMPLEX_UNIT_DIP, 12, getResources().getDisplayMetrics()));
        (findViewById(R.id.before_baggage_text4) as TextView).text = text
        //TODO 2018.05.16 yschoi [ITSM-IM00126707] [부가사업팀] 앱-네이트브 - 홈페이지 모바일 수하물 문구 수정 요청드립니다.
        text = SpannableStringBuilder()
        text = StringUtil()
            .appendAndSize(
                text,
                mLanguageJson.optString("before_baggage_text1004_2"),
                Color.parseColor("#000000"),
                TypedValue
                    .applyDimension(
                        TypedValue.COMPLEX_UNIT_DIP,
                        12f,
                        getResources().getDisplayMetrics()
                    ).toInt()
            )
        //        text = StringUtil
//                .appendAndSizeBold(text, mLanguageJson.optString("before_baggage_text1004_1"), Color.parseColor("#f15a22"), (int) TypedValue
//                        .applyDimension(TypedValue.COMPLEX_UNIT_DIP, 12, getResources().getDisplayMetrics()));
        (findViewById(R.id.before_baggage_text6) as TextView).text = text
        text = SpannableStringBuilder()
        text = StringUtil()
            .appendAndSize(
                text,
                mLanguageJson.optString("before_baggage_text1005"),
                Color.parseColor("#000000"),
                TypedValue
                    .applyDimension(
                        TypedValue.COMPLEX_UNIT_DIP,
                        12f,
                        getResources().getDisplayMetrics()
                    ).toInt()
            )
        (findViewById(R.id.before_baggage_text8) as TextView).text = text
        //TODO 2018.05.16 yschoi [ITSM-IM00126707] [부가사업팀] 앱-네이트브 - 홈페이지 모바일 수하물 문구 수정 요청드립니다.
//TODO text9, text10 -> view is GONE
        (findViewById(R.id.before_baggage_text9) as TextView).setText(mLanguageJson.optString("before_baggage_text1006"))
        (findViewById(R.id.before_baggage_text10) as TextView).setText(mLanguageJson.optString("before_baggage_text1007"))
        (findViewById(R.id.before_baggage_text11) as TextView).setText(mLanguageJson.optString("before_baggage_text1008"))
        text = SpannableStringBuilder()
        text = StringUtil()
            .appendAndSize(
                text,
                mLanguageJson.optString("before_baggage_text1009"),
                Color.parseColor("#000000"),
                TypedValue
                    .applyDimension(
                        TypedValue.COMPLEX_UNIT_DIP,
                        12f,
                        getResources().getDisplayMetrics()
                    ).toInt()
            )
        text = StringUtil()
            .appendAndSizeBold(
                text,
                mLanguageJson.optString("before_baggage_text1009_1"),
                Color.parseColor("#f15a22"),
                TypedValue
                    .applyDimension(
                        TypedValue.COMPLEX_UNIT_DIP,
                        12f,
                        getResources().getDisplayMetrics()
                    ).toInt()
            )
        (findViewById(R.id.before_baggage_text12) as TextView).text = text
        (findViewById(R.id.before_baggage_text14) as TextView).setText(mLanguageJson.optString("before_baggage_text1020"))
        (findViewById(R.id.before_baggage_text16) as TextView).setText(mLanguageJson.optString("before_baggage_text1013"))
        (findViewById(R.id.before_baggage_text17) as TextView).setText(mLanguageJson.optString("before_baggage_text1014"))
        (findViewById(R.id.before_baggage_text21) as TextView).setText(mLanguageJson.optString("before_baggage_text1018"))
        //TODO 2018.05.16 yschoi [ITSM-IM00126707] [부가사업팀] 앱-네이트브 - 홈페이지 모바일 수하물 문구 수정 요청드립니다.
        (findViewById(R.id.before_baggage_text22) as TextView).setText(mLanguageJson.optString("before_baggage_text1019"))
        //TODO 2018.07.26 yschoi [ITSM-IM00134298] 시스템 사용료 문구 추가
        val spannableString =
            SpannableString(mLanguageJson.optString("before_baggage_system_fees_underline"))
        val clickableSpan: ClickableSpan = object : ClickableSpan() {
            override fun onClick(textView: View) {
                DialogUtil().showCommonAlertDialog(
                    this@BeforeBaggage,
                    mLanguageJson.optString("airplaneModeText1009")
                )
            }
        }
        val systemFees: TextView = findViewById(R.id.before_baggage_system_fees)
        spannableString.setSpan(
            clickableSpan,
            0,
            spannableString.length,
            Spanned.SPAN_EXCLUSIVE_EXCLUSIVE
        )
        text = SpannableStringBuilder()
        text.append(mLanguageJson.optString("before_baggage_system_fees_prefix"))
        text.append(spannableString)
        text.append(mLanguageJson.optString("before_baggage_system_fees_suffix"))
        systemFees.setText(text, TextView.BufferType.SPANNABLE)
        systemFees.movementMethod = LinkMovementMethod.getInstance()
    }
}