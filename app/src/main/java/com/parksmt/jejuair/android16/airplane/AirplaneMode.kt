package com.parksmt.jejuair.android16.airplane

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.viewpager.widget.ViewPager.OnPageChangeListener
import com.google.android.material.tabs.TabLayout
import com.parksmt.jejuair.android16.R
import com.parksmt.jejuair.android16.common.DLog
import com.parksmt.jejuair.android16.common.IntentKey
import com.parksmt.jejuair.android16.util.Util
import com.parksmt.jejuair.android16.view.BaseViewPager
import com.parksmt.jejuair.android16.view.CommonToast
import com.parksmt.jejuair.android16.view.TabAdapter

/**
 * 비행기모드
 *
 *
 * Created by ui-jun on 2017-06-28.
 */
class AirplaneMode : AirplaneModeMenuActivity() {
    private val FINISH_TIME = 1000
    private var mFinishTime: Long = 0
    private lateinit var mMainViewPager: BaseViewPager
    private lateinit var mTabAdapter: TabAdapter
    private lateinit var mCurrentFragment: AirplaneModeFragment
    private lateinit var mFinishToast: CommonToast
    override val uiName: String
        protected get() = "S-MUI-13-001"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.airplane_mode)
        init()
        executeIntent(intent)
    }

    override fun onNewIntent(intent: Intent) {
        super.onNewIntent(intent)
        executeIntent(intent)
    }

    override fun onBackPressed() {
        if (supportFragmentManager.backStackEntryCount > 0) {
            DLog().d(
                "onBackPressed BackStackEntryCount : " + supportFragmentManager.backStackEntryCount
            )
            super.onBackPressed()
        } else {
            if (mCurrentFragment != null) {
                if (mCurrentFragment!!.onBackKey()) {
                    return
                }
            }
            if (onBackKeyListener != null) {
                DLog().d("onBackPressed getOnBackKeyListener() != null")
                super.onBackPressed()
            } else {
                if (System.currentTimeMillis() - mFinishTime < FINISH_TIME) {
                    if (mFinishToast != null) {
                        mFinishToast.cancel()
                    }
                    super.onBackPressed()
                } else {
                    if (mMainViewPager!!.currentItem != AirplaneModePage.HOME.position) {
                        mMainViewPager!!.setCurrentItem(AirplaneModePage.HOME.position, true)
                    } else {
                        if (mFinishToast == null) {
                            mFinishToast = CommonToast.makeToast(
                                this,
                                mLanguageJson.optString("airplaneModeText1008")
                            )
                        }
                        mFinishToast.show()
                        mFinishTime = System.currentTimeMillis()
                    }
                }
            }
        }
    }

    private fun init() {
        setNavigationIcon(0)
        mMainViewPager =
            findViewById<View>(R.id.airplane_mode_view_pager) as BaseViewPager
        mTabAdapter = TabAdapter(supportFragmentManager)
        mTabAdapter.addFragment(AirplaneModeHome(), mLanguageJson.optString("airplaneModeText1001"))
        mTabAdapter.addFragment(
            AirplaneModeInFlightService(),
            mLanguageJson.optString("airplaneModeText1002")
        )
        //        mTabAdapter.addFragment(new AirplaneModeAirCafe(), mLanguageJson.optString("airplaneModeText1003"));
        mTabAdapter.addFragment(
            AirplaneModeBaggage(),
            mLanguageJson.optString("airplaneModeText1004")
        )
        if (Util().isKorean(this)) {
            mTabAdapter.addFragment(
                AirplaneModeImmigrationForm(),
                mLanguageJson.optString("airplaneModeText1005")
            )
            //TODO 2018.09.03 yschoi [ITSM-IM00139926] [모바일][AOS] 모바일 웹툰메뉴 제거요청
//mTabAdapter.addFragment(new AirplaneModeWebtoon(), mLanguageJson.optString("airplaneModeText1006"));
        }
        mMainViewPager!!.adapter = mTabAdapter
        mMainViewPager!!.offscreenPageLimit = mTabAdapter.getCount()
        (findViewById<View>(R.id.airplane_mode_tab_layout) as TabLayout).setupWithViewPager(
            mMainViewPager
        )
        mMainViewPager!!.addOnPageChangeListener(object : OnPageChangeListener {
            override fun onPageScrolled(
                position: Int,
                positionOffset: Float,
                positionOffsetPixels: Int
            ) {
            }

            override fun onPageSelected(position: Int) {
                setAirplaneModeViewPagerPage(position)
            }

            override fun onPageScrollStateChanged(state: Int) {}
        })
    }

    private fun executeIntent(intent: Intent) {
        if (intent.getBooleanExtra(IntentKey.GO_MAIN_PAGE, false)) {
            val mainPage =
                intent.getSerializableExtra(IntentKey.MAIN_PAGE_POSITION) as AirplaneModePage
            mMainViewPager!!.currentItem = mainPage.position
            closeSideMenu(false)
        }
    }

    private fun setAirplaneModeViewPagerPage(position: Int) {
        val fragment: Fragment = mTabAdapter.getItem(position)
        if (fragment is AirplaneModeFragment) {
            mCurrentFragment = fragment
        }
        DLog().d("onPageSelected  position : $position")
    }

    enum class AirplaneModePage(var position: Int) {
        HOME(0), IN_FLIGHT_SERVICE(1),  /*AIR_CAFE(2),*/
        BAGGAGE(2), IMMIGRATION_FORM(3), WEBTOON(5);

    }
}