package com.parksmt.jejuair.android16.util

import android.content.Context
import android.content.pm.PackageManager
import android.graphics.Point
import android.os.Build
import android.util.Base64
import android.util.Log
import android.util.Patterns
import android.view.WindowManager
import org.json.JSONObject
import java.text.DecimalFormat
import java.util.*

class CommonUtils {
    private var _context: Context? = null
    fun init(theContext: Context?): CommonUtils? {
        _context = theContext
        return _shared
    }

    fun getDecimalFormat(num: Long): String {
        val df = DecimalFormat("###,###")
        return df.format(num)
    }

    companion object {
        var _shared: CommonUtils? = null
        fun shared(): CommonUtils? {
            synchronized(CommonUtils::class.java) {
                if (_shared == null) {
                    _shared = CommonUtils()
                }
            }
            return _shared
        }

        /**
         * 스크린 사이즈
         * @param context
         * @return
         */
        fun screenSize(`$context`: Context): Point {
            val result = Point()
            val manager =
                `$context`.getSystemService(Context.WINDOW_SERVICE) as WindowManager
            val display = manager.defaultDisplay
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) display.getRealSize(
                result
            ) else display.getSize(result)
            return result
        }

        /**
         * 프로퍼티 읽기
         *
         * @param key 프로퍼티 키
         * @param context
         * @return 프로퍼티 값
         */
        fun getProperty(key: String?, context: Context): String {
            try {
                val properties = Properties()
                val assetManager = context.assets
                val inputStream = assetManager.open("config.properties")
                properties.load(inputStream)
                return properties.getProperty(key)
            } catch (e: Exception) {
                e.printStackTrace()
            }
            return ""
        }

        /**
         * JSON 오브젝트 처리
         * @param obj
         * @param name
         * @param defaultValue
         * @return
         */
        fun getJsonValue(
            obj: JSONObject,
            name: String?,
            defaultValue: String
        ): String {
            var re = defaultValue
            try {
                if (obj == null) re = defaultValue
                if (name == null) re = defaultValue
                if (defaultValue == null) re = ""
                re = if (obj!!.has(name)) obj.getString(name) else defaultValue
                if (re == "null") re = defaultValue
                if (re == "") re = defaultValue
            } catch (e: Exception) {
                e.printStackTrace()
            }
            return re
        }

        fun getJsonValue(obj: JSONObject, name: String?, defaultValue: Int): Int {
            var name = name
            var re = defaultValue
            if (name == "") name = "0"
            try {
                if (obj == null) re = defaultValue
                if (name == null) re = defaultValue
                re = if (obj!!.has(name)) obj.getInt(name) else defaultValue
            } catch (e: Exception) {
                e.printStackTrace()
            }
            return re
        }

        fun getJsonValue(
            obj: JSONObject,
            name: String?,
            defaultValue: Float
        ): Float {
            var re = defaultValue.toDouble()
            try {
                re = if (obj.has(name)) obj.getDouble(name) else defaultValue.toDouble()
            } catch (e: Exception) {
                e.printStackTrace()
            }
            return re.toFloat()
        }

        fun getJsonValue(
            obj: JSONObject,
            name: String?,
            defaultValue: Boolean
        ): Boolean {
            var re = defaultValue
            try {
                re = if (obj.has(name)) obj.getBoolean(name) else defaultValue
            } catch (e: Exception) {
                e.printStackTrace()
            }
            return re
        }

        /**
         * 앱버젼 확인
         *
         * @param context
         * @return 앱버전
         */
        fun getAppVersionName(context: Context): String {
            return try {
                val packageInfo =
                    context.packageManager.getPackageInfo(context.packageName, 0)
                packageInfo.versionName
            } catch (e: PackageManager.NameNotFoundException) {
                throw RuntimeException("버전코드 확인 에러: $e")
            }
        }

        /**
         * DP/PX 변환
         *
         * @param context
         * @param px
         * @return
         */
        fun dpFromPx(context: Context, px: Float): Float {
            var scale = context.resources.displayMetrics.density
            scale = 3.0f
            return px / scale
        }

        /**
         * DP/PX 변환
         *
         * @param context
         * @param dp
         * @return
         */
        fun pxFromDp(context: Context, dp: Float): Float {
            val scale = context.resources.displayMetrics.density
            return dp * scale
        }

        fun checkEmail(email: String?): Boolean {
            return Patterns.EMAIL_ADDRESS.matcher(email).matches()
        }

        fun getBase64encode(content: String): String {
            return Base64.encodeToString(content.toByteArray(), 0)
        }

        /**
         * Base64 디코딩
         */
        fun getBase64decode(content: String?): String {
            return String(Base64.decode(content, 0))
        }
    }
}