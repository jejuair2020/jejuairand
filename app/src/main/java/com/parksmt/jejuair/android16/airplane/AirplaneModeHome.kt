package com.parksmt.jejuair.android16.airplane

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.os.Build
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.parksmt.jejuair.android16.R
import com.parksmt.jejuair.android16.util.Util

/**
 * 비행기모드 HOME
 *
 *
 * Created by ui-jun on 2017-06-28.
 */
class AirplaneModeHome : AirplaneModeFragment(), View.OnClickListener {
    private lateinit var mAirplaneModeReceiver: BroadcastReceiver
    lateinit var mContext: Context


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        super.onCreateView(inflater, container, savedInstanceState)
        val rootView =
            inflater.inflate(R.layout.airplane_mode_home, container, false)
        initOnClick(rootView)
        setText(rootView)
        //안드로이드8.0 비행기모드 리시버 등록, 2018.09.03, 박재성
        mContext = this.requireContext()
        registAirplaneModeReceiver()
        return rootView
    }

    override fun onDestroy() {
        super.onDestroy()
        unRegistAirplaneModeReceiver()
    }

    private fun registAirplaneModeReceiver() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            mAirplaneModeReceiver = object : BroadcastReceiver() {
                override fun onReceive(
                    context: Context,
                    intent: Intent
                ) {
                    if (intent.action == Intent.ACTION_AIRPLANE_MODE_CHANGED) {
                        if (Util().isAirplaneModeOn(mContext) === false) {
                            AirplaneModeBroadcastReceiver.goAirplaneMode(mContext)
                        }
                    }
                }
            }
            mContext.registerReceiver(
                mAirplaneModeReceiver,
                IntentFilter(Intent.ACTION_AIRPLANE_MODE_CHANGED)
            )
        }
    }

    private fun unRegistAirplaneModeReceiver() {
        if (mAirplaneModeReceiver != null) {
            mContext.unregisterReceiver(mAirplaneModeReceiver)
        }
    }

    override fun onClick(v: View) {
        when (v.id) {
            R.id.airplane_mode_home_btn1 -> goMain(AirplaneMode.AirplaneModePage.IN_FLIGHT_SERVICE)
            R.id.airplane_mode_home_btn3 -> goMain(AirplaneMode.AirplaneModePage.BAGGAGE)
            R.id.airplane_mode_home_btn4 -> goMain(AirplaneMode.AirplaneModePage.IMMIGRATION_FORM)
        }
    }

    private fun initOnClick(rootView: View) {
        rootView.findViewById<View>(R.id.airplane_mode_home_btn1)
            .setOnClickListener(this)
        //        rootView.findViewById(R.id.airplane_mode_home_btn2).setOnClickListener(this);
        rootView.findViewById<View>(R.id.airplane_mode_home_btn3)
            .setOnClickListener(this)
        rootView.findViewById<View>(R.id.airplane_mode_home_btn4)
            .setOnClickListener(this)
        //        rootView.findViewById(R.id.airplane_mode_home_btn5).setOnClickListener(this);
//        rootView.findViewById(R.id.airplane_mode_home_btn6).setOnClickListener(this);
    }

    private fun setText(rootView: View) {
        loadLanguage("airplanemode/airplaneModeHome.json")
        (rootView.findViewById<View>(R.id.airplane_mode_home_textview1) as TextView).text =
            mLanguageJson.optString("airplaneModeHomeText1000")
        (rootView.findViewById<View>(R.id.airplane_mode_home_textview2) as TextView).text =
            mLanguageJson.optString("airplaneModeHomeText1001")
        (rootView.findViewById<View>(R.id.airplane_mode_home_btn1) as TextView).text =
            mLanguageJson.optString("airplaneModeHomeText1002")
        //        ((TextView) rootView.findViewById(R.id.airplane_mode_home_btn2)).setText(mLanguageJson.optString("airplaneModeHomeText1003"));
        (rootView.findViewById<View>(R.id.airplane_mode_home_btn3) as TextView).text =
            mLanguageJson.optString("airplaneModeHomeText1004")
        (rootView.findViewById<View>(R.id.airplane_mode_home_btn4) as TextView).text =
            mLanguageJson.optString("airplaneModeHomeText1005")
        if (Util().isKorean(this.requireContext())) {
            rootView.findViewById<View>(R.id.airplane_mode_home_btn4).visibility = View.VISIBLE
        } else {
            rootView.findViewById<View>(R.id.airplane_mode_home_btn4).visibility = View.GONE
            //            rootView.findViewById(R.id.airplane_mode_home_btn_layout).setVisibility(View.GONE);
        }
    }
}