package com.parksmt.jejuair.android16.enumdata

/**
 * 회원 종류
 * Created by ui-jun on 2017-04-28.
 */
enum class MemberType(//SM:SNS비회원 -> 없어지고
    val code: String
) {
    NO_MEMBER(""),  //비회원
    GENERAL_MEMBER("GM"),  //GM:일반회원 / SNS 간편회원
    EMAIL_MEMBER("EM"),  //EM:이메일비회원
    SNS_MEMBER("SM");

    companion object {
        fun getMemberType(code: String): MemberType {
            var state = NO_MEMBER
            for (temp in values()) {
                if (temp.code == code) {
                    state = temp
                    break
                }
            }
            return state
        }
    }

}