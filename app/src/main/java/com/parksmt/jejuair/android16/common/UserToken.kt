package com.parksmt.jejuair.android16.common

import android.content.Context
import android.util.Log
import com.google.gson.Gson
import com.parksmt.jejuair.android16.enumdata.MemberType
import com.parksmt.jejuair.android16.enumdata.SnsType
import com.parksmt.jejuair.android16.util.AES256Cipher
import com.parksmt.jejuair.android16.util.StringUtil
import org.json.JSONObject
import java.util.*

/**
 * token
 *
 *
 * Created by ui-jun on 2017-08-22.
 */
class UserToken (context: Context) {
    // END   : 모바일 탑승권
    private val tag = "UserToken"

    lateinit var token //사용자 토큰
            : String
    lateinit var userID: String
    lateinit var fFPNo //FFP회원번호
            : String
    lateinit var pushKey: String
    lateinit var email //이메일
            : String
    lateinit var memberType //(GM:일반회원 , EM:이메일비회원 , SM:SNS비회원)
            : MemberType
    var isAutoLogin //자동로그인
            : Boolean = false

    /**
     * 로그인 여부
     *
     * @return true 로그인, false, 비로그인
     */
    var isLogin //로그인여부
            : Boolean = false
    var isSimpleLogin = false //간편 로그인 1회성 사용 = false
    lateinit var simpleLoginToken //간편 로그인 1회성 사용
            : String
    lateinit var foreignYN //외국인여부
            : String
    lateinit var sex //성별
            : String
    lateinit var birthDate //생일
            : String
    lateinit var country //거주국가
            : String
    lateinit var nationality : String //국적
    lateinit var snsLinked //sns 연결 여부 Y,N
            : String
    lateinit var snsPw //sns 비밀번호 여부 Y,N
            : String
    lateinit var snsInfoList : ArrayList<UserSnsInfo> // 유저 sns 정보
    lateinit var aes256Cipher: AES256Cipher


    fun setUserToken(context: Context, data: JSONObject) {
        setToken(context, data.optString("CURRENT_APP_TOKEN"))
        setPushKey(context, data.optString("PUSH_KEY"))
        setMemberType(context, data.optString("MEMBER_TYPE"))
        val temp = data.optJSONObject("USER_INFO") ?: return
        setUserID(context, temp.optString("userID"))
        setFFPNo(context, temp.optString("FFPNo"))
        setSex(context, temp.optString("sex"))
        setBirthDate(context, temp.optString("birthDate"))
        setEmail(context, temp.optString("email"))
        setForeignYN(context, temp.optString("foreignYN"))
        setCountry(context, temp.optString("country"))
        setNationality(context, temp.optString("nationality"))
        setSnsLinked(context, temp.optString("snsLinked"))
    }

    fun setUserToken(
        context: Context,
        data: JSONObject,
        memberType: MemberType
    ) {
        when (memberType) {
            MemberType.GENERAL_MEMBER, MemberType.SNS_MEMBER -> setUserToken(context, data)
            else -> {
                setToken(context, data.optString("CURRENT_APP_TOKEN"))
                setPushKey(context, data.optString("PUSH_KEY"))
                setMemberType(context, data.optString("MEMBER_TYPE"))
                setEmail(context, data.optString("EMAILADDR"))
                if (StringUtil().isNotNull(data.optString("USER_ID"))) {
                    setUserID(context, data.optString("USER_ID"))
                } else {
                    setUserID(context, email)
                }
            }
        }
    }

    private fun setToken(context: Context, token: String) {
        try {
            Common.putString(
                TOKEN_KEY,
                aes256Cipher.AESEncode(token),
                context
            )
            Common.commit(context)
            this.token = token
        } catch (e: Exception) {
            DLog().e("Exception" + e)
        }
    }

    private fun setUserID(context: Context, userID: String) {
        try {
            Common.putString(
                USER_ID_KEY,
                aes256Cipher.AESEncode(userID),
                context
            )
            Common.commit(context)
            this.userID = userID
        } catch (e: Exception) {
            DLog().e("Exception" + e)
        }
    }

    private fun setFFPNo(context: Context, FFPNo: String) {
        try {
            Common.putString(
                FFP_NO_KEY,
                aes256Cipher.AESEncode(FFPNo),
                context
            )
            Common.commit(context)
            fFPNo = FFPNo
        } catch (e: Exception) {
            DLog().e("Exception"+ e)
        }
    }

    private fun setPushKey(context: Context, pushKey: String) {
        try {
            Common.putString(
                PUSH_KEY,
                aes256Cipher.AESEncode(pushKey),
                context
            )
            Common.commit(context)
            this.pushKey = pushKey
        } catch (e: Exception) {
            DLog().e("Exception" + e)
        }
    }

    fun setEmail(context: Context, email: String) {
        try {
            Common.putString(
                EMAIL_KEY,
                aes256Cipher.AESEncode(email),
                context
            )
            Common.commit(context)
            this.email = email
        } catch (e: Exception) {
            DLog().e("Exception" + e)
        }
    }

    fun getMemberType(): String {
        return memberType.code
    }

    val memberTypeEnum: MemberType
        get() = memberType

    private fun setMemberType(
        context: Context,
        memberType: String
    ) {
        setMemberType(context, MemberType.getMemberType(memberType))
    }

    private fun setMemberType(
        context: Context,
        memberType: MemberType
    ) {
        this.memberType = memberType
    }

    fun setAutoLogin(context: Context, autoLogin: Boolean) {
        Common.putBoolean(
            AUTO_LOGIN_KEY,
            autoLogin,
            context
        )
        Common.commit(context)
        isAutoLogin = autoLogin
    }

    fun setLogin(context: Context, login: Boolean) {
        Common.putBoolean(
            IS_LOGIN_KEY,
            login,
            context
        )
        Common.commit(context)
        isLogin = login
    }

    /**
     * 일반 회원여부
     *
     * @return true : 일반회원, false : 비회원 or 비로그인
     */
    val isMemberLogin: Boolean
        get() = isLogin && memberType === MemberType.GENERAL_MEMBER

    val snsUserID: String?
        get() = if (memberType === MemberType.SNS_MEMBER) {
            email
        } else {
            userID
        }

    private fun setForeignYN(
        context: Context,
        foreignYN: String
    ) {
        Common.putString(
            FOREIGN_YN_KEY,
            foreignYN,
            context
        )
        Common.commit(context)
        this.foreignYN = foreignYN
    }

    val isForeigner: Boolean
        get() = "Y" == foreignYN

    private fun setSex(context: Context, sex: String) {
        Common.putString(
            SEX_KEY,
            sex,
            context
        )
        Common.commit(context)
        this.sex = sex
    }

    private fun setBirthDate(
        context: Context,
        birthDate: String
    ) {
        if (StringUtil().isNotNull(birthDate) && birthDate.length > 4) {
            Common.putString(
                BIRTH_DATE_KEY,
                birthDate.substring(0, 4),
                context
            )
            Common.commit(context)
            this.birthDate = birthDate.substring(0, 4)
        }
    }

    fun setCountry(context: Context, country: String) {
        Common.putString(
            COUNTRY_KEY,
            country,
            context
        )
        Common.commit(context)
        this.country = country
    }

    fun setNationality(context: Context, nationality: String) {
        Common.putString(
            NATIONALITY_KEY,
            nationality,
            context
        )
        Common.commit(context)
        this.nationality = nationality
    }

    fun setSnsInfo(
        context: Context,
        snsType: SnsType,
        snsId: String
    ) {
        val snsInfoList = makeSnsInfoList(context)
        if (snsInfoList != null && snsInfoList.size > 0) {
            var exist = false
            for (snsInfo in snsInfoList) {
                if (snsInfo.snsId == snsId && snsInfo.snsType?.code.equals(snsType.code)) {
                    exist = true
                    break
                }
            }
            if (exist) {
                for (snsInfo in snsInfoList) {
                    if (snsInfo.snsId == snsId && snsInfo.snsType?.code.equals(snsType.code)) {
                        snsInfo.isCheck = true
                    } else {
                        snsInfo.isCheck = false
                    }
                }
            } else {
                for (snsInfo in snsInfoList) {
                    snsInfo.isCheck = false
                }
                val userSnsInfo = UserSnsInfo()
                userSnsInfo.isCheck = true
                userSnsInfo.snsType = snsType
                userSnsInfo.snsId = snsId
                snsInfoList.add(userSnsInfo)
            }
        } else {
            val userSnsInfo = UserSnsInfo()
            userSnsInfo.isCheck = true
            userSnsInfo.snsType = snsType
            userSnsInfo.snsId = snsId
            snsInfoList!!.add(userSnsInfo)
        }
        this.snsInfoList!!.clear()
        this.snsInfoList.addAll(snsInfoList!!)
    }

    fun makeSnsInfoList(context: Context): ArrayList<UserSnsInfo> {
        val gson = Gson()
        val userSnsInfos: UserSnsInfoCover = gson.fromJson<UserSnsInfoCover>(
            Common.getString(
                SNS_INFO_KEY,
                "",
                context
            ), UserSnsInfoCover::class.java
        )
        return if (userSnsInfos != null && userSnsInfos.userSnsInfos != null) {
            userSnsInfos.userSnsInfos!!
        } else {
            ArrayList()
        }
    }

    fun setSnsLinked(context: Context, snsLinked: String) {
        Common.putString(
            SNS_LINK_KEY,
            snsLinked,
            context
        )
        Common.commit(context)
        this.snsLinked = snsLinked
    }

    fun setSnsPw(context: Context, snsPw: String) {
        Common.putString(
            SNS_PW_KEY,
            snsPw,
            context
        )
        Common.commit(context)
        this.snsPw = snsPw
    }

    /**
     * 회원 SNS연동 정보
     */
    class UserSnsInfo {
        var snsType // sns 로그인 sns 종류
                : SnsType? = null
        var snsId // sns id
                : String? = null
        var isCheck = false// 사용여부 = false
    }

    class UserSnsInfoCover {
        lateinit var userSnsInfos: ArrayList<UserSnsInfo>
    }

    companion object {
        private const val TOKEN_KEY = "TOKEN_KEY"
        private const val USER_ID_KEY = "USER_ID_KEY"
        private const val FFP_NO_KEY = "FFP_NO_KEY"
        private const val PUSH_KEY = "PUSH_KEY"
        private const val EMAIL_KEY = "EMAIL_KEY"
        private const val MEMBER_TYPE_KEY = "MEMBER_TYPE_KEY"
        private const val AUTO_LOGIN_KEY = "AUTO_LOGIN_KEY"
        private const val IS_LOGIN_KEY = "IS_LOGIN_KEY"
        private const val FOREIGN_YN_KEY = "FOREIGN_YN_KEY"
        private const val SEX_KEY = "SEX_KEY"
        private const val BIRTH_DATE_KEY = "BIRTH_DATE_KEY"
        private const val COUNTRY_KEY = "COUNTRY_KEY"
        private const val NATIONALITY_KEY = "NATIONALITY_KEY"
        private const val SNS_INFO_KEY = "SNS_INFO_KEY"
        private const val SNS_LINK_KEY = "SNS_LINK_KEY"
        private const val SNS_PW_KEY = "SNS_PW_KEY"
        // BEGIN : 모바일 탑승권
//         Add by citrineins on 2018. 2. 21..
//
        private const val SESSION_MOBILE_TICKET_EVENT_ID = "SESSION_MOBILE_TICKET_EVENT_ID"
        private const val SESSION_MOBILE_TICKET_EVENT_PUSH_YN =
            "SESSION_MOBILE_TICKET_EVENT_PUSH_YN"
        private const val AIRPORT_NAME = "AIRPORT_NAME"
        //var mTicketLists: ArrayList<MobileTicketItem> = ArrayList<MobileTicketItem>()
        //       : 위도, 경도를 GpsLocationService 에서 값을 넣어 가까운 공항에 사용자가 위치하면 이벤트 표시
        var latitude = 0.0
        var longitude = 0.0
        var airport_latitude = 0.0
        var airport_longitude = 0.0
        lateinit var mInstance: UserToken
        fun getInstance(context: Context): UserToken {
            if (mInstance == null) {
                mInstance = UserToken(context)
            }
            return mInstance
        }

        fun removeToken(context: Context) {
            Common.remove(
                TOKEN_KEY,
                context
            )
            Common.remove(
                USER_ID_KEY,
                context
            )
            Common.remove(
                FFP_NO_KEY,
                context
            )
            Common.remove(
                PUSH_KEY,
                context
            )
            Common.remove(
                EMAIL_KEY,
                context
            )
            Common.remove(
                MEMBER_TYPE_KEY,
                context
            )
            Common.remove(
                AUTO_LOGIN_KEY,
                context
            )
            Common.remove(
                IS_LOGIN_KEY,
                context
            )
            Common.remove(SEX_KEY, context)
            Common.remove(
                BIRTH_DATE_KEY,
                context
            )
            Common.remove(
                COUNTRY_KEY,
                context
            )
            Common.remove(
                NATIONALITY_KEY,
                context
            )
            Common.commit(context)
            //mInstance = null
            UserInfo.removeInfo()
            MyInfo.removeMyInfo()
            DLog().d("removeToken")
        }

        fun finish() {
            //mInstance = null
            UserInfo.removeInfo()
            MyInfo.removeMyInfo()
        }

        //
// 모바일 탑승권 : BEGIN
//
        fun setMobileTicketEventId(
            context: Context,
            eventId: String
        ) {
            Common.putString(
                SESSION_MOBILE_TICKET_EVENT_ID,
                eventId,
                context
            )
            Common.commit(context)
        }

        fun getMobileTicketEventId(context: Context): String {
            return Common.getString(
                SESSION_MOBILE_TICKET_EVENT_ID,
                "0",
                context
            ).toString()
        }

        fun setMobileTicketEventPushYn(
            context: Context,
            eventId: String
        ) {
            Common.putString(
                SESSION_MOBILE_TICKET_EVENT_PUSH_YN,
                eventId,
                context
            )
            Common.commit(context)
        }

        fun getMobileTicketEventPushYn(context: Context): String {
            return Common.getString(
                SESSION_MOBILE_TICKET_EVENT_PUSH_YN,
                "N",
                context
            ).toString()
        }

        //
// 모바일 탑승권 : END
//
        fun setAirportName(
            context: Context,
            airportname: String
        ) {
            Common.putString(
                AIRPORT_NAME,
                airportname,
                context
            )
            Common.commit(context)
        }

        fun getAirportName(context: Context): String {
            return Common.getString(
                AIRPORT_NAME,
                "",
                context
            ).toString()
        }
    }

    init {
        try {
            aes256Cipher = AES256Cipher.getInstance(context)!!
            token = aes256Cipher.AESDecode(
                Common.getString(
                    TOKEN_KEY,
                    "",
                    context
                ).toString()
            )
            userID = aes256Cipher.AESDecode(
                Common.getString(
                    USER_ID_KEY,
                    "",
                    context
                ).toString()
            )
            fFPNo = aes256Cipher.AESDecode(
                Common.getString(
                    FFP_NO_KEY,
                    "",
                    context
                ).toString()
            )
            pushKey = aes256Cipher.AESDecode(
                Common.getString(
                    PUSH_KEY,
                    "",
                    context
                ).toString()
            )
            email = aes256Cipher.AESDecode(
                Common.getString(
                    EMAIL_KEY,
                    "",
                    context
                ).toString()
            )
        } catch (e: Exception) {
            Log.e(tag, "Exception", e)
        }
        memberType = MemberType.getMemberType(
            Common.getString(
                MEMBER_TYPE_KEY,
                "",
                context
            ).toString()
        )
        isAutoLogin = Common.getBoolean(
            AUTO_LOGIN_KEY,
            false,
            context
        )
        isLogin = Common.getBoolean(
            IS_LOGIN_KEY,
            false,
            context
        )
        foreignYN = Common.getString(
            FOREIGN_YN_KEY,
            "",
            context
        ).toString()
        sex = Common.getString(
            SEX_KEY,
            "",
            context
        ).toString()
        birthDate = Common.getString(
            BIRTH_DATE_KEY,
            "",
            context
        ).toString()
        country = Common.getString(
            COUNTRY_KEY,
            "",
            context
        ).toString()
        nationality = Common.getString(
            NATIONALITY_KEY,
            "",
            context
        ).toString()
        snsInfoList = makeSnsInfoList(context)
    }
}