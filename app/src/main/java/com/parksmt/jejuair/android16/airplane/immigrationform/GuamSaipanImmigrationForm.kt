package com.parksmt.jejuair.android16.airplane.immigrationform

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.ImageView
import com.parksmt.jejuair.android16.R
import com.parksmt.jejuair.android16.airplane.AirplaneModeMenuActivity

import com.parksmt.jejuair.android16.util.PinchImageViewActivity
import kotlinx.android.synthetic.main.immigration_form_guam_saipan_layout.*

/**
 * Created by 12154 on 2018-06-05.
 */
class GuamSaipanImmigrationForm : AirplaneModeMenuActivity() {
    private var mTabImage1: ImageView? = null
    private var mTabImage2: ImageView? = null
    private var mArrivalImageView: ImageView? = null
    protected override val uiName: String
        protected get() = "S-MUI-08-030"

    protected override fun onCreate(savedInstanceState: Bundle?) {
        useLanguage = false
        super.onCreate(savedInstanceState)
        setContentView(R.layout.immigration_form_guam_saipan_layout)
        initView()
        setText()
        initClickEvent()
    }

    private fun initView() {
        mTabImage1 =
            findViewById(R.id.immigration_form_guam_saipan_tab_image1) as ImageView?
        mTabImage2 =
            findViewById(R.id.immigration_form_guam_saipan_tab_image2) as ImageView?
        mArrivalImageView =
            findViewById(R.id.immigration_form_guam_saipan_imageview) as ImageView?
    }

    private fun setText() {
        setTitleText(getString(R.string.immigration_form_guam_saipan_title))
    }

    private fun initClickEvent() {
        immigration_form_guam_saipan_tab_text1.setOnClickListener(View.OnClickListener {
            mTabImage1!!.visibility = View.VISIBLE
            mTabImage2!!.visibility = View.GONE
            mArrivalImageView!!.setImageResource(R.drawable.arrival_13)
            //괌 세관신고서 스크린 태그 전송 ui값 변경해야됨
            //TagUtil.sendScreenTag(this@GuamSaipanImmigrationForm, "S-MUI-08-030")
        })
        immigration_form_guam_saipan_tab_text2.setOnClickListener(View.OnClickListener {
            mTabImage1!!.visibility = View.GONE
            mTabImage2!!.visibility = View.VISIBLE
            mArrivalImageView!!.setImageResource(R.drawable.arrival_14)
            //사이판 세관신고서 스크린 태그 전송 ui값 변경해야됨
            //TagUtil.sendScreenTag(this@GuamSaipanImmigrationForm, "S-MUI-08-031")
        })
        immigration_form_guam_saipan_imageview.setOnClickListener(View.OnClickListener {
            val intent =
                Intent(this@GuamSaipanImmigrationForm, PinchImageViewActivity::class.java)
            //이미지 파일인 경우
            if (mTabImage1!!.visibility == View.VISIBLE) {
                intent.putExtra("path", R.drawable.arrival_13)
            } else {
                intent.putExtra("path", R.drawable.arrival_14)
            }
            startActivity(intent)
        })
    }
}