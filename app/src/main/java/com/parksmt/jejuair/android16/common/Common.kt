package com.parksmt.jejuair.android16.common

import android.annotation.SuppressLint
import android.content.Context
import android.content.SharedPreferences

class Common {

    companion object {
        private var prefs: SharedPreferences? = null
        private var editor: SharedPreferences.Editor? = null
        var isShowing = false

        var startTime: Long = 0

        @SuppressLint("CommitPrefEdits")
        fun initSharedPreferences(context: Context) {
            if (prefs == null || editor == null) {
                prefs = context.getSharedPreferences(Setting.PREFERENCE_NAME, Context.MODE_PRIVATE)
                editor = prefs!!.edit()
            }
        }

        fun getBoolean(key: String, defValue: Boolean, context: Context): Boolean {
            initSharedPreferences(context)
            return prefs!!.getBoolean(key, defValue)
        }

        fun getFloat(key: String, defValue: Float, context: Context): Float {
            initSharedPreferences(context)
            return prefs!!.getFloat(key, defValue)
        }

        fun getInt(key: String, defValue: Int, context: Context): Int {
            initSharedPreferences(context)
            return prefs!!.getInt(key, defValue)
        }

        fun getLong(key: String, defValue: Long, context: Context): Long {
            initSharedPreferences(context)
            return prefs!!.getLong(key, defValue)
        }

        fun getString(key: String, defValue: String, context: Context): String? {
            initSharedPreferences(context)
            return prefs!!.getString(key, defValue)
        }

        fun putBoolean(key: String, value: Boolean, context: Context): SharedPreferences.Editor {
            initSharedPreferences(context)
            return editor!!.putBoolean(key, value)
        }

        fun putFloat(key: String, value: Float, context: Context): SharedPreferences.Editor {
            initSharedPreferences(context)
            return editor!!.putFloat(key, value)
        }

        fun putInt(key: String, value: Int, context: Context): SharedPreferences.Editor {
            initSharedPreferences(context)
            return editor!!.putInt(key, value)
        }

        fun putLong(key: String, value: Long, context: Context): SharedPreferences.Editor {
            initSharedPreferences(context)
            return editor!!.putLong(key, value)
        }

        fun putString(key: String, value: String, context: Context): SharedPreferences.Editor {
            initSharedPreferences(context)
            return editor!!.putString(key, value)
        }

        fun commit(context: Context): Boolean {
            initSharedPreferences(context)
            return editor!!.commit()
        }

        fun clear(context: Context): SharedPreferences.Editor {
            initSharedPreferences(context)
            return editor!!.clear()
        }

        fun remove(key: String, context: Context): SharedPreferences.Editor {
            initSharedPreferences(context)
            return editor!!.remove(key)
        }
    }


}
